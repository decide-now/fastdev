# FastDev #

## Описание ##
Компонент для быстрой разработки web-приложений на основе Laravel.

## Установка ##
### Добавление пакета ###
Для установки компонента необходимо, находясь в корне приложения, выполнить две команды, которые вносят изменения в файл *composer.json*:

* первая добавляет репозиторий
* вторая добавляет пакет в раздел *require* и обновляет пакеты (загружает пакет *decidenow/fastdev* в папку *vendor*)
```
composer config repositories.fastdev vcs https://bitbucket.org/decide-now/fastdev
composer require decidenow/fastdev:dev-master
```

### Изменение конфигурации (Laravel < 5.5) ###
Для работы компонента необходимо добавить провайдер в конфигурационном файле **config/app.php**
```
...
'providers' => [
   ...
   DecideNow\FastDev\FastDevServiceProvider::class,
   ...
],
...
```

### Внедрение инструментов разработки ###
Внедрение инструментов разработки в приложение осуществляется через web-интерфейс.

Для доступа к web-интерфейсу нужно открыть в браузере страницу
```
http://path-to-application/decidenow/fastdev
```

## Автор ##
Соколов Александр

E-mail: sanya_sokolov@inbox.ru

Skype: s0k0v0_av