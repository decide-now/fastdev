<?php
namespace DecideNow\FastDev\ORM;

use Illuminate\Database\Eloquent\Collection;

trait Treeable
{
	
	public function parent()
	{
		return $this->belongsTo(get_class($this), 'parent_id');
	}
	
	public function children()
	{
		return $this->hasMany(get_class($this), 'parent_id');
	}
	
	public function getFullNameAttribute($item = null, $field_name = 'name', $separator = ' / ') {
		if (!$item) {
			$item = $this;
		}
		$parent = $item->parent;
		if ($parent) {
			return  $parent->getFullNameAttribute().$separator.$this->$field_name;
		} else {
			return $separator.$this->$field_name;
		}
	}
	
	private $tree_level_attr;
	public function getTreeLevelAttribute()
	{
		return $this->tree_level_attr;
	}
	public function setTreeLevelAttribute($value)
	{
		$this->tree_level_attr = $value;
	}
	private $is_first_attr;
	public function getIsFirstAttribute()
	{
		return $this->is_first_attr;
	}
	public function setIsFirstAttribute($value)
	{
		$this->is_first_attr = $value;
	}
	private $is_last_attr;
	public function getIsLastAttribute()
	{
		return $this->is_last_attr;
	}
	public function setIsLastAttribute($value)
	{
		$this->is_last_attr = $value;
	}
	
	public function getTreeLevelStringAttribute()
	{
		$ret = '<span style="font-family: monospace;">';
		$tree_level = $this->tree_level;
		for ($ind = 0; $ind < $tree_level; $ind++) {
			$ret .= '&#9474;&nbsp;';
		}
		$ret .= '&#9500;&#9472;';
		$ret .= '</span>';
		return $ret;
	}
	
	private static function addToTree(&$result, $query, $item, $parent_id, $level, $is_first, $is_last)
	{
		$q_clone = clone($query);
		$data = $q_clone->where('parent_id', $item->id)->get();
		
		$i = 1;
		$last_i = count($data);
		
		$item->tree_level = $level;
		$item->tree_first = $is_first;
		$item->tree_last = $is_last;
		$item->tree_parent_id = $parent_id;
		$item->tree_has_children = ($last_i > 0);
		$result->put($item->id, $item);
		
		
		$is_first = true;
		$is_last = false;
		
		foreach ($data as $data_item) {
			$is_last = ($i == $last_i);
			static::addToTree($result, $query, $data_item, $item->id, $level + 1, $is_first, $is_last);
			$is_first = false;
			$i++;
		}
	}
	
	public static function getTree($query, $item_id = null)
	{
		$result = new Collection();
		
		$q_clone = clone($query);
		if ($item_id) {
			$data = $q_clone->where('id', '=', $item_id)->get();
		} else {
			$data = $q_clone->whereNull('parent_id')->get();
		}
		
		$i = 1;
		$last_i = count($data);
		$is_first = true;
		$is_last = false;
		
		foreach($data as $item) {
			$is_last = ($i == $last_i);
			static::addToTree($result, $query, $item, $item_id, 0, $is_first, $is_last);
			$is_first = false;
			$i++;
		}
		return $result;
	}
	
	public static function addQueryByParentId($query = null, $parent_id = '')
	{
		$query = ($query) ?: new self();
		if ($parent_id) {
			$query = $query->where('parent_id', '=', $parent_id);
		}
		return $query;
	}
}
