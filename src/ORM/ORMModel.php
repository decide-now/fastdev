<?php
namespace DecideNow\FastDev\ORM;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;

class ORMModel extends Model
{
	public static function getTableStatic()
	{
		$instance = new static;
		return $instance->getTable();
	}
	
	public static function getAliases()
	{
		return ['id' => 'ID', 'name' =>'Наименование'];
	}
	
	public static function getFilterFields($filter_key = '')
	{
		return ['id' => '', 'name' => ''];
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return ['id' => 'asc', 'name' => ''];
	}

	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'id', '');
		if ($tmp != '') {
			$query = $query->where(self::getTableStatic().'.id', '=', ''.$tmp);
		}
		$tmp = Arr::get($filter_data, 'name', '');
		if ($tmp != '') {
			$query = $query->where(self::getTableStatic().'.name', 'LIKE', '%'.$tmp.'%');
		}
		return $query;
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'id', '');
		if ($tmp != '') {
			$query = $query->orderBy(self::getTableStatic().'.id', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'name', '');
		if ($tmp != '') {
			$query = $query->orderBy(self::getTableStatic().'.name', $tmp);
		}
		return $query;
	}
	
	public function getPermission($permission, $param = '')
	{
		return true;
	}
	
	public function toString()
	{
		$ret = $this->name;
		return $ret;
	}
	
	public static function idToString($id)
	{
		$ret = '';
		$rec = static::find($id);
		if ($rec != null) {
			$ret = $rec->toString();
		}
		return $ret;
	}
	
	
	public static function getModelNameStatic()
	{
		$instance = new static;
		return $instance->getModelName();
	}
	
	public function getModelName()
	{
		return Str::camel(class_basename($this));
	}
	
	public function getAliasesWithModelName()
	{
		$aliases = static::getAliases();
		$ret = array();
		foreach ($aliases as $k=>$v) {
			$ret[$this->getModelName().'.'.$k] = $v;
		}
		return $ret;
	}
	
	public static function getAlias($field_name)
	{
		$aliases = static::getAliases();
		if (array_key_exists($field_name, $aliases)) {
			return $aliases[$field_name];
		} else {
			return $field_name; 
		}
	}
	
	public static function defaultListQuery($query = null)
	{
		$class = get_called_class();
		return ($query) ?: new $class;
	}
	
	public static function addQueryFilterAndOrdering($query = null)
	{
		return self::defaultListQuery($query);
	}

	/**
		Old method
	 */
	public static function prepareFilter($filter_data, $scene_key= '')
	{
		$className = get_called_class();
		
		// значения по умолчанию
		$filter = static::getFilterFields($scene_key);
		
		// значения из сессии		
		$filter_session = session($className.'.filter'.($scene_key? '.'.$scene_key: ''), $filter);
		foreach ($filter_session as $filterField => $val) {
			if (!array_key_exists($filterField, $filter)) {
				continue;
			}
			
			if (empty($val)) {
				if (is_array($filter[$filterField])) {
					$val = [];
				}
			} else {
				if (is_string($filter[$filterField])) {
					$val = trim($val);
				}
			}
			$filter[$filterField] = $val;
		}
		
		// значения из запроса
		if ($filter_data != null) {
			foreach ($filter_data as $filterField => $val) {
				if (!array_key_exists($filterField, $filter)) {
					continue;
				}
				
				if (empty($val)) {
					if (is_array($filter[$filterField])) {
						$val = [];
					}
				} else {
					if (is_string($filter[$filterField])) {
						$val = trim($val);
					}
				}
				$filter[$filterField] = $val;
			}
		}
		
		session([$className.'.filter'.($scene_key? '.'.$scene_key: '')=> $filter]);
		
		return $filter;
	}
	
	/**
		Old method
	 */
	public static function prepareOrdering($ordering_data, $scene_key = '')
	{
		$className = get_called_class();
		$ordering = static::getOrderingFields($scene_key);
		
		if ($ordering_data!= null) {
			foreach (array_keys($ordering) as $orderingField) {
				$ordering[$orderingField] = trim(Arr::get($ordering_data, $orderingField, ''));
			}
			session([$className.'.ordering'.($scene_key? '.'.$scene_key: '')=> $ordering]);
		} else {
			$ordering= session($className.'.ordering'.($scene_key? '.'.$scene_key: ''), $ordering);
		}
		return $ordering;
	}
	
	
	private static function mergeRequestArrays($array_to, $array_from)
	{
		$ret = $array_to;
		foreach ($array_from as $field => $value) {
			if (!array_key_exists($field, $array_to)) {
				continue;
			}
			
			$val = $value;
			if (empty($value)) {
				if (is_array($array_to[$field])) {
					$val = [];
				}
			} else {
				if (is_string($array_to[$field])) {
					$val = trim($value);
				}
			}
			$ret[$field] = $val;
		}
		return $ret;
	}
	
	public static function filterPutToSession($request, $filter_key = '', $force_clear = false, $request_filter_name = 'filter')
	{
		$className = get_called_class();
		
		// значения по умолчанию
		$filter_default = static::getFilterFields($filter_key);
		$ret = $filter_default;
		
		if (!$force_clear) {
			
			// очищаем значения по умолчанию
			foreach($ret as $field_key => $field_value) {
				$ret[$field_key] =  (is_array($field_value)) ? [] : '';
			}
			
			// значения из запроса
			$filter_request = $request->get($request_filter_name, []);
			$ret =  self::mergeRequestArrays($ret, $filter_request);
			
		}
		
		Session::put($className.'.filter'.($filter_key ? '.' . $filter_key : ''), $ret);
		
		return $ret; 
		
	}
	
	public  static function filterGetFromSession($filter_key = '')
	{
		$className = get_called_class();
		
		// значения по умолчанию
		$filter_default = static::getFilterFields($filter_key);
		$ret = $filter_default;
		
		// значения из сессии
		$filter_session = Session::get($className.'.filter'.($filter_key ? '.' . $filter_key : ''), []);
		$ret =  self::mergeRequestArrays($ret, $filter_session);
		
		return $ret;
	}
	
	public static function orderingPutToSession($request, $ordering_key = '', $force_clear = false, $request_ordering_name = 'ordering')
	{
		$className = get_called_class();
		
		// значения по умолчанию
		$ordering_default = static::getOrderingFields($ordering_key);
		$ret = $ordering_default;
		
		if (!$force_clear) {
			
			// очищаем значения по умолчанию
			foreach($ret as $field_key => $field_value) {
				$ret[$field_key] =  (is_array($field_value)) ? [] : '';
			}
			
			// значения из запроса
			$ordering_request = $request->get($request_ordering_name, []);
			$ret =  self::mergeRequestArrays($ret, $ordering_request);
			
		}
		
		Session::put($className.'.ordering'.($ordering_key ? '.' . $ordering_key : ''), $ret);
		
	}
	
	public  static function orderingGetFromSession($ordering_key = '')
	{
		$className = get_called_class();
		
		// значения по умолчанию
		$ordering_default = static::getOrderingFields($ordering_key);
		$ret = $ordering_default;
		
		// значения из сессии
		$ordering_session = Session::get($className.'.ordering'.($ordering_key ? '.' . $ordering_key : ''), []);
		$ret =  self::mergeRequestArrays($ret, $ordering_session);
		
		return $ret;
	}

	public static function applySessionFilter($query, &$filter, $filter_key = '')
	{
		$filter = static::filterGetFromSession($filter_key);
		return static::applyFilter($query, $filter);
	}
	
	public static function applySessionOrdering($query, &$ordering, $ordering_key = '')
	{
		$ordering = static::orderingGetFromSession($ordering_key);
		return static::applyOrdering($query, $ordering);
	}
	
}