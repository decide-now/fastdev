<?php 
namespace DecideNow\FastDev\ORM\Relations;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class RelationChain extends Relation
{
	protected $result_table;
	protected $chain;
	protected $parent_table;
	protected $parent;
	protected $single_record;
	
	public function __construct(Builder $query, Model $parent, $result_table, array $chain, $single_record = false)
	{
		$this->result_table = $result_table;
		$this->chain = $chain;
		$this->parent_table = $parent->getTable();
		$this->parent = $parent;
		$this->single_record = $single_record;
		parent::__construct($query, $parent);
	}
	
	public function deeper(Builder $query = null, $chain = [], $relation_left_table = null, $relation_left_key = null)
	{
		$chain_keys = array_keys($chain);
		$relation_data = array_shift($chain);
		$relation_right_key = array_shift($chain_keys);
		
		$relation_left_table = ($relation_left_table) ?: $this->result_table;
		$relation_right_table = (array_key_exists('table', $relation_data)) ? $relation_data['table'] : $this->parent_table;
		$relation_left_id = (array_key_exists('left_key', $relation_data)) ? $relation_data['left_key'] : 'id';
		$relation_right_id = (array_key_exists('right_key', $relation_data)) ? $relation_data['right_key'] : 'id';
		
		$relation_right_key = ($relation_right_key) ?: $relation_right_table;
		$relation_left_key = ($relation_left_key) ?: $relation_left_table;
		
		$query = $query->join(
			$relation_right_table . ' as ' . $relation_right_key,
			$relation_left_key.'.'.$relation_left_id,
			'=',
			$relation_right_key.'.'.$relation_right_id
		);
		
		if (count($chain)) {
			$query = $this->deeper($query, $chain, $relation_right_table, $relation_right_key);
		}
		
		return $query;
	}
	
	public function addConstraints()
	{
		$this->query = $this
			->deeper($this->query, $this->chain)
			->select($this->result_table.'.*', $this->getParentTableAlias().'.id as relation_map_id');
			
		if (static::$constraints) {
			$this->query->where($this->getParentTableAlias().'.id', '=', $this->parent->id);
		}
		
	}
	
	public function getRelationExistenceQuery(Builder $query, Builder $parentQuery, $columns = ['*'])
	{
		$query= $this->deeper($query, $this->chain);
		
		return $query->select($columns)->whereColumn(
			$this->getExistenceCompareKey(), '=', $this->getParentTableAlias().'.id'
		);
	}

	public function addEagerConstraints(array $models)
	{
		$this->query->whereIn($this->getParentTableAlias().'.id', $this->getKeys($models, 'id'));
	}
	
	protected function buildDictionary(Collection $results)
	{
		$dictionary = [];
		foreach ($results as $result) {
			$dictionary[$result->relation_map_id][] = $result;
		}
		
		return $dictionary;
	}

	public function match(array $models, Collection $results, $relation)
	{
		$dictionary = $this->buildDictionary($results);
		
		foreach ($models as $model) {
			if (isset($dictionary[$key = $model->getKey()])) {
				$model->setRelation($relation, $this->related->newCollection($dictionary[$key]));
			}
		}
		
		return $models;
	}

	public function getResults()
	{
		$ret = $this->get();
		if ($this->single_record) {
			$ret = $ret->first();
		}
		return $ret;
	}

	public function initRelation(array $models, $relation)
	{
		foreach ($models as $model) {
			$model->setRelation($relation, $this->related->newCollection());
		}
		
		return $models;
	}
	
	public function getParentTableAlias()
	{
		$chain = $this->chain;
		end($chain);
		$key = key($chain); 
		return (is_string($key)) ? $key : $this->parent_table;
	}
	
	public function getQualifiedForeignKeyName()
	{
		return $this->parent_table.'.id';
	}
	
	public function getExistenceCompareKey()
	{
		return $this->getQualifiedForeignKeyName();
	}
}