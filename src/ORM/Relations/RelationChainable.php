<?php
namespace DecideNow\FastDev\ORM\Relations;

trait RelationChainable
{
	public function hasRelationChain($related, $chain, $single_record = false)
	{
		$model_instance = new $related;
		$instance = $this->newRelatedInstance($related);
		return new RelationChain($instance->newQuery(), $this, $model_instance->getTable(), $chain, $single_record);
	}
}
