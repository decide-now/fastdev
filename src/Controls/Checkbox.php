<?php

namespace DecideNow\FastDev\Controls;

use Illuminate\Support\Str;

class Checkbox extends CtrlCustom
{	
	protected $is_checked;
	
	public function __construct()
	{
		parent::__construct();
		$this->is_checked= false;
		$this->value = 1;
	}
	public function constructByName($name)
	{
		$this->ctrl_name = $name;
		return $this;
	}
	public function constructByNameAndModel($name, $model)
	{
		$this->ctrl_name = Str::camel(class_basename($model)).'['.$name.']';
		$this->ctrl_label = $model::getAlias($name);
		return $this;
	}
	
	public function isChecked($flag = true)
	{
		$this->is_checked = ($flag) ? true : false;
		return $this;
	}
	
	protected function prepareOut()
	{	
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		
		$this->ctrl .= '<div class="checkbox';
		$this->ctrl .= ($this->ctrl_ext_class) ? ' '.$this->ctrl_ext_class : '';
		$this->ctrl .= '">';

		$this->ctrl .= ($this->ctrl_label) ? '<label class="control-label" for="'.$this->ctrl_id.'">' : '';
		
		$this->ctrl .= '<input type="checkbox"';
		$this->ctrl .= ' id="'.$this->ctrl_id.'"'; 
		$this->ctrl .= 'name="'.$this->ctrl_name.'"';
		$this->ctrl .= ($this->ctrl_value != '') ? ' value="'.$this->ctrl_value.'"' : '';
		$this->ctrl .= ($this->is_checked) ? ' checked' : '';
		
		$this->outFlagAttributes();
		$this->outCtrlAttr();
		$this->outCtrlData();
		
		$this->ctrl .= '>';
		
		$this->ctrl .= ($this->ctrl_label) ? $this->ctrl_label.'</label>' : ''; 
		
		$this->ctrl .= '</div>'; // .checkbox
		
		$this->outHelper();
		$this->closeFormGroup();
	}
}