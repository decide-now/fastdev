<?php

namespace DecideNow\FastDev\Controls;

use Illuminate\Support\Arr;

class PopoverLink extends CtrlCustom
{	
	protected $ctrl_label;
	protected $ctrl_direction;
	protected $ctrl_filter;
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_label = '';
		$this->ctrl_direction = '';
		$this->ctrl_filter = '';
	}
	public function constructByName($name)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		return $instance; 
	}
	public function constructByNameAndModel($name, $model, $ordering, $filter)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		$instance->ctrl_label = $model::getAlias($name);
		return $instance;
	}
	public function constructByNameAndModelAndParam($name, $model, $ordering, $filter)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		$instance->ctrl_label = $model::getAlias($name);
		$instance->ctrl_direction = Arr::get($ordering, $name, '');
		$instance->ctrl_filter = Arr::get($filter, $name, '');
		return $instance;
	}
	
	public function label($label)
	{
		$this->ctrl_label = $label;
		return $this;
	}
	public function column($column)
	{
		$this->ctrl_column = $column;
		return $this;
	}
	public function direction($direction)
	{
		$direction= strtolower($direction);
		if (
			$direction!= Ctrl::SORTABLE_DIRECTION_ASC() &&
			$direction!= Ctrl::SORTABLE_DIRECTION_DESC()
		) {
			$this->ctrl_direction = '';
		} else {
			$this->ctrl_direction = $direction;
		}
		return $this;
	}
	
	public function filter($filter)
	{
		$this->ctrl_filter = $filter;
		return $this;
	}
	
	protected function prepareOpen()
	{
		$this->idFromName();
		$this->ctrl = '<a href="" href="#" rel="popover" data-placement="bottom" id="'.$this->ctrl_id.'" name="'.$this->ctrl_name.'"';
		$this->ctrl .= ' data-field="'.$this->ctrl_name.'"';
		$this->ctrl .= ' data-popover-content="#'.$this->ctrl_name.'-filter-popover"'; 
		$this->ctrl .= ($this->ctrl_is_hidden) ? ' class="hidden"' : '';
		$this->ctrl .= '>';
		if ($this->ctrl_direction == Ctrl::SORTABLE_DIRECTION_ASC()) {
			$this->ctrl .= '<span class="glyphicon glyphicon-sort-by-attributes"></span> ';
		} elseif ($this->ctrl_direction == Ctrl::SORTABLE_DIRECTION_DESC()) {
			$this->ctrl .= '<span class="glyphicon glyphicon-sort-by-attributes-alt"></span> ';
		}
		if (!empty($this->ctrl_filter)) {
			$this->ctrl .= '<i class="glyphicon glyphicon-filter"></i>';
		}
		$this->ctrl .= $this->ctrl_label;
		$this->ctrl .= '</a>';
		
		$this->ctrl .= '<div data-field="'.$this->ctrl_name.'" id="'.$this->ctrl_name.'-filter-popover" class="popover-content hidden"><div class="container-fluid"><div class="row"><div class="col-sm-12">';
	}
	
	protected function prepareClose()
	{
		$this->ctrl = '</div></div></div></div>';
	}
}