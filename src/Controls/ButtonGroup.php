<?php

namespace DecideNow\FastDev\Controls;

class ButtonGroup extends CtrlCustom
{	
	protected $ctrl_is_justified;
	protected $ctrl_is_centered;
	protected $available_ctrl_sizes = [
		Ctrl::CONTROL_SIZE_XS,
		Ctrl::CONTROL_SIZE_SM,
		Ctrl::CONTROL_SIZE_MD,
		Ctrl::CONTROL_SIZE_LG
	];
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_is_justified = false;
		$this->ctrl_is_centered = false;
		$this->ctrl_is_group = false;
	}
	public function constructByName($name)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		return $instance; 
	}
	
	public function isJustified($value = true)
	{
		$this->ctrl_is_justified = $value;
		return $this;
	}
	
	public function isCentered($value = true)
	{
		$this->ctrl_is_centered= $value;
		return $this;
	}
	
	public function isFormGroup($flag = true)
	{
		$this->ctrl_is_group = $flag;
		return $this;
	}
	
	protected function prepareOpen()
	{
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		
		$this->ctrl .= '<div class="btn-group';
		$this->ctrl .= ($this->ctrl_is_justified) ? ' btn-group-justified' : '';
		$this->ctrl .= ($this->ctrl_size) ? ' btn-group-'.$this->ctrl_size : '';
		$this->ctrl .= '">';
	}
	protected function prepareClose()
	{
		$this->ctrl .= '</div>';
		$this->closeFormGroup();
	}
}