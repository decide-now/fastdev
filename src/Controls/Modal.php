<?php

namespace DecideNow\FastDev\Controls;

class Modal extends CtrlCustom
{	
	protected $ctrl_dock;
	protected $is_static;
	protected $title;
	protected $has_close_cross;
	
	protected $available_ctrl_sizes = [
		Ctrl::CONTROL_SIZE_SM,
		Ctrl::CONTROL_SIZE_LG,
		Ctrl::CONTROL_SIZE_FS,
	];
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_dock = '';
		$this->is_static = false;
		$this->title= '';
		$this->has_close_cross= false;
	}
	public function constructByName($name)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		return $instance; 
	}
	
	public function dock($dock)
	{
		$this->ctrl_dock = $dock;
		return $this;
	}
	
	public function title($title)
	{
		$this->title = $title;
		return $this;
	}
	
	public function hasCloseCross()
	{
		$this->has_close_cross= true;
		return $this;
	}
	public function isStatic()
	{
		$this->is_static= true;
		return $this;
	}
	
	protected function prepareOpen()
	{
		$this->idFromName();
		$this->ctrl = '<div id="'.$this->ctrl_id.'" class="modal fade';
		$this->ctrl .= ($this->ctrl_dock) ? ' '.$this->ctrl_dock : '';
		$this->ctrl .= '"';
		$this->ctrl .= ' tabindex="-1" role="dialog"';
		$this->ctrl .= ($this->is_static) ? ' data-backdrop="static"' : '';
		
		$this->outFlagAttributes();
		$this->outCtrlAttr();
		$this->outCtrlData();
		
		$this->ctrl .= '>';
		$this->ctrl .= '<div class="modal-dialog';
		$this->ctrl .= ($this->ctrl_size != '') ? ' modal-'.$this->ctrl_size : '';
		$this->ctrl .= '"><div class="modal-content">';
		if ($this->title != '') {
			$this->ctrl .= '<div class="modal-header">';
			$this->ctrl .= ($this->has_close_cross) ? '<button type="button" class="close" data-dismiss="modal">&times;</button>' : '';
			$this->ctrl .= '<h4 class="modal-title">'.$this->title.'</h4>';
			$this->ctrl .= '</div>';
		}
		
		$this->ctrl .= '<div class="modal-body">';
		$this->ctrl .= '<div class="container-fluid">';
	}
	
	protected function prepareClose()
	{
		$this->ctrl = '</div></div></div></div></div>';
	}
}