<?php

namespace DecideNow\FastDev\Controls;

trait Optionable 
{

	protected $ctrl_options;
	protected $ctrl_options_data;
	
	public function option($id, $text, $data)
	{
		$this->ctrl_options[$id] = $text;
		$this->ctrl_options_data[$id] = $data;
		return $this;
	}
	public function options($options, $data = [])
	{
		$this->ctrl_options = $this->ctrl_options + $options;
		$this->ctrl_options_data = $this->ctrl_options_data + $data;
		return $this;
	}
	public function optionsData($data)
	{
		$this->ctrl_options_data = $this->ctrl_options_data + $data;
		return $this;
	}
	public function optionsAndData($options_and_data)
	{
		if (array_key_exists('options', $options_and_data)) {
			$this->ctrl_options = $this->ctrl_options + $options_and_data['options'];
		}
		if (array_key_exists('options_data', $options_and_data)) {
			$this->ctrl_options_data = $this->ctrl_options_data + $options_and_data['options_data'];
		}
		return $this;
	}
	
}