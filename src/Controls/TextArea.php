<?php

namespace DecideNow\FastDev\Controls;

class TextArea extends CtrlText
{	
	protected $tmp_has_spans;
	
	public function __construct()
	{
		parent::__construct();
	}
	
	protected function prepareOpen()
	{
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$this->tmp_has_spans = ( (count($this->span_before) > 0) || (count($this->span_after) > 0) );
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		$this->openLabel();
		$this->openInputGroup($this->tmp_has_spans);
		$this->outSpansBefore();
		
		$this->ctrl .= '<textarea ';
		$this->ctrl .= ' id="'.$this->ctrl_id.'" class="form-control';
		$this->ctrl .= ($this->ctrl_size != '') ? ' input-'.$this->ctrl_size : '';
		$this->ctrl .= ($this->ctrl_ext_class) ? ' '.$this->ctrl_ext_class : '';
		$this->ctrl .='" name="'.$this->ctrl_name.'"';
		$this->ctrl .= ($this->ctrl_placeholder) ? ' placeholder="'.$this->ctrl_placeholder.'"' : '';
		
		$this->outFlagAttributes();
		$this->outCtrlAttr();
		$this->outCtrlData();
		$this->ctrl .= '>';
	}
	
	protected function prepareClose() {
		$this->ctrl .= '</textarea>';
		$this->outSpansAfter();
		$this->closeInputGroup($this->tmp_has_spans);
		$this->closeLabel();
		$this->outHelper();
		$this->closeFormGroup();
	}
	
	protected function prepareOut() {
		$this->prepareOpen();
		$this->ctrl .= $this->ctrl_value;
		$this->prepareClose();
	}
}