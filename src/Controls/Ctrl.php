<?php

namespace DecideNow\FastDev\Controls;

class Ctrl
{
	const CONTROL_STATE_SUCCESS	= 'success';
	const CONTROL_STATE_WARNING	= 'warning';
	const CONTROL_STATE_ERROR	= 'error';
	
	const CONTROL_SIZE_XS	= 'xs';
	const CONTROL_SIZE_SM	= 'sm';
	const CONTROL_SIZE_MD	= 'md';
	const CONTROL_SIZE_LG	= 'lg';
	const CONTROL_SIZE_FS	= 'fs';
	
	const BUTTON_STYLE_DEFAULT	= 'default';
	const BUTTON_STYLE_PRIMARY	= 'primary';
	const BUTTON_STYLE_SUCCESS	= 'success';
	const BUTTON_STYLE_INFO		= 'info';
	const BUTTON_STYLE_WARNING	= 'warning';
	const BUTTON_STYLE_DANGER	= 'danger';
	const BUTTON_STYLE_LINK		= 'link';
	
	const SORTABLE_DIRECTION_ASC	= 'asc';
	const SORTABLE_DIRECTION_DESC	= 'desc';
	
	public static function collectionMap($collection, $fields = [])
	{
		$ret = [];
		foreach ($collection as $item) {
			if (count($fields) < 2) {
				$ret[$item->id] = $item->toString();
			} else {
				$key = $fields[0];
				$val = $fields[1];
				$ret[$item->$key] = $item->$val;
			}
		}
		return $ret;
	}
	
	public static function textField($name, $model = null)
	{
		if ($model == null) {
			return (new TextField)->constructByName($name);
		} else {
			return (new TextField)->constructByNameAndModel($name, $model);
		}
	}
	
	public static function textArea($name, $model = null)
	{
		if ($model == null) {
			return (new TextArea)->constructByName($name);
		} else {
			return (new TextArea)->constructByNameAndModel($name, $model);
		}
	}
	
	public static function selectField($name, $model = null)
	{
		if ($model == null) {
			return (new SelectField)->constructByName($name);
		} else {
			return (new SelectField)->constructByNameAndModel($name, $model);
		}
	}
	
	public static function checkbox($name, $model = null)
	{
		if ($model == null) {
			return (new Checkbox)->constructByName($name);
		} else {
			return (new Checkbox)->constructByNameAndModel($name, $model);
		}
	}
	
	public static function checkboxList($name, $model = null, $array_key = '')
	{
		if ($model == null) {
			return (new CheckboxList)->constructByName($name);
		} else {
			return (new CheckboxList)->constructByNameAndModel($name, $model, $array_key);
		}
	}
	
	public static function radioList($name, $model = null, $array_key = '')
	{
		if ($model == null) {
			return (new RadioList)->constructByName($name);
		} else {
			return (new RadioList)->constructByNameAndModel($name, $model, $array_key);
		}
	}
	
	public static function radioButtonList($name, $model = null)
	{
		if ($model == null) {
			return (new RadioButtonList)->constructByName($name);
		} else {
			return (new RadioButtonList)->constructByNameAndModel($name, $model);
		}
	}
	
	public static function button($name)
	{
		return (new Button)->constructByName($name);
	}
	
	public static function buttonGroup($name)
	{
		return (new ButtonGroup)->constructByName($name);
	}
	
	public static function modal($name)
	{
		return (new Modal)->constructByName($name);
	}
	
	public static function sortableLink($name, $model = null, $ordering = null)
	{
		if ($model == null) {
			return (new SortableLink)->constructByName($name);
		} else {
			return (new SortableLink)->constructByNameAndModelAndParam($name, $model, $ordering);
		}
	}
	
	public static function popoverLink($name, $model = null, $ordering = null, $filter= null)
	{
		if ($model == null) {
			return (new PopoverLink)->constructByName($name);
		} else {
			return (new PopoverLink)->constructByNameAndModelAndParam($name, $model, $ordering, $filter);
		}
	}
	
}