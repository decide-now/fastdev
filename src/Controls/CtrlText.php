<?php

namespace DecideNow\FastDev\Controls;

use Illuminate\Support\Str;

class CtrlText extends CtrlCustom
{	
	protected $ctrl_placeholder;
	
	protected $span_before;
	protected $span_after;
	
	protected $available_ctrl_sizes = [
		Ctrl::CONTROL_SIZE_SM, Ctrl::CONTROL_SIZE_LG,
	];
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_placeholder= '';
		$this->ctrl_size = '';
		$this->ctrl_helper = '';
		$this->ctrl_label_width = 0;
		
		$this->span_before = [];
		$this->span_after = [];
	}
	public function constructByName($name)
	{
		$this->ctrl_name = $name;
		return $this; 
	}
	public function constructByNameAndModel($name, $model)
	{
		$this->ctrl_name = Str::camel(class_basename($model)).'['.$name.']';
		$this->ctrl_label = $model::getAlias($name);
		$this->ctrl_placeholder = $model::getAlias($name);
		$this->ctrl_value = $model->getAttribute($name);
		return $this;
	}
	
	public function placeholder($placeholder)
	{
		$this->ctrl_placeholder= $placeholder;
		return $this;
	}
		
	public function iconBefore($icon)
	{
		$this->span_before[] = ['type'=>'icon', 'label'=>$icon];
		return $this;
	}
	public function iconAfter($icon)
	{
		$this->span_after[] = ['type'=>'icon', 'label'=>$icon];
		return $this;
	}
	public function textBefore($text)
	{
		$this->span_before[] = ['type'=>'text', 'label'=>$text];
		return $this;
	}
	public function textAfter($text)
	{
		$this->span_after[] = ['type'=>'text', 'label'=>$text];
		return $this;
	}
	
	protected function openInputGroup($flag)
	{
		if ($flag) {
			$this->ctrl .= '<div class="input-group';
			$this->ctrl .= ($this->ctrl_size != '') ? ' input-group-'.$this->ctrl_size : '';
			$this->ctrl .= '">';
		}
	}
	protected function closeInputGroup($flag)
	{
		$this->ctrl .= ($flag) ? '</div>' : ''; 
	}
	
	protected function outSpansBefore()
	{
		foreach ($this->span_before as $span) {
			if ($span['type'] != 'icon' && $span['type'] != 'text') continue;
			$this->ctrl .= '<span class="input-group-addon">';
			$this->ctrl .= ($span['type'] == 'icon') ? '<i class="'.$span['label'].'"></i>' : $span['label'];
			$this->ctrl .= '</span>';
		}
	}
	protected function outSpansAfter()
	{
		foreach ($this->span_after as $span) {
			if ($span['type'] != 'icon' && $span['type'] != 'text') continue;
			$this->ctrl .= '<span class="input-group-addon">';
			$this->ctrl .= ($span['type'] == 'icon') ? '<i class="'.$span['label'].'"></i>' : $span['label'];
			$this->ctrl .= '</span>';
		}
	}
	
	protected function prepareOut()
	{
		parent::prepareOut();
	}
}