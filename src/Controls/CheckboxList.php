<?php

namespace DecideNow\FastDev\Controls;

use Illuminate\Support\Str;

class CheckboxList extends CtrlCustom
{	
	use Optionable;
	
	protected $ctrl_values;
	
	protected $is_inline;
	
	protected $options_to_skip;
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_options = [];
		$this->ctrl_options_data = [];
		$this->ctrl_values = [];
		
		$this->is_inline = false;
		
		$this->options_to_skip = [];
	}
	public function constructByName($name)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		return $instance; 
	}
	public function constructByNameAndModel($name, $model, $array_key = '')
	{
		$instance = new self();
		$instance->ctrl_name = Str::camel(class_basename($model)).'['.$name.']';
		$instance->ctrl_name .= ($array_key) ? '['.$array_key.']' : '';
		$instance->ctrl_label = $model::getAlias($name);
		$instance->value($model->getAttribute($name));
		return $instance;
	}
	
	public function value($value)
	{
		$this->ctrl_values[] = $value;
		return $this;
	}
	public function values($values)
	{
		if (!$values) {
			$values = [];
		}
		$this->ctrl_values = array_merge($this->ctrl_values, $values);
		return $this;
	}
	
	public function isInline()
	{
		$this->is_inline = true;
		return $this;
	}
	
	protected function addCheckbox($id, $text)
	{	
		$checked = false;
		if (in_array($id, $this->ctrl_values)) {
			$checked = true;
		}
		$this->ctrl .= ($this->ctrl_is_disabled && $checked) ? '<input type="hidden" name="'.$this->ctrl_name.'[]" value="'.$id.'">' : '';
		$this->ctrl .= ($this->is_inline) ? '' : '<div class="checkbox"';
		if (array_key_exists($id, $this->ctrl_options_data)) {
			foreach($this->ctrl_options_data[$id] as $data_key => $data_value) {
				$this->ctrl .= ' data-'.$data_key.'="'.(($data_value === true) ? '1' : ( ($data_value === false) ? '0' : $data_value )).'"';
			}
		}
		$this->ctrl .= ($this->is_inline) ? '' : '>';
		$this->ctrl .= '<label';
		$this->ctrl .= ($this->is_inline) ? ' class="checkbox-inline"' : '';
		
		$this->ctrl .= '>';
		$this->ctrl .= '<input type="checkbox" id="'.$this->ctrl_id.'_'.$id.'" name="'.$this->ctrl_name.'['. $id .']"';
		$this->ctrl .= ' value="'.$id.'"';
		$this->ctrl .= ($checked) ? ' checked' : '';
		$this->ctrl .= ($this->ctrl_is_disabled) ? ' disabled' : '';
		$this->ctrl .= '>';
		$this->ctrl .= $text;
		if (array_key_exists($id, $this->ctrl_options_data)) {
			if (array_key_exists('label-description', $this->ctrl_options_data[$id])) {
				$this->ctrl .= ' <sup><a href="" onclick="return false" data-trigger="focus" data-toggle="popover" data-html="true" data-content="';
				$this->ctrl .= $this->ctrl_options_data[$id]['label-description'];
				$this->ctrl .= '"><i class="glyphicon glyphicon-question-sign" ></i></a></sup>';
			}
		}
		$this->ctrl .= '</label>';
		
		if (array_key_exists($id, $this->ctrl_options_data)) {
			if (array_key_exists('tree-has-children', $this->ctrl_options_data[$id])) {
				if ($this->ctrl_options_data[$id]['tree-has-children']) {
					$this->ctrl .= '<a href="#tree-group-'.$id.'" data-toggle="collapse" class="checkbox-group-toggle"></a>';
				}
			}
		}
		
		$this->ctrl .= ($this->is_inline) ? '' : '</div>';
		
		$this->options_to_skip[] = $id;
	}
	
	protected function openGroup($id)
	{
		if (!$this->ctrl_options_data[$id]['tree-has-children']) return;
		
		$this->ctrl .= '<div id="tree-group';
		$this->ctrl .= ($id) ? '-'.$id : '';
		$this->ctrl .= '" class="collapse">';
	}
	protected function closeGroup($id)
	{
		if (!$this->ctrl_options_data[$id]['tree-has-children']) return;
		$this->ctrl .= '</div>';
	}
	
	protected function addCheckboxByTreeParent($opt_k, $opt_v)
	{	
		$this->addCheckbox($opt_k, $opt_v);

		if (!count($this->ctrl_options_data)) return;
		
		$children = [];
		foreach ($this->ctrl_options as $ch_k => $ch_v) {
			if (in_array($ch_k, $this->options_to_skip)) continue;
			if (!array_key_exists($ch_k, $this->ctrl_options_data)) continue;
			if (!array_key_exists('tree-parent-id', $this->ctrl_options_data[$ch_k])) continue;
			if ($this->ctrl_options_data[$ch_k]['tree-parent-id'] !== $opt_k) continue;
			$children[$ch_k] = $ch_v;
		}
		
		if (count($children)) {
			$this->openGroup($opt_k);
			foreach ($children as $ch_k => $ch_v) {
				$this->addCheckboxByTreeParent($ch_k, $ch_v);
			}
			$this->closeGroup($opt_k);
		}
	}
	
	protected function prepareOut()
	{
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		$this->openLabel();
		
		$this->ctrl .= '<div id="'.$this->ctrl_id.'" class="checkbox-list">';
		foreach ($this->ctrl_options as $opt_k => $opt_v) {
			if (in_array($opt_k, $this->options_to_skip)) continue;
			$this->addCheckboxByTreeParent($opt_k, $opt_v);
		}
		$this->ctrl .= '</div>';
		
		$this->closeLabel();
		$this->outHelper();
		$this->closeFormGroup();
	}
}