<?php

namespace DecideNow\FastDev\Controls;

class TextField extends CtrlText
{	
	protected $ctrl_type;
	
	protected $ctrl_visible_value;
	
	protected $has_visible_value;
	protected $has_clear_button;
	protected $has_select_button;
	protected $has_select_button_data;
	protected $has_select_scene_id;
	protected $are_buttons_enabled; // if ctrl is disabled
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_type = 'text';
		$this->ctrl_visible_value = '';
		
		$this->has_visible_value = false;
		$this->has_clear_button = false;
		$this->has_select_button = false;
		$this->has_select_button_data = [];
		$this->has_select_scene_id = '';
		$this->are_buttons_enabled = false;
	}
	
	public function type($type)
	{
		$this->ctrl_type = strtolower($type);
		return $this;
	}
	
	public function visibleValue($value)
	{
		$this->ctrl_visible_value = $value;
		$this->has_visible_value = true;
		return $this;
	}
	
	public function hasClearButton($flag = true)
	{
		$this->has_clear_button = $flag;
		return $this;
	}
	
	public function hasSelectButton($select_scene_id = '', $data = [], $flag = true)
	{
		$this->has_select_scene_id = $select_scene_id;
		$this->has_select_button = $flag;
		$this->has_select_button_data = $data;
		return $this;
	}
	
	public function buttonsEnabled($flag = true)
	{
		$this->are_buttons_enabled = $flag;
		return $this;
	}
	
	public function buttonBefore($label, $prefix, $title = '', $data = [], $raw = false)
	{
		$span = ['type'=>'button', 'label'=>$label, 'prefix'=>$prefix, 'title'=>$title, 'data'=>$data, 'raw'=>$raw,];
		$span['style'] = (array_key_exists('style', $data)) ? $data['style'] : '';
		$this->span_before[] = $span;
		return $this;
	}
	public function buttonAfter($label, $prefix, $title = '', $data = [], $raw = false)
	{
		$span = ['type'=>'button', 'label'=>$label, 'prefix'=>$prefix, 'title'=>$title, 'data'=>$data, 'raw'=>$raw,];
		$span['style'] = (array_key_exists('style', $data)) ? $data['style'] : '';
		$this->span_after[] = $span;
		return $this;
	}
	
	protected function prepareHiddenInput()
	{
		$this->ctrl .= '<input';
		$this->ctrl .= ($this->ctrl_type) ? ' type="'.$this->ctrl_type.'"' : '';
		$this->ctrl .= ' id="'.$this->ctrl_id.'"';
		$this->ctrl .= ' name="'.$this->ctrl_name.'"';
		$this->ctrl .= ($this->ctrl_value) ? ' value="'.htmlentities($this->ctrl_value).'"' : '';
		$this->outCtrlAttr();
		$this->outCtrlData();
		$this->ctrl .= '>';
		return;
	}
	
	protected function prepareOut()
	{
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$has_buttons = ($this->has_clear_button || $this->has_select_button);
		$has_spans = ( (count($this->span_before) > 0) || (count($this->span_after) > 0) );
		
		if ($this->ctrl_type == 'date' && $this->ctrl_value != '') {
			$this->ctrl_value = date('Y-m-d', strtotime($this->ctrl_value));
		}
		
		$visible_name = ''; 
		if ($this->has_visible_value) {
			$visible_name = $this->ctrl_name;
			$postfix = '';
			if (substr($visible_name, -1) == ']') { 
				$postfix = ']';
				$visible_name = substr($visible_name, 0, -1);
			}
			$visible_name .= '_text'.$postfix;
		}
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		
		$this->ctrl .= ($this->has_visible_value) ? '<input type="hidden" id="'.$this->ctrl_id.'" name="'.$this->ctrl_name.'" value="'.$this->ctrl_value.'">' : '';
		
		$this->openLabel();
		$this->openInputGroup(($has_buttons || $has_spans));
		
		// before
		$this->outSpansBefore();
		
		$has_buttons_span = false;
		foreach ($this->span_before as $span) {
			if ($span['type'] == 'button') {
				if (!$has_buttons_span) {
					$this->ctrl .= '<span class="input-group-btn">';
					$has_buttons_span= true;
				}
				$this->ctrl .= '<button type="button" id="btn_'.$span['prefix'].'_'.$this->ctrl_id.'" class="btn';
				$this->ctrl .= ($span['style']) ? ' btn-"'.$span['style'] : ' btn-default';
				$this->ctrl .= '"';
				$this->ctrl .= ' data-role="field-handling"';
				$this->ctrl .= ($this->ctrl_is_disabled && !$this->are_buttons_enabled) ? ' disabled' : '';
				$this->ctrl .= ($span['title']) ? ' title="'.$span['title'].'"' : '';
				foreach($span['data'] as $key => $value) {
					$this->ctrl .= ' data-'.$key.'="'.$value.'"';
				}
				$this->ctrl .= '>';
				$this->ctrl .= ($span['raw']) ? $span['label'] : '<i class="'.$span['label'].'"></i>';
				$this->ctrl .= '</button>';
			}
		}
		if ($has_buttons_span) {
			$this->ctrl .= '</span>';
		}
		// /before
		
		$this->ctrl .= '<input id="'.$this->ctrl_id.(($this->has_visible_value) ? '_text' : '').'" class="form-control';
		$this->ctrl .= ($this->ctrl_size != '') ? ' input-'.$this->ctrl_size : '';
		$this->ctrl .= ($this->ctrl_ext_class) ? ' '.$this->ctrl_ext_class : '';
		$this->ctrl .= '" name="'.(($this->has_visible_value) ? $visible_name: $this->ctrl_name).'"';
		$this->ctrl .= ($this->ctrl_type) ? ' type="'.$this->ctrl_type.'"' : '';
		$this->ctrl .= ($this->ctrl_value != '') ? (' value="'.(($this->has_visible_value) ? htmlentities($this->ctrl_visible_value) : htmlentities($this->ctrl_value)).'"') : '';
		$this->ctrl .= ($this->ctrl_placeholder) ? ' placeholder="'.$this->ctrl_placeholder.'"' : '';
		$this->ctrl .= ($this->has_visible_value && $this->has_select_button) ? ' field-has-select="true"' : '';
		
		$this->outFlagAttributes();
		$this->outCtrlAttr();
		$this->outCtrlData();
		
		$this->ctrl .= '>';
		
		// after
		$this->outSpansAfter();
		
		$has_buttons_span = false;
		if ($has_buttons) {
			$this->ctrl .= '<span class="input-group-btn">';
			$has_buttons_span= true;
			if ($this->has_select_button) {
				$this->ctrl .= '<button type="button" id="btn_select_'.$this->ctrl_id.'" class="btn btn-default"';
				$this->ctrl .= ' field-id="'.$this->ctrl_id.(($this->has_visible_value) ? '_text' : '').'"';
				$this->ctrl .= ($this->has_visible_value) ?  ' hidden-field-id="'.$this->ctrl_id.'"': '';
				$this->ctrl .= ' data-field-id="'.$this->ctrl_id.(($this->has_visible_value) ? '_text' : '').'"';
				$this->ctrl .= ($this->has_visible_value) ?  ' data-hidden-field-id="'.$this->ctrl_id.'"': '';
				$this->ctrl .= ($this->ctrl_is_disabled && !$this->buttons_enabled) ? ' disabled' : '';
				$this->ctrl .= ' data-role="field-handling"';
				$this->ctrl .= ($this->has_select_scene_id) ? ' data-select-scene-id="'.$this->has_select_scene_id.'"' : '';
				foreach ($this->has_select_button_data as $data_key => $data_value) {
					$this->ctrl .= ' data-'.$data_key.'="'.$data_value.'"';
				}
				$this->ctrl .= '>...</button>';
			}
			if ($this->has_clear_button) {
				$this->ctrl .= '<button type="button" id="btn_clear_'.$this->ctrl_id.'" class="btn btn-default"';
				$this->ctrl .= ' field-id="'.$this->ctrl_id.(($this->has_visible_value) ? '_text' : '').'"';
				$this->ctrl .= ($this->has_visible_value) ?  ' hidden-field-id="'.$this->ctrl_id.'"': '';
				$this->ctrl .= ' data-field-id="'.$this->ctrl_id.(($this->has_visible_value) ? '_text' : '').'"';
				$this->ctrl .= ($this->has_visible_value) ?  ' data-hidden-field-id="'.$this->ctrl_id.'"': '';
				$this->ctrl .= ($this->ctrl_is_disabled && !$this->buttons_enabled)? ' disabled' : '';
				$this->ctrl .= ' data-role="field-handling"';
				$this->ctrl .= '>X</button>';
			}
		}
		foreach ($this->span_after as $span) {
			if ($span['type'] == 'button') {
				if (!$has_buttons_span) {
					$this->ctrl .= '<span class="input-group-btn">';
					$has_buttons_span= true;
				}
				$this->ctrl .= '<button type="button" id="btn_'.$span['prefix'].'_'.$this->ctrl_id.'" class="btn';
				$this->ctrl .= ($span['style']) ? ' btn-'.$span['style'] : ' btn-default';
				$this->ctrl .= '"';
				$this->ctrl .= ($this->ctrl_is_disabled && !$this->are_buttons_enabled) ? ' disabled' : '';
				$this->ctrl .= ($span['title']) ? ' title="'.$span['title'].'"' : '';
				foreach($span['data'] as $key => $value) {
					$this->ctrl .= ' data-'.$key.'="'.$value.'"';
				}
				$this->ctrl .= '>';
				$this->ctrl .= ($span['raw']) ? $span['label'] : '<i class="'.$span['label'].'"></i>';
				$this->ctrl .= '</button>';
			}
		}
		if ($has_buttons_span) {
			$this->ctrl .= '</span>';
		}
		// /after
		
		$this->closeInputGroup(($has_buttons || $has_spans));
		$this->closeLabel();
		
		$this->ctrl .= ($this->ctrl_helper) ? '<p class="help-block">'.$this->ctrl_helper.'</p>' : '';
		
		$this->closeFormGroup();
	}

}