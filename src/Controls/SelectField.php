<?php

namespace DecideNow\FastDev\Controls;

class SelectField extends CtrlText
{
	use Optionable;
	
	protected $ctrl_values;
	
	protected $ctrl_label_width;
	protected $empty_text;
	protected $empty_disabled;
	
	protected $is_multiple;
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_options = [];
		$this->ctrl_options_data = [];
		$this->ctrl_values = [];
		$this->empty_text = null;
		$this->empty_disabled = false;
		
		$this->is_multiple = false;
	}
	
	public function constructByNameAndModel($name, $model)
	{
		$instance = parent::constructByNameAndModel($name, $model);
		$instance->ctrl_values[] = $instance->ctrl_value;
		return $instance;
	}
	
	public function value($value)
	{
		$instance = parent::value($value);
		$instance->ctrl_values[] = $value;
		return $instance;
	}
	public function values($values)
	{
		if (!$values) {
			$values = [];
		}
		$this->ctrl_values = array_merge($this->ctrl_values, $values);
		return $this;
	}
	
	public function emptyText($text, $empty_disabled = false)
	{
		$this->empty_text = $text;
		$this->empty_disabled = $empty_disabled;
		return $this;
	}
	
	public function isMultiple($flag = true)
	{
		$this->is_multiple = $flag;
		return $this;
	}
	
	public function buttonBefore($label, $prefix, $title = '', $data = [], $raw = false)
	{
		$this->span_before[] = ['type'=>'button', 'label'=>$label, 'prefix'=>$prefix, 'title'=>$title, 'data'=>$data, 'raw'=>$raw,];
		return $this;
	}
	public function buttonAfter($label, $prefix, $title = '', $data = [], $raw = false)
	{
		$this->span_after[] = ['type'=>'button', 'label'=>$label, 'prefix'=>$prefix, 'title'=>$title, 'data'=>$data, 'raw'=>$raw,];
		return $this;
	}
	
	protected function addOption($id, $text, $option_disabled = false)
	{
		$selected = false;
		if ($id === '') {
			if (count($this->ctrl_values) == 0) {
				$selected = true;
			} elseif ( count($this->ctrl_values) == 1 && ($this->ctrl_values[0] === '' || $this->ctrl_values[0] === null) ) {
				$selected = true;
			}
		} elseif ( in_array($id, $this->ctrl_values) && ($this->ctrl_values[0] !== '' && $this->ctrl_values[0] !== null) ) {
			$selected = true;
		}
		if ($this->ctrl_is_disabled && !$selected) {
			return;
		}
		$this->ctrl .= '<option value="'.$id.'"';
		$this->ctrl .= ($selected) ? ' selected' : '';
		$this->ctrl .= ($option_disabled) ? ' disabled' : '';
		$this->ctrl .= '>';
		$this->ctrl .= $text;
		$this->ctrl .= '</option>';
	}
	protected function prepareOut()
	{
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$has_spans = ( (count($this->span_before) > 0) || (count($this->span_after) > 0) );
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		$this->openLabel();
		
		$this->openInputGroup($has_spans);
		
		// before
		$this->outSpansBefore();
		
		$has_buttons_span = false;
		foreach ($this->span_before as $span) {
			if ($span['type'] == 'button') {
				if (!$has_buttons_span) {
					$this->ctrl .= '<span class="input-group-btn">';
					$has_buttons_span= true;
				}
				$this->ctrl .= '<button type="button" id="btn_'.$span['prefix'].'_'.$this->ctrl_id.'" class="btn btn-default"';
				$this->ctrl .= ($this->ctrl_is_disabled && !$this->are_buttons_enabled) ? ' disabled' : '';
				$this->ctrl .= ($span['title']) ? ' title="'.$span['title'].'"' : '';
				foreach($span['data'] as $key => $value) {
					$this->ctrl .= ' data-'.$key.'="'.$value.'"';
				}
				$this->ctrl .= '>';
				$this->ctrl .= ($span['raw']) ? $span['label'] : '<i class="'.$span['label'].'"></i>';
				$this->ctrl .= '</button>';
			}
		}
		if ($has_buttons_span) {
			$this->ctrl .= '</span>';
		}
		// /before
		
		
		$this->ctrl .= '<select id="'.$this->ctrl_id.'" class="form-control';
		$this->ctrl .= ($this->ctrl_size != '') ? ' input-'.$this->ctrl_size : '';
		$this->ctrl .= ($this->ctrl_ext_class != '') ? ' '.$this->ctrl_ext_class : '';
		$this->ctrl .= '" name="'.$this->ctrl_name.'"';
		$this->ctrl .= ($this->is_multiple) ? ' multiple' : '';
		$this->ctrl .= ($this->ctrl_title) ? ' title="'.$this->ctrl_title.'"' : '';
		
		$this->outFlagAttributes();
		$this->outCtrlAttr();
		$this->outCtrlData();
		
		$this->ctrl .= '>';
		
		if ($this->empty_text != null) {
			$this->addOption('', $this->empty_text, $this->empty_disabled);
		}
		foreach ($this->ctrl_options as $opt_k => $opt_v) {
			$this->addOption($opt_k, $opt_v);
		}
		
		$this->ctrl .= '</select>';
		
		
		// after
		$this->outSpansAfter();
		
		$has_buttons_span = false;
		foreach ($this->span_after as $span) {
			if ($span['type'] == 'button') {
				if (!$has_buttons_span) {
					$this->ctrl .= '<span class="input-group-btn">';
					$has_buttons_span= true;
				}
				$this->ctrl .= '<button type="button" id="btn_'.$span['prefix'].'_'.$this->ctrl_id.'" class="btn btn-default"';
				$this->ctrl .= ($this->ctrl_is_disabled && !$this->are_buttons_enabled) ? ' disabled' : '';
				$this->ctrl .= ($span['title']) ? ' title="'.$span['title'].'"' : '';
				foreach($span['data'] as $key => $value) {
					$this->ctrl .= ' data-'.$key.'="'.$value.'"';
				}
				$this->ctrl .= '>';
				$this->ctrl .= ($span['raw']) ? $span['label'] : '<i class="'.$span['label'].'"></i>';
				$this->ctrl .= '</button>';
			}
		}
		if ($has_buttons_span) {
			$this->ctrl .= '</span>';
		}
		// /after
		
		$this->closeInputGroup($has_spans);
		$this->closeLabel();
		$this->outHelper();
		$this->closeFormGroup();
	}
}