<?php

namespace DecideNow\FastDev\Controls;

use Illuminate\Support\Str;

class RadioButtonList extends CtrlCustom
{	
	use Optionable;
	
	protected $ctrl_is_justified;
	protected $ctrl_style;
	
	protected $available_ctrl_sizes = [
		Ctrl::CONTROL_SIZE_XS,
		Ctrl::CONTROL_SIZE_SM,
		Ctrl::CONTROL_SIZE_MD,
		Ctrl::CONTROL_SIZE_LG
	];
	
	public function __construct()
	{
		parent::__construct();
		$this->ctrl_options = [];
		$this->ctrl_options_data = [];
		$this->ctrl_is_justified = false;
		$this->ctrl_style = 'default';
	}
	public function constructByName($name)
	{
		$instance = new self();
		$instance->ctrl_name = $name;
		return $instance; 
	}
	public function constructByNameAndModel($name, $model)
	{
		$instance = new self();
		$instance->ctrl_name = Str::camel(class_basename($model)).'['.$name.']';
		$instance->ctrl_label = $model::getAlias($name);
		$instance->value($model->getAttribute($name));
		return $instance;
	}
	
	public function isJustified($value = true)
	{
		$this->ctrl_is_justified = $value;
		return $this;
	}
	public function style($style = 'default')
	{
		$this->ctrl_style = $style;
		return $this;
	}
	
	protected function addRadio($id, $text)
	{
		$actual_id = trim($id);
		if (is_bool($this->ctrl_value)) {
			$actual_id = !($id === '0' || $id === 0 || $id === '' || $id === false);
		}
		$checked = ($actual_id == trim($this->ctrl_value));
		
		$option_style = $this->ctrl_style;
		if (array_key_exists($id, $this->ctrl_options_data)) {
			if (array_key_exists('style', $this->ctrl_options_data[$id])) {
				$option_style = $this->ctrl_options_data[$id]['style'];
			}
		}
		
		$option_title = '';
		if (array_key_exists($id, $this->ctrl_options_data)) {
			if (array_key_exists('title', $this->ctrl_options_data[$id])) {
				$option_title= $this->ctrl_options_data[$id]['title'];
			}
		}
		 
		
		$this->ctrl .= '<label class="btn';
		$this->ctrl .= ($option_style) ? ' btn-'.$option_style : '';
		$this->ctrl .= ($checked) ? ' active' : '';
		$this->ctrl .= '"';
		$this->ctrl .= ($option_title) ? ' title="'.$option_title.'"' : '';
		$this->ctrl .= ($this->ctrl_is_disabled) ? ' disabled' : '';
		if (array_key_exists($id, $this->ctrl_options_data)) {
			foreach($this->ctrl_options_data[$id] as $data_key => $data_value) {
				if ($data_key == 'style' || $data_key == 'title') continue;
				$this->ctrl .= ' data-'.$data_key.'="'.(($data_value === true) ? '1' : ( ($data_value === false) ? '0' : $data_value )).'"';
			}
		}
		$this->ctrl .= '>';
		$this->ctrl .= ($this->ctrl_is_disabled && $checked) ? '<input type="hidden" name="'.$this->ctrl_name.'" value="'.$id.'">' : '';
		$this->ctrl .= '<input type="radio" id="'.$this->ctrl_id.'_'.$id.'" name="'.$this->ctrl_name.'"';
		$this->ctrl .= ' value="'.$id.'"';
		$this->ctrl .= ($checked) ? ' checked' : '';
		$this->ctrl .= ' autocomplete="off"';
		$this->ctrl .= ($this->ctrl_is_disabled) ? ' disabled' : '';
		$this->ctrl .= '>';
		$this->ctrl .= $text;
		$this->ctrl .= '</label>';
	}
	protected function prepareOut()
	{
		parent::prepareOut();
		
		if ($this->ctrl_type == 'hidden') {
			return $this->prepareHiddenInput();
		}
		
		$this->ctrl = '';
		
		$this->openFormGroup();
		$this->openLabel();
		
		$this->ctrl .= '<br>';
		
		$this->ctrl .= '<div id="'.$this->ctrl_id.'" data-toggle="buttons" class="btn-group';
		$this->ctrl .= ($this->ctrl_ext_class) ? ' '.$this->ctrl_ext_class : '';
		$this->ctrl .= ($this->ctrl_is_justified) ? ' btn-group-justified' : '';
		$this->ctrl .= ($this->ctrl_size) ? ' btn-group-'.$this->ctrl_size : '';
		$this->ctrl .= '"';
		$this->outFlagAttributes();
		$this->outCtrlAttr();
		$this->outCtrlData();
		$this->ctrl .= '>';
		foreach ($this->ctrl_options as $opt_k => $opt_v) {
			$this->addRadio($opt_k, $opt_v);
		}
		$this->ctrl .= '</div>';
		
		$this->closeLabel();
		$this->outHelper();
		$this->closeFormGroup();
	}
}