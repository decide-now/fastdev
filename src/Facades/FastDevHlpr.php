<?php
namespace DecideNow\FastDev\Facades;

use Illuminate\Support\Facades\Facade;

/**
 *
 * @method static string getMonthString(int $month_number, string $lang='ru')
 * @method static string translateMonthString(string $date_string, string $lang='ru')
 *        
 * @see DecideNow\FastDev\Helper\Helper
 */
class FastDevHlpr extends Facade
{

	protected static function getFacadeAccessor()
	{
		return 'fastdev.hlpr';
	}
}
