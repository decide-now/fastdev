<?php

namespace DecideNow\FastDev\Facades;

use Illuminate\Support\Facades\Facade;

class FastDevCtrl extends Facade{
	
	protected static function getFacadeAccessor()
	{
		return 'fastdev.ctrl';
	}
}
