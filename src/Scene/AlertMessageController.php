<?php
namespace DecideNow\FastDev\Scene;

use Illuminate\Http\Request;

class AlertMessageController extends SceneBaseController
{	
	static $scene_id = 'alert-message';
	
	public $success_messages = [];
	public $error_messages = [];
	
	public function __construct($scene_parent = null) {
		parent::__construct($scene_parent);
		$this->no_content = false;
	}
	
	public function prepareContent(Request $request)
	{
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			$request->flash();
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		}
	}	
}