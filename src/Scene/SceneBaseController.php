<?php
namespace DecideNow\FastDev\Scene;

use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Routing\ControllerMiddlewareOptions;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class SceneBaseController extends BaseController {
	
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	public function sceneVersion()
	{
		return 1;
	}

	static $scene_id = '';
	protected $instance_scene_id = '';
	public $scene_id_suffix;
	public $template_content = '';
	public $template_code = '';
	static $default_update_url = '';
	public $nest_layout = 'base.base_nest';
	
	public $no_content = true;
	public $scene_children = null;
	public $scene_parent = null;
	public $scene_parent_id = '';
	
	protected $response_data = [];
	protected $content_error = false;
	public $has_content;
	
	private $primitives_array  = [];
	protected $permanent_primitives = [ 'task' => ['default' => '', 'is_hidden' => true] ];
	public $primitives = [ 'task' => ['default' => '', 'is_hidden' => true] ];
	public function setPrimitives($key, $value)
	{
		$this->primitives_array[$key] = $value;
	}
	public function getPrimitives($key = '', $add_hidden = false) {
		$pr = $this->primitives + $this->permanent_primitives;
		if ($key) {
			if (array_key_exists($key, $this->primitives_array)) {
				$ret = $this->primitives_array[$key];
			} else {
				$value = $pr[$key];
				$ret = (is_array($value)) ? $value['default'] : $value;
			}
		} else {
			$ret = $this->primitives_array;
			$hidden_primitives = [];
			foreach($pr as $primitive_key => $primitive_def) {
				if (is_array($primitive_def) && array_key_exists('is_hidden', $primitive_def) && $primitive_def['is_hidden']) {
					$hidden_primitives[] = $primitive_key;
				}
			}
			if (!$add_hidden) {
				$ret = array_diff_key($ret, array_flip($hidden_primitives));
			}
		}
		return $ret;
	}
	protected function transferPrimitives($request = null)
	{
		if ($request) {
			$this->primitivesFromRequest($request);
		} else {
			$this->primitivesFromSession();
		}
		
	}
	protected function primitivesFromRequest($request)
	{
		$pr = $this->primitives + $this->permanent_primitives;
		foreach($pr as $key => $value) {
			$pr_default = (is_array($value)) ? $value['default'] : $value;
			$pr_val = $request->get($key, $pr_default);
			$pr[$key] = $pr_val;
		}
		Session::flash('primitives', $pr);
		$this->primitives_array = $pr;
	}
	protected function primitivesFromSession()
	{
		$pr = $this->primitives + $this->permanent_primitives;
		$prs = Session::get('primitives', []);
		foreach($pr as $key => $value) {
			if (array_key_exists($key, $prs)) {
				$pr[$key] = $prs[$key];
			} else {
				$pr_default = (is_array($value)) ? $value['default'] : $value;
				$pr[$key] = $pr_default;
			}
		}
		$this->primitives_array = $pr;
	}
	
	public function __construct($scene_parent = null) {
		$this->instance_scene_id = static::$scene_id;
		$this->scene_parent = $scene_parent;
		$this->scene_children = new Collection();
		$this->defineChildren();
	}
	
	
	protected $default_actions_tech = 'ajax';
	protected $actions_tech = [
		/*
		 'btn_refresh' => 'ajax',
		 */
	];
	
	public function getActionTechnology($action)
	{
		if (array_key_exists($action, $this->actions_tech)) {
			return $this->actions_tech[$action];
		}
		return $this->default_actions_tech;
	}
	
	
	protected function contentErrorRedirect()
	{
		return redirect()->route('base');
	}
	
	protected function defineAlertChildScene()
	{
		$alert_message_scene = new AlertMessageController($this);
		$this->scene_children->put('alert_message_scene', $alert_message_scene);
	}
	
	protected function defineChildren()
	{
		//
	}
	
	protected function transferSceneId($request = null)
	{
		if ($request) {
			$val = $request->get('scene_id', static::$scene_id);
			Session::flash('scene_id', $val);
			return $val;
		} else {
			static::$scene_id = Session::get('scene_id', static::$scene_id);
			foreach ($this->scene_children as $child) {
				$child->scene_parent_id = static::$scene_id;
			}
			return static::$scene_id;
		}
	}
	
	protected function transferAJAXFlag($request = null, $forget = false)
	{
		if ($request) {
			Session::flash('ajax', $request->ajax());
		} else {
			return Session::get('ajax', false);
		}
	}
	
	protected function transferData($name, $default = null, $request = null)
	{
		if ($request) {
			$val = $request->get($name, $default);
			Session::flash($name, $val);
		} else {
			$val = Session::get($name, $default);
		}
		return $val;
	}
	
	protected function transferForget($data = [])
	{
		$keys = [];
		$keys[] = 'scene_id';
		$keys[] = 'ajax';
		$keys[] = 'primitives';
		$keys = array_merge($keys, $data);
		
		$flash_new = Session::get('_flash.new', []);
		$flash_old = Session::get('_flash.old', []);
		foreach ($keys as $key) {
			$pos_new = array_search($key, $flash_new);
			if ($pos_new !== false) {
				unset($flash_new[$pos_new]);
			}
			$pos_old = array_search($key, $flash_old);
			if ($pos_old === false) {
				$flash_old[] = $key;
			}
		}
		Session::put('_flash.new', $flash_new);
		Session::put('_flash.old', $flash_old);
	}
	
	protected function handleBoolean($val)
	{
		$true_values = ['true', '1'];
		$false_values = ['null', 'false', '0'];
		
		if (in_array($val, $true_values)) return true;
		if (in_array($val, $false_values)) return false;
		
		return $val;
	}
	
	protected function handleRequestBoolean($name, $default = false, $request = null)
	{
		$ret = $request->input($name, $default);
		$ret = $this->handleBoolean($ret);
		return $ret;
	}

	protected function getSceneMessages($type = 'error')
	{
		$ret = [];
		if (Session::has('flash_'.$type.'_messages')) {
			$msgs = Session::get('flash_'.$type.'_messages');
			foreach ($msgs as $msg) {
				$ret[] = $msg;
			}
		}
		return $ret;
	}
	
	protected function setContentError($message)
	{
		$this->content_error = true;
		if (is_string($message)) {
			Session::flash('flash_error_messages', [$message]);
		} else {
			Session::flash('flash_error_messages', $message);
		}
	}
	
	protected function resetFlashMessages()
	{
		Session::forget(Session::get('_flash.old', []));
		Session::put('_flash.old', Session::get('_flash.new', []));
		Session::put('_flash.new', []);
	}

	protected function sceneOutput() {
		
		if (Session::get('ajax', false) === true) {
			$this->response_data += $this->getPrimitives();
			if ($this->content_error) {
				$reponseData = ['message' => Session::get('flash_error_messages')];
				$reponseData += $this->response_data;
				$this->resetFlashMessages();
				return response()->json($reponseData, 422);
			}
			if ($this->no_content) {
				$reponseData = ['page' => ''];
				$reponseData += $this->response_data;
				return response()->json($reponseData);
			}
			if (Session::has('flash_error_messages')) {
				$reponseData = ['message' => Session::get('flash_error_messages')];
				$reponseData += $this->response_data;
				$this->resetFlashMessages();
				return response()->json($reponseData, 422);
			} else {
				$page = view($this->getTemplateContent(), $this->sceneArray());
				$reponseData = ['page' => $page->render()];
				if (Session::has('flash_success_messages')) {
					$reponseData += ['message' => Session::get('flash_success_messages')];
				}
				$reponseData += $this->response_data;
				$this->resetFlashMessages();
				return response()->json($reponseData);
			}
		} else {
			if ($this->content_error) {
				return $this->contentErrorRedirect();
			}
			
			if ($this->scene_children->has('alert_message_scene')) {
				$alert_message_scene = $this->scene_children->get('alert_message_scene');
				$alert_message_scene->success_messages = $this->getSceneMessages('success');
				$alert_message_scene->error_messages = $this->getSceneMessages('error');
			}
			
			$this->resetFlashMessages();

			return view('layouts.' . $this->nest_layout, $this->sceneArray());
		}
	}
	
	public static function controllerPath()
	{
		$full_path =  get_called_class();
		return str_replace("App\\Http\\Controllers\\", "", $full_path);
	}
	
	public static function methodPath($method_name)
	{
		return self::controllerPath().'@'.$method_name;
	}
	
	public function getProperty($property_name)
	{
		return static::$$property_name;
	}
	
	public function sceneArray()
	{
		return
			['scene' => $this] + 
			get_object_vars($this) + 
			$this->getPrimitives('', true);
	}
	
	public function sceneVariables()
	{
		return $this->sceneArray();
	}

	public function sceneId()
	{
		$ret = static::$scene_id.$this->scene_id_suffix;
		if ($this->scene_parent) {
			$ret = $this->scene_parent->sceneId() . '-' . $ret;
		} elseif ($this->scene_parent_id) {
			$ret = $this->scene_parent_id . '-' . $ret;
		}
		return $ret;
	}
	
	public function updateURL($parameters = [])
	{
		$url = $this->getProperty('default_update_url');
		if (!$url) {
			$url = action(self::methodPath('get'), $parameters);
		}
		return $url;
	}
	
	public function template_content()
	{
		$template = $this->template_content;
		if (!$template) {
			$scene_id = $this->instance_scene_id;
			$path = str_replace('-', '.', $scene_id);
			$template = $path . '.' . $scene_id . '_content';
		}
		return $template;
	}
	
	public function getTemplateContent()
	{
		return $this->template_content();
	}
	
	public function template_code()
	{
		$template = $this->template_code;
		if (!$template) {
			$scene_id = $this->instance_scene_id;
			$path = str_replace('-', '.', $scene_id);
			$template = $path . '.' . $scene_id . '_code';
		}
		return $template;
	}
	
	public function getTemplateCode()
	{
		return $this->template_code();
	}
	
	protected function getDatabaseErrorMessage(QueryException $e)
	{
		return ($e->errorInfo [0] == 23000) ? 'В базе данных имеются ссылки на удаляемую запись!' : 'Ошибка базы данных. Код: ' . $e->errorInfo [0];
	}
	
	protected function middlewareUpdate($middleware, $options)
	{
		$mddlwr = $this->middleware;
		foreach($mddlwr as $k => $mddlwr_item) {
			if ($mddlwr_item['middleware'] == $middleware) {
				$this->middleware[$k]['options'] = $options;
				return new ControllerMiddlewareOptions($options);
			}
		}
		return $this->middleware($middleware, $options);
	}
	
	public function customValidationException($message)
	{
		$validator = Validator::make([], []);
		$validator->errors()->add('message', $message);
		throw new ValidationException($validator);
	}
	
	public function childrenFlatten()
	{
		$ret = new Collection;
		foreach ($this->scene_children as $subscene) {
			$ret = $ret->merge($subscene->childrenFlatten());
		}
		$ret->add($this);
		return $ret;
	}
}
