<?php

namespace DecideNow\FastDev\Helper;

use Carbon\Carbon;

class Helper {
	
	/* Values conversion */
	
	private static $month_rus = [
		'января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря',
	];
	private static $month_eng = [
		'january','february','march','april','may','june','july','august','september','october','november','december',
	];
	
	public static function getMonthString($month_number, $lang='ru')
	{
		$month = (int) $month_number;
		if ($lang = 'ru') {
			return self::$month_rus[$month - 1];
		} else {
			return self::$month_eng[$month - 1];
		}
	}
	
	public static function translateMonthString($date_string, $lang='ru')
	{
		if ($lang == 'ru') {
			$ret = str_replace(self::$month_eng,  self::$month_rus, $date_string);
		} else {
			$ret = str_replace( self::$month_rus, self::$month_eng, $date_string);
		}
		return $ret;
	}
	
	public static function dateFormat($date, $format='DMY', $lang='ru')
	{
		$ret = '';
		if ($date) {
			$date = self::translateMonthString($date, 'en');
			$date = new Carbon($date);
			if ($format == 'DMY') {
				$ret = $date->format('d.m.Y');
			} elseif ($format == 'YMD') {
				$ret = $date->format('Y-m-d');
			} elseif ($format == 'DMYT') {
				$ret = $date->format('d.m.Y H:i:s');
			} elseif ($format == 'DMMY') {
				$ret = $date->format('d').' '.self::getMonthString($date->format('m'), $lang).' '.$date->format('Y');
			} elseif ($format == 'T') {
				$ret = $date->format('H:i:s');
			} else {
				$ret = $date;
			}
		}
		return $ret;
	}
	
	public static function clearNumeric($str)
	{
		$ret = '';
		$str = str_replace(',', '.', $str);
		$numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '-'];
		$array = str_split($str);
		foreach ($array as $char) {
			$ret .= in_array($char, $numbers) ? $char : '';
		}
		return ''.floatval($ret);
	}
	
	public static function numberFormat($val, $decimals = 2, $suffix = '', $format_empty_value = false)
	{
		if (empty($val) && !$format_empty_value) {
			return '';
		}
		$ret = number_format($val, $decimals, ',', 'a');
		$ret = str_replace('a', '&nbsp;', $ret);
		if ($suffix) {
			$ret = $ret . '&nbsp;' . $suffix;
		}
		return $ret;
	}

	public static function moneyFormat($val) {
		return self::numberFormat($val, 2, '&#8381;', true);
	}

	public static function translit($text, $result = 'lat')
	{
		$cyr = [
			'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
			'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
			'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
			'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
		];
		$lat = [
			'a','b','v','g','d','e','yo','zh','z','i','y','k','l','m','n','o','p',
			'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
			'A','B','V','G','D','E','Yo','Zh','Z','I','Y','K','L','M','N','O','P',
			'R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya'
		];
		if ($result == 'lat') {
			$ret = str_replace($cyr, $lat, $text);
		} else {
			$ret = str_replace($lat, $cyr, $text);
		}
		
		return $ret;
	}
	
	public static function translitForURL($text)
	{
		$ret = static::translit($text, 'lat');
		$ret = strtolower(trim(preg_replace('/\s+/', ' ', $ret)));
		$ret = preg_replace ("/[^a-z0-9\s]/", '-', $ret);
		$pos = strpos($ret, '--');
		while ($pos !== false) {
			$ret = str_replace('--', '-', $ret);
			$pos = strpos($ret, '--');
		}
		if (substr($ret, -1) == '-') {
			$ret = rtrim($ret, '-');
		}
		return $ret;
	}
	
	
	/* Array tools */
	
	public static function array_remove_empty($haystack)
	{
		foreach ($haystack as $key => $value) {
			if (is_array($value)) {
				$haystack[$key] = self::array_remove_empty($haystack[$key]);
			}
			
			if (empty($haystack[$key]) || $haystack[$key] === 'null') {
				unset($haystack[$key]);
			}
		}
		
		return $haystack;
	}
	
	public static function array_is_empty($haystack)
	{
		return (!count(self::array_remove_empty($haystack)));
	}
	
	
	/* File upload */
	
	public static function file_upload_max_size() {
		static $max_size = -1;
		
		if ($max_size < 0) {
			// Start with post_max_size.
			$post_max_size = static::parse_size(ini_get('post_max_size'));
			if ($post_max_size > 0) {
				$max_size = $post_max_size;
			}
			
			// If upload_max_size is less, then reduce. Except if upload_max_size is
			// zero, which indicates no limit.
			$upload_max = static::parse_size(ini_get('upload_max_filesize'));
			if ($upload_max > 0 && $upload_max < $max_size) {
				$max_size = $upload_max;
			}
		}
		return $max_size;
	}
	
	private static function parse_size($size) {
		$unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
		$size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
		if ($unit) {
			// Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
			return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
		}
		else {
			return round($size);
		}
	}
}