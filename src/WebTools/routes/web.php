<?php

Route::get('/decidenow/fastdev', '\DecideNow\FastDev\WebTools\ToolsController@index');

Route::get('/decidenow/create/skeleton',	'\DecideNow\FastDev\WebTools\ToolsController@createSkeleton');
Route::get('/decidenow/create/site',		'\DecideNow\FastDev\WebTools\ToolsController@createSite');
Route::get('/decidenow/create/app',			'\DecideNow\FastDev\WebTools\ToolsController@createApp');
Route::get('/decidenow/create/auth',		'\DecideNow\FastDev\WebTools\ToolsController@createAuth');
Route::get('/decidenow/view/controls',		'\DecideNow\FastDev\WebTools\ToolsController@viewControls');
Route::post('/decidenow/view/templates',	'\DecideNow\FastDev\WebTools\ToolsController@viewTemplates');

Route::post('/decidenow/create/crud',			'\DecideNow\FastDev\WebTools\ToolsController@createCRUD');
Route::get('/decidenow/create/file-storage',	'\DecideNow\FastDev\WebTools\ToolsController@createFileStorage');
Route::post('/decidenow/create/select-field',	'\DecideNow\FastDev\WebTools\ToolsController@createSelectField');
Route::get('/decidenow/create/blog',			'\DecideNow\FastDev\WebTools\ToolsController@createBlog');
Route::get('/decidenow/create/feedback',		'\DecideNow\FastDev\WebTools\ToolsController@createFeedback');
Route::get('/decidenow/create/carousel',		'\DecideNow\FastDev\WebTools\ToolsController@createCarousel');