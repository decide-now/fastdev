<?php

namespace DecideNow\FastDev\WebTools\Helpers;

use Illuminate\Support\Facades\Schema;

class SchemaHelper
{
	public static function getColumns($table_name, $include_timestamps = false)
	{
		$ret = Schema::getColumnListing($table_name);
		if (!$include_timestamps) {
			if ( ($key = array_search('created_at', $ret)) !== false ) {
				unset($ret[$key]);
			}
			if ( ($key = array_search('updated_at', $ret)) !== false ) {
				unset($ret[$key]);
			}
		}
		return $ret;
	}
	
	public static function tableExists($table_name)
	{
		return Schema::hasTable($table_name);
	}
	
	public static function columnExists($table_name, $column_name)
	{
		return Schema::hasColumn($table_name, $column_name);
	}
}
