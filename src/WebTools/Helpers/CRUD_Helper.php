<?php

namespace DecideNow\FastDev\WebTools\Helpers;

use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class CRUD_Helper
{
	public static function getFillableList($columns) {
		$ret = '';
		foreach ($columns as $fld) {
			if ($fld == 'id') continue;
			$ret .= "'".$fld."', ";
		}
		return $ret;
	}
	
	public static function getAliasList($columns, $table_name) {
		$ret = '';
		$ret .= "\t\t\t'table_name' => '".$table_name."', \r\n";
		$ret .= "\t\t\t'record_name' => '".$table_name."_record', \r\n";
		foreach ($columns as $fld) {
			if ($fld == 'name' || $fld == 'id') continue;
			$ret .= "\t\t\t'".$fld."' => '".$fld."', \r\n";
		}
		return $ret;
	}
	
	public static function getFilterList($columns) {
		$ret = '';
		$ret .= '\'universal\' => \'\', ';
		foreach ($columns as $fld) {
			if ($fld == 'name' || $fld == 'id') continue;
			$ret .= "'".$fld."' => '', ";
		}
		return $ret;
	}
	
	public static function getFilterActionsOld($columns) {
		$ret = '';
		foreach ($columns as $fld) {
			if ($fld == 'name' || $fld == 'id') continue;
			$ret .= 
				"\t\t".'$tmp = Arr::get($filter_data, \''.$fld.'\', \'\');'."\r\n".
				"\t\t".'if ($tmp != \'\') {'."\r\n".
				"\t\t\t".'$query = $query->where(\''.$fld.'\', \'=\', \'\'.$tmp.\'\');'."\r\n".
				"\t\t".'}'."\r\n";
		}
		return $ret;
	}
	
	public static function getFilterActions($columns) {
		$ret = '';
		$ret .= 
				"\t\t".'$tmp = Arr::get($filter_data, \'universal\', \'\');'."\r\n".
				"\t\t".'if ($tmp != \'\') {'."\r\n".
				"\t\t\t".'$query = $query';
		$cnt = count($columns);
		$idx = 0; 
		foreach ($columns as $fld) {
			$idx++;
			if ($fld == 'id' || $fld == 'priority' || $fld == 'is_published') continue;
			$ret .= 
			"\r\n\t\t\t".'->orWhere(\''.$fld.'\', \'LIKE\', \'%\'.$tmp.\'%\')'.(($idx == $cnt) ? ';' : '');
		}
		$ret .= 
				";\r\n\t\t".'}'."\r\n";
		return $ret;
	}
	
	public static function getOrderingList($columns) {
		$ret = '';
		foreach ($columns as $fld) {
			if ($fld == 'name' || $fld == 'id') continue;
			$ret .= "'".$fld."' => '', ";
		}
		return $ret;
	}
	
	public static function getOrderingActions($columns) {

		$ret = '';
		foreach ($columns as $fld) {
			if ($fld == 'name' || $fld == 'id') continue;
			$ret .= 
				"\t\t".'$tmp = Arr::get($ordering_data, \''.$fld.'\', \'\');'."\r\n".
				"\t\t".'if ($tmp != \'\') {'."\r\n".
				"\t\t\t".'$query = $query->orderBy(\''.$fld.'\', $tmp);'."\r\n".
				"\t\t".'}'."\r\n";
		}
		return $ret;
	}

	
	public static function getRulesList($columns) {
		$ret = '';
		foreach ($columns as $fld) {
			if ($fld == 'id') continue;
			$ret .= "\t\t\t".'$itemClass.\'.'.$fld.'\' => \'required\','."\r\n";
		}
		return $ret;
	}
	
	public static function getGridHeadersOld($columns, $template_type, $parent_field_id = '') {
		$ret = '';
		$ret .= "\t\t".'<div class="col-sm-2 visible-xs">Сортировка: </div>'."\r\n";
		$after_id = false;
		foreach ($columns as $fld) {
			$pre_tab = '';
			if ($fld == $parent_field_id && $template_type == 'dependant') {
				$ret .= "\t\t".'@if (!$is_dependant)'."\r\n";
				$pre_tab = "\t";
			}
			if ($fld == 'id') {
				$ret .= "\t\t".'@if ($model->getPermission(\'list\', \'id\'))'."\r\n";
				$pre_tab = "\t";
			}
			
			$ret .= $pre_tab."\t\t".'<div class="col-sm-';
			if ($after_id) {
				$ret .= '{{ ($model->getPermission(\'list\', \'id\')) ? \'1\' : \'2\' }}';
			} else {
				$ret .= '1';
			}
			$ret .= '">{!! FastDevCtrl::sortableLink(\''.$fld.'\', $model, $ordering)->data(\'action-tech\', $scene->getActionTechnology(\'list_sortable_header\'))->out() !!}</div>'."\r\n";
			
			if ($fld == 'id') {
				$ret .= "\t\t".'@endif'."\r\n";
			}
			if ($fld == $parent_field_id && $template_type == 'dependant') {
				$ret .= "\t\t".'@endif'."\r\n";
			}
			if ($fld == 'id') {
				$after_id = true;
			} else {
				$after_id = false;
			}
		}
		return $ret;
	}
	
	public static function getGridHeaders($columns, $template_type, $parent_field_id = '') {
		$ret = '';
		$ret .= "\t\t".'<div class="col-sm-2 visible-xs">Сортировка: </div>'."\r\n";
		$after_id = false;
		foreach ($columns as $fld) {
			$pre_tab = '';
			if ($fld == $parent_field_id && $template_type == 'dependant') {
				$ret .= "\t\t".'@if (!$is_nested || !$'.$parent_field_id.')'."\r\n";
				$pre_tab = "\t";
			}
			if ($fld == 'id') {
				$ret .= "\t\t".'@if ($model->getPermission(\'list\', \'id\'))'."\r\n";
				$pre_tab = "\t";
			}
			
			$ret .= $pre_tab."\t\t".'<div class="col-sm-';
			if ($after_id) {
				$ret .= '{{ ($model->getPermission(\'list\', \'id\')) ? \'1\' : \'2\' }}';
			} else {
				$ret .= '1';
			}
			$ret .= '">{!! FastDevCtrl::sortableLink(\''.$fld.'\', $model, $ordering)->action(\'list_sortable_header\', $scene)->out() !!}</div>'."\r\n";
			
			if ($fld == 'id') {
				$ret .= "\t\t".'@endif'."\r\n";
			}
			if ($fld == $parent_field_id && $template_type == 'dependant') {
				$ret .= "\t\t".'@endif'."\r\n";
			}
			if ($fld == 'id') {
				$after_id = true;
			} else {
				$after_id = false;
			}
		}
		return $ret;
	}

	public static function getGridLines($columns, $template_type, $parent_field_id = '') {
		$ret = '';
		$after_id = false;
		foreach ($columns as $fld) {
			$pre_tab = '';
			if ($fld == $parent_field_id && $template_type == 'dependant') {
				$ret .= "\t\t".'@if (!$is_nested || !$'.$parent_field_id.')'."\r\n";
				$pre_tab = "\t";
			}
			if ($fld == 'id') {
				$ret .= "\t\t".'@if ($model->getPermission(\'list\', \'id\'))'."\r\n";
				$pre_tab = "\t";
			}
			$ret .= $pre_tab."\t\t".'<div class="col-sm-';
			if ($after_id) {
				$ret .= '{{ ($model->getPermission(\'list\', \'id\')) ? \'1\' : \'2\' }}';
			} else {
				$ret .= '1';
			}
			$ret .= '">';
			$ret .= (
				($fld == 'name') 
				? 
					"\r\n".
					"\t\t\t".'@if($item->getPermission(\'edit\')) <a id="link_edit_item" href="" data-action-tech="{{ $scene->getActionTechnology(\'list_link_edit_item\') }}"> @endif'."\r\n".
					"\t\t\t\t".'{{ $item->toString() }}'."\r\n".
					"\t\t\t".'@if($item->getPermission(\'edit\')) </a> @endif'."\r\n\t\t"
				: '{{ $item->'.$fld.' }}'
			);
			$ret .= '</div>'."\r\n";
			if ($fld == 'id') {
				$ret .= "\t\t".'@endif'."\r\n";
			}
			if ($fld == $parent_field_id && $template_type == 'dependant') {
				$ret .= "\t\t".'@endif'."\r\n";
			}
			if ($fld == 'id') {
				$after_id = true;
			} else {
				$after_id = false;
			}
		}
		return $ret;
	}
	
	public static function getTextFields($columns, $template_type = '', $parent_field_id = '') {
		$ret = '';
		foreach ($columns as $fld) {
			if ($fld == 'id') continue;
			$pre_tab = '';
			if ($fld == $parent_field_id && $template_type == 'dependant') {
				$ret .= "\t\t\t".'@if (!$is_nested || !$item->'.$parent_field_id.')'."\r\n";
				$pre_tab = "\t";
			}
			$ret .= 
				$pre_tab."\t\t\t".'<div class="col-sm-6">'."\r\n".
				$pre_tab."\t\t\t\t".'{!! FastDevCtrl::textField(\''.$fld.'\', $item)->out() !!}'."\r\n".
				$pre_tab."\t\t\t".'</div>'."\r\n";
			if ($fld == $parent_field_id && $template_type == 'dependant') {
				$ret .= 
				$pre_tab."\t\t".'@else'."\r\n".
				$pre_tab."\t\t\t".'{!! FastDevCtrl::textField(\''.$parent_field_id.'\', $item)->type(\'hidden\')->out() !!}'."\r\n".
				$pre_tab."\t\t".'@endif'."\r\n";
			}
		}
		return $ret;
	}
	
	
	public static function addTypeSpecificBlock(&$blocks, $block_id, $template_type, $parent_field_id = '') {
		$ret = '';
		
		if ($template_type == 'dependant') {
			
			$parentName = (substr($parent_field_id, -3) == '_id') ? substr($parent_field_id, 0, -3) : $parent_field_id;
			$parentName = Str::camel($parentName);
			$PascalParentName = ucfirst($parentName);
			
			if ($block_id == 'model_relation') {
				$ret .= "\t".'public function '.$parentName.'()'."\r\n";
				$ret .= "\t".'{'."\r\n";
				$ret .= "\t\t".'return $this->belongsTo('.$PascalParentName.'::class);'."\r\n";
				$ret .= "\t".'}'."\r\n";
			}
			if ($block_id == 'model_list_query') {
				$ret .= "\r\n\t".'public static function addQueryBy'.$PascalParentName.'Id($query, $'.$parent_field_id.')'."\r\n";
				$ret .= "\t".'{'."\r\n";
				$ret .= "\t\t".'$query = parent::addQueryFilterAndOrdering($query);'."\r\n";
				$ret .= "\t\t".'if (!empty($'.$parent_field_id.')) {'."\r\n";
				$ret .= "\t\t\t".'$query = $query->where(\''.$parent_field_id.'\', \'=\', $'.$parent_field_id.');'."\r\n";
				$ret .= "\t\t".'}'."\r\n";
				$ret .= "\t\t".'return $query;'."\r\n";
				$ret .= "\t".'}'."\r\n";
			}
			if ($block_id == 'item_create') {
				$ret .= "\t\t".'$ret += [\''.$parent_field_id.'\' => $request->get(\''.$parent_field_id.'\', null)];'."\r\n";
			}
			if ($block_id == 'field_declare') {
				$ret .= "\t".'public $'.$parent_field_id.';'."\r\n";
			}
			if ($block_id == 'field_to_state') {
				$ret .= ', \'' . $parent_field_id . '\'';
			}
			if ($block_id == 'add_list_query') {
				$ret .= "\t\t".'$list_data = $this->model->addQueryBy'.$PascalParentName.'Id($list_data, $this->'.$parent_field_id.');'."\r\n";
			}
			if ($block_id == 'scene_get') {
				$ret .= "\t\t".'$this->'.$parent_field_id.' = $this->transferData(\''.$parent_field_id.'\', \'\');'."\r\n";
			}
			if ($block_id == 'scene_post') {
				$ret .= "\t\t".'$this->transferData(\''.$parent_field_id.'\', \'\', $request);'."\r\n";
			}
			
			if ($block_id == 'list_parent_field') {
				$ret .= "\t\t".'{!! FastDevCtrl::textField(\''.$parent_field_id.'\')->type(\'hidden\')->value(!empty($'.$parent_field_id.') ? $'.$parent_field_id.' : \'\')->out() !!}';
			}
			if ($block_id == 'item_parent_field') {
				$ret .= "\t\t".'{!! FastDevCtrl::textField(\''.$parent_field_id.'\')->value($item->'.$parent_field_id.')->type(\'hidden\')->out() !!}'."\r\n";
				$ret .= "\t\t".'{!! FastDevCtrl::textField(\''.$parent_field_id.'\', $item)->type(\'hidden\')->out() !!}';
			}
			if ($block_id == 'parent_id_to_post') {
				//$ret .= "\t\t".'data.fields.'.$parent_field_id.' = thisScene.getElement(\'input[id="'.$parent_field_id.'"]\').val();'."\r\n";
				$ret .= "\t\t".'data.form.'.$parent_field_id.' = thisScene.getElement(\'input[id="'.$parent_field_id.'"]\').val();'."\r\n";
			}
			
			if ($block_id == 'item_parent_header') {
				$ret .= "\t\t\t\t".'@if ($item->'.$parent_field_id.' || $item->id)'."\r\n";
				$ret .= "\t\t\t\t\t".'<span class="small">('."\r\n";
				$ret .= "\t\t\t\t".'@endif'."\r\n";
				$ret .= "\t\t\t\t".'@if ($item->'.$parent_field_id.')'."\r\n";
				$ret .= "\t\t\t\t\t".'{{ $item->'.$parentName.'->toString() }}'."\r\n";
				$ret .= "\t\t\t\t".'@endif'."\r\n";
				$ret .= "\t\t\t\t".'@if ($item->'.$parent_field_id.' && !$item->id)'."\r\n";
				$ret .= "\t\t\t\t\t".')</span>'."\r\n";
				$ret .= "\t\t\t\t".'@elseif ($item->'.$parent_field_id.' && $item->id)'."\r\n";
				$ret .= "\t\t\t\t\t".'|'."\r\n";
				$ret .= "\t\t\t\t".'@endif'."\r\n";
				$ret .= "\t\t\t\t".'@if ($item->id)'."\r\n";
				$ret .= "\t\t\t\t\t".'id: {{ $item->id }}'."\r\n";
				$ret .= "\t\t\t\t".'@endif'."\r\n";
				$ret .= "\t\t\t\t".'@if ($item->id)'."\r\n";
				$ret .= "\t\t\t\t\t".')</span>'."\r\n";
				$ret .= "\t\t\t\t".'@endif'."\r\n";
			}
		}
		
		$blocks[$block_id] = $ret;
	}
}
