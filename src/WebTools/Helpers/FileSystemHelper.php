<?php

namespace DecideNow\FastDev\WebTools\Helpers;

class FileSystemHelper
{
	
	public static function copy_r($source, $dest, &$messages)
	{
		// Check for symlinks
		if (is_link($source)) {
			return symlink(readlink($source), $dest);
		}
		
		// Simple copy for a file
		if (is_file($source)) {
			$name = basename($source);
			
			if (substr($name, 0, 1) == '_') {
				// добавить в файл
				$lines = file($source);
				$dest_path = pathinfo($dest, PATHINFO_DIRNAME).'/'.substr($name, 1);
				foreach ($lines as $line) {
					file_put_contents($dest_path, $line, FILE_APPEND);
				}
				return true;
			} elseif (substr($name, 0, 1) == '!') {
				$messages[] = str_replace (base_path(), '', $dest);
				return copy($source, $dest);
			} else {
				return copy($source, $dest);
			}
		}
		
		// Make destination directory
		if (!is_dir($dest)) {
			mkdir($dest);
		}
		
		// Loop through the folder
		$dir = dir($source);
		while (false !== $entry = $dir->read()) {
			// Skip pointers
			if ($entry == '.' || $entry == '..') {
				continue;
			}
			
			// Deep copy directories
			self::copy_r($source.'/'.$entry, $dest.'/'.$entry, $messages);
		}
		
		// Clean up
		$dir->close();
		return true;
	}
	
	public static function rm_r($source)
	{
		if (is_file($source)) {
			unlink($source);
		}
		if (is_dir($source)) {
			$dir = dir($source);
			while (false !== $entry = $dir->read()) {
				if ($entry == '.' || $entry == '..') {
					continue;
				}
				self::rm_r($source.'/'.$entry);
			}
			$dir->close();
			rmdir($source);
		}
		return true;
	}

}
