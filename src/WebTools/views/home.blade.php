
<style>
	h1 {
		margin-bottom: 5px;
	}
	h1 + h4 {
		margin-top: 5px;
	}
	h2 {
		margin-top: 40px;
	}
	.after-radio {
		margin-left: 21px;
		font-style: italic;
	}
</style>

<h1>FastDev</h1>
<h4>Инструменты для быстрой разработки</h4>
<p><a href="{{ url('/') }}" target="blank">На сайт</a></p>
<hr />


@if ($messages)
	@foreach ($messages as $message)
		@if ($loop->index == 1) <ul> @endif
		@if ($loop->first)
			<h3> 
		@else
			<li>
		@endif
		{!! $message !!}
		@if ($loop->first)
			</h3> 
		@else
			</li>
		@endif
	@endforeach
	@if (count($messages) > 1)
		</ul>
	@endif
	<hr />
@endif


@if (!empty($code_template))
	@include('fastdev_web::'.$code_template, $code_data)
	<br>
	<hr />
@endif


<h2>Базовый шаблон для приложения с целевой страницей (Landing Page)</h2>
<a href="{{ url('/decidenow/create/skeleton') }}">Создать</a>


<h2>Шаблон для CRUD</h2>
<form action="{{ url('/decidenow/create/crud') }}" method="post">
	{{ csrf_field() }}
	<label for="table_name">Имя таблицы: </label><input id="table_name" type="text" name="table_name" /><br>
	<p>
		<input id="template_type_classic" type="radio" name="template_type" value="classic" checked /><label for="template_type_classic">классический шаблон</label><br>
		<input id="template_type_singlepage" type="radio" name="template_type" value="singlepage" /><label for="template_type_singlepage">одностраничный (с всплывающими окнами)</label><br>
		<input id="template_type_dependant" type="radio" name="template_type" value="dependant" /><label for="template_type_dependant">подчиненный</label><br>
		<label for="parent_id" class="after-radio">поле связи с владельцем: </label><input id="parent_id" type="text" name="parent_id" />
	</p>
	<label for="just_crete"><input id="just_crete" type="checkbox" name="just_create" value="true" checked />Только создать шаблон, не перемещать в приложение</label>
	<p><input type="submit" value="Создать" /></p>
</form>

<h2>Изменение шаблонов</h2>
<form action="{{ url('/decidenow/view/templates') }}" method="post" target="blank">
	{{ csrf_field() }}
	<label for="parent_id">поле связи с владельцем: </label><input id="parent_id" type="text" name="parent_id" /> (для подчинённого шабллона)
	<p><input type="submit" value="Примеры" /></p>
</form>

<h2>Поле выбора</h2>
<form action="{{ url('/decidenow/create/select-field') }}" method="post" target="_blank">
	{{ csrf_field() }}
	<table>
		<tr>
			<td><label for="table1_name">Имя таблицы, в которую подставляется значение: </label></td>
			<td><input id="table1_name" type="text" name="table1_name" /></td>
		</tr>
		<tr>
			<td><label for="table2_name">Имя таблицы, из которой выбираются значения: </label></td>
			<td><input id="table2_name" type="text" name="table2_name" /></td>
		</tr>
	</table>
	<p><input type="submit" value="Создать" /></p>
</form>


<h2>Элементы управления</h2>
<a href="{{ url('/decidenow/view/controls') }}" target="blank">Примеры</a>


<h2>Файловое хранилице</h2>
<a href="{{ url('/decidenow/create/file-storage') }}">Создать</a>

<h2>Блог</h2>
<a href="{{ url('/decidenow/create/blog') }}">Создать</a>

<h2>Форма обратной связи</h2>
<a href="{{ url('/decidenow/create/feedback') }}">Создать</a>

<h2>Карусель</h2>
<a href="{{ url('/decidenow/create/carousel') }}">Создать</a>
