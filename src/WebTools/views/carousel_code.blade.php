<h2>Карусель</h2>
<h4>SiteHomeController.php</h4>
<pre>
	...
	public $carousel;
	...
	public function prepareContent(Request $request)
	{
		...
		$this->carousel = Carousel::listQueryForSite()->get();
		...
	}
	...
</pre>
<h4>site-home_content.blade.php</h4>
<pre>
	...
	{{ '@' }}include('carousel.template.carousel-template_content')
	...
</pre>
<br>