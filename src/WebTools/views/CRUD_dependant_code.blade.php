<i>
// v1 - когда родительская форма ({{ $parent_name }}) загружается НЕ по AJAX<br>
// v2 - когда родительская форма ({{ $parent_name }}) загружается по AJAX
</i>

<h4>{{ $parent_name_pascal }}ItemController.php</h4>
<pre>
	...
	/* v1 & v2 */
	protected function childrenDefine()
	{
		...
		$this->childAdd(new {{ $table_name_camel }}ListController($this, '{{ $table_name }}-list-scene'));
		...
	}
	...
	
	/* v1 */
	public function prepareContent($request, $item_id, $has_content = true)
	{
		...
		if ($this->item_id) {
			${{ $table_name }}_list_scene = $this->childGet('{{ $table_name }}-list-scene');
			${{ $table_name }}_list_scene->{{ $parent_id }} = $this->item->id;
			${{ $table_name }}_list_scene->prepareContent($request);
		}
		...
	}
	...
</pre>		

<h4>{{ $parent_name }}-item_code.blade.php</h4>
<pre>
	...
	/* v2 */
	init : function() {
		...
		if ($.trim(thisScene.getRoot().html()) !== '') {
			var {{ $table_name }}_list_scene = DecideNowObjects.stage.getScene('&#123;&#123; $scene->childGet("{{ $table_name }}-list-scene")->sceneId() &#125;&#125;');
			if ({{ $table_name }}_list_scene) {
				var item_id = thisScene.getElement('input[id="item_id"]').val();
				{{ $table_name }}_list_scene.sceneRefresh({ form: { {{ $parent_id }}: item_id, is_nested: 1, }, technology: 'ajax' });
			}
		}
		...
	},
</pre>
<h4>{{ $parent_name }}-item_content.blade.php</h4>
<pre>
...
/* v1 & v2 */
{{ (true) ? '@' : '' }}if ($item->id)
	&lt;div class="row"&gt;&lt;div class="col-sm-12"&gt;&lt;hr&gt;&lt;/div&gt;&lt;/div&gt;
	{{ (true) ? '@' : '' }}include('scene::content', $scene->childGet('{{ $table_name }}-list-scene')->sceneVariables())
{{ (true) ? '@' : '' }}endif
</pre>