<h2>Форма</h2>
<h4>SiteContactController.php</h4>
<pre>
	...
	protected function defineChildren()
	{
		...
		$feedback_scene = new FeedbackItemPublicController($this);
		$this->scene_children->put('feedback_scene', $feedback_scene);
		...
	}
	...
	public function prepareContent(Request $request)
	{
		...
		$feedback_scene = $this->scene_children->get('feedback_scene');
		$feedback_scene->prepareContent($request);
		...
	}
	...
</pre>		

<h4>site-contact_content.blade.php</h4>
<pre>
	...
	&lt;fieldset>
		&lt;legend>Обратная связь&lt;/legend>
		{{ '@' }}include('fastdev::scene_content', $scene_children->get('feedback_scene')->sceneArray())
	&lt;/fieldset>
</pre>

<h2>Кнопка</h2>
<h4>SiteBaseController.php</h4>
<pre>
	public function prepareFeedbackButtonScene($request)
	{
		$feedback_button_scene = new FeedbackButtonPublicController($this);
		$this->scene_children->put('feedback_button_scene', $feedback_button_scene);
		$feedback_button_scene->prepareContent($request);
	}
</pre>
<br>	
<h4>site_navbar.blade.php</h4>
<pre>
	{{ '@' }}if (isset($scene_children)) 
		{{ '@' }}if ($scene_children->has('feedback_button_scene'))
			{{ '@' }}include('fastdev::scene_content', $scene_children->get('feedback_button_scene')->sceneArray())
		{{ '@' }}endif
	{{ '@' }}endif
</pre>
<br>
<h4>SiteHomeController.php</h4>
<pre>
	...
	public function prepareContent(Request $request)
	{
		$this->prepareFeedbackButtonScene($request);
		...
	}
	...
</pre>
<br>