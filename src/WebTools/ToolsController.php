<?php

namespace DecideNow\FastDev\WebTools;

use DecideNow\FastDev\WebTools\Helpers\CRUD_Helper;
use DecideNow\FastDev\WebTools\Helpers\FileSystemHelper;
use DecideNow\FastDev\WebTools\Helpers\SchemaHelper;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;

class ToolsController extends Controller
{
	public function __construct()
	{
		$this->middleware('web');
	}
	
	public function index()
	{
		$messages = Session::get('messages', null);
		$code_template = Session::get('code_template', '');
		$code_data = Session::get('code_data', []);
		return view('fastdev_web::home')
			->with('messages', $messages)
			->with('code_template', $code_template)
			->with('code_data', $code_data);
	}
	
	
	private function createSkeletonCommands(&$messages)
	{
		$source = __DIR__.'/../Skeleton/source/';
		$dest = base_path();
		
		$dir = dir($source);
		while (false !== $entry = $dir->read()) {
			if ($entry == '.' || $entry == '..') {
				continue;
			}
			FileSystemHelper::copy_r($source.'/'.$entry, $dest.'/'.$entry, $messages);
		}
	}
	
	public function createSkeleton()
	{
		$messages = [];
		$messages[] = 'СОЗДАН Базовый шаблон для приложения с целевой страницей (Landing Page)';
		
		$this->createSkeletonCommands($messages);
		return redirect()
			->action('\DecideNow\FastDev\WebTools\ToolsController@index')
			->with('messages', $messages);
	}
	
	
	private function createCRUDCommands(&$messages, $table_name, $parent_id, $template_type, $just_create, &$code_template, &$code_data)
	{
		// шаблоны для подставновки имён полей
		$table_fields		= SchemaHelper::getColumns($table_name);
		$_FillableList_		= CRUD_Helper::getFillableList($table_fields);
		$_AliasList_		= CRUD_Helper::getAliasList($table_fields, $table_name);
		$_FilterList_		= CRUD_Helper::getFilterList($table_fields);
		$_FilterActions_	= CRUD_Helper::getFilterActions($table_fields);
		$_OrderingList_		= CRUD_Helper::getOrderingList($table_fields);
		$_OrderingActions_	= CRUD_Helper::getOrderingActions($table_fields);
		$_RulesList_		= CRUD_Helper::getRulesList($table_fields);
		$_GridHeaders_		= CRUD_Helper::getGridHeaders($table_fields, $template_type, $parent_id);
		$_GridLines_		= CRUD_Helper::getGridLines($table_fields, $template_type, $parent_id);
		$_TextFields_		= CRUD_Helper::getTextFields($table_fields, $template_type, $parent_id);
		
		$_Type_specific_	= [];
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'model_relation', $template_type, $parent_id);
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'model_list_query', $template_type, $parent_id);
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'item_create', $template_type, $parent_id);
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'field_declare', $template_type, $parent_id);
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'field_to_state', $template_type, $parent_id);
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'add_list_query', $template_type, $parent_id);
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'scene_get', $template_type, $parent_id);
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'scene_post', $template_type, $parent_id);
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'list_parent_field', $template_type, $parent_id);
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'item_parent_header', $template_type, $parent_id);
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'item_parent_field', $template_type, $parent_id);
		CRUD_Helper::addTypeSpecificBlock($_Type_specific_, 'parent_id_to_post', $template_type, $parent_id);
		
		
		// задаем имена в разных форматах
		//$old_name = 'tmplt';
		$old_name_camel = 'Tmplt';
		$old_name_snake = 'tmplt';
		$old_name_kebab = 'tmp-lt';
		
		//$old_parent_id = 'parent_field_id';
		$old_parent_id_snake = 'parent_field_id';
		$old_parent_id_camel = 'ParentFieldId';
		
		$new_name = $table_name;
		$new_name_camel = ucfirst(Str::camel($new_name));
		$new_name_snake = Str::snake($new_name_camel);
		$new_name_kebab = Str::kebab($new_name_camel);
		
		$new_parent_id = $parent_id;
		$new_parent_id_camel = ucfirst(Str::camel($new_parent_id));
		$new_parent_id_snake = Str::snake($new_parent_id_camel);
		
		
		// определяем каталоги
		$code_template = '';
		$code_data = '';
		if (($template_type == 'dependant') && $parent_id != '') {
			$parent_name = str_replace('_id', '', $parent_id);
			$parent_name_pascal = ucfirst(Str::camel($parent_name));
			$code_template = 'CRUD_dependant_code';
			$code_data = [
				'table_name' => $new_name, 
				'table_name_camel' => $new_name_camel, 
				'parent_id' => $parent_id,
				'parent_name' => $parent_name,
				'parent_name_pascal' => $parent_name_pascal,
			];
		}
		
		$source = __DIR__.'/../Scaffolding/CRUD2/';
		$tmp_dest = __DIR__.'/../Scaffolding/TmpCRUD2/';
			
		$dest = base_path();
		
		// удаляем временный каталог (на всякий случай)
		FileSystemHelper::rm_r($tmp_dest);
		
		// копируем во временный каталог
		$tmp_message = [];
		FileSystemHelper::copy_r($source, $tmp_dest, $tmp_message);
		
		// переименовываем файлы
		$di = new \RecursiveIteratorIterator(
			new \RecursiveDirectoryIterator($tmp_dest, \FilesystemIterator::SKIP_DOTS),
			\RecursiveIteratorIterator::CHILD_FIRST
		);
		foreach($di as $name => $fio) {
			$newname = $fio->getFilename();
			$newname = str_replace($old_name_snake, $new_name_snake, $newname);
			$newname = str_replace($old_name_camel, $new_name_camel, $newname);
			$newname = $fio->getPath() . DIRECTORY_SEPARATOR . $newname;
			rename($name, $newname);
		}
		
		// замена имен
		$di = new \RecursiveIteratorIterator(
			new \RecursiveDirectoryIterator($tmp_dest, \FilesystemIterator::SKIP_DOTS),
			\RecursiveIteratorIterator::LEAVES_ONLY 
		);
		foreach($di as $name => $fio) {
			if ($fio->isDir()) continue;
			$filepath = $fio->getRealPath();
			$lines = file($filepath);
				file_put_contents($filepath, '');
			foreach($lines as $line) {
				$newline = $line;
				
				$newline = str_replace('/*_FillableList_*/', $_FillableList_, $newline);
				$newline = str_replace('/*_AliasList_*/', $_AliasList_, $newline);
				$newline = str_replace('/*_FilterList_*/', $_FilterList_, $newline);
				$newline = str_replace('/*_FilterActions_*/', $_FilterActions_, $newline);
				$newline = str_replace('/*_OrderingList_*/', $_OrderingList_, $newline);
				$newline = str_replace('/*_OrderingActions_*/', $_OrderingActions_, $newline);
				$newline = str_replace('/*_RulesList_*/', $_RulesList_, $newline);
				$newline = str_replace('/*_GridHeaders_*/', $_GridHeaders_, $newline);
				$newline = str_replace('/*_GridLines_*/', $_GridLines_, $newline);
				$newline = str_replace('/*_TextFields_*/', $_TextFields_, $newline);
				
				foreach($_Type_specific_ as $block_id => $block_value) {
					$newline = str_replace('/*_Type_specific_'.$block_id.'_*/', $block_value, $newline);
				}
				
				$newline = str_replace($old_name_snake, $new_name_snake, $newline);
				$newline = str_replace($old_name_camel, $new_name_camel, $newline);
				$newline = str_replace($old_name_kebab, $new_name_kebab, $newline);
				
				$newline = str_replace($old_parent_id_snake, $new_parent_id_snake, $newline);
				$newline = str_replace($old_parent_id_camel, $new_parent_id_camel, $newline);
				
				file_put_contents($filepath, $newline, FILE_APPEND);
			}
		}
		
		if (!$just_create) {
			// копируем в приложение
			$source = $tmp_dest;
			$dir = dir($source);
			while (false !== $entry = $dir->read()) {
				if ($entry == '.' || $entry == '..') {
					continue;
				}
				FileSystemHelper::copy_r($source.'/'.$entry, $dest.'/'.$entry, $messages);
			}
			
			// удаляем временный каталог
			FileSystemHelper::rm_r($tmp_dest);
		}
	}
	
	public function createCRUD(Request $request)
	{
		$table_name = $request->get('table_name', '');
		$parent_id = $request->get('parent_id', '');
		$template_type = $request->get('template_type', 'classic');
		$just_create = $request->has('just_create');
		
		$messages = [];
		
		if (!$table_name) {
			return redirect()
			->action('\DecideNow\FastDev\WebTools\ToolsController@index')
			->with('messages', ['Не задано имя таблицы!']);
		}
		if (!SchemaHelper::tableExists($table_name)) {
			return redirect()
			->action('\DecideNow\FastDev\WebTools\ToolsController@index')
			->with('messages', ['Таблица не найдена!']);
		}
		if ($parent_id) {
			if (!SchemaHelper::columnExists($table_name, $parent_id)) {
				return redirect()
				->action('\DecideNow\FastDev\WebTools\ToolsController@index')
				->with('messages', ['Поле связи с владельцем не найдено!']);
			}
			$template_type = 'dependant';
		}
		
		$messages[] = 'Шаблон для CRUD создан';
		
		$code_template = '';
		$code_data = [];
		$this->createCRUDCommands($messages, $table_name, $parent_id, $template_type, $just_create, $code_template, $code_data);
		return redirect()
			->action('\DecideNow\FastDev\WebTools\ToolsController@index')
			->with('messages', $messages)
			->with('code_template', $code_template)
			->with('code_data', $code_data);
	}
	
	
	private function createFileStorageCommands(&$messages)
	{
		$source = __DIR__.'/../Scaffolding/FileStorage/';
		$dest = base_path();
		
		$dir = dir($source);
		while (false !== $entry = $dir->read()) {
			if ($entry == '.' || $entry == '..') {
				continue;
			}
			FileSystemHelper::copy_r($source.'/'.$entry, $dest.'/'.$entry, $messages);
		}
	}
	
	public function createFileStorage()
	{
		$messages = [];
		$messages[] = 'Файловое хранилище создано';
		$messages[] = 'compposer require folklore/image';
		$messages[] = 'composer dump-autoload';
		$messages[] = 'php artisan db:seed --class=FileStorageTableSeeder';
		$messages[] = '<a href="'.url('create-public-storage').'">Создать ссылку на хранилище</a>';
		$this->createFileStorageCommands($messages);
		return redirect()
			->action('\DecideNow\FastDev\WebTools\ToolsController@index')
			->with('messages', $messages);
	}
	
	
	private function createBlogCommands(&$messages)
	{
		$source = __DIR__.'/../Scaffolding/Blog/';
		$dest = base_path();
		
		$dir = dir($source);
		while (false !== $entry = $dir->read()) {
			if ($entry == '.' || $entry == '..') {
				continue;
			}
			FileSystemHelper::copy_r($source.'/'.$entry, $dest.'/'.$entry, $messages);
		}
	}
	
	public function createBlog()
	{
		$messages = [];
		$messages[] = 'Блог создан';
		$this->createBlogCommands($messages);
		return redirect()
		->action('\DecideNow\FastDev\WebTools\ToolsController@index')
		->with('messages', $messages);
	}
	
	
	private function createFeedbackCommands(&$messages)
	{
		$source = __DIR__.'/../Scaffolding/Feedback/';
		$dest = base_path();
		
		$dir = dir($source);
		while (false !== $entry = $dir->read()) {
			if ($entry == '.' || $entry == '..') {
				continue;
			}
			FileSystemHelper::copy_r($source.'/'.$entry, $dest.'/'.$entry, $messages);
		}
	}
	
	public function createFeedback()
	{
		$code_template = 'feedback_code';
		$messages = [];
		$messages[] = 'Форма обратной связи создана';
		$this->createFeedbackCommands($messages);
		return redirect()
			->action('\DecideNow\FastDev\WebTools\ToolsController@index')
			->with('messages', $messages)
			->with('code_template', $code_template)
			->with('code_data', []);
	}
	
	
	private function createCarouselCommands(&$messages)
	{
		$source = __DIR__.'/../Scaffolding/Carousel/';
		$dest = base_path();
		
		$dir = dir($source);
		while (false !== $entry = $dir->read()) {
			if ($entry == '.' || $entry == '..') {
				continue;
			}
			FileSystemHelper::copy_r($source.'/'.$entry, $dest.'/'.$entry, $messages);
		}
	}
	
	public function createCarousel()
	{
		$code_template = 'carousel_code';
		$messages = [];
		$messages[] = 'Карусель создана';
		$this->createCarouselCommands($messages);
		return redirect()
			->action('\DecideNow\FastDev\WebTools\ToolsController@index')
			->with('messages', $messages)
			->with('code_template', $code_template)
			->with('code_data', []);
	}
	
	
	private function createSelectFieldCommands(&$messages, $table1_name, $table2_name)
	{
		// задаем имена в разных форматах
		//$old1_name = 'table1';
		$old1_name_camel = 'tabLe1';
		$old1_name_pascal = 'Table1';
		$old1_name_snake = 'tab_le1';
		$old1_name_kebab = 'tab-le1';
		
		//$old2_name = 'table2';
		$old2_name_camel = 'tabLe2';
		$old2_name_pascal = 'Table2';
		$old2_name_snake = 'tab_le2';
		$old2_name_kebab = 'tab-le2';
		
		$new1_name = $table1_name;
		$new1_name_camel = Str::camel($new1_name);
		$new1_name_pascal = ucfirst($new1_name_camel);
		$new1_name_snake = Str::snake($new1_name_camel);
		$new1_name_kebab = Str::kebab($new1_name_camel);
		
		$new2_name = $table2_name;
		$new2_name_camel = Str::camel($new2_name);
		$new2_name_pascal = ucfirst($new2_name_camel);
		$new2_name_snake = Str::snake($new2_name_camel);
		$new2_name_kebab = Str::kebab($new2_name_camel);
		
		// определяем шаблон
		$source = __DIR__.'/SelectField/tmplt_select_field.html';
		$destination = __DIR__.'/SelectField/tmplt_select_field2.html';
		
		
		// удаляем временный файл (на всякий случай)
		FileSystemHelper::rm_r($destination);
		
		// копируем во временный каталог
		$tmp_message = [];
		FileSystemHelper::copy_r($source, $destination, $tmp_message);
		
		// замена имен
		$lines = file($destination);
		file_put_contents($destination, '');
		foreach($lines as $line) {
			$newline = $line;
			$newline = str_replace($old1_name_snake, $new1_name_snake, $newline);
			$newline = str_replace($old1_name_pascal, $new1_name_pascal, $newline);
			$newline = str_replace($old1_name_camel, $new1_name_camel, $newline);
			$newline = str_replace($old1_name_kebab, $new1_name_kebab, $newline);
			$newline = str_replace($old2_name_snake, $new2_name_snake, $newline);
			$newline = str_replace($old2_name_pascal, $new2_name_pascal, $newline);
			$newline = str_replace($old2_name_camel, $new2_name_camel, $newline);
			$newline = str_replace($old2_name_kebab, $new2_name_kebab, $newline);
			file_put_contents($destination, $newline, FILE_APPEND);
		}
		
		$ret = file_get_contents($destination);
		
		// удаляем временный файл
		FileSystemHelper::rm_r($destination);
		
		return $ret;
	}
	
	public function createSelectField(Request $request)
	{
		$table1_name = $request->get('table1_name');
		$table2_name = $request->get('table2_name');
		
		$messages = [];
		$ret = $this->createSelectFieldCommands($messages, $table1_name, $table2_name);
		$messages[] = 'Шаблон для поля выбора создан';
		return $ret;
	}
	
	
	public function viewControls()
	{
		return view('fastdev_web::controls_exmpl');
	}
	
	
	private function viewTemplatesCommands(&$messages, $parent_id)
	{
		// задаем имена в разных форматах
		$old_name = 'table1';
		$old_name_pascal = 'Table1';
		
		$parent_name = str_replace('_id', '', ($parent_id) ?: 'parent');
		$new_name = $parent_name;
		$new_name_pascal = ucfirst($parent_name);
		
		// определяем шаблон
		$source = __DIR__.'/views/templates_exmpl.html';
		$destination = __DIR__.'/views/templates_exmpl2.html';
		
		
		// удаляем временный файл (на всякий случай)
		FileSystemHelper::rm_r($destination);
		
		// копируем во временный каталог
		$tmp_message = [];
		FileSystemHelper::copy_r($source, $destination, $tmp_message);
		
		// замена имен
		$lines = file($destination);
		file_put_contents($destination, '');
		foreach($lines as $line) {
			$newline = $line;
			$newline = str_replace($old_name, $new_name, $newline);
			$newline = str_replace($old_name_pascal, $new_name_pascal, $newline);
			file_put_contents($destination, $newline, FILE_APPEND);
		}
		
		$ret = file_get_contents($destination);
		
		// удаляем временный файл
		FileSystemHelper::rm_r($destination);
		
		return $ret;
	}
	
	public function viewTemplates(Request $request)
	{
		$parent_id = $request->get('parent_id');
		$messages = [];
		$ret = $this->viewTemplatesCommands($messages, $parent_id);
		return $ret;
	}
	
}
