<?php

namespace DecideNow\FastDev;

use Illuminate\Support\ServiceProvider;

class FastDevServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->loadRoutesFrom(__DIR__.'/WebTools/routes/web.php');
		$this->loadViewsFrom(__DIR__.'/WebTools/views', 'fastdev_web');
		
		$this->loadViewsFrom(__DIR__.'/Skeleton/views', 'fastdev');
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{	
		$this->app->singleton('fastdev.ctrl', function() {
			return new \DecideNow\FastDev\Controls\Ctrl;
		});
		$this->app->singleton('fastdev.hlpr', function() {
			return new \DecideNow\FastDev\Helper\Helper;
		});
	}
	
	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [
			'fastdev.ctrl',
			'fastdev.hlpr',
		];
	}
}
