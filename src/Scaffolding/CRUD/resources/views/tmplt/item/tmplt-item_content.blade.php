@include('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-8">
			<h3>
				{{ $item::getAlias('record_name') }}
				@if ($item->id)
					<span class="small">(
				@endif
/*_Type_specific_item_parent_header_*/
				@if ($item->id)
					id: {{ $item->id }}
				@endif
				
				@if ($item->id)
					)</span>
				@endif
			</h3>
		</div>
		<div class="col-sm-4 text-right">
		{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
			{!! FastDevCtrl::button('btn-ok-item')->title('Сохранить и выйти')->iconBefore('glyphicon glyphicon-ok')->data('action-tech', $scene->getActionTechnology('item_btn_ok_item'))->out() !!}
			{!! FastDevCtrl::button('btn-save-item')->title('Сохранить')->iconBefore('glyphicon glyphicon-floppy-save')->data('action-tech', $scene->getActionTechnology('item_btn_save_item'))->out() !!}
			{!! FastDevCtrl::button('btn-cancel-item')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->data('action-tech', $scene->getActionTechnology('item_btn_cancel_item'))->out() !!}
			{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->data('action-tech', $scene->getActionTechnology('item_btn_refresh'))->out() !!}
		{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
		</div>
	</div>
			
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		@include('fastdev::scene_primitives')
		{!! FastDevCtrl::textField('item_id')->type('hidden')->value($item->id)->out() !!}
/*_Type_specific_item_parent_field_*/
		<div class="row">
/*_TextFields_*/
		</div>
	</form>
		
</div></div>