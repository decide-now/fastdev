<script>

new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;
		
		thisScene.getElement('[id="btn_cancel_filter"]').click( { thisScene: this }, thisScene.btnCancelFilter_Click );
		thisScene.getElement('[id="btn_ok_filter"]').click( { thisScene: this }, thisScene.btnOkFilter_Click );
		thisScene.getElement('[id="btn_clear_filter"]').click( { thisScene: this }, thisScene.btnClearFilter_Click );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		var thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', '');
	},
	
	
	/**/
	
	btnCancelFilter_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.filterReturn({});
	},

	btnOkFilter_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var fields_val = thisScene.serializeData({});
		
		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.filterReturn({ fields: fields_val });
	},

	btnClearFilter_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var fields_val = thisScene.serializeData({}, true);

		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.filterReturn({ fields: fields_val });
	},
	
}).init();

</script>
