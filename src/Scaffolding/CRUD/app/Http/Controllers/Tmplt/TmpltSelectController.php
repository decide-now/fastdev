<?php
namespace App\Http\Controllers\Tmplt;

use Illuminate\Http\Request;

class TmpltSelectController extends TmpltController
{	
	static $scene_id = 'tmplt-select';
	
	public $list;
	public $filter;
	public $ordering;
/*_Type_specific_field_declare_*/
	public $primitives = [
		'task' => ['default' => '', 'is_hidden' => true],
		'call_button_id' => '',
		'page' => '1',
	];
	
	protected function defineChildren()
	{	
		$this->defineAlertChildScene();
		$this->scene_children->put('filter_scene', new TmpltFilterController($this));
	}
	
	public function prepareContent(Request $request)
	{	
		$list_data = new $this->model;
		$list_data = $this->model->defaultListQuery($list_data);
/*_Type_specific_add_list_query_*/
		$list_data = $this->model->applySessionFilter($list_data, $this->filter);
		$list_data = $this->model->applySessionOrdering($list_data, $this->ordering);
		
		$this->page = ($request->get('page')) ?: $this->getPrimitives('page');
		$this->list = $this->paginateExt($list_data);
		
		$this->no_content = false;
	}

	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->transferPrimitives();
/*_Type_specific_scene_get_*/
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferPrimitives($request);
/*_Type_specific_scene_post_*/
		$task = $this->getPrimitives('task');
		
		if ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
		} elseif ($task == 'ordering') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->orderingPutToSession($request, '', $force_clear);
		}
		return redirect()->action(self::methodPath('get'));
	}
}