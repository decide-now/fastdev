<?php
namespace App\Http\Controllers\Tmplt;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TmpltItemController extends TmpltController
{	
	static $scene_id = 'tmplt-item';

	public $primitives = [
		'task' => ['default' => '', 'is_hidden' => true],
		'is_dependant' => '0',
		'refresh_parent_scene' => '0',
	];
	
	public $item;
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
	}
	
	public function prepareContent(Request $request)
	{
		$task = $this->getPrimitives('task');
		$ajax = $this->transferAJAXFlag();
		if ($ajax && ($task == 'ok' || $task == 'cancel')) {
			return;
		}
		if ($this->content_error) {
			return;
		}
		if (!$this->item) {
			$this->item = $this->itemCreate($request);
		}
		if ($this->getPrimitives('is_dependant')) {
			$this->default_actions_tech = 'ajax';
		}
		
		$this->no_content = false;
	}
	
	public function get(Request $request, $item_id = '')
	{
		$this->transferSceneId();
		$this->transferPrimitives();
/*_Type_specific_scene_get_*/
		$this->item = $this->checkPermissionsItem($request, $item_id);
		$this->response_data['item_id'] = ($this->item) ? $this->item->id : '';
		
		$task = $this->getPrimitives('task');
		if ($task == 'ok' || $task == 'save') {
			$this->setPrimitives('refresh_parent_scene', '1');
		}
		
		$this->content_error = Session::has('flash_error_messages');
		
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request, $item_id = '')
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferPrimitives($request);
/*_Type_specific_scene_post_*/
		$task = $this->getPrimitives('task');
		
		$item = null;
		if ($task == 'create') {
			$item = $this->itemCreate($request);
		} elseif ($task == 'edit') {
			$item = $this->itemFind($request);
		} elseif ($task == 'cancel') {
			$item = $this->itemFind($request);
			if (! $request->ajax()) {
				return $this->redirectToList($item);
			}
		} elseif ($task == 'ok') {
			$redirect = null;
			$item = $this->itemValidateAndSave($request, $redirect);
			if ($redirect) {
				return $redirect;
			}
			if (! $request->ajax()) {
				return $this->redirectToList($item);
			}
		} elseif ($task == 'save') {
			$redirect = null;
			$item = $this->itemValidateAndSave($request, $redirect);
			if ($redirect) {
				return $redirect;
			}
		}
		$item = ($item) ?: $this->itemFind($request);
		return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
	}
	
}