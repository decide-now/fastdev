<style>
	/* Carousel */
	
	.carousel-content {
		position: relative;
	}
	.carousel-content img {
		opacity: 0;
		min-height: 350px;
	}
	.carousel-content .img-replace {
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		background-size: cover;
		background-position: left;
	}
	.carousel-control.left, .carousel-control.right {
		background-image: none;
		color: #ec1a22;
	}
	
	.carousel-indicators .active {
		background-color: #ec1a22;
	}
	
	.carousel-indicators li {
		border: 1px solid #ec1a22;
	}
	
	@media (max-width: 767px) {
		.carousel-content .btn-lg {
			padding-top: 5px;
			padding-bottom: 5px; 
		}
	}
	
</style>