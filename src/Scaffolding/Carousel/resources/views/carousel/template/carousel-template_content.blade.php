<div class="container full-width"><div class="row"><div class="col-sm-12">
	<div id="carousel-main" class="carousel slide" data-ride="carousel" data-interval="5000">
	
		<!-- Indicators -->
		<ol class="carousel-indicators">
			@foreach($carousel as $banner)
				<li data-target="#carousel-main" data-slide-to="{{ $loop->index }}" class="{{ ($loop->first) ? 'active' : '' }}"></li>
			@endforeach
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			@foreach($carousel as $banner)
				<div class="item {{ ($loop->first) ? 'active' : '' }}" id="carousel-item-{{ $banner->id }}">
					<div class="carousel-content">
						<div class="img-replace" style="background-image: url('{{ $banner->image->getUrl() }}'); background-position-x: {{ $banner->positionX }};  background-position-y: {{ $banner->positionY }}; "></div>
						@if ($banner->button_link && !$banner->button_text)
							<a href="{{ $banner->button_link }}">
						@endif 
						<img src="{{ $banner->image->getUrl() }}" alt="" class="img-responsive">
						@if ($banner->button_link && !$banner->button_text)
							</a>
						@endif
					</div>
					<div class="carousel-caption">
						@if ($banner->show_name)
						<div class="container-fluid"><div class="row text-center"><div class="col-sm-12">
							<h2 class="banner-name">{{ $banner->name }}</h2>
						</div></div></div>
						@endif
						
						@if ($banner->description)
						<div class="container-fluid"><div class="row text-center space-bottom-1"><div class="col-sm-12">
							<div class="banner-description">{!! $banner->description !!}</div>
						</div></div></div>
						@endif
						
						@if ($banner->button_type != 'none' && $banner->button_text)
						<div class="container-fluid"><div class="row text-center"><div class="col-sm-12">
							<div class="form-group">
								<a href="{{ $banner->button_link }}" class="btn btn-{{ $banner->button_type }} btn-lg">{{ $banner->button_text }}</a>
							</div>
						</div></div></div>
						@endif
					</div>
				</div>
			@endforeach
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-main" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		</a>
		<a class="right carousel-control" href="#carousel-main" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		</a>
	</div>
</div></div></div>