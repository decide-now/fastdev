@include('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())

{!! FastDevCtrl::modal('select-form')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('file_storage_select_scene')->sceneArray())
{!! FastDevCtrl::modal('select-form')->close() !!}

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-8">
			<h3>
				{{ $item::getAlias('record_name') }}
				@if ($item->id)
					<span class="small">(
				@endif

				@if ($item->id)
					id: {{ $item->id }}
				@endif
				
				@if ($item->id)
					)</span>
				@endif
			</h3>
		</div>
		<div class="col-sm-4 text-right">
		{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
			{!! FastDevCtrl::button('btn-ok-item')->title('Сохранить и выйти')->iconBefore('glyphicon glyphicon-ok')->data('action-tech', $scene->getActionTechnology('item_btn_ok_item'))->out() !!}
			{!! FastDevCtrl::button('btn-save-item')->title('Сохранить')->iconBefore('glyphicon glyphicon-floppy-save')->data('action-tech', $scene->getActionTechnology('item_btn_save_item'))->out() !!}
			{!! FastDevCtrl::button('btn-cancel-item')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->data('action-tech', $scene->getActionTechnology('item_btn_cancel_item'))->out() !!}
			{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->data('action-tech', $scene->getActionTechnology('item_btn_refresh'))->out() !!}
		{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
		</div>
	</div>
			
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		@include('fastdev::scene_primitives')
		{!! FastDevCtrl::textField('item_id')->type('hidden')->value($item->id)->out() !!}

		<div class="row">
			<div class="col-sm-5">
				{!! FastDevCtrl::textField('name', $item)->out() !!}
			</div>
			<div class="col-sm-3">
				{!! FastDevCtrl::radioButtonList('show_name', $item)->options(['0' => 'Нет', '1' => 'Да'])->isJustified()->out() !!}
			</div>
			<div class="col-sm-2">
				{!! FastDevCtrl::textField('priority', $item)->out() !!}
			</div>
			<div class="col-sm-2">
				{!! FastDevCtrl::textField('code', $item)->out() !!}
			</div>
			<div class="col-sm-4">
				{!! FastDevCtrl::textField('image_id', $item)
					->visibleValue(\App\ORM\FileStorage::idToString(($item->image) ? $item->image->id : null ))
					->hasClearButton()
					->hasSelectButton()
					->out()
				!!}
			</div>
			<div class="col-sm-2">
				{!! FastDevCtrl::radioButtonList('is_published', $item)->options([0 => $item->isPublishedIconForValue(0), '1' => $item->isPublishedIconForValue(1)])->isJustified()->out() !!}
			</div>
			
			<div class="col-sm-12">
				{!! FastDevCtrl::textArea('description', $item)->open() !!}{{ $item->description }}{!! FastDevCtrl::textArea('description', $item)->close() !!}
			</div>
			
			<div class="col-sm-3">
				{!! FastDevCtrl::selectField('button_type', $item)
					->options(['none' => 'Нет', 'default' => 'Обычная', 'primary' => 'Яркая', 'link' => 'Ссылка', ])
					->out()
				!!}
			</div>
			<div class="col-sm-3">
				{!! FastDevCtrl::textField('button_text', $item)->out() !!}
			</div>
			<div class="col-sm-4">
				{!! FastDevCtrl::textField('button_link', $item)->out() !!}
			</div>
			<div class="col-sm-2">
				{!! FastDevCtrl::selectField('image_position', $item)->options(App\ORM\Carousel::imagePositions())->out() !!}
			</div>
		</div>
	</form>
		
</div></div>