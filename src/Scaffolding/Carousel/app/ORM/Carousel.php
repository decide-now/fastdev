<?php

namespace App\ORM;

use DecideNow\FastDev\ORM\ORMModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class Carousel extends ORMModel
{
	protected $table = 'carousel';
	protected $fillable = [
		'name', 'show_name', 'code', 'priority', 'description', 'button_text', 'button_type', 'button_link', 'is_published', 'image_position', 'image_id', 
	];
	
	public static function getAliases()
	{
		return [
			'table_name' => 'Баннеры',
			'record_name' => 'Баннер',
			'name' => 'Заголовок',
			'show_name' => 'Показывать заголовок',
			'code' => 'Код',
			'priority' => 'Приоритет',
			'description' => 'Описание',
			'button_text' => 'Текст кнопки',
			'button_type' => 'Тип кнопки',
			'button_link' => 'Ссылка кнопки',
			'is_published' => 'Опубликован',
			'image_position' => 'Выравнивание',
			'image_id' => 'Изображение', 

		] + parent::getAliases();
	}
		
	
	public function getIsPublishedStringAttribute()
	{
		return ($this->is_published) ? 'Да' : 'Нет';
	}
	public function getIsPublishedIconAttribute()
	{
		return $this->isPublishedIconForValue($this->is_published);
	}
	public static function isPublishedIconForValue($val)
	{
		return ($val) ? '<i class="fa fa-sun-o"></i>' : '<i class="fa fa-moon-o"></i>';
	}
	
	
	public static function imagePositions()
	{
		return [
			'LeftTop'		=> 'Лево-Верх',
			'LeftCenter'	=> 'Лево-Центр',
			'LeftBottom'	=> 'Лево-Низ',
			'RightTop'		=> 'Право-Верх',
			'RightCenter'	=> 'Право-Центр',
			'RightBottom'	=> 'Право-Низ',
			'CenterTop'		=> 'Центр-Верх',
			'CenterCenter'	=> 'Центр-Центр',
			'CenterBottom'	=> 'Центр-Низ',
		];
	}
	
	public function getPositionXAttribute()
	{
		if (substr($this->image_position, 0, 4) == 'Left') return 'left';
		if (substr($this->image_position, 0, 5) == 'Right') return 'right';
		if (substr($this->image_position, 0, 6) == 'Center') return 'center';
	}
	
	public function getPositionYAttribute()
	{
		if (substr($this->image_position, -3) == 'Top') return 'top';
		if (substr($this->image_position, -6) == 'Bottom') return 'bottom';
		if (substr($this->image_position, -6) == 'Center') return 'center';
	}
	
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ 'show_name' => '', 'code' => '', 'priority' => '', 'description' => '', 'button_text' => '', 'button_type' => '', 'button_link' => '', 'is_published' => '', 'image_id' => '', ] + parent::getFilterFields();
	}
	
	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'show_name', '');
		if ($tmp != '') {
			$query = $query->where('show_name', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'code', '');
		if ($tmp != '') {
			$query = $query->where('code', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'priority', '');
		if ($tmp != '') {
			$query = $query->where('priority', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'description', '');
		if ($tmp != '') {
			$query = $query->where('description', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'button_text', '');
		if ($tmp != '') {
			$query = $query->where('button_text', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'button_type', '');
		if ($tmp != '') {
			$query = $query->where('button_type', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'button_link', '');
		if ($tmp != '') {
			$query = $query->where('button_link', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'is_published', '');
		if ($tmp != '') {
			$query = $query->where('is_published', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'image_id', '');
		if ($tmp != '') {
			$query = $query->where('image_id', '=', ''.$tmp.'');
		}

		$query = parent::applyFilter($query, $filter_data);
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return [ 'show_name' => '', 'code' => '', 'priority' => '', 'description' => '', 'button_text' => '', 'button_type' => '', 'button_link' => '', 'is_published' => '', 'image_id' => '', ] + parent::getOrderingFields();
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'show_name', '');
		if ($tmp != '') {
			$query = $query->orderBy('show_name', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'code', '');
		if ($tmp != '') {
			$query = $query->orderBy('code', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'priority', '');
		if ($tmp != '') {
			$query = $query->orderBy('priority', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'description', '');
		if ($tmp != '') {
			$query = $query->orderBy('description', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'button_text', '');
		if ($tmp != '') {
			$query = $query->orderBy('button_text', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'button_type', '');
		if ($tmp != '') {
			$query = $query->orderBy('button_type', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'button_link', '');
		if ($tmp != '') {
			$query = $query->orderBy('button_link', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'is_published', '');
		if ($tmp != '') {
			$query = $query->orderBy('is_published', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'image_id', '');
		if ($tmp != '') {
			$query = $query->orderBy('image_id', $tmp);
		}

		$query = parent::applyOrdering($query, $ordering_data);
		
		return $query;
	}
	
	
	/* relations */
	
	public function image()
	{
		return $this->belongsTo(FileStorage::class, 'image_id');
	}
	
	
	/* list queries */
	
	public static function defaultListQuery($query = null)
	{
		$query = parent::defaultListQuery($query);
		//$query = $query->select('carousel.*')->distinct();
		return $query;
	}

	public static function listQueryForSite($query = null)
	{
		$query = self::defaultListQuery($query);
		$query = $query->where('is_published', '=', true)->whereHas('image')->orderBy('priority', 'desc');
		return $query;
	}
	
	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if ($param == 'id') {
				if (Auth::user()->userRole->code == 'admin') {
					return true;
				}
				return false;
			}
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
}
