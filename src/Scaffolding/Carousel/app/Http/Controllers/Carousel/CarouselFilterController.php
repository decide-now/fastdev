<?php
namespace App\Http\Controllers\Carousel;

use Illuminate\Http\Request;

class CarouselFilterController extends CarouselController
{	
	static $scene_id = 'carousel-filter';
	
	public $filter;
	
	public function prepareContent(Request $request)
	{
		$this->filter = $this->model->filterGetFromSession();
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->transferPrimitives();
		
		$this->prepareContent($request);
		
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferPrimitives($request);
		
		$task = $this->getPrimitives('task');
		
		if ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
		}
		return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
	}	
}