<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarouselTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carousel', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->boolean('show_name')->default(false);
			$table->string('code')->nullable();
			$table->integer('priority')->nullable();
			$table->longText('description')->nullable();
			$table->string('button_text')->nullable();
			$table->string('button_type')->default('default');
			$table->string('button_link')->nullable();
			$table->boolean('is_published')->default(true);
			$table->string('image_position')->default('LeftTop');
			$table->integer('image_id')->unsigned()->nullable();
			$table->foreign('image_id')->references('id')->on('file_storage');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('carousel', function (Blueprint $table) {
			$table->dropForeign(['image_id']);
		});
		Schema::dropIfExists('carousel');
	}
}
