// Carousel

Route::get('/carousel', 'Carousel\CarouselListController@get');
Route::post('/carousel', 'Carousel\CarouselListController@post');
Route::get('/carousel/item/{id?}', 'Carousel\CarouselItemController@get');
Route::post('/carousel/item/{id?}', 'Carousel\CarouselItemController@post');
Route::get('/carousel/select', 'Carousel\CarouselSelectController@get');
Route::post('/carousel/select', 'Carousel\CarouselSelectController@post');
Route::get('/carousel/filter', 'Carousel\CarouselFilterController@get');
Route::post('/carousel/filter', 'Carousel\CarouselFilterController@post');
