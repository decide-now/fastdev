@include('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())

<div class="row space-top-1"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-6">
			<h3>Заголовок</h3>
		</div>
		<div class="col-sm-6 text-right">
			<div class="btn-group btn-group-sm">
				{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
					{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->data('action-tech', $scene->getActionTechnology('btn_refresh'))->out() !!}
				{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			Содержимое...
		</div>
	</div>
	
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
	</form>
	
</div></div>
		