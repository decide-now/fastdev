<script>

new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;
		
		thisScene.getElement('[id="btn_refresh"]').click( { thisScene: this }, thisScene.btnRefresh_Click );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;
		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},
	afterSuccessAJAX : function(response) {
		//
	},

	
	/**/
	
	btnRefresh_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { ajax: true, };
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
}).init();

</script>