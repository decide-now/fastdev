<?php
namespace App\Http\Controllers\RegisterUser;

use DecideNow\FastDev\Scene\SceneBaseController;
use Illuminate\Http\Request;

class RegisterUserEmailController extends SceneBaseController
{	
	static $scene_id = 'register_user-email';
	static $nest_layout = 'site.site_nest';
	
	protected $actions_tech = [
		/*
		'btn_refresh' => '',
		*/
	];
	
	public $short_footer = true;
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
	}
	
	public function prepareContent(Request $request)
	{
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{	
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);

		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			return redirect()->action(self::methodPath('get'));
		}
	}
}