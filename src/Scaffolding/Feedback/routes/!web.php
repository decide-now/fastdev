<?php

// Feedback

Route::get('/feedback', 'Feedback\FeedbackListController@get');
Route::post('/feedback', 'Feedback\FeedbackListController@post');
Route::get('/feedback/item/{id?}', 'Feedback\FeedbackItemController@get');
Route::post('/feedback/item/{id?}', 'Feedback\FeedbackItemController@post');
Route::get('/feedback/select', 'Feedback\FeedbackSelectController@get');
Route::post('/feedback/select', 'Feedback\FeedbackSelectController@post');
Route::get('/feedback/filter', 'Feedback\FeedbackFilterController@get');
Route::post('/feedback/filter', 'Feedback\FeedbackFilterController@post');
Route::get('/feedback/item-public', 'Feedback\FeedbackItemPublicController@get');
Route::post('/feedback/item-public', 'Feedback\FeedbackItemPublicController@post');
Route::get('/feedback/button-public', 'Feedback\FeedbackButtonPublicController@get');
Route::post('/feedback/button-public', 'Feedback\FeedbackButtonPublicController@post');
