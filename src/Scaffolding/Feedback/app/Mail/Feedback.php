<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Feedback extends Mailable
{
    use Queueable, SerializesModels;
    
    protected function setAddress($address, $name = null, $property = 'to') {
    	foreach ( $this->addressesToArray($address, $name) as $recipient ) {
    		$recipient = $this->normalizeRecipient($recipient);
    		
    		$this->{$property} [] = [
    			'name' => isset($recipient->name) ? $recipient->name : null,
    			'address' => (filter_var($recipient->username, FILTER_VALIDATE_EMAIL)) ? $recipient->username : config('mail.from.address')
    		];
    	}
    	
    	return $this;
    }
    
    
    public $feedback;
    public $fb_author;
    public $fb_contact;
    public $fb_subject;
    public $fb_content;
    public $button;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($feedback)
    {
    	$this->feedback = $feedback;
    	$this->fb_author = $feedback->author;
    	$this->fb_contact = $feedback->contact;
    	$this->fb_subject = $feedback->subject;
    	$this->fb_content = $feedback->content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	return $this->markdown('email.feedback')
    	->subject(html_entity_decode(config('app.name')).'. Сообщение обратной связи');
    }
}
