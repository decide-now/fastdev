<?php
namespace App\Http\Controllers\Feedback;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FeedbackItemPublicController extends FeedbackController
{
	static $scene_id = 'feedback-public-item';
	static $template_content = 'feedback.item_public.feedback-item_public_content';
	static $template_code = 'feedback.item_public.feedback-item_public_code';
	
	public $item;
	public $task;
	public $refresh_parent_scene;
	
	public function __construct($scene_parent = null)
	{
		parent::__construct($scene_parent);
		$this->middlewareUpdate('auth', [ 'except' => ['get', 'post'] ]);
	}
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
	}
	
	public function prepareContent(Request $request)
	{
		$this->item = new $this->model;
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->task = $this->transferData('task', '');
		$this->refresh_parent_scene = $this->transferData('refresh_parent_scene', '0');
		
		$this->response_data = ['refresh_parent_scene' => $this->refresh_parent_scene, 'task' => $this->task, ];
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferData('refresh_parent_scene', '0', $request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'send_feedback') {
			$validated = $this->itemValidate($request);
			$item = Session::get('item');
			if (!$validated) {
				return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax())->withErrors($this->validator);
			}
			$this->itemSave($request, $item, true);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		}
	}
}