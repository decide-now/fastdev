<?php
namespace App\Http\Controllers\Feedback;

use Illuminate\Http\Request;

class FeedbackButtonPublicController extends FeedbackController
{
	static $scene_id = 'feedback-button-public';
	static $template_content = 'feedback.button_public.feedback-button_public_content';
	static $template_code = 'feedback.button_public.feedback-button_public_code';
	
	protected function defineChildren()
	{
		$feedback_scene = new FeedbackItemPublicController($this);
		$this->scene_children->put('feedback_scene', $feedback_scene);
	}
	
	public function prepareContent(Request $request)
	{	
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->task = $this->transferData('task', '');
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		}
	}
}