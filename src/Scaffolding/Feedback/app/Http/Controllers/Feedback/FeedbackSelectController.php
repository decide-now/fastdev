<?php
namespace App\Http\Controllers\Feedback;

use Illuminate\Http\Request;

class FeedbackSelectController extends FeedbackController
{	
	static $scene_id = 'feedback-select';
	static $template_content = 'feedback.select.feedback-select_content';
	static $template_code = 'feedback.select.feedback-select_code';
	
	public $list;
	public $filter;
	public $ordering;
	
	public $parent_id;
	public $page;
	public $call_button_id;
	
	protected function defineChildren()
	{	
		$this->defineAlertChildScene();
	
		$filter_scene = new FeedbackFilterController($this);
		$this->scene_children->put('filter_scene', $filter_scene);
	}
	
	public function prepareContent(Request $request)
	{	
		$model = $this->model;
		
		$this->filter = $model::filterGetFromSession();
		$this->ordering = $model::orderingGetFromSession();
		
		$list_data = new $this->model;
		$list_data = $model::addQueryFilterAndOrdering($list_data);
		$list_data = $model::addQueryByParentId($list_data, $this->parent_id);
		$list_data = $model::applyFilter($list_data, $this->filter);
		$list_data = $model::applyOrdering($list_data, $this->ordering);
		
		$this->list = $list_data->paginate(null, ['*'], 'page', $this->page);
		
		$this->no_content = false;
	}

	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->page = $this->transferData('page', '1');
		$this->parent_id = $this->transferData('parent_id', '');
		$this->call_button_id = $this->transferData('call_button_id', '');
		$data = $this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferData('page', '1', $request);
		$this->transferData('parent_id', '', $request);
		$this->transferData('call_button_id', '', $request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'ordering') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->orderingPutToSession($request, '', $force_clear);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		}
	}
}