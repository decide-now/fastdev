<?php
namespace App\Http\Controllers\Feedback;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FeedbackListController extends FeedbackController
{	
	static $scene_id = 'feedback-list';
	static $template_content = 'feedback.list.feedback-list_content';
	static $template_code = 'feedback.list.feedback-list_code';
	
	public $list;
	public $filter;
	public $ordering;
	
	public $page;

	protected function defineChildren()
	{
		$this->defineAlertChildScene();
		
		$filter_scene = new FeedbackFilterController($this);
		$this->scene_children->put('filter_scene', $filter_scene);
		
		if ($this->scene_template_type != 'classic') {
			$item_scene = new FeedbackItemController($this);
			$this->scene_children->put('item_scene', $item_scene);
		}
	}
	
	public function prepareContent(Request $request)
	{	
		$model = $this->model;
		
		$this->filter = $model::filterGetFromSession();
		$this->ordering = $model::orderingGetFromSession();
		
		$list_data = new $this->model;
		$list_data = $model::addQueryFilterAndOrdering($list_data);

		$list_data = $model::applyFilter($list_data, $this->filter);
		$list_data = $model::applyOrdering($list_data, $this->ordering);
		
		if ($this->scene_template_type != 'classic') {
			$this->list = $list_data->paginate(null, ['*'], 'page', $this->page)->withPath(action($this->methodPath('get')), [], false);
		} else {
			$this->list = $list_data->paginate();
		}
		
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->page = $this->transferData('page', '1');

		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{	
		$this->transferSceneId($request);
		$this->transferData('page', '1', $request);

		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'ordering') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->orderingPutToSession($request, '', $force_clear);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'create') {
			$this->itemCreate($request);
			return redirect()->action(FeedbackItemController::methodPath('get'));
		} elseif ($task == 'edit') {
			$this->itemFind($request);
			$item = Session::get('item');
			return redirect()->action(FeedbackItemController::methodPath('get'), ['id' => ($item) ? $item->id : '']);
		} elseif ($task == 'delete') {
			$this->itemDelete($request);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		}
	}
}