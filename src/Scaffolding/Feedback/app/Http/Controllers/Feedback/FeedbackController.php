<?php
namespace App\Http\Controllers\Feedback;

use App\Mail\Feedback as MailFeedback;
use App\ORM\Feedback;
use App\ORM\User;
use DecideNow\FastDev\Scene\SceneBaseController;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class FeedbackController extends SceneBaseController
{	
	protected $model;
	protected $validator;
	public $scene_template_type;
	
	public function __construct($scene_parent = null)
	{
		parent::__construct($scene_parent);
		$this->middleware('auth');
		$this->model = new Feedback;
		$this->scene_template_type = 'classic';
	}
	
	protected function itemCreate($request)
	{
		$item = new $this->model;

		Session::flash('item', $item);
	}
	
	protected function itemFind($request, $item_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		Session::flash('item', $item);
	}
	
	protected function itemDelete($request, $item_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		
		DB::beginTransaction();
		try {
			$item->delete();
			DB::commit();
			Session::flash('flash_success_messages', ['Запись удалена!']);
		} catch (QueryException $e) {
			DB::rollBack();
			$msg = $this->getDatabaseErrorMessage($e);
			Session::flash('flash_error_messages', [$msg]);
		}
	}
	
	protected function itemFromInput($request)
	{
		$itemClass = $this->model->getModelName();
		
		$item_id = $request->get('item_id');
		$item = ($item_id) ? $item = $this->model->find($item_id) : new $this->model;
		
		$item_input = $request->get($itemClass);
		
		$item->fill($item_input);
		
		return $item;
	}
	
	protected function itemValidate($request)
	{
		$item = $this->itemFromInput($request);
		Session::flash('item', $item);
		
		$this->validator = null;
		$itemClass = $this->model->getModelName();
		$item_id = $request->get('item_id');
		
		$rules = [
			//$itemClass.'.author' => 'required|max:255',
			$itemClass.'.contact' => 'required|max:255',
			//$itemClass.'.subject' => 'required|max:255',
			$itemClass.'.content' => 'required|max:255',

		];
		
		$this->validator = Validator::make($request->all(), $rules, [], $this->model->getAliasesWithModelName());
		if ($this->validator->fails()) {
			if ($request->ajax()) {
				Session::flash('flash_error_messages', $this->validator->errors()->messages());
			}
			return false;
		} else {
			return true;
		}
	}
	
	protected function itemSave($request, $item = null, $send = false)
	{
		if (!$item) {
			$item = $this->itemFromInput($request);
		}
		
		DB::beginTransaction();
		$item->save();
		DB::commit();
		
		if ($send) {
			//$system_users = User::where('recieve_system_mails', '=', true)->get();
			$system_users = [User::superUser()];
			foreach ($system_users as $system_user) {
				Mail::to($system_user)->send(new MailFeedback($item));
			}
			Session::flash('flash_success_messages', ['Ваше сообщение отправлено!']);
		}
		
		Session::flash('item', $item);
	}

}