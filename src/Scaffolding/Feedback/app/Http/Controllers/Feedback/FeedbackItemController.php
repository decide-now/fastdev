<?php
namespace App\Http\Controllers\Feedback;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FeedbackItemController extends FeedbackController
{	
	static $scene_id = 'feedback-item';
	static $template_content = 'feedback.item.feedback-item_content';
	static $template_code = 'feedback.item.feedback-item_code';
	
	public $item;
	
	public $task;
	public $refresh_parent_scene;
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
	}
	
	public function prepareContent(Request $request)
	{
		if ($this->scene_template_type != 'classic') {
			$this->content_error = Session::has('flash_error_messages');
			if ($this->task == 'ok' || $this->task == 'cancel') {
				return;
			}
		}
		
		$this->item = Session::get('item');
		if ($this->item == null) {
			$this->itemCreate($request);
			$this->item = Session::get('item');
		}
		
		$this->no_content = false;
	}
	
	public function get(Request $request, $item_id = '')
	{
		$this->transferSceneId();
		$this->task = $this->transferData('task', '');

		$this->refresh_parent_scene = ($this->task == 'ok' || $this->task == 'save') ? '1' : $this->transferData('refresh_parent_scene', '0');
		
		if ($this->scene_template_type != 'classic') {
			$this->response_data = ['refresh_parent_scene' => $this->refresh_parent_scene];
		}
		
		if (!Session::has('item')) {
			if ($item_id) {
				$this->itemFind($request, $item_id);
				$item = Session::get('item');
				if (!$item) {
					$this->content_error = true;
					Session::flash('flash_error_messages', ['Не найдена запись с кодом '.$item_id.'!']);
				}
			}
		}
		if (!$this->content_error) {
			$this->prepareContent($request);
		}
		return $this->sceneOutput();
	}
	
	public function post(Request $request, $item_id = '')
	{
		$this->transferSceneId($request);
		$task = $this->transferData('task', 'refresh', $request);

		$this->transferData('refresh_parent_scene', '0', $request);
		
		if ($task == 'refresh') {
			$this->itemFind($request);
			$item = Session::get('item');
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])->with('ajax', $request->ajax());
		} elseif ($task == 'create') {
			$this->itemCreate($request);
			$item = Session::get('item');
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])->with('ajax', $request->ajax());
		} elseif ($task == 'edit') {
			$this->itemFind($request);
			$item = Session::get('item');
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])->with('ajax', $request->ajax());
		} elseif ($task == 'cancel') {
			if ($this->scene_template_type != 'classic') {
				$this->itemFind($request);
				$item = Session::get('item');
				return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])->with('ajax', $request->ajax());
			} else {
				return redirect()->action(FeedbackListController::methodPath('get'));
			}
		} elseif ($task == 'ok') {
			$validated = $this->itemValidate($request);
			$item = Session::get('item');
			if (!$validated) {
				return redirect()
				->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])
				->with('ajax', $request->ajax())
				->withErrors($this->validator);
			}
			$this->itemSave($request, $item);
			if ($this->scene_template_type != 'classic') {
				return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])->with('ajax', $request->ajax());
			} else {
				return redirect()->action(FeedbackListController::methodPath('get'));
			}
		} elseif ($task == 'save') {
			$validated = $this->itemValidate($request);
			$item = Session::get('item');
			if (!$validated) {
				return redirect()
				->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])
				->with('ajax', $request->ajax())
				->withErrors($this->validator);
			}
			$this->itemSave($request, $item);
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])->with('ajax', $request->ajax());
		}
	}
}