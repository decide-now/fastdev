<?php

namespace App\ORM;

use DecideNow\FastDev\ORM\ORMModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class Feedback extends ORMModel
{
	protected $table = 'feedback';
	protected $fillable = [
		'author', 'contact', 'subject', 'content', 
	];
	
	public static function getAliases()
	{
		return [
			'table_name' => 'Обратная связь',
			'record_name' => 'Сообщение обратной связи',
			'author' => 'Ваше имя',
			'contact' => 'Ваш e-mail или телефон',
			'subject' => 'Тема сообщения',
			'content' => 'Текст сообщения',
			'created_at' => 'Дата',
			
			'author_b' => 'Имя',
			'contact_b' => 'Контакт',
			'subject_b' => 'Тема',
			'content_b' => 'Текст',
			
			'contact_descr' => 'Контактные данные',
			'subject_descr' => 'Тема сообщения',
			'content_descr' => 'Текст сообщения',

		] + parent::getAliases();
	}
	
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ 'author' => '', 'contact' => '', 'subject' => '', 'content' => '', ] + parent::getFilterFields();
	}
	
	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'author', '');
		if ($tmp != '') {
			$query = $query->where('author', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'contact', '');
		if ($tmp != '') {
			$query = $query->where('contact', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'subject', '');
		if ($tmp != '') {
			$query = $query->where('subject', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'content', '');
		if ($tmp != '') {
			$query = $query->where('content', '=', ''.$tmp.'');
		}

		
		$query = parent::applyFilter($query, $filter_data);
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return [ 'id'=> '', 'created_at' => 'desc', 'author' => '', 'contact' => '', 'subject' => '', 'content' => '', ] + parent::getOrderingFields();
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'created_at', '');
		if ($tmp != '') {
			$query = $query->orderBy('created_at', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'author', '');
		if ($tmp != '') {
			$query = $query->orderBy('author', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'contact', '');
		if ($tmp != '') {
			$query = $query->orderBy('contact', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'subject', '');
		if ($tmp != '') {
			$query = $query->orderBy('subject', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'content', '');
		if ($tmp != '') {
			$query = $query->orderBy('content', $tmp);
		}

		
		$query = parent::applyOrdering($query, $ordering_data);
		
		return $query;
	}
	
	
	/* relations */


	
	
	/* list queries */
	
	public static function addQueryFilterAndOrdering($query = null)
	{
		//$query = $query->select('feedback.*')->distinct();
		return $query;
	}


	
	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
}
