<?php

return [
	/* ... */
	'markdown' => [
		'theme' => 'style',
		'paths' => [
			resource_path('views/email'),
		],
	],
	/* ... */

];