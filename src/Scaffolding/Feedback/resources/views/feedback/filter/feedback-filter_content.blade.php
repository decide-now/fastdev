<div class="row taskbar">
	<div class="col-sm-12 text-right">
		{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->open() !!}
			{!! FastDevCtrl::button('btn-ok-filter')->title('Применить')->iconBefore('glyphicon glyphicon-ok')->out() !!}
			{!! FastDevCtrl::button('btn-cancel-filter')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->out() !!}
			{!! FastDevCtrl::button('btn-clear-filter')->title('Очистить')->iconBefore('glyphicon glyphicon-erase')->out() !!}
		{!! FastDevCtrl::buttonGroup('taskbar')->close() !!}
	</div>
</div>
<div class="row"><div class="col-sm-12">
	<form data-role="data_form">
		{{ csrf_field() }}
		{!! FastDevCtrl::textField('filter[name]')->label($model::getAlias('name'))->value($filter['name'])->hasClearButton()->out() !!}
	</form>
</div></div>
