@component('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())
@endcomponent

{!! FastDevCtrl::modal('filter-form')->title('Фильтр')->isStatic()->size('fs')->open() !!}
	@component('fastdev::scene_content', $scene_children->get('filter_scene')->sceneArray())
	@endcomponent
{!! FastDevCtrl::modal('filter-form')->close() !!}

@if($scene_template_type != 'classic')
	{!! FastDevCtrl::modal('item-form')->isStatic()->size('fs')->open() !!}
		@component('fastdev::scene_content', $scene_children->get('item_scene')->sceneArray())
		@endcomponent
	{!! FastDevCtrl::modal('item-form')->close() !!}
@endif

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-6">
			<h3>{{ $model::getAlias('table_name') }}</h3>
		</div>
		<div class="col-sm-6 text-right">
			<div class="btn-group btn-group-sm">
				{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->open() !!}
					{!! FastDevCtrl::button('btn-create-item')->title('Создать')->iconBefore('glyphicon glyphicon-plus')->isDisabled(!$model->getPermission('create'))->out() !!}
					{!! FastDevCtrl::button('btn-show-filter')->title('Фильтр')->iconBefore('glyphicon glyphicon-filter')->style( (FastDevHlpr::array_is_empty($filter)) ? '' : 'success' )->out() !!}
					{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->out() !!}
				{!! FastDevCtrl::buttonGroup('taskbar')->close() !!}
			</div>
		</div>
	</div>
	
	<div class="row list-hdr">
		<div class="col-sm-2 visible-xs">Сортировка: </div>
		<div class="col-sm-2">{!! FastDevCtrl::sortableLink('created_at', $model, $ordering)->out() !!}</div>
		<div class="col-sm-2">{!! FastDevCtrl::sortableLink('author', $model, $ordering)->label($model::getAlias('author_b'))->out() !!}</div>
		<div class="col-sm-2">{!! FastDevCtrl::sortableLink('contact', $model, $ordering)->label($model::getAlias('contact_b'))->out() !!}</div>
		<div class="col-sm-2">{!! FastDevCtrl::sortableLink('subject', $model, $ordering)->label($model::getAlias('subject_b'))->out() !!}</div>
		<div class="col-sm-2">{!! FastDevCtrl::sortableLink('content', $model, $ordering)->label($model::getAlias('content_b'))->out() !!}</div>
		<div class="col-sm-2 hidden-xs">&nbsp;</div>
	</div>
	
	@foreach($list as $item)
	<div class="row list-item bottom-border" data-role="list-item" data-item-id = "{{ $item->id }}" data-item-text="{{ $item->toString() }}">
		<div class="col-sm-2">{{ FastDevHlpr::dateFormat($item->created_at, 'DMYT') }}</div>
		<div class="col-sm-2">{{ $item->author }}</div>
		<div class="col-sm-2">{{ $item->contact }}</div>
		<div class="col-sm-2">{{ $item->subject }}</div>
		<div class="col-sm-2">{{ $item->content }}</div>

		<div class="col-sm-2 text-right">
			{!! FastDevCtrl::buttonGroup('row-actions')->size('sm')->isFormGroup(false)->open() !!}
				{!! FastDevCtrl::button('btn-edit-item')->iconBefore('glyphicon glyphicon-pencil')->title('Редактировать')->isDisabled(!$model->getPermission('edit'))->out() !!}
				{!! FastDevCtrl::button('btn-delete-item')->iconBefore('glyphicon glyphicon-trash')->title('Удалить')->isDisabled(!$model->getPermission('delete'))->out() !!}
			{!! FastDevCtrl::buttonGroup('row-actions')->close() !!}
		</div>
	</div>
	@endforeach
	
	<div class="row">
		<div class="col-sm-12">
			{{ $list->links() }}
		</div>
	</div>
	
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}

	</form>
	
</div></div>
		