<a data-role="feedback-toggle" class="feedback-toggle" href="#"><i class="fa fa-commenting"></i></a>
{!! FastDevCtrl::modal('feedback-form')->title('Обратная связь')->hasCloseCross()->open() !!}
	@component('fastdev::scene_content', $scene_children->get('feedback_scene')->sceneArray())
	@endcomponent
{!! FastDevCtrl::modal('filter-form')->close() !!}