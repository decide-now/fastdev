
<script>
var contactScene = new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;
		thisScene.getElement('[data-role="feedback-toggle"]').click({ thisScene: this }, thisScene.btnToggleFeedback_Click);
	},

	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;
		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},
	

	/**/

	btnToggleFeedback_Click : function(e) {
		e.preventDefault();
		var feedback_scene = stage.getScene('{{ $scene->scene_children->get("feedback_scene")->sceneId() }}');
		feedback_scene.sceneRefresh({ ajax: true, fields: { no_field_labels: true} });
		feedback_scene.getRootElement().closest('.modal').modal('show');
		return false;
	},

	feedbackReturn : function (data) {

		var alertsScene = null;
		@if (isset($scene_children)) 
			@if ($scene_children->has('alert_message_scene'))
				alertsScene = stage.getScene('{{ $scene->scene_parent->scene_children->get("alert_message_scene")->sceneId() }}');
				alertsScene.clearAlerts();
			@endif
		@endif
		
		var child_scene = data.scene;
		var child_form = child_scene.getRootElement();
		child_form.closest('.modal').modal('hide');
		child_form.html('');
		for (var msg in data.message  && alertsScene) {
			alertsScene.showSuccess(data.message[msg]);
		}
	}

}).init();

</script>
