@component('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())
@endcomponent

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-6">
			<h3>{{ $item::getAlias('record_name') }}</h3>
		</div>
		<div class="col-sm-6 text-right">
		{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->open() !!}
			{!! FastDevCtrl::button('btn-ok-item')->title('Сохранить и выйти')->iconBefore('glyphicon glyphicon-ok')->out() !!}
			{!! FastDevCtrl::button('btn-save-item')->title('Сохранить')->iconBefore('glyphicon glyphicon-floppy-save')->out() !!}
			{!! FastDevCtrl::button('btn-cancel-item')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->out() !!}
			{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->out() !!}
		{!! FastDevCtrl::buttonGroup('taskbar')->close() !!}
		</div>
	</div>
			
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		{!! FastDevCtrl::textField('item_id')->type('hidden')->value($item->id)->out() !!}
		{!! FastDevCtrl::textField('refresh_parent_scene')->type('hidden')->value($refresh_parent_scene)->out() !!}

		<div class="row">
			<div class="col-sm-4">
				{!! FastDevCtrl::textField('author', $item)->label($item::getAlias('author_b'))->out() !!}
			</div>
			<div class="col-sm-4">
				{!! FastDevCtrl::textField('contact', $item)->label($item::getAlias('contact_b'))->out() !!}
			</div>
			<div class="col-sm-4">
				{!! FastDevCtrl::textField('subject', $item)->label($item::getAlias('subject_b'))->out() !!}
			</div>
			<div class="col-sm-12">
				{!! FastDevCtrl::textArea('content', $item)->label($item::getAlias('content_b'))->out() !!}
			</div>
			@if ($item->id)
				<div class="col-sm-12">
					{{ $item::getAlias('created_at') }}: {{ FastDevHlpr::dateFormat($item->created_at, 'DMYT') }}
				</div>
			@endif

		</div>
	</form>
		
</div></div>