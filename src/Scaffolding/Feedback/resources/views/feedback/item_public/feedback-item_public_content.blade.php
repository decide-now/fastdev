@component('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())
@endcomponent

<div class="row"><div class="col-sm-12">
			
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		{!! FastDevCtrl::textField('item_id')->type('hidden')->value($item->id)->out() !!}
		{!! FastDevCtrl::textField('contact', $item)->placeholder($item->getAlias('contact_descr'))->out() !!}
		{!! FastDevCtrl::textField('subject', $item)->placeholder($item->getAlias('subject_descr'))->out() !!}
		{!! FastDevCtrl::textArea('content', $item)->placeholder($item->getAlias('content_descr'))->out() !!}
		{!! FastDevCtrl::button('btn-send-feedback')->label('Отправить')->out() !!} 
	</form>
		
</div></div>