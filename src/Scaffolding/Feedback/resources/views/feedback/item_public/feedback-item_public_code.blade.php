
<script>
var contactScene = new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;
		
		thisScene.getElement('[id="btn_send_feedback"]').click({ thisScene: this }, thisScene.btnSendFeedback_Click);
	},

	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;
		thisScene.defaultSceneRefresh(data, '{{ URL("/feedback/item-public") }}', 'refresh');
	},

	afterSuccessAJAX : function(response) {
		thisScene = response.thisScene;
		if (response.page == '') {
			var parentScene = stage.getScene('{{ ($scene_parent) ? $scene_parent->sceneId() : "" }}');
			if (response.refresh_parent_scene) {
				parentScene.sceneRefresh({ajax: true, fields: { refresh_parent_scene: 0 }, });
			}
		} else {
			if (response.task == 'send_feedback') {
				var parentScene = stage.getScene('{{ ($scene_parent) ? $scene_parent->sceneId() : "" }}');
				parentScene.feedbackReturn({ scene: thisScene, message: response.message });
			}
		}
	},

	
	/**/
	
	btnSendFeedback_Click : function(e) {
		var thisScene = e.data.thisScene;
		thisScene.sceneRefresh({ ajax: true, fields: { task: 'send_feedback', }, });
	},

}).init();

</script>
