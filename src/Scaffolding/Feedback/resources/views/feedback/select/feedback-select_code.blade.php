<script>

new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;

		thisScene.getElement('[id="btn_cancel_select"]').click( { thisScene: this }, thisScene.btnCancelSelect_Click );
		
		thisScene.getElement('[data-role="sortable-header"]').click( { thisScene: this }, thisScene.hdrSortable_Click );
		thisScene.getElement('[id="btn_show_filter"]').click( { thisScene: this }, thisScene.btnShowFilter_Click );

		thisScene.getElement('[id^="btn_select_item"]').click( { thisScene: this }, thisScene.btnSelectItem_Click );
		thisScene.getElement('[id^="link_edit_item"]').click( { thisScene: this }, thisScene.linkEditItem_Click );

		thisScene.getElement('.pagination a').click( { thisScene: this }, thisScene.pagination_Click );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},
	
	
	/**/
	
	getListItemData : function(el) {
		var list_item = $(el.closest('[data-role="list-item"]'));
		var item_id_val = list_item.data('item-id');
		var item_text_val = list_item.data('item-text');
		return { id: item_id_val, text: item_text_val, }
	},

	btnCancelSelect_Click : function(e) {
		var thisScene = e.data.thisScene;

		var data = { scene:thisScene, };

		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.selectReturn(data);
	},

	btnSelectItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var item_data = thisScene.getListItemData(this);
		var call_button_id_val = thisScene.getElement('[id^="call_button_id"]').val();
		
		var data = { scene:thisScene, call_button_id: call_button_id_val, item_id: item_data.id, item_text: item_data.text, };
		
		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.selectReturn(data);
		
		return false;
	},

	linkEditItem_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var item_data = thisScene.getListItemData(this);
		var call_button_id_val = thisScene.getElement('[id^="call_button_id"]').val();
		
		var data = { scene:thisScene, call_button_id: call_button_id_val, item_id: item_data.id, item_text: item_data.text, };
		
		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.selectReturn(data);
		
		return false;
	},


	/* filter */
	
	btnShowFilter_Click : function(e) {
		var filterScene = stage.getScene('{{ $scene_children->get("filter_scene")->sceneId() }}');

		var data = { ajax: true, };
		
		filterScene.sceneRefresh(data);
		filterScene.getRootElement().closest('.modal').modal('show');

		return false;
	},

	filterReturn : function(data) {
		var thisScene = this;

		var filterScene = stage.getScene('{{ $scene_children->get("filter_scene")->sceneId() }}');
		filterScene.getRootElement().closest('.modal').modal('hide');
		filterScene.getRootElement().html('');

		if (!$.isEmptyObject(data)) {
			data.fields = ('fields' in data) ? data.fields : {};
			data.fields.task = 'filter';
			data.ajax = true;
			thisScene.sceneRefresh(data);
		}
	},

	
	/* ordering */
	
	hdrSortable_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data_field = $(this).attr('data-field');
		var data_direction = $(this).attr('data-direction');
		data_direction = ( (data_direction == null || data_direction == 'desc') ? 'asc' : 'desc' );
		
		var data = { fields: { task: 'ordering', }, ajax: true, };
		data.fields['ordering[' + data_field + ']'] = data_direction;
		
		thisScene.sceneRefresh(data);
		return false;
	},

	
	/* pagination */
	
	pagination_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		var data = {ajax: true, url: e.target.href, }
		thisScene.sceneRefresh(data)
		return false;
	},
	
}).init();

</script>