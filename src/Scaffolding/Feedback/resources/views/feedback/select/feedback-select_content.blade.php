@component('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())
@endcomponent

{!! FastDevCtrl::modal('filter-form')->title('Фильтр')->isStatic()->size('fs')->open() !!}
	@component('fastdev::scene_content', $scene_children->get('filter_scene')->sceneArray())
	@endcomponent
{!! FastDevCtrl::modal('filter-form')->close() !!}

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-12 text-right">
			<div class="btn-group btn-group-sm">
				{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->open() !!}
					{!! FastDevCtrl::button('btn-show-filter')->title('Фильтр')->iconBefore('glyphicon glyphicon-filter')->style( (FastDevHlpr::array_is_empty($filter)) ? '' : 'success' )->out() !!}
					{!! FastDevCtrl::button('btn-cancel-select')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->out() !!}
				{!! FastDevCtrl::buttonGroup('taskbar')->close() !!}
			</div>
		</div>
	</div>
	
	<div class="row list-hdr">
		<div class="col-sm-2 visible-xs">Сортировка: </div>
		<div class="col-sm-2">{!! FastDevCtrl::sortableLink('id', $model, $ordering)->out() !!}</div>
		<div class="col-sm-8">{!! FastDevCtrl::sortableLink('name', $model, $ordering)->out() !!}</div>
		<div class="col-sm-2 hidden-xs">&nbsp;</div>
	</div>
	
	@foreach($list as $item)
	<div class="row list-item bottom-border" data-role="list-item" data-item-id = "{{ $item->id }}" data-item-text="{{ $item->toString() }}">
		<div class="col-sm-2">{{ $item->id }}</div>
		<div class="col-sm-8"><a id="link_edit_item" href="">{{ $item->toString() }}</a></div>
		<div class="col-sm-2 text-right">
			{!! FastDevCtrl::buttonGroup('row-actions')->size('sm')->isFormGroup(false)->open() !!}
				{!! FastDevCtrl::button('btn-select-item')->iconBefore('glyphicon glyphicon-ok')->title('Выбрать')->out() !!}
			{!! FastDevCtrl::buttonGroup('row-actions')->close() !!}
		</div>
	</div>
	@endforeach
	
	<div class="row">
		<div class="col-sm-12">
			{{ $list->links() }}
		</div>
	</div>
	
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		{!! FastDevCtrl::textField('call_button_id')->type('hidden')->value($call_button_id)->out() !!}
	</form>

</div></div>
