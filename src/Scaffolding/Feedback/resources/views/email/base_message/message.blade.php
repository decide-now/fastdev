@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('email.base_message.header', ['url' => config('app.url'), 'title' => config('app.name')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('email.base_message.footer')
        @endcomponent
    @endslot
@endcomponent
