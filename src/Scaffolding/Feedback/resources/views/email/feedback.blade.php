@component('email.base_message.message')

<h1>Сообщение формы обратной связи</h1>
<tr>
	<td>
	
<b>Автор:</b> {{ $fb_author }}<br>
<b>Контакт:</b> {{ $fb_contact }}<br>
<b>Тема:</b> {{ $fb_subject }}<br>
<b>Сообщение:</b><br>
{{ $fb_content }}<br>
	
	<br>
	</td>
</tr>

@if ($button)
<tr>
	<td>
@component('mail::button', ['url' => $button->url])
{{ $button->text }}
@endcomponent
	</td>
</tr>
@endif

@endcomponent