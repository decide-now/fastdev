<style>

	/* Feedback */
	
	.feedback-toggle {
		position: fixed;
		bottom: 100px;
		right: 20px;
		color: #fff;
		font-size: 25px;
		background-color: #ccc;
		width: 55px;
		height: 55px;
		text-align: center;
		line-height: 25px;
		border-radius: 50%;
		padding-top: 12px;
		border: 3px solid #fff;
		opacity: .5;
		-webkit-transition: opacity 1s;
    	transition: opacity 1s;
    	z-index: 5;
	}
	.feedback-toggle:hover {
		color: #fff;
		box-shadow: 0 0 10px 3px #777;
		opacity: 1;
	}
	
</style>