<?php
namespace App\Http\Controllers\Tmplt;

use App\ORM\Tmplt;
use DecideNow\Scene\Controllers\SceneBaseController;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TmpltController extends SceneBaseController
{
	protected $model;
/*_Type_specific_field_declare_*/

	public static $default_actions_technology = 'standard';
	public static $actions_technologies = [
		'list_btn_create_item' => 'ajax',
		'list_btn_edit_item' => 'ajax',
	];

	public $page;
	public $item_id;
	protected function paginateExtPrepare($request)
	{
		$this->page = ($request->get('page')) ?: $this->propertyFromSession('page', '1');
		$this->item_id = ($request->get('item_id')) ?: $this->propertyFromSession('item_id', '');
	}
	public function paginateExt($data, $perPage = null)
	{
		$perPage = $perPage ?: $data->getModel()->getPerPage();
		$list_page = ($this->page) ?: '1';
		$item_id = ($this->item_id) ?: '';

		$dt = clone($data);
		$ids = $dt->select($this->model->getTable().'.id');

		$item_key = $ids->pluck('id')->search($item_id);
		if ($item_id && $item_key !== false) {
			$row_num = $item_key + 1;
			$list_page = ceil($row_num / $perPage);
		}
		
		$total_rows = $ids->count();
		$total_pages = ceil($total_rows / $perPage);
		if ($list_page > $total_pages) {
			$list_page = $total_pages;
		}

		return $data
		->paginate($perPage, ['*'], 'page', $list_page)
		->withPath(action($this->methodPath('get')), [], false);
	}
	
	public function __construct($scene_parent = null, $scene_id = '', $scene_id_suffix = '')
	{
		parent::__construct($scene_parent, $scene_id, $scene_id_suffix);
		$this->middleware('auth');
		$this->model = new Tmplt;
		$this->layout = 'layouts.base.base_nest';
	}
	
	protected function itemDefaultFields($request)
	{
		$ret = [
			//'is_published' => true,
		];
/*_Type_specific_item_create_*/
		return $ret;
	}
	
	protected function itemCreate($request)
	{
		$item = new $this->model;
		$item->fill($this->itemDefaultFields($request));
		return $item;
	}
	
	protected function itemFind($request, $item_id = '')
	{
		$item_id = ($item_id) ?: $request->get('item_id');
		$item = $this->model->find($item_id);
		return $item;
	}
	
	protected function itemDelete($request, $item_id = '')
	{
		$item = $this->itemFind($request, $item_id);
		if (!$item) {
			return;
		}
		DB::beginTransaction();
		try {
			$item->delete();
			DB::commit();
			$this->messages->addSuccess('Запись удалена!');
		} catch (QueryException $e) {
			DB::rollBack();
			$this->messages->addError($this->getDatabaseErrorMessage($e));
		}
	}

	protected function multipleDelete($request, $item_ids = [])
	{
		$item_ids = (count($item_ids)) ? $item_ids : $request->get('item_ids', []);
		if (!is_array($item_ids)) {
			$item_ids = [$item_ids];
		}
		$deleted_ids = [];
		foreach ($item_ids as $item_id) {
			$item = $this->itemFind($request, $item_id);
			if (!$item) {
				return;
			}
			
			DB::beginTransaction();
			try {
				$item->delete();
				DB::commit();
				$deleted_ids[] = $item_id;
			} catch (QueryException $e) {
				DB::rollBack();
				$this->messages->addError($this->getDatabaseErrorMessage($e));
			}
		}
		if (count($deleted_ids)) {
			$this->messages->addSuccess('Записи удалены: ' . count($deleted_ids));
		}
	}
	
	protected function itemFromInput($request)
	{
		$itemClass = $this->model->getModelName();
		$item_input = $request->get($itemClass);

		$item = $this->itemFind($request) ?: new $this->model;
		$item->fill($item_input);
		
		return $item;
	}
	
	protected function validationRules($itemClass = '', $data = [])
	{
		$ret = [
/*_RulesList_*/
		];
		return $ret;
	}
	
	protected function itemValidate($request)
	{
		$this->validator = Validator::make(
			$request->all(), 
			$this->validationRules($this->model->getModelName()), 
			[], 
			$this->model->getAliasesWithModelName()
		);
		if ($this->validator->fails()) {
			$this->messages->addError($this->validator->errors()->messages());
			return false;
		}
		return true;
	}
	
	protected function itemSave($request, $item = null)
	{
		$item = $this->itemFromInput($request);
		DB::beginTransaction();
		$item->save();
		DB::commit();
		return $item;
	}

	protected function itemValidateAndSave($request)
	{
		$validated = $this->itemValidate($request);
		if (!$validated) {
			return null;
		}
		return $this->itemSave($request);
	}
	
	
	protected function redirectToItem($item_id = '')
	{
		return $this->redirect()->action(TmpltItemController::methodPath('get'), ['id' => $item_id]);
	}
	
	protected function redirectToList($item_id = '')
	{
		$this->item_id = $item_id;
		$this->propertyToSession('item_id');
		return redirect()->action(TmpltListController::methodPath('get'));
	}

	public function contentErrorRedirect()
	{
		$this->has_content_error = false;
		return $this->redirect()->route('base');
	}
	
	
	protected function checkAccessList($request) {
		if (!$this->model->getPermission('list')) {
			$this->setContentError('Доступ запрещен!');
			return false;
		}
		return true;
	}
	
	protected function checkAccessItem($request, $item_id = '') {
		if (!$item_id && !$this->item) {
			if (!$this->model->getPermission('create')) {
				$this->setContentError('Доступ запрещен!');
				return false;
			} else {
				$this->item = $this->itemCreate($request);
			}
		} else {
			$this->item = ($this->item) ?: $this->itemFind($request, $item_id);
			if (!$this->item) {
				$this->setContentError('Не найдена запись с кодом '.$item_id.'!');
				return false;
			} else {
				if (!$this->item->getPermission('edit')) {
					$this->setContentError('Доступ запрещен!');
					return false;
				}
			}
		}
		return true;
	}
}