<?php
namespace App\Http\Controllers\Tmplt;

use DecideNow\SceneMessage\Traits\SceneHasAlertTrait;
use Illuminate\Http\Request;

class TmpltListController extends TmpltController
{	
	use SceneHasAlertTrait;
	
	public static $scene_key = 'tmplt-list';
	
	public $list;
	public $per_page;
	public $filter;
	public $ordering;

	public $multiple_mode = 0;
	public $state = ['page', 'per_page', ['name' => 'item_id', 'frontend_hidden' => true], 'multiple_mode'/*_Type_specific_field_to_state_*/];
	
	public function childrenDefine()
	{
		$this->childAddAlert();
		$this->childAdd(new TmpltItemController($this, 'tmplt-item-scene'));
		$this->childAdd(new TmpltFilterController($this, 'tmplt-filter-scene'));
	}
	
	public function prepareContent($request, $has_content = true)
	{
		if (!$this->checkAccessList($request)) {
			return false;
		}
		
		$list_data = new $this->model;
		$list_data = $this->model->defaultListQuery($list_data);
/*_Type_specific_add_list_query_*/
		
		$list_data = $this->model->applySessionFilter($list_data, $this->filter);
		$list_data = $this->model->applySessionOrdering($list_data, $this->ordering);

		$this->paginateExtPrepare($request);
		$this->list = $this->paginateExt($list_data, $this->per_page);
		
		$this->has_content = $has_content;
		return true;
	}

	public function prepareResponse($request, $has_content = true)
	{
		if ($has_content) {
			$this->prepareContent($request, $has_content);
		}
		return $this->sceneResponse();
	}
	
	public function get(Request $request)
	{
		$this->prepareGet($request);
		$this->page = ($request->get('page', $this->page));
		return $this->prepareResponse($request);
	}
	
	public function post(Request $request)
	{
		$this->preparePost($request);
		
		if ($this->task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
		} elseif ($this->task == 'ordering') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->orderingPutToSession($request, '', $force_clear);
		} elseif ($this->task == 'create') {
			return $this->redirectToItem();
		} elseif ($this->task == 'edit') {
			$item_id = $request->get('item_id');
			return $this->redirectToItem($item_id);
		} elseif ($this->task == 'delete') {
			$this->itemDelete($request);
		} elseif ($this->task == 'multiple_mode_toggle') {
			$this->multiple_mode = ($this->multiple_mode) ? 0 : 1;
		} elseif ($this->task == 'multiple_delete') {
			$this->multipleDelete($request);
		}
		
		return $this->prgRedirect(['page' => $this->page]) ?: $this->prepareResponse($request);
	}
}