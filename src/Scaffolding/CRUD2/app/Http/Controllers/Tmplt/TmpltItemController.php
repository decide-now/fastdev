<?php
namespace App\Http\Controllers\Tmplt;

use DecideNow\SceneMessage\Traits\SceneHasAlertTrait;
use Illuminate\Http\Request;

class TmpltItemController extends TmpltController
{
	use SceneHasAlertTrait;
	
	public static $scene_key = 'tmplt-item';
	
	public $item;
	public $item_id;
	public $is_changed = 0;
	public $state = [['name' => 'item', 'frontend_hidden' => true], 'is_changed', 'item_id'/*_Type_specific_field_to_state_*/];
	
	public function childrenDefine()
	{
		$this->childAddAlert();
	}
	
	public function prepareContent($request, $item_id, $has_content = true)
	{
		if (!$this->checkAccessItem($request, $item_id)) {
			return false;
		}
		
		$this->item_id = $this->item->id;

		$this->has_content = $has_content;
		return true;
	}

	public function prepareResponse($request, $item_id, $has_content = true)
	{
		if ($has_content) {
			$this->prepareContent($request, $item_id, $has_content);
		}
		return $this->sceneResponse();
	}
	
	public function get(Request $request, $item_id = '')
	{
		$this->prepareGet($request);
		return $this->prepareResponse($request, $item_id);
	}
	
	public function post(Request $request, $item_id = '')
	{
		$this->preparePost($request);
		
		$response_has_content = true;

		if ($this->task == 'create') {
			//
		} elseif ($this->task == 'edit') {
			$this->item = $this->itemFind($request);
		} elseif ($this->task == 'cancel') {
			$this->item = $this->itemFind($request);
			$item_id = ($this->item) ? $this->item->id : '';
			$response_has_content = false;
			if ($this->scene_technology !== 'ajax') {
				return $this->redirectToList($item_id);
			}
		} elseif ($this->task == 'ok') {
			$this->item = $this->itemValidateAndSave($request);
			if ($this->item === null) {
				$this->item = $this->itemFromInput($request);
			} else {
				$item_id = ($this->item) ? $this->item->id : '';
				$this->is_changed = 1;
				$response_has_content = false;
				if ($this->scene_technology !== 'ajax') {
					return $this->redirectToList($item_id);
				}
			}
		} elseif ($this->task == 'save') {
			$this->item = $this->itemValidateAndSave($request);
			if ($this->item === null) {
				$this->item = $this->itemFromInput($request);
			} else {
				$this->is_changed = 1;
			}
		}
		
		if ($item_id && !$this->item) {
			$this->item = $this->itemFind($request, $item_id);
		}
		if (!$this->item_id && $this->item) {
			$this->item_id = $this->item->id;
		}

		return $this->prgRedirect(['id' => $this->item_id]) ?: $this->prepareResponse($request, ($this->item) ? $this->item->id : '', $response_has_content);
	}
	
}