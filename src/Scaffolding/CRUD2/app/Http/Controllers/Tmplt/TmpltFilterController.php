<?php
namespace App\Http\Controllers\Tmplt;

use Illuminate\Http\Request;

class TmpltFilterController extends TmpltController
{	
	public static $scene_key = 'tmplt-filter';
	
	public $filter;
	public $filter_key = '';
	
	public $state = ['filter_key'];
	
	public function prepareContent($request, $has_content = true)
	{
		$this->filter = $this->model->filterGetFromSession();

		$this->has_content = $has_content;
		return true;
	}

	public function prepareResponse($request, $has_content = true)
	{
		if ($has_content) {
			$this->prepareContent($request, $has_content);
		}
		return $this->sceneResponse();
	}
	
	public function get(Request $request)
	{
		$this->prepareGet($request);
		return $this->prepareResponse($request);
	}
	
	public function post(Request $request)
	{
		$this->preparePost($request);
		
		if ($this->task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
		}
		return $this->prgRedirect() ?: $this->prepareResponse($request);
	}	
}