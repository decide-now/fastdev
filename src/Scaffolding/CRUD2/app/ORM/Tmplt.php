<?php

namespace App\ORM;

use DecideNow\SceneCrud\ORM\ORMModel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class Tmplt extends ORMModel
{
	protected $table = 'tmplt';
	protected $fillable = [
		/*_FillableList_*/
	];
	
	public static function getAliases()
	{
		return [
/*_AliasList_*/
		] + parent::getAliases();
	}
	
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ /*_FilterList_*/ ];
	}
	
	public static function applyFilter($query, $filter_data)
	{
/*_FilterActions_*/
		$query = parent::applyFilter($query, $filter_data);
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return [ /*_OrderingList_*/ ] + parent::getOrderingFields();
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
/*_OrderingActions_*/
		$query = parent::applyOrdering($query, $ordering_data);
		
		return $query;
	}
	
	
	/* relations */
	
/*_Type_specific_model_relation_*/
	
	/* list queries */
	
	public static function defaultListQuery($query = null)
	{
		$query = parent::defaultListQuery($query);
		//$query = $query->select('tmplt.*')->distinct();
		return $query;
	}
/*_Type_specific_model_list_query_*/
	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if ($param == 'id') {
				if (Auth::user()->userRole->code == 'admin') {
					//return true;
					return false;
				}
				return false;
			}
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
}
