@include('scene-message::alert')

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-8">
			<h3>
				{{ $item::getAlias('record_name') }}
/*_Type_specific_item_parent_header_*/
			</h3>
		</div>
		<div class="col-sm-4 text-right">
		{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
			{!! FastDevCtrl::button('btn-ok-item')->title('Сохранить и выйти')->iconBefore('glyphicon glyphicon-ok')->action('item_btn_ok_item', $scene)->out() !!}
			{!! FastDevCtrl::button('btn-save-item')->title('Сохранить')->iconBefore('glyphicon glyphicon-floppy-save')->action('item_btn_save_item', $scene)->out() !!}
			{!! FastDevCtrl::button('btn-cancel-item')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->action('item_btn_cancel_item', $scene)->out() !!}
			{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->action('item_btn_refresh', $scene)->out() !!}
		{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
		</div>
	</div>
			
	<form method="post" action="" data-role="data_form">
		@include('scene::state')
		<div class="row">
/*_TextFields_*/
		</div>
	</form>
		
</div></div>