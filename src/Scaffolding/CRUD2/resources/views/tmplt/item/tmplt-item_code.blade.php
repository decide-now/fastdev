<script>

new DecideNowObjects.SceneClass('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function () {
		var thisScene = this;
		thisScene.defaultInit();
		
		thisScene.getElement('[id="btn_ok_item"]').click( { thisScene: this }, thisScene.btnOkItem_Click );
		thisScene.getElement('[id="btn_save_item"]').click( { thisScene: this }, thisScene.btnSaveItem_Click );
		thisScene.getElement('[id="btn_cancel_item"]').click( { thisScene: this }, thisScene.btnCancelItem_Click );
		thisScene.getElement('[id="btn_refresh"]').click( { thisScene: this }, thisScene.btnRefresh_Click );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		var thisScene = this;
		
		var refresh_url = '{{ url($scene->sceneURL()) }}';
		var item_id = ('item_id' in data) ? data.item_id : thisScene.getElement('[id="item_id"]').val();
		item_id = (item_id) ? ('/' + item_id) : '';
		
		thisScene.defaultSceneRefresh(refresh_url + item_id, data, thisScene.afterSuccessAJAX);
	},
	
	afterSuccessAJAX : function(response) {
		thisScene = response.thisScene;
		if (!response.content) {
			var parentScene = DecideNowObjects.stage.getScene('{{ ($scene_parent) ? $scene_parent->sceneId() : '' }}');
			if (parentScene && parentScene.itemReturn) {
				parentScene.itemReturn({ is_changed: response.is_changed, item_id: response.item_id, scene: thisScene });
			}
		}
	},
	
	/**/
	
	btnOkItem_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var data = { form: { task: 'ok' } };
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
	btnSaveItem_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var data = { form: { task: 'save' } };
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
	btnCancelItem_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var data = { form: { task: 'cancel' } };
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.sceneRefresh(data);
		return false;
		
	},
	
	btnRefresh_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var data = {};
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
}).init();

</script>