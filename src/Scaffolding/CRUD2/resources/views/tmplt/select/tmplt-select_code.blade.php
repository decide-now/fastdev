<script>

new DecideNowObjects.SceneClass('{{ $scene->sceneId() }}', {

	/* init */

	init : function () {
		var thisScene = this;
		thisScene.defaultInit();
		
		thisScene.getElement('[id="btn_refresh"]').click( { thisScene: this }, thisScene.btnRefresh_Click );
		thisScene.getElement('[id="btn_cancel_select"]').click( { thisScene: this }, thisScene.btnCancelSelect_Click );
		thisScene.getElement('[id="btn_show_filter"]').click( { thisScene: this }, thisScene.btnShowFilter_Click );

		thisScene.getElement('[id^="btn_select_item"]').click( { thisScene: this }, thisScene.btnSelectItem_Click );
		thisScene.getElement('[id^="link_edit_item"]').click( { thisScene: this }, thisScene.linkEditItem_Click );

		thisScene.getElement('.pagination a').click( { thisScene: this }, thisScene.pagination_Click );
		thisScene.getElement('[data-role="sortable-header"]').click( { thisScene: this }, thisScene.hdrSortable_Click );
	},


	/* refresh */

	sceneRefresh : function (data) {
		var thisScene = this;
		
		var refresh_url = '{{ url($scene->sceneURL()) }}';
		
		thisScene.defaultSceneRefresh(refresh_url, data);
	},

	
	/**/

	parentScene_selectReturn : function (data) {
		var thisScene = this;
		var caller_id = thisScene.getElement('[id="caller_id"]').val();
		var parentScene = DecideNowObjects.stage.getScene('{{ ($scene_parent) ? $scene_parent->sceneId() : '' }}');
		if (parentScene && parentScene.selectReturn) {
			data.scene = thisScene;
			data.caller_id = caller_id,
			parentScene.selectReturn(data);
		}
	},
	
	getListItemData : function (el) {
		var list_item = $(el).closest('[data-role="list-item"]');
		var item_id_val = list_item.data('item-id');
		var item_text_val = list_item.data('item-text');
		return { id: item_id_val, text: item_text_val, }
	},

	btnCancelSelect_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		thisScene.parentScene_selectReturn({});
		return false;
	},

	btnSelectItem_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var item_data = thisScene.getListItemData(this);
		var data = { item_id: item_data.id, item_text: item_data.text, };
		
		thisScene.parentScene_selectReturn(data);
		return false;
	},

	linkEditItem_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var item_data = thisScene.getListItemData(this);
		var data = { item_id: item_data.id, item_text: item_data.text };
		
		thisScene.parentScene_selectReturn(data);
		return false;
	},


	/* pagination */
	
	pagination_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var data = { url: e.target.href, }
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.sceneRefresh(data);
		return false;
	},

	
	/* ordering */

	hdrSortable_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var data_field = $(this).attr('data-field');
		var data_direction = $(this).attr('data-direction');
		data_direction = ( (data_direction == null || data_direction == 'desc') ? 'asc' : 'desc' );

		var data = { form: { task: 'ordering', } };
		data.form['ordering[' + data_field + ']'] = data_direction;
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
	
	/* filter */

	btnShowFilter_Click : function(e) {
		e.preventDefault();

		var filterScene = DecideNowObjects.stage.getScene('{{ $scene->childGet("tmplt-filter-scene")->sceneId() }}');
		filterScene.modalShow({}, {position: 'right'});

		return false;
	},

	filterReturn : function(data) {
		var thisScene = this;

		var filterScene = data.scene;
		delete data['scene'];
		filterScene.modalHide();
		
		if (!$.isEmptyObject(data)) {
			data.form = ('form' in data) ? data.form : {};
			data.form.task = 'filter';
			data.technology = '{{ $scene->getActionTechnology("list_sortable_header") }}';
			thisScene.sceneRefresh(data);
		}
	},
	
}).init();

</script>