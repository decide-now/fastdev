@include('scene-message::alert')
@include('scene::content', $scene->childGet('tmplt-filter-scene')->sceneVariables())

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-8">
			<h3>{{ $model::getAlias('table_name') }}</h3>
		</div>
		<div class="col-sm-4 text-right">
			<div class="btn-group btn-group-sm">
				{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
					{!! FastDevCtrl::button('btn-show-filter')->title('Фильтр')->iconBefore('glyphicon glyphicon-filter')->style( (FastDevHlpr::array_is_empty($filter)) ? '' : 'success' )->out() !!}
					{!! FastDevCtrl::button('btn-cancel-select')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->out() !!}
				{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
			</div>
		</div>
	</div>
	
	<div class="row list-hdr">
		<div class="col-sm-2 visible-xs">Сортировка: </div>
		@if ($model->getPermission('list', 'id'))
			<div class="col-sm-2">{!! FastDevCtrl::sortableLink('id', $model, $ordering)->action('select_sortable_header', $scene)->out() !!}</div>
		@endif
		<div class="col-sm-{{ ($model->getPermission('list', 'id')) ? '8' : '10' }}">{!! FastDevCtrl::sortableLink('name', $model, $ordering)->action('select_sortable_header', $scene)->out() !!}</div>
		<div class="col-sm-2 hidden-xs">&nbsp;</div>
	</div>
	
	@foreach($list as $item)
	<div class="row list-item bottom-border" data-role="list-item" data-item-id = "{{ $item->id }}" data-item-text="{{ $item->toString() }}">
		@if ($model->getPermission('list', 'id'))
			<div class="col-sm-2">{{ $item->id }}</div>
		@endif
		<div class="col-sm-{{ ($model->getPermission('list', 'id')) ? '8' : '10' }}">
			{!! $item->tree_level_string !!}
			<a id="link_edit_item" href="">{{ $item->toString() }}</a>
		</div>
		<div class="col-sm-2 text-right">
			{!! FastDevCtrl::buttonGroup('row-actions')->size('sm')->open() !!}
				{!! FastDevCtrl::button('btn-select-item')->iconBefore('glyphicon glyphicon-ok')->title('Выбрать')->out() !!}
			{!! FastDevCtrl::buttonGroup('row-actions')->close() !!}
		</div>
	</div>
	@endforeach
	
	<div class="row">
		<div class="col-sm-12">
			{{ $list->links('scene-crud-views::pagination.default', ['scene' => $scene, 'view_tag' => 'select']) }}
		</div>
	</div>

</div></div>

<form data-role="data_form">
	@include('scene::state')
</form>
