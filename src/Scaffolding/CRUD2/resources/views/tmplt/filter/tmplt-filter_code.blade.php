<script>

new DecideNowObjects.SceneClass('{{ $scene->sceneId() }}', {

	/* init */

	init : function () {
		var thisScene = this;
		thisScene.defaultInit();
		
		thisScene.getElement('[id="btn_cancel_filter"]').click( { thisScene: this }, thisScene.btnCancelFilter_Click );
		thisScene.getElement('[id="btn_ok_filter"]').click( { thisScene: this }, thisScene.btnOkFilter_Click );
		thisScene.getElement('[id="btn_clear_filter"]').click( { thisScene: this }, thisScene.btnClearFilter_Click );
		thisScene.getRoot().on( 'keypress', { thisScene: this }, thisScene.keyPress );
	},
	
	
	/* refresh */
	
	sceneRefresh : function (data) {
		var thisScene = this;
		
		var refresh_url = '{{ url($scene->sceneURL()) }}';
		
		thisScene.defaultSceneRefresh(refresh_url, data);
	},
	
	
	/**/
	
	parentScene_filterReturn : function (data) {
		var thisScene = this;
		var parentScene = DecideNowObjects.stage.getScene('{{ ($scene_parent) ? $scene_parent->sceneId() : '' }}');
		if (parentScene && parentScene.filterReturn) {
			data.scene = thisScene;
			parentScene.filterReturn(data);
		}
	},

	btnCancelFilter_Click : function( e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		thisScene.parentScene_filterReturn({});
		return false;
	},

	btnOkFilter_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var form_data = thisScene.getFormData();
		
		thisScene.parentScene_filterReturn({ form: form_data });
		return false;
	},

	btnClearFilter_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var form_data = thisScene.getFormData({}, true);

		thisScene.parentScene_filterReturn({ form: form_data });
		return false;
	},

	keyPress : function (e) {
		var thisScene = e.data.thisScene;

		if(e.which == 13) {
			e.preventDefault();
			thisScene.btnOkFilter_Click(e);
			return false;
		}
		
		return true;
	},
	
}).init();

</script>
