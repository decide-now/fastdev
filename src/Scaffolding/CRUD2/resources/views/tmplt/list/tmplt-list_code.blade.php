<script>

new DecideNowObjects.SceneClass('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function () {
		var thisScene = this;
		thisScene.defaultInit();
		
		thisScene.getElement('[id^="btn_per_page"]').click( { thisScene: this }, thisScene.btnPerPage_Click );
		thisScene.getElement('[id="btn_refresh"]').click( { thisScene: this }, thisScene.btnRefresh_Click );
		thisScene.getElement('[id="btn_create_item"]').click( { thisScene: this }, thisScene.btnCreateItem_Click );
		thisScene.getElement('[id="btn_show_filter"]').click( { thisScene: this }, thisScene.btnShowFilter_Click );

		thisScene.getElement('[id="btn_multiple_mode_toggle"]').click( { thisScene: this }, thisScene.btnMultipleModeToggle_Click );
		thisScene.getElement('[id="btn_multiple_check"]').click( { thisScene: this, state: '1' }, thisScene.btnMultipleCheck_Click );
		thisScene.getElement('[id="btn_multiple_uncheck"]').click( { thisScene: this, state: '0' }, thisScene.btnMultipleCheck_Click );

		thisScene.getElement('[id^="btn_multiple_delete"]').click( { thisScene: this }, thisScene.btnMultipleDelete_Click );

		thisScene.getElement('[id^="btn_edit_item"]').click( { thisScene: this }, thisScene.btnEditItem_Click );
		thisScene.getElement('[id^="btn_delete_item"]').click( { thisScene: this }, thisScene.btnDeleteItem_Click );
		thisScene.getElement('[id^="link_edit_item"]').click( { thisScene: this }, thisScene.linkEditItem_Click );
		thisScene.getElement('[id^="btn_check_item"]').click( { thisScene: this }, thisScene.btnCheckItem_Click );

		thisScene.getElement('.pagination a').click( { thisScene: this }, thisScene.pagination_Click );
		thisScene.getElement('[data-role="sortable-header"]').click( { thisScene: this }, thisScene.hdrSortable_Click );
	},
	
	
	/* refresh */
	
	sceneRefresh : function (data) {
		var thisScene = this;
		
		var refresh_url = '{{ url($scene->sceneURL()) }}';
		
		thisScene.defaultSceneRefresh(refresh_url, data);
	},
	
	itemRefresh : function(data) {
		var thisScene = this;
		if (data.technology && data.technology == 'ajax') {
			if ('form' in data) {
				data.form.is_nested = '1';
			}
			var itemScene = DecideNowObjects.stage.getScene('{{ $scene->childGet("tmplt-item-scene")->sceneId() }}');
			itemScene.modalShow(data, {size: 'lg', position: 'right'});
		} else {
			thisScene.sceneRefresh(data);
			return false;
		}
	},
	

	/**/
	
	getListItemData : function (el) {
		var list_item = $(el).closest('[data-role="list-item"]');
		var item_id_val = list_item.data('item-id');
		var item_text_val = list_item.data('item-text');
		return { id: item_id_val, text: item_text_val, }
	},
	
	btnCreateItem_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var data = { form: { task: 'create' } };
/*_Type_specific_parent_id_to_post_*/
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.itemRefresh(data);
		return false;
	},

	btnEditItem_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;

		var item_data = thisScene.getListItemData(this);
		var data = { form: { task: 'edit', item_id: item_data.id } };
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.itemRefresh(data);
		return false;
	},

	linkEditItem_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;

		var item_data = thisScene.getListItemData(this);
		var data = { form: { task: 'edit', item_id: item_data.id } };
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.itemRefresh(data);
		return false;
	},

	btnDeleteItem_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		if (confirm("Удалить запись?")) {
			var item_data = thisScene.getListItemData(this);

			var data = { form: { task: 'delete', item_id: item_data.id } };
			data = thisScene.addActionTechnology(e.currentTarget, data);
			
			thisScene.sceneRefresh(data);
		}
		return false;
	},


	btnPerPage_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var per_page = $(e.currentTarget).data('count');
		var data = { form: { per_page: per_page } };
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.sceneRefresh(data);
		return false;
	},

	btnRefresh_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var data = {};
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
	itemReturn : function(data) {
		var thisScene = this;
		
		var itemScene = data.scene;
		itemScene.modalHide();
		
		if (data.is_changed == '1') {
			var list_data = {form: {}};
			list_data.form.item_id = data.item_id;
			list_data.technology = '{{ $scene->getActionTechnology('') }}';
			thisScene.sceneRefresh(list_data);
		}
	},
	
	/* pagination */
	
	pagination_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var data = { url: e.target.href, }
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.sceneRefresh(data);
		return false;
	},

	
	/* ordering */

	hdrSortable_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var data_field = $(this).attr('data-field');
		var data_direction = $(this).attr('data-direction');
		data_direction = ( (data_direction == null || data_direction == 'desc') ? 'asc' : 'desc' );

		var data = { form: { task: 'ordering', } };
		data.form['ordering[' + data_field + ']'] = data_direction;
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
	
	/* filter */

	btnShowFilter_Click : function(e) {
		e.preventDefault();

		var filterScene = DecideNowObjects.stage.getScene('{{ $scene->childGet("tmplt-filter-scene")->sceneId() }}');
		filterScene.modalShow({}, {size: 'md', position: 'right'});

		return false;
	},

	filterReturn : function(data) {
		var thisScene = this;

		var filterScene = data.scene;
		delete data['scene'];
		filterScene.modalHide();
		
		if (!$.isEmptyObject(data)) {
			data.form = ('form' in data) ? data.form : {};
			data.form.task = 'filter';
			data.technology = '{{ $scene->getActionTechnology('') }}';
			thisScene.sceneRefresh(data);
		}
	},


	/* multiple */

	btnMultipleModeToggle_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var data = { form: { task: 'multiple_mode_toggle' } };
		data = thisScene.addActionTechnology(e.currentTarget, data);
		
		thisScene.sceneRefresh(data);
		return false;
	},

	toggleCheckState : function (target, state) {
		var is_checked = target.data('is-checked');
		if (state) {
			is_checked = state;
		} else {
			is_checked = (is_checked == '1' ? '0' : '1')
		}
		$(target)
			.data('is-checked', is_checked)
			.attr('data-is-checked', is_checked)
			.find('.far')
			.removeClass('fa-square')
			.removeClass('fa-check-square')
			.addClass((is_checked == '1' ? 'fa-check-square' : 'fa-square'));
	},

	multipleGetChecked : function () {
		var thisScene = this;
		var ids = [];

		thisScene.getElement('[data-role="list-item"] [id="btn_check_item"][data-is-checked="1"]').each(function (e) {
			var id = $(this).closest('[data-role="list-item"]').data('item-id');
			ids.push(id);
		}, ids);

		return ids;
	},

	btnCheckItem_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		thisScene.toggleCheckState($(e.currentTarget));

		return false;
	},

	btnMultipleCheck_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		var state = e.data.state;
		
		thisScene.getElement('[data-role="list-item"] [id="btn_check_item"]').each(function (e) {
			thisScene.toggleCheckState($(this), state);
		});
		
		return false;
	},

	btnMultipleDelete_Click : function (e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		
		var ids = thisScene.multipleGetChecked();
		if (confirm("Удалить записи: " + ids.length + "?")) {

			var data = { form: { task: 'multiple_delete', item_ids: ids } };
			data = thisScene.addActionTechnology(e.currentTarget, data);
			
			thisScene.sceneRefresh(data);
		}
		
		return false;
	},

}).init();

</script>