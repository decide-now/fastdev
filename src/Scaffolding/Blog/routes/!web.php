// Blog

// BlogCategory

Route::get('/blog-category', 'BlogCategory\BlogCategoryListController@get');
Route::post('/blog-category', 'BlogCategory\BlogCategoryListController@post');
Route::get('/blog-category/item/{id?}', 'BlogCategory\BlogCategoryItemController@get');
Route::post('/blog-category/item/{id?}', 'BlogCategory\BlogCategoryItemController@post');
Route::get('/blog-category/select', 'BlogCategory\BlogCategorySelectController@get');
Route::post('/blog-category/select', 'BlogCategory\BlogCategorySelectController@post');
Route::get('/blog-category/filter', 'BlogCategory\BlogCategoryFilterController@get');
Route::post('/blog-category/filter', 'BlogCategory\BlogCategoryFilterController@post');

// BlogPostStatus

Route::get('/blog-post-status', 'BlogPostStatus\BlogPostStatusListController@get');
Route::post('/blog-post-status', 'BlogPostStatus\BlogPostStatusListController@post');
Route::get('/blog-post-status/item/{id?}', 'BlogPostStatus\BlogPostStatusItemController@get');
Route::post('/blog-post-status/item/{id?}', 'BlogPostStatus\BlogPostStatusItemController@post');
Route::get('/blog-post-status/select', 'BlogPostStatus\BlogPostStatusSelectController@get');
Route::post('/blog-post-status/select', 'BlogPostStatus\BlogPostStatusSelectController@post');
Route::get('/blog-post-status/filter', 'BlogPostStatus\BlogPostStatusFilterController@get');
Route::post('/blog-post-status/filter', 'BlogPostStatus\BlogPostStatusFilterController@post');

// BlogPost

Route::get('/blog-post', 'BlogPost\BlogPostListController@get');
Route::post('/blog-post', 'BlogPost\BlogPostListController@post');
Route::get('/blog-post/item/{id?}', 'BlogPost\BlogPostItemController@get');
Route::post('/blog-post/item/{id?}', 'BlogPost\BlogPostItemController@post');
Route::get('/blog-post/select', 'BlogPost\BlogPostSelectController@get');
Route::post('/blog-post/select', 'BlogPost\BlogPostSelectController@post');
Route::get('/blog-post/filter', 'BlogPost\BlogPostFilterController@get');
Route::post('/blog-post/filter', 'BlogPost\BlogPostFilterController@post');

// BlogTag

Route::get('/blog-tag', 'BlogTag\BlogTagListController@get');
Route::post('/blog-tag', 'BlogTag\BlogTagListController@post');
Route::get('/blog-tag/item/{id?}', 'BlogTag\BlogTagItemController@get');
Route::post('/blog-tag/item/{id?}', 'BlogTag\BlogTagItemController@post');
Route::get('/blog-tag/select', 'BlogTag\BlogTagSelectController@get');
Route::post('/blog-tag/select', 'BlogTag\BlogTagSelectController@post');
Route::get('/blog-tag/filter', 'BlogTag\BlogTagFilterController@get');
Route::post('/blog-tag/filter', 'BlogTag\BlogTagFilterController@post');

// BlogPostTag

Route::get('/blog-post-tag', 'BlogPostTag\BlogPostTagListController@get');
Route::post('/blog-post-tag', 'BlogPostTag\BlogPostTagListController@post');
Route::get('/blog-post-tag/item/{id?}', 'BlogPostTag\BlogPostTagItemController@get');
Route::post('/blog-post-tag/item/{id?}', 'BlogPostTag\BlogPostTagItemController@post');
Route::get('/blog-post-tag/select', 'BlogPostTag\BlogPostTagSelectController@get');
Route::post('/blog-post-tag/select', 'BlogPostTag\BlogPostTagSelectController@post');
Route::get('/blog-post-tag/filter', 'BlogPostTag\BlogPostTagFilterController@get');
Route::post('/blog-post-tag/filter', 'BlogPostTag\BlogPostTagFilterController@post');
Route::get('/blog-post-tag/complete', 'BlogPostTag\BlogPostTagListController@complete');

// BlogTemplate

Route::get('/blog', 'BlogTemplate\BlogTemplateController@get');
Route::get('/blog/category/{category_id}', 'BlogTemplate\BlogTemplateController@get');
Route::get('/post/{post_id}', 'BlogTemplate\BlogTemplateItemController@get');
