<div class="row space-bottom-2"><div class="col-sm-{{ $show_categories ? '9' : '12' }}">

	@if($show_crumbs)
		<div class="row"><div class="col-sm-12">
			@include('blog_template.elements.crumbs')
		</div></div>
	@endif
	
	<div class="row"><div class="col-sm-12">
		@if($blog_category)
			<h1 class="underlined text-center">{{ $blog_category->name }}</h1>
			<div>{!! $blog_category->description !!}</div>
		@else
			<h1 class="underlined text-center">Блог</h1>
		@endif
	</div></div>

	@foreach($list as $item)
		
		<div class="row"><div class="col-sm-12">
			<div class="row"><div class="col-sm-12"><h2>{{ $item->toString() }}</h2></div></div>
			<div class="row meta-info">
				@if ($item->user)
					<div class="col-sm-8">
						Автор: {{ $item->user->toString() }}
					</div>
				@endif
				<div class="col-sm-4 col-sm-offset-{{ 0 + (($item->user) ? 0 : 8) }} text-right">
					Опубликовано: {{ FastDevHlpr::dateFormat($item->published_at, 'DMY') }}
				</div>		
			</div>
			<div class="row">
				@if ($item->mainimage)
					<div class="col-sm-3">
						<img class="img-responsive img-postimage in-list" src="{{ $item->mainimage->getUrl() }}">
					</div>
				@endif
				<div class="col-sm-{{ ($item->mainimage) ? 9 : 12 }}">
					<div class="row"><div class="col-sm-12">
						{!! htmlspecialchars_decode($item->content_short) !!}
					</div></div>
					<div class="row">
						<div class="col-sm-6">
							<a href="{{ url($path_item.'/'.$item->code) }}" class="btn btn-default btn-lg">Читать полностью</a>
						</div>
						<div class="col-sm-6 text-right">
							@foreach($item->blogTags as $tag)
								<div class="tag-item">
									<a href="{{ url($path_base.'?tags[]='.$tag->toString()) }}">
										<i class="glyphicon glyphicon-tag"></i> {{ $tag->toString() }}
									</a>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div></div>
		
		@if (!$loop->last)
			<div class="row"><div class="col-sm-12"><hr></div></div>
		@endif
		
	@endforeach
	
	<div class="row">
		<div class="col-md-12">
			{{ $list->links() }}
		</div>
	</div>

	
</div>

@if ($show_categories)
	<div class="col-sm-3">
		@include('blog_template.elements.categories')
	</div>
@endif

</div>

		