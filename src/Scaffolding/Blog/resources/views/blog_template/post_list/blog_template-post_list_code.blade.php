<script>

new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;
	},

	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ $scene->updateUrl($blog_category ? $blog_category->code : '') }}', 'refresh');
	},
	
}).init();

</script>
