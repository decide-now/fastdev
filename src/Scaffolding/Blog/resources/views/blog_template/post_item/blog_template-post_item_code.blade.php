<script>

new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;
	},

	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ $scene->updateUrl($item->code) }}', 'refresh');
	},
	
}).init();

</script>
