
<div class="row"><div class="col-sm-{{ $show_categories ? '9' : '12' }}">

	@if($show_crumbs)
		<div class="row"><div class="col-sm-12">
			@include('blog_template.elements.crumbs')
		</div></div>
	@endif

	<div class="row"><div class="col-sm-12"><h1>{{ $item->toString() }}</h1></div></div>
	<div class="row meta-info">
		@if ($item->blogCategory && $show_category)
			<div class="col-sm-4">Категория: <a href="{{ url('/blog/category/'.$item->blogCategory->code) }}">{{ $item->blogCategory->toString() }}</a></div>
		@endif
		@if ($item->user)
			<div class="col-sm-4 col-sm-offset-{{ ($item->blogCategory) ? 0 : 4 }}">
				Автор: {{ $item->user->toString() }}
			</div>
		@endif
		<div class="col-sm-4 col-sm-offset-{{ ($item->user) ? 0 : (($item->blogCategory) ? 4 : 8) }} text-right">
			Опубликовано: {{ FastDevHlpr::dateFormat($item->published_at, 'DMY') }}
		</div>		
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="row"><div class="col-sm-12">
				@if ($item->show_mainimage && $item->mainimage)
					<img class="img-responsive img-postimage" src="{{ ($item->mainimage) ? $item->mainimage->getUrl() : '' }}">
				@endif
				{!! htmlspecialchars_decode($item->content) !!}
			</div></div>
			<div class="row">
				<div class="col-sm-12 text-right">
					@foreach($item->blogTags as $tag)
						<div class="tag-item">
							<a href="{{ url($path_base.'?tags[]='.$tag->toString()) }}">
								<i class="glyphicon glyphicon-tag"></i>{{ $tag->toString() }}
							</a>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	
	<div class="row space-top-1 space-bottom-1">
		@if($p_item)
		<div class="col-xs-12  col-sm-4 form-group">
			<a href="{{ url($path_item.'/'.$p_item->code) }}" class="btn btn-default btn-block">Предыдущая</a>
		</div>
		@endif
		@if($n_item)
		<div class="col-xs-12 col-sm-4 form-group col-xs-12-offset-0 col-sm-offset-{{ 4 + (($p_item) ? 0 : 4)}}">
			<a href="{{ url($path_item.'/'.$n_item->code) }}" class="btn btn-default btn-block">Следующая</a>
		</div>
		@endif
	</div>
	
</div>

@if ($show_categories)
	<div class="col-sm-3">
		@include('blog_template.elements.categories')
	</div>
@endif
</div>
