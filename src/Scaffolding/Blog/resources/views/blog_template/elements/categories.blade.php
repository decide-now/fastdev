<div class="row space-top-2"><div class="col-sm-10 col-sm-offset-2 left-border">
	<h3>Рубрикатор</h3>
	<ul>
	@foreach($categories as $category)
		<li><a href="{{ url('/blog/category/'.$category->code) }}">{{ $category->name }}</a></li>
	@endforeach
	</ul>
</div></div>