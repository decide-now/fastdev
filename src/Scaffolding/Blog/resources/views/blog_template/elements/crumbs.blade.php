@if ($crumbs->count() > 2)
<div class="row space-top-1"><div class="col-sm-12">
	<ol class="breadcrumb">
	@foreach($crumbs as $crumb)
		<li class="{{ (!$loop->last) ? 'active' : '' }}">
			@if(!$loop->last)
			<a href="{{ url($crumb['url']) }}">
			@endif
				{!! $crumb['name'] !!}
			@if(!$loop->last)
			</a>
			@endif
		</li>
	@endforeach
	</ol>
</div></div>
@endif