@include('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())

{!! FastDevCtrl::modal('filter-form')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('filter_scene')->sceneArray())
{!! FastDevCtrl::modal('filter-form')->close() !!}

{!! FastDevCtrl::modal('item-form')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('item_scene')->sceneArray())
{!! FastDevCtrl::modal('item-form')->close() !!}

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-8">
			{!! ($is_dependant) ? '<h4>' : '<h3>' !!}{{ $model::getAlias('table_name') }}{!! ($is_dependant) ? '</h4>' : '</h3>' !!}
		</div>
		
		<div class="col-sm-4">
			@if ($model->getPermission('create') && $is_dependant)
				{!! FastDevCtrl::textField('new_tag')->buttonAfter('glyphicon glyphicon-plus', 'add', 'Добавить', ['action-tech' => $scene->getActionTechnology('list_btn_create_item')])->size('sm')->out() !!}
			@endif
		</div>
		{{--
		<div class="col-sm-2 text-right">
			<div class="btn-group btn-group-sm">
				{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
					{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->data('action-tech', $scene->getActionTechnology('list_btn_refresh'))->out() !!}
				{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
			</div>
		</div>
		--}}
	</div>
	
	<div class="row list-hdr">
		<div class="col-sm-2 visible-xs">Сортировка: </div>
		<div class="col-sm-10">{!! FastDevCtrl::sortableLink('blog_tag_id', $model, $ordering)->data('action-tech', $scene->getActionTechnology('list_sortable_header'))->out() !!}</div>
		<div class="col-sm-2 hidden-xs">&nbsp;</div>
	</div>
	
	@foreach($list as $item)
	<div class="row list-item bottom-border" data-role="list-item" data-item-id = "{{ $item->id }}" data-item-text="{{ $item->toString() }}">
		<div class="col-sm-10">{{ ($item->blogTag) ? $item->blogTag->toString() : '' }}</div>
		<div class="col-sm-2 text-right">
			{!! FastDevCtrl::buttonGroup('row-actions')->size('sm')->open() !!}
				{{--
				@if ($item->getPermission('edit')) 
					{!! FastDevCtrl::button('btn-edit-item')->iconBefore('glyphicon glyphicon-pencil')->title('Редактировать')->data('action-tech', $scene->getActionTechnology('list_btn_edit_item'))->out() !!}
				@endif
				--}}
				@if ($item->getPermission('delete')) 
					{!! FastDevCtrl::button('btn-delete-item')->iconBefore('glyphicon glyphicon-trash')->title('Удалить')->data('action-tech', $scene->getActionTechnology('list_btn_delete_item'))->out() !!}
				@endif
			{!! FastDevCtrl::buttonGroup('row-actions')->close() !!}
		</div>
	</div>
	@endforeach
	
	<div class="row">
		<div class="col-sm-12">
			{{ $list->links('fastdev::pagination.default', ['scene' => $scene]) }}
		</div>
	</div>
	
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		@include('fastdev::scene_primitives')
		{!! FastDevCtrl::textField('blog_post_id')->type('hidden')->value(!empty($blog_post_id) ? $blog_post_id : '')->out() !!}
	</form>
	
</div></div>
		