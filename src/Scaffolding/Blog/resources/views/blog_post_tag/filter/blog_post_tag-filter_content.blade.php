<div class="row taskbar">
	<div class="col-sm-8">
		<h3>Фильтр</h3>
	</div>
	<div class="col-sm-4 text-right">
		{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
			{!! FastDevCtrl::button('btn-ok-filter')->title('Применить')->iconBefore('glyphicon glyphicon-ok')->out() !!}
			{!! FastDevCtrl::button('btn-cancel-filter')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->out() !!}
			{!! FastDevCtrl::button('btn-clear-filter')->title('Очистить')->iconBefore('glyphicon glyphicon-erase')->out() !!}
		{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
	</div>
</div>
<div class="row"><div class="col-sm-12">
	<form data-role="data_form">
		{{ csrf_field() }}
		@include('fastdev::scene_primitives')
		{!! FastDevCtrl::textField('filter[name]')->label($model::getAlias('name'))->value($filter['name'])->hasClearButton()->out() !!}
	</form>
</div></div>
