@include('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())
{!! FastDevCtrl::modal('select-form')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('blog_tag_select_scene')->sceneArray())
{!! FastDevCtrl::modal('select-form')->close() !!}

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-8">
			<h3>
				{{ $item::getAlias('record_name') }}
				@if ($item->id)
					<span class="small">(
				@endif
				@if ($item->blog_post_id && !$item->id)
					<span class="small">(
				@endif
				@if ($item->blog_post_id)
					{{ $item->blogPost->toString() }}
				@endif
				@if ($item->blog_post_id && !$item->id)
					)</span>
				@elseif ($item->blog_post_id && $item->id)
					|
				@endif

				@if ($item->id)
					id: {{ $item->id }}
				@endif
				
				@if ($item->id)
					)</span>
				@endif
			</h3>
		</div>
		<div class="col-sm-4 text-right">
		{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
			{!! FastDevCtrl::button('btn-ok-item')->title('Сохранить и выйти')->iconBefore('glyphicon glyphicon-ok')->data('action-tech', $scene->getActionTechnology('item_btn_ok_item'))->out() !!}
			{!! FastDevCtrl::button('btn-save-item')->title('Сохранить')->iconBefore('glyphicon glyphicon-floppy-save')->data('action-tech', $scene->getActionTechnology('item_btn_save_item'))->out() !!}
			{!! FastDevCtrl::button('btn-cancel-item')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->data('action-tech', $scene->getActionTechnology('item_btn_cancel_item'))->out() !!}
			{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->data('action-tech', $scene->getActionTechnology('item_btn_refresh'))->out() !!}
		{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
		</div>
	</div>
			
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		@include('fastdev::scene_primitives')
		{!! FastDevCtrl::textField('item_id')->type('hidden')->value($item->id)->out() !!}
		{!! FastDevCtrl::textField('blog_post_id')->value($item->blog_post_id)->type('hidden')->out() !!}
		{!! FastDevCtrl::textField('blog_post_id', $item)->type('hidden')->out() !!}
		<div class="row">
			@if (!$is_dependant)
				<div class="col-sm-6">
					{!! FastDevCtrl::textField('blog_post_id', $item)->out() !!}
				</div>
			@endif
			<div class="col-sm-6">
				{!! FastDevCtrl::textField('blog_tag_id', $item)
					->visibleValue(\App\ORM\BlogTag::idToString(($item->blogTag) ? $item->blogTag->id : null ))
					->hasClearButton()
					->hasSelectButton()
					->out()
				!!}
			</div>

		</div>
	</form>
		
</div></div>