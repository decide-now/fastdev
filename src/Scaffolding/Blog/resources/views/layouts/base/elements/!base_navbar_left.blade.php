...
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
		Блог <span class="caret"></span>
	</a>
	<ul class="dropdown-menu" role="menu">
		<li><a href="{{ url('/blog-post') }}">{{ App\ORM\BlogPost::getAlias('table_name') }}</a></li>
		<li><a href="{{ url('/blog-category') }}">{{ App\ORM\BlogCategory::getAlias('table_name') }}</a></li>
		<li><a href="{{ url('/blog-post-status') }}">{{ App\ORM\BlogPostStatus::getAlias('table_name') }}</a></li>
		<li><a href="{{ url('/blog-tag') }}">{{ App\ORM\BlogTag::getAlias('table_name') }}</a></li>
	</ul>
</li>
...