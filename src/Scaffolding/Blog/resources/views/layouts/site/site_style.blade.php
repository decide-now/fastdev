<style>

	/* Blog */
	
	p.info-block {
		padding: 15px;
	}
	
	.meta-info {
		color: #aaaaaa;
		margin-bottom: 20px;
		font-size: .9em;
	}
	
	.tag-item {
		display: inline-block;
		margin-left: 15px;
		font-size: 12px;
	}
	
	li.tag-item {
		margin-left: 0;
		margin-right: 5px;
	}
	
	.img-postimage {
		margin-bottom: 20px;
	}
	
	@media ( min-width : 768px) {
		.img-postimage {
			float: left;
			margin: 0 20px 20px 0;
			max-width: 20%;
		}
		.img-postimage.in-list {
			max-width: 100%;
		}
	}
	
	 .left-border {
	 	border-left: 1px solid #ddd;
	 }
	 
	 .breadcrumb>li+li:before {
		font-family: 'Glyphicons Halflings';
		content: "\e250";
		font-size: .6em;
	}

</style>