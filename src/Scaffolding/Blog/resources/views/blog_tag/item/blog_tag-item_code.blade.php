<script>

new Scene('{{ $scene->sceneId() }}', {
	
	/* init */
	
	init : function() {
		var thisScene = this;
		
		thisScene.getElement('[id="btn_ok_item"]').click( { thisScene: this }, thisScene.btnOkItem_Click );
		thisScene.getElement('[id="btn_save_item"]').click( { thisScene: this }, thisScene.btnSaveItem_Click );
		thisScene.getElement('[id="btn_cancel_item"]').click( { thisScene: this }, thisScene.btnCancelItem_Click );
		thisScene.getElement('[id="btn_refresh"]').click( { thisScene: this }, thisScene.btnRefresh_Click );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;
		
		var update_url_base = '{{ url($scene->updateURL()) }}';
		var item_id = ('item_id' in data) ? data.item_id : thisScene.getElement('[id="item_id"]').val();
		item_id = (item_id) ? ('/' + item_id) : '';
		
		thisScene.defaultSceneRefresh(data, update_url_base + item_id, '');
	},
	
	afterSuccessAJAX : function(response) {
		thisScene = response.thisScene;
		if (response.page == '') {
			var parentScene = stage.getScene('{{ ($scene_parent) ? $scene_parent->sceneId() : "" }}');
			parentScene.itemReturn({ refresh: response.refresh_parent_scene, item_id: response.item_id, });
		}
	},
	
	
	/**/
	
	btnOkItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { fields: { task: 'ok', }, };
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
	btnSaveItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { fields: { task: 'save', }, };
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
	btnCancelItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { fields: { task: 'cancel', }, };
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
	btnRefresh_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = {};
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
}).init();

</script>