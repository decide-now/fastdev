<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script>

new Scene('{{ $scene->sceneId() }}', {
	
	/* init */
	
	init : function() {
		var thisScene = this;
		
		thisScene.getElement('[id="btn_ok_item"]').click( { thisScene: this }, thisScene.btnOkItem_Click );
		thisScene.getElement('[id="btn_save_item"]').click( { thisScene: this }, thisScene.btnSaveItem_Click );
		thisScene.getElement('[id="btn_cancel_item"]').click( { thisScene: this }, thisScene.btnCancelItem_Click );
		thisScene.getElement('[id="btn_refresh"]').click( { thisScene: this }, thisScene.btnRefresh_Click );

		thisScene.getElement('[id="btn_select_blog_post_blog_post_status_id"]').click( { thisScene: this }, thisScene.btnSelectBlogPostBlogPostStatusId_Click );
		thisScene.getElement('[id="btn_select_blog_post_blog_category_id"]').click( { thisScene: this }, thisScene.btnSelectBlogPostBlogCategoryId_Click );
		thisScene.getElement('[id="btn_select_blog_post_mainimage_id"]').click( { thisScene: this }, thisScene.btnSelectBlogPostMainImageId_Click );

		thisScene.getElement('[id="blog_post_published_at"]').closest('.input-group').datetimepicker(stage.calendar_t_format);

		thisScene.initTinyMCE('[id="blog_post_content"]');
		thisScene.initTinyMCE('[id="blog_post_content_short"]');
	},

	initTinyMCE : function(area_selector) {
		tinymce.remove();
		tinymce.init({
			selector : area_selector,
			content_style : ".img-half-width { max-width: 100%; } @media (min-width: 768px) { .img-half-width { max-width: 50%; } } .img-center { margin-left: auto; margin-right: auto; } ",
			language: 'ru',
			plugins : ['advlist autolink lists link image charmap print preview anchor', 'searchreplace visualblocks codesample code fullscreen', 'insertdatetime media table contextmenu paste'],
			toolbar : 'insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image codesample | code',
			height : 300,
			relative_urls: false,
			file_browser_callback: function(field_name, url, type, win) {
				if (type == 'image') {
					var field_el = $(win.document.getElementById(field_name));
					field_el.closest('[role="dialog"]').css('z-index', 999);
					$('#mce-modal-block').css('z-index', 998);
					stage.getScene('{{ $scene->sceneId() }}').selectTinyMCEImage(field_name);
				}
			},
			image_class_list: [
				{ title: 'Standard', value: '' },
				{ title: 'Responsive', value: 'img-responsive' },
				{ title: 'Responsive + Cener', value: 'img-responsive img-center' },
				{ title: 'Half-width', value: 'img-responsive img-half-width' },
				{ title: 'Half-width + Cener', value: 'img-responsive img-half-width img-center' },
			],
			document_base_url: '{{ env("APP_URL") }}',
			relative_urls : true,
			remove_script_host : false,
			branding: false,
		});
	},

	selectTinyMCEImage : function(field_name) {
		
		var selectScene = stage.getScene('{{ $scene_children->get("file_storage_select_scene")->sceneId() }}');
		var call_button_id_val = field_name;

		var data = { fields: { call_button_id: call_button_id_val, }, ajax: true, };
		
		selectScene.sceneRefresh(data);
		selectScene.getRootElement().closest('.modal').modal('show');
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;
		
		var update_url_base = '{{ url($scene->updateURL()) }}';
		var item_id = ('item_id' in data) ? data.item_id : thisScene.getElement('[id="item_id"]').val();
		item_id = (item_id) ? ('/' + item_id) : '';
		
		thisScene.defaultSceneRefresh(data, update_url_base + item_id, '');
	},
	
	afterSuccessAJAX : function(response) {
		thisScene = response.thisScene;
		if (response.page == '') {
			var parentScene = stage.getScene('{{ ($scene_parent) ? $scene_parent->sceneId() : "" }}');
			parentScene.itemReturn({ refresh: response.refresh_parent_scene, item_id: response.item_id, });
		}
	},


	/* select fields */

	btnSelectBlogPostBlogPostStatusId_Click : function(e) {
		var thisScene = e.data.thisScene;
		thisScene.defaultSelect_Click(e, '{{ $scene_children->get("blog_post_status_select_scene")->sceneId() }}');
	},

	btnSelectBlogPostBlogCategoryId_Click : function(e) {
		var thisScene = e.data.thisScene;
		thisScene.defaultSelect_Click(e, '{{ $scene_children->get("blog_category_select_scene")->sceneId() }}');
	},

	btnSelectBlogPostMainImageId_Click : function(e) {
		var thisScene = e.data.thisScene;
		thisScene.defaultSelect_Click(e, '{{ $scene_children->get("file_storage_select_scene")->sceneId() }}');
	},
	
	/**/
	
	btnOkItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { fields: { task: 'ok', }, };

		var edt1 = tinymce.get('blog_post_content');
		data.fields['blogPost[f_content]'] = edt1.getContent();

		var edt2 = tinymce.get('blog_post_content_short');
		data.fields['blogPost[f_content_short]'] = edt2.getContent();
		
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
	btnSaveItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { fields: { task: 'save', }, };

		var edt1 = tinymce.get('blog_post_content');
		data.fields['blogPost[f_content]'] = edt1.getContent();

		var edt2 = tinymce.get('blog_post_content_short');
		data.fields['blogPost[f_content_short]'] = edt2.getContent();
		
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
	btnCancelItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { fields: { task: 'cancel', }, };
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
	btnRefresh_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = {};
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.sceneRefresh(data);
		return false;
	},
	
}).init();

</script>