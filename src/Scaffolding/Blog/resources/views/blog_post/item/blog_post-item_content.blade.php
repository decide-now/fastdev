@include('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())
{!! FastDevCtrl::modal('select-form')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('blog_post_status_select_scene')->sceneArray())
{!! FastDevCtrl::modal('select-form')->close() !!}
{!! FastDevCtrl::modal('file-storage-select-form')->title('Выбор изображения...')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('file_storage_select_scene')->sceneArray())
{!! FastDevCtrl::modal('file-storage-select-form')->close() !!}
{!! FastDevCtrl::modal('select-form')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('blog_category_select_scene')->sceneArray())
{!! FastDevCtrl::modal('select-form')->close() !!}

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-8">
			<h3>
				{{ $item::getAlias('record_name') }}
				@if ($item->id)
					<span class="small">(
				@endif

				@if ($item->id)
					id: {{ $item->id }}
				@endif
				
				@if ($item->id)
					)</span>
				@endif
			</h3>
		</div>
		<div class="col-sm-4 text-right">
		{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
			{!! FastDevCtrl::button('btn-ok-item')->title('Сохранить и выйти')->iconBefore('glyphicon glyphicon-ok')->data('action-tech', $scene->getActionTechnology('item_btn_ok_item'))->out() !!}
			{!! FastDevCtrl::button('btn-save-item')->title('Сохранить')->iconBefore('glyphicon glyphicon-floppy-save')->data('action-tech', $scene->getActionTechnology('item_btn_save_item'))->out() !!}
			{!! FastDevCtrl::button('btn-cancel-item')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->data('action-tech', $scene->getActionTechnology('item_btn_cancel_item'))->out() !!}
			{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->data('action-tech', $scene->getActionTechnology('item_btn_refresh'))->out() !!}
		{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
		</div>
	</div>
			
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		@include('fastdev::scene_primitives')
		{!! FastDevCtrl::textField('item_id')->type('hidden')->value($item->id)->out() !!}

		<div class="row">
			<div class="col-sm-6">
				{!! FastDevCtrl::textField('title', $item)->out() !!}
			</div>
			<div class="col-sm-3">
				{!! FastDevCtrl::textField('blog_post_status_id', $item)
					->visibleValue(\App\ORM\BlogPostStatus::idToString(($item->blogPostStatus) ? $item->blogPostStatus->id : null ))
					->hasClearButton()
					->hasSelectButton()
					->out()
				!!}
			</div>
			<div class="col-sm-3">
				{!! FastDevCtrl::textField('published_at', $item)
					->value(FastDevHlpr::dateFormat($item->published_at, 'DMYT'))
					->iconAfter('glyphicon glyphicon-time')
					->out()
				!!}
			</div>
			<div class="col-sm-12">
				{!! FastDevCtrl::textArea('content', $item)->open() !!}{{ $item->content }}{!! FastDevCtrl::textArea('content', $item)->close() !!}
			</div>
			<div class="col-sm-12">
				{!! FastDevCtrl::textArea('content_short', $item)->open() !!}{{ $item->content_short }}{!! FastDevCtrl::textArea('content_short', $item)->close() !!}
			</div>
			<div class="col-sm-6">
				{!! FastDevCtrl::textField('blog_category_id', $item)
					->visibleValue(\App\ORM\BlogCategory::idToString(($item->blogCategory) ? $item->blogCategory->id : null ))
					->hasClearButton()
					->hasSelectButton()
					->out()
				!!}
			</div>
			<div class="col-sm-6">
				{!! FastDevCtrl::textField('mainimage_id', $item)
					->visibleValue(\App\ORM\FileStorage::idToString(($item->mainimage) ? $item->mainimage->id : null ))
					->hasClearButton()
					->hasSelectButton()
					->out()
				!!}
			</div>
			<div class="col-sm-3">
				{!! FastDevCtrl::radioButtonList('show_mainimage', $item)->options([0 => __('util.no'), 1 => __('util.yes')])->isJustified()->out() !!}
			</div>
			<div class="col-sm-3">
				{!! FastDevCtrl::textField('code', $item)->out() !!}
			</div>
		</div>
	</form>
		
</div></div>

@if ($item->id)
	<div class="row"><div class="col-sm-12"><hr></div></div>
	@include('fastdev::scene_content', $scene_children->get('blog_post_tag_list_scene')->sceneArray())
@endif