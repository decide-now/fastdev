<?php
namespace App\Http\Controllers\BlogPost;

use Illuminate\Http\Request;

class BlogPostSelectController extends BlogPostController
{	
	static $scene_id = 'blog_post-select';
	
	public $list;
	public $filter;
	public $ordering;

	public $primitives = [
		'task' => ['default' => '', 'is_hidden' => true],
		'call_button_id' => '',
		'page' => '1',
	];
	
	protected function defineChildren()
	{	
		$this->defineAlertChildScene();
		$this->scene_children->put('filter_scene', new BlogPostFilterController($this));
	}
	
	public function prepareContent(Request $request)
	{	
		$list_data = new $this->model;
		$list_data = $this->model->defaultListQuery($list_data);

		$list_data = $this->model->applySessionFilter($list_data, $this->filter);
		$list_data = $this->model->applySessionOrdering($list_data, $this->ordering);
		
		$this->page = ($request->get('page')) ?: $this->getPrimitives('page');
		$this->list = $this->paginateExt($list_data);
		
		$this->no_content = false;
	}

	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->transferPrimitives();

		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferPrimitives($request);

		$task = $this->getPrimitives('task');
		
		if ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
		} elseif ($task == 'ordering') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->orderingPutToSession($request, '', $force_clear);
		}
		return redirect()->action(self::methodPath('get'));
	}
}