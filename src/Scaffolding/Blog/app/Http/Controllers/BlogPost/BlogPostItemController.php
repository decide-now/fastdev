<?php
namespace App\Http\Controllers\BlogPost;

use App\Http\Controllers\BlogCategory\BlogCategorySelectController;
use App\Http\Controllers\BlogPostStatus\BlogPostStatusSelectController;
use App\Http\Controllers\BlogPostTag\BlogPostTagListController;
use App\Http\Controllers\FileStorage\FileStorageCombinedSelectController;
use Illuminate\Http\Request;

class BlogPostItemController extends BlogPostController
{	
	static $scene_id = 'blog_post-item';

	public $primitives = [
		'task' => ['default' => '', 'is_hidden' => true],
		'is_dependant' => '0',
		'refresh_parent_scene' => '0',
	];
	
	public $item;
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
		$this->scene_children->put('blog_post_status_select_scene', new BlogPostStatusSelectController($this));
		$this->scene_children->put('file_storage_select_scene', new FileStorageCombinedSelectController($this));
		$this->scene_children->put('blog_category_select_scene', new BlogCategorySelectController($this));
		$this->scene_children->put('blog_post_tag_list_scene', new BlogPostTagListController($this));
	}
	
	public function prepareContent(Request $request)
	{
		$task = $this->getPrimitives('task');
		$ajax = $this->transferAJAXFlag();
		if ($ajax && ($task == 'ok' || $task == 'cancel')) {
			return;
		}
		if ($this->content_error) {
			return;
		}
		if (!$this->item) {
			$this->item = $this->itemCreate($request);
		}
		if ($this->getPrimitives('is_dependant')) {
			$this->default_actions_tech = 'ajax';
		}
		
		$blog_post_tag_list_scene = $this->scene_children->get('blog_post_tag_list_scene');
		$blog_post_tag_list_scene->blog_post_id = $this->item->id;
		$blog_post_tag_list_scene->setPrimitives('is_dependant', 1);
		$blog_post_tag_list_scene->prepareContent($request);
		
		$this->no_content = false;
	}
	
	public function get(Request $request, $item_id = '')
	{
		$this->transferSceneId();
		$this->transferPrimitives();

		$this->item = $this->checkPermissionsItem($request, $item_id);
		$this->response_data['item_id'] = ($this->item) ? $this->item->id : '';
		
		$task = $this->getPrimitives('task');
		if ($task == 'ok' || $task == 'save') {
			$this->setPrimitives('refresh_parent_scene', '1');
		}
		
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request, $item_id = '')
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferPrimitives($request);

		$task = $this->getPrimitives('task');
		
		$item = null;
		if ($task == 'create') {
			$item = $this->itemCreate($request);
		} elseif ($task == 'edit') {
			$item = $this->itemFind($request);
		} elseif ($task == 'cancel') {
			$item = $this->itemFind($request);
			if (! $request->ajax()) {
				return $this->redirectToList($item);
			}
		} elseif ($task == 'ok') {
			$redirect = null;
			$item = $this->itemValidateAndSave($request, $redirect);
			if ($redirect) {
				return $redirect;
			}
			if (! $request->ajax()) {
				return $this->redirectToList($item);
			}
		} elseif ($task == 'save') {
			$redirect = null;
			$item = $this->itemValidateAndSave($request, $redirect);
			if ($redirect) {
				return $redirect;
			}
		}
		return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
	}
	
}