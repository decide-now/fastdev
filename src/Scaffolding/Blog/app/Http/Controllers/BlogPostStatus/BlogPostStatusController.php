<?php
namespace App\Http\Controllers\BlogPostStatus;

use App\ORM\BlogPostStatus;
use DecideNow\FastDev\Scene\SceneBaseController;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BlogPostStatusController extends SceneBaseController
{	
	protected $default_actions_tech = '';
	
	protected function itemDefaultFields($request)
	{
		$ret = [
			// 'is_published' => true,
		];

		return $ret;
	}
	
	protected function validationRules($itemClass = '', $data = [])
	{
		$ret = [
			$itemClass.'.name' => 'required',
			$itemClass.'.code' => 'required',

		];
		return $ret;
	}
	
	protected $model;
	protected $validator;
	

	public $page;
	public $item_id;
	public function paginateExt($data, $perPage = null)
	{
		$perPage = $perPage ?: $data->getModel()->getPerPage();
		$list_page = ($this->page) ?: '1';
		$item_id = ($this->item_id) ?: '';
		if ($item_id) {
			$dt = clone($data);
			$row_num = $dt->select($this->model->getTable().'.id')->pluck('id')->search($item_id) + 1;
			$list_page = ceil($row_num / $perPage);
		}
		return $data
		->paginate($perPage, ['*'], 'page', $list_page)
		->withPath(action($this->methodPath('get')), [], false);
	}
	
	public function __construct($scene_parent = null)
	{
		parent::__construct($scene_parent);
		$this->middleware('auth');
		$this->model = new BlogPostStatus;
	}
	
	protected function itemCreate($request)
	{
		$item = new $this->model;
		
		$default_fields = $this->itemDefaultFields($request);
		foreach($default_fields as $key => $value) {
			$item->$key = $value;
		}
		
		Session::flash('item', $item);
		return $item;
	}
	
	protected function itemFind($request, $item_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		
		Session::flash('item', $item);
		return $item;
	}
	
	protected function itemDelete($request, $item_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		
		DB::beginTransaction();
		try {
			$item->delete();
			DB::commit();
			Session::flash('flash_success_messages', ['Запись удалена!']);
		} catch (QueryException $e) {
			DB::rollBack();
			$msg = $this->getDatabaseErrorMessage($e);
			Session::flash('flash_error_messages', [$msg]);
		}
	}
	
	protected function itemFromInput($request)
	{
		$itemClass = $this->model->getModelName();
		
		$item_id = $request->get('item_id');
		$item = ($item_id) ? $item = $this->model->find($item_id) : new $this->model;
		
		$item_input = $request->get($itemClass);
		
		$item->fill($item_input);
		
		return $item;
	}
	
	protected function itemValidate($request)
	{
		$item = $this->itemFromInput($request);
		Session::flash('item', $item);
		//$item_id = $request->get('item_id');
		
		$this->validator = Validator::make(
			$request->all(), 
			$this->validationRules($this->model->getModelName() /*, [ 'item_id' => $item_id ]*/), 
			[], 
			$this->model->getAliasesWithModelName()
		);
		if ($this->validator->fails()) {
			if ($request->ajax()) {
				Session::flash('flash_error_messages', $this->validator->errors()->messages());
			}
			return false;
		}
		return true;
	}
	
	protected function itemSave($request, $item = null)
	{
		if (!$item) {
			$item = $this->itemFromInput($request);
		}
		
		DB::beginTransaction();
		$item->save();
		DB::commit();
		
		Session::flash('item', $item);
		return $item;
	}

	protected function itemValidateAndSave($request, &$redirect_on_error)
	{
		$validated = $this->itemValidate($request);
		$item = Session::get('item');
		if ($validated) {
			$this->itemSave($request, $item);
		} else {
			$redirect_on_error = redirect()
			->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])
			->with('ajax', $request->ajax())
			->withErrors($this->validator);
		}
		return $item;
	}
	
	
	protected function redirectToItem($item)
	{
		return redirect()->action(BlogPostStatusItemController::methodPath('get'), ['id' => ($item) ? $item->id : '']);
	}
	
	protected function redirectToList($item)
	{
		return redirect()
		->action(BlogPostStatusListController::methodPath('get'))
		->with('item_id', ($item) ? $item->id : '');
	}

	
	protected function checkPermissionsList() {
		if (!$this->model->getPermission('list')) {
			$this->setContentError('Доступ запрещен!');
		}
	}
	
	protected function checkPermissionsItem($request, $item_id) {
		$item = null;
		if (Session::has('item')) {
			$item = Session::get('item');
		} else {
			
			if ($item_id) {
				$item = $this->itemFind($request, $item_id);
				if (!$item) {
					$this->setContentError('Не найдена запись с кодом '.$item_id.'!');
				} else {
					if (!$item->getPermission('edit')) {
						$this->setContentError('Доступ запрещен!');
					}
				}
			} else {
				if (!$this->model->getPermission('create')) {
					$this->setContentError('Доступ запрещен!');
				}
			}
		}
		return $item;
	}
	
}