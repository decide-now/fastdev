<?php
namespace App\Http\Controllers\BlogPostStatus;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BlogPostStatusListController extends BlogPostStatusController
{	
	static $scene_id = 'blog_post_status-list';
	
	public $list;
	public $filter;
	public $ordering;

	public $primitives = [
		'task' => ['default' => '', 'is_hidden' => true],
		'is_dependant' => '0',
		'page' => '1',
	];
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
		$this->scene_children->put('filter_scene', new BlogPostStatusFilterController($this));
		$this->scene_children->put('item_scene', new BlogPostStatusItemController($this));
	}
	
	public function prepareContent(Request $request)
	{	
		if ($this->content_error) {
			return;
		}
		
		$list_data = new $this->model;
		$list_data = $this->model->defaultListQuery($list_data);

		$list_data = $this->model->applySessionFilter($list_data, $this->filter);
		$list_data = $this->model->applySessionOrdering($list_data, $this->ordering);
		
		$this->page = ($request->get('page')) ?: $this->getPrimitives('page');
		$this->item_id = Session::get('item_id');
		$this->list = $this->paginateExt($list_data);
		
		if ($this->getPrimitives('is_dependant')) {
			$this->default_actions_tech = 'ajax';
			$item_scene = $this->scene_children->get('item_scene');
			$item_scene->is_dependant = 1;
		}
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->transferPrimitives();

		$this->checkPermissionsList();
		
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferPrimitives($request);

		$task = $this->getPrimitives('task');
		
		if ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
		} elseif ($task == 'ordering') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->orderingPutToSession($request, '', $force_clear);
		} elseif ($task == 'create') {
			$item = $this->itemCreate($request);
			return $this->redirectToItem($item);
		} elseif ($task == 'edit') {
			$item = $this->itemFind($request);
			return $this->redirectToItem($item);
		} elseif ($task == 'delete') {
			$this->itemDelete($request);
		}
		return redirect()
		->action(self::methodPath('get'), ['page' => $request->get('page')])
		->with('item_id', $request->get('item_id'));
	}
}