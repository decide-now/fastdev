<?php
namespace App\Http\Controllers\BlogPostTag;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\BlogTag\BlogTagSelectController;

class BlogPostTagItemController extends BlogPostTagController
{	
	static $scene_id = 'blog_post_tag-item';

	public $primitives = [
		'task' => ['default' => '', 'is_hidden' => true],
		'is_dependant' => '0',
		'refresh_parent_scene' => '0',
	];
	
	public $item;
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
		$this->scene_children->put('blog_tag_select_scene', new BlogTagSelectController($this));
	}
	
	public function prepareContent(Request $request)
	{
		$task = $this->getPrimitives('task');
		$ajax = $this->transferAJAXFlag();
		if ($ajax && ($task == 'ok' || $task == 'cancel')) {
			return;
		}
		if ($this->content_error) {
			return;
		}
		if (!$this->item) {
			$this->item = $this->itemCreate($request);
		}
		if ($this->getPrimitives('is_dependant')) {
			$this->default_actions_tech = 'ajax';
		}
		
		$this->no_content = false;
	}
	
	public function get(Request $request, $item_id = '')
	{
		$this->transferSceneId();
		$this->transferPrimitives();
		$this->blog_post_id = $this->transferData('blog_post_id', '');

		$this->item = $this->checkPermissionsItem($request, $item_id);
		$this->response_data['item_id'] = ($this->item) ? $this->item->id : '';
		
		$task = $this->getPrimitives('task');
		if ($task == 'ok' || $task == 'save') {
			$this->setPrimitives('refresh_parent_scene', '1');
		}
		
		$this->content_error = Session::has('flash_error_messages');
		
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request, $item_id = '')
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferPrimitives($request);
		$this->transferData('blog_post_id', '', $request);

		$task = $this->getPrimitives('task');
		
		$item = null;
		if ($task == 'create') {
			$item = $this->itemCreate($request);
		} elseif ($task == 'edit') {
			$item = $this->itemFind($request);
		} elseif ($task == 'cancel') {
			$item = $this->itemFind($request);
			if (! $request->ajax()) {
				return $this->redirectToList($item);
			}
		} elseif ($task == 'ok') {
			$redirect = null;
			$item = $this->itemValidateAndSave($request, $redirect);
			if ($redirect) {
				return $redirect;
			}
			if (! $request->ajax()) {
				return $this->redirectToList($item);
			}
		} elseif ($task == 'save') {
			$redirect = null;
			$item = $this->itemValidateAndSave($request, $redirect);
			if ($redirect) {
				return $redirect;
			}
		}
		return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
	}
	
}