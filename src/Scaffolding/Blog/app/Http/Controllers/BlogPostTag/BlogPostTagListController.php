<?php
namespace App\Http\Controllers\BlogPostTag;

use App\ORM\BlogTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BlogPostTagListController extends BlogPostTagController
{	
	static $scene_id = 'blog_post_tag-list';
	
	public $list;
	public $filter;
	public $ordering;
	public $blog_post_id;

	public $primitives = [
		'task' => ['default' => '', 'is_hidden' => true],
		'is_dependant' => '0',
		'page' => '1',
	];
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
		$this->scene_children->put('filter_scene', new BlogPostTagFilterController($this));
		$this->scene_children->put('item_scene', new BlogPostTagItemController($this));
	}
	
	public function prepareContent(Request $request)
	{	
		if ($this->content_error) {
			return;
		}
		
		$list_data = new $this->model;
		$list_data = $this->model->defaultListQuery($list_data);
		$list_data = $this->model->addQueryByBlogPostId($list_data, $this->blog_post_id);

		$list_data = $this->model->applySessionFilter($list_data, $this->filter);
		$list_data = $this->model->applySessionOrdering($list_data, $this->ordering);
		
		$this->page = ($request->get('page')) ?: $this->getPrimitives('page');
		$this->item_id = Session::get('item_id');
		$this->list = $this->paginateExt($list_data);
		
		if ($this->getPrimitives('is_dependant')) {
			$this->default_actions_tech = 'ajax';
			$item_scene = $this->scene_children->get('item_scene');
			$item_scene->is_dependant = 1;
		}
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->transferPrimitives();
		$this->blog_post_id = $this->transferData('blog_post_id', '');

		$this->checkPermissionsList();
		
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferPrimitives($request);
		$this->transferData('blog_post_id', '', $request);

		$task = $this->getPrimitives('task');
		
		if ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
		} elseif ($task == 'ordering') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->orderingPutToSession($request, '', $force_clear);
		} elseif ($task == 'create') {
			$item = $this->itemCreate($request);
			return $this->redirectToItem($item);
		} elseif ($task == 'edit') {
			$item = $this->itemFind($request);
			return $this->redirectToItem($item);
		} elseif ($task == 'delete') {
			$this->itemDelete($request);
		} elseif ($task == 'create_simple') {
			$item = $this->itemCreateSimple($request);
		}
		return redirect()
		->action(self::methodPath('get'), ['page' => $request->get('page')])
		->with('item_id', $request->get('item_id'));
	}

	public function complete(Request $request)
	{
		$list = BlogTag::select('id', 'name as label')->where('name', 'LIKE', '%'.$request->get('query').'%');
		$list = $list->get()->toArray();
		return response()->json($list);
	}
}