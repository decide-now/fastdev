<?php

namespace App\Http\Controllers\BlogTemplate;

use App\Http\Controllers\Site\SiteBaseController;
use App\ORM\BlogCategory;
use App\ORM\BlogPost;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\ORM\BlogTag;

class BlogTemplateController extends SiteBaseController
{	
	static $scene_id = 'blog_template-post_list';
	
	public $show_categories = true;
	public $show_crumbs = true;
	public $path_base = '/blog';
	public $path_item = '/blog/post/';
	
	public $list;
	public $blog_category = '';
	public $blog_tags = [];
	public $blog_tag_ids = [];
	public $categories;
	
	public $header_title;
	public $header_description;
	
	public $crumbs;
	
	public function __construct($scene_parent = null)
	{
		parent::__construct($scene_parent);
		$this->model = new BlogPost;
		$this->crumbs = new Collection();
		$this->crumbs->add(['name' => '<i class="glyphicon glyphicon-home"></i>', 'url' => '/']);
		$this->crumbs->add(['name' => 'Блог', 'url' => '/blog']);
	}
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
	}
	
	public function prepareTagIds($request)
	{
		$this->blog_tags = [];
		$this->blog_tag_ids = [];
		$tags = $request->get('tags');
		
		if (is_array($tags)) {
			foreach ($tags as $tag) {
				$blog_tag = BlogTag::findByName($tag);
				if ($blog_tag) {
					$this->blog_tag_ids[] = $blog_tag->id;
					$this->blog_tags[] = $blog_tag->toString();
				}
			}
		}
		if (is_string($tags)) {
			$blog_tag = BlogTag::findByName($tags);
			if ($blog_tag) {
				$this->blog_tag_ids[] = $blog_tag->id;
				$this->blog_tags[] = $blog_tag->toString();
			}
		}
	}
	
	public function prepareContent(Request $request)
	{
		$this->active_page = 'blog';
		
		$list_data = new $this->model;
		
		$list_data = $list_data->orderBy('published_at', 'desc');
		if ($this->blog_category) {
			$list_data = $list_data->where('blog_category_id', '=', $this->blog_category->id);
		}
		
		if (count($this->blog_tag_ids)) {
			$list_data = $list_data->whereHas('blogTags', function($query) {
				$query->whereIn('blog_tag.id', $this->blog_tag_ids);
			});
		}
		
		$this->list = $list_data->paginate(15);
		
		$this->categories = BlogCategory::all();
		
		$this->header_title = 'Блог о банковский гарантиях от экспертов Finaxe.';
		$this->header_description = 'Пишем обзорные статьи на финансовые продукты, обозреваем и сравниваем банковские гарантии, а также делимся опытом от настоящих экспертов рынка.';
		
		if ($this->blog_category) {
			$this->crumbs->add(['name' => $this->blog_category->name, 'url' => '/blog/category/'.$this->blog_category->code]);
		}
		
		
		
		$this->no_content = false;
	}
	
	public function get(Request $request, $category_code = '')
	{
		if (!$this->blog_category) {
			$this->blog_category = BlogCategory::where('code', '=', $category_code)->first();
		}
		$this->prepareTagIds($request);
		$this->transferSceneId();
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request, $category_code = '')
	{	
		$this->transferSceneId($request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			$request->flash();
			return redirect()->action(self::methodPath('get', ['category_code' => $category_code]))->with('ajax', $request->ajax());
		}
	}
}