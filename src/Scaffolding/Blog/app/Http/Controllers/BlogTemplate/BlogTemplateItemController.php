<?php
namespace App\Http\Controllers\BlogTemplate;

use App\ORM\BlogPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Site\SiteBaseController;
use Illuminate\Database\Eloquent\Collection;
use App\ORM\BlogCategory;

class BlogTemplateItemController extends SiteBaseController
{	
	static $scene_id = 'blog_template-post_item';
	
	public $show_categories = false;
	public $show_crumbs = false;
	public $show_category = false;
	public $path_base = '/post';
	public $path_item = '/post';
	
	public $item;
	public $n_item;
	public $p_item;
	public $categories;
	
	public $header_title;
	public $header_description;
	
	public $crumbs;
	
	public function __construct($scene_parent = null)
	{
		parent::__construct($scene_parent);
		$this->model = new BlogPost;
		
		$this->crumbs = new Collection();
		$this->crumbs->add(['name' => '<i class="glyphicon glyphicon-home"></i>', 'url' => '/']);
		$this->crumbs->add(['name' => 'Блог', 'url' => '/blog']);
	}
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
	}
	
	public function prepareContent(Request $request)
	{
		$this->active_page = 'blog';
		
		$p_item_q = BlogPost::orderBy('published_at', 'desc')->orderBy('id', 'desc')
			->where('id', '<>', $this->item->id)
			->where('blog_category_id', '=', $this->item->blog_category_id)
			->where('published_at', '<', $this->item->published_at);
		
		$n_item_q = BlogPost::orderBy('published_at', 'asc')->orderBy('id', 'asc')
			->where('id', '<>', $this->item->id)
			->where('blog_category_id', '=', $this->item->blog_category_id)
			->where('published_at', '>', $this->item->published_at);
		
		$this->p_item = $p_item_q->take(1)->get()->first();
		$this->n_item = $n_item_q->take(1)->get()->first();
		
		$this->header_title = 'Блог о банковский гарантиях от экспертов Finaxe.';
		$this->header_description = 'Пишем обзорные статьи на финансовые продукты, обозреваем и сравниваем банковские гарантии, а также делимся опытом от настоящих экспертов рынка.';
		
		$this->categories = BlogCategory::all();
		
		$this->crumbs->add(['name' => $this->item->blogCategory->name, 'url' => '/blog/category/'.$this->item->blogCategory->code]);
		$this->crumbs->add(['name' => $this->item->title, 'url' => '/blog/post/'.$this->item->code]);
		
		$this->no_content = false;
	}
	
	public function preparePathBase()
	{
		$blog_category = $this->item->blogCategory;
		if (!$blog_category) {
			$this->path_base = '/blog';
			return;
		}
		if (!$blog_category->url) {
			$this->path_base = '/blog';
			return;
		}
		$this->path_base = $blog_category->url;
	}
	
	public function get(Request $request, $post_code = '')
	{
		$this->item = BlogPost::where('code', '=', $post_code)->first();
		$this->preparePathBase();
		$this->transferSceneId();
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request, $post_code = '')
	{
		$this->transferSceneId($request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			$this->itemFind($request);
			return redirect()->action(self::methodPath('get', ['post_code' => $post_code]))->with('ajax', $request->ajax());
		}
	}
}