<?php
namespace App\Http\Controllers\BlogCategory;

use Illuminate\Http\Request;

class BlogCategoryFilterController extends BlogCategoryController
{	
	static $scene_id = 'blog_category-filter';
	
	public $filter;
	
	public function prepareContent(Request $request)
	{
		$this->filter = $this->model->filterGetFromSession();
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->transferPrimitives();
		
		$this->prepareContent($request);
		
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferPrimitives($request);
		
		$task = $this->getPrimitives('task');
		
		if ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
		}
		return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
	}	
}