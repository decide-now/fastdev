<?php
namespace App\Http\Controllers\BlogCategory;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class BlogCategorySelectController extends BlogCategoryController
{	
	static $scene_id = 'blog_category-select';
	
	public $list;
	public $filter;
	public $ordering;

	public $primitives = [
		'task' => ['default' => '', 'is_hidden' => true],
		'call_button_id' => '',
		'page' => '1',
	];
	
	protected function defineChildren()
	{	
		$this->defineAlertChildScene();
		$this->scene_children->put('filter_scene', new BlogCategoryFilterController($this));
	}
	
	private function paginateTree(Collection $collection, $perPage = 20)
	{
		$currentPage = $this->page;
		$currentPath = LengthAwarePaginator::resolveCurrentPath();
		$currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
		$paginetedItems = new LengthAwarePaginator($currentPageItems, count($collection), $perPage, $currentPage);
		$paginetedItems->setPath($currentPath);
		return $paginetedItems;
	}
	
	public function prepareContent(Request $request)
	{	
		$list_data = new $this->model;
		$list_data = $this->model->defaultListQuery($list_data);

		$list_data = $this->model->applySessionFilter($list_data, $this->filter);
		$list_data = $this->model->applySessionOrdering($list_data, $this->ordering);
		
		$this->page = ($request->get('page')) ?: $this->getPrimitives('page');
		
		$filtered_data = clone($list_data);
		$filtered_data->select('id');
		$filtered_ids = $filtered_data->get()->pluck('id')->all();
		
		$list_data = $this->model->getTree($list_data);
		
		foreach ($list_data as $list_item) {
			if (!in_array($list_item->id, $filtered_ids)) {
				$list_data->forget($list_item->id);
			}
		}
		$this->list = $this->paginateTree($list_data);
		
		$this->no_content = false;
	}

	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->transferPrimitives();

		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferPrimitives($request);

		$task = $this->getPrimitives('task');
		
		if ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
		} elseif ($task == 'ordering') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->orderingPutToSession($request, '', $force_clear);
		}
		return redirect()->action(self::methodPath('get'));
	}
}