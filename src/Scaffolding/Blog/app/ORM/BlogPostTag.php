<?php

namespace App\ORM;

use DecideNow\FastDev\ORM\ORMModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class BlogPostTag extends ORMModel
{
	protected $table = 'blog_post_tag';
	protected $fillable = [
		'blog_post_id', 'blog_tag_id', 
	];
	
	public static function getAliases()
	{
		return [
			'table_name' => 'Тэги публикации',
			'table_name_short' => 'Тэги',
			'record_name' => 'Тэг публикации',
			'blog_post_id' => 'Публикация',
			'blog_tag_id' => 'Тэг',

		] + parent::getAliases();
	}
	
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ 'blog_post_id' => '', 'blog_tag_id' => '', ] + parent::getFilterFields();
	}
	
	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'blog_post_id', '');
		if ($tmp != '') {
			$query = $query->where('blog_post_id', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'blog_tag_id', '');
		if ($tmp != '') {
			$query = $query->where('blog_tag_id', '=', ''.$tmp.'');
		}

		$query = parent::applyFilter($query, $filter_data);
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return [ 'blog_post_id' => '', 'blog_tag_id' => '', ] + parent::getOrderingFields();
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'blog_post_id', '');
		if ($tmp != '') {
			$query = $query->orderBy('blog_post_id', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'blog_tag_id', '');
		if ($tmp != '') {
			$query = $query->orderBy('blog_tag_id', $tmp);
		}

		$query = parent::applyOrdering($query, $ordering_data);
		
		return $query;
	}
	
	
	/* relations */
	
	public function blogPost()
	{
		return $this->belongsTo(BlogPost::class);
	}

	public function blogTag()
	{
		return $this->belongsTo('App\ORM\BlogTag');
	}
	
	/* list queries */
	
	public static function defaultListQuery($query = null)
	{
		$query = parent::defaultListQuery($query);
		//$query = $query->select('blog_post_tag.*')->distinct();
		return $query;
	}

	public static function addQueryByBlogPostId($query, $blog_post_id)
	{
		$query = parent::defaultListQuery($query);
		if (!empty($blog_post_id)) {
			$query = $query->where('blog_post_id', '=', $blog_post_id);
		}
		return $query;
	}

	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if ($param == 'id') {
				if (Auth::user()->userRole->code == 'admin') {
					return true;
				}
				return false;
			}
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
}
