<?php

namespace App\ORM;

use DecideNow\FastDev\ORM\ORMModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class BlogPost extends ORMModel
{
	protected $table = 'blog_post';
	protected $fillable = [
		'title', 'code', 'content', 'content_short', 'mainimage_id', 'show_mainimage', 'blog_post_status_id', 'blog_category_id', 'user_id', 'published_at', 'rating', 
	];
	
	public static function getAliases()
	{
		return [
			'table_name' => 'Публикации',
			'record_name' => 'Публикация',
			'title' => 'Заголовок',
			'code' => 'Код',
			'content' => 'Содержимое',
			'content_short' => 'Содержимое (кратко)',
			'f_content' => 'Содержимое',
			'f_content_short' => 'Содержимое (кратко)',
			'mainimage_id' => 'Осн. изображение',
			'show_mainimage' => 'Показывать осн. изображение',
			'blog_post_status_id' => 'Статус',
			'blog_category_id' => 'Категория',
			'user_id' => 'Пользователь',
			'published_at' => 'Дата публикации',
			'rating' => 'Рейтинг', 

		] + parent::getAliases();
	}
	
	public function toString()
	{
		return $this->title;
	}
	
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ 'title' => '', 'code' => '', 'content' => '', 'content_short' => '', 'mainimage_id' => '', 'show_mainimage' => '', 'blog_post_status_id' => '', 'blog_category_id' => '', 'user_id' => '', 'published_at' => '', 'rating' => '', ] + parent::getFilterFields();
	}
	
	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'title', '');
		if ($tmp != '') {
			$query = $query->where('title', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'code', '');
		if ($tmp != '') {
			$query = $query->where('code', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'content', '');
		if ($tmp != '') {
			$query = $query->where('content', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'content_short', '');
		if ($tmp != '') {
			$query = $query->where('content_short', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'mainimage_id', '');
		if ($tmp != '') {
			$query = $query->where('mainimage_id', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'show_mainimage', '');
		if ($tmp != '') {
			$query = $query->where('show_mainimage', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'blog_post_status_id', '');
		if ($tmp != '') {
			$query = $query->where('blog_post_status_id', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'blog_category_id', '');
		if ($tmp != '') {
			$query = $query->where('blog_category_id', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'user_id', '');
		if ($tmp != '') {
			$query = $query->where('user_id', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'published_at', '');
		if ($tmp != '') {
			$query = $query->where('published_at', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'rating', '');
		if ($tmp != '') {
			$query = $query->where('rating', '=', ''.$tmp.'');
		}

		$query = parent::applyFilter($query, $filter_data);
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return [ 'title' => '', 'code' => '', 'content' => '', 'content_short' => '', 'mainimage_id' => '', 'show_mainimage' => '', 'blog_post_status_id' => '', 'blog_category_id' => '', 'user_id' => '', 'published_at' => '', 'rating' => '', ] + parent::getOrderingFields();
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'title', '');
		if ($tmp != '') {
			$query = $query->orderBy('title', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'code', '');
		if ($tmp != '') {
			$query = $query->orderBy('code', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'content', '');
		if ($tmp != '') {
			$query = $query->orderBy('content', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'content_short', '');
		if ($tmp != '') {
			$query = $query->orderBy('content_short', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'mainimage_id', '');
		if ($tmp != '') {
			$query = $query->orderBy('mainimage_id', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'show_mainimage', '');
		if ($tmp != '') {
			$query = $query->orderBy('show_mainimage', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'blog_post_status_id', '');
		if ($tmp != '') {
			$query = $query->orderBy('blog_post_status_id', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'blog_category_id', '');
		if ($tmp != '') {
			$query = $query->orderBy('blog_category_id', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'user_id', '');
		if ($tmp != '') {
			$query = $query->orderBy('user_id', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'published_at', '');
		if ($tmp != '') {
			$query = $query->orderBy('published_at', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'rating', '');
		if ($tmp != '') {
			$query = $query->orderBy('rating', $tmp);
		}

		$query = parent::applyOrdering($query, $ordering_data);
		
		return $query;
	}
	
	
	/* relations */
	
	public function blogCategory()
	{
		return $this->belongsTo('App\ORM\BlogCategory');
	}
	
	public function blogPostStatus()
	{
		return $this->belongsTo('App\ORM\BlogPostStatus');
	}
	
	public function user()
	{
		return $this->belongsTo('App\ORM\User');
	}
	
	public function mainimage()
	{
		return $this->belongsTo('App\ORM\FileStorage', 'mainimage_id');
	}
	
	public function blogTags()
	{
		return $this->belongsToMany('App\ORM\BlogTag', 'blog_post_tag');
	}
	
	
	/* list queries */
	
	public static function defaultListQuery($query = null)
	{
		$query = parent::defaultListQuery($query);
		//$query = $query->select('blog_post.*')->distinct();
		return $query;
	}

	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if ($param == 'id') {
				if (Auth::user()->userRole->code == 'admin') {
					return true;
				}
				return false;
			}
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
}
