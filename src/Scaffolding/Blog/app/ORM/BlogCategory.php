<?php

namespace App\ORM;

use DecideNow\FastDev\ORM\ORMModel;
use DecideNow\FastDev\ORM\Treeable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class BlogCategory extends ORMModel
{
	protected $table = 'blog_category';
	protected $fillable = [
		'name', 'parent_id', 'code', 'url', 'is_published', 'description', 
	];
	
	use Treeable; 
	
	public static function getAliases()
	{
		return [
			'table_name' => 'Категории',
			'record_name' => 'Категория',
			'parent_id' => 'Родитель',
			'code' => 'Код',
			'is_published' => 'Опубликована',
			'description' => 'Описание', 

		] + parent::getAliases();
	}
	
	public static function findByCode($code)
	{
		return self::where('code', '=', $code)->first();
	}
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ 'parent_id' => '', 'code' => '', 'is_published' => '', 'description' => '', ] + parent::getFilterFields();
	}
	
	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'parent_id', '');
		if ($tmp != '') {
			$query = $query->where('parent_id', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'code', '');
		if ($tmp != '') {
			$query = $query->where('code', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'is_published', '');
		if ($tmp != '') {
			$query = $query->where('is_published', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'description', '');
		if ($tmp != '') {
			$query = $query->where('description', '=', ''.$tmp.'');
		}

		$query = parent::applyFilter($query, $filter_data);
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return [ 'parent_id' => '', 'code' => '', 'is_published' => '', 'description' => '', ] + parent::getOrderingFields();
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'parent_id', '');
		if ($tmp != '') {
			$query = $query->orderBy('parent_id', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'code', '');
		if ($tmp != '') {
			$query = $query->orderBy('code', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'is_published', '');
		if ($tmp != '') {
			$query = $query->orderBy('is_published', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'description', '');
		if ($tmp != '') {
			$query = $query->orderBy('description', $tmp);
		}

		$query = parent::applyOrdering($query, $ordering_data);
		
		return $query;
	}
	
	
	/* relations */
	

	
	/* list queries */
	
	public static function defaultListQuery($query = null)
	{
		$query = parent::defaultListQuery($query);
		//$query = $query->select('blog_category.*')->distinct();
		return $query;
	}

	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if ($param == 'id') {
				if (Auth::user()->userRole->code == 'admin') {
					return true;
				}
				return false;
			}
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
}
