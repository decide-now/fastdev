<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		
		Schema::create('blog_category', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('code')->nullable();
			$table->string('url')->nullable();
			$table->boolean('is_published')->default(false);
			$table->longText('description')->nullable();
			$table->timestamps();
		});
		Schema::table('blog_category', function (Blueprint $table) {
			$table->integer('parent_id')->unsigned()->nullable()->after('name');
			$table->foreign('parent_id')->references('id')->on('blog_category');
		});
		
		Schema::create('blog_post_status', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('code')->nullable();
			$table->timestamps();
		});
		
		Schema::create('blog_post', function (Blueprint $table) {
			$table->increments('id');
			$table->text('title');
			$table->string('code')->nullable();
			$table->longText('content');
			$table->longText('content_short')->nullable();
			$table->integer('mainimage_id')->unsigned()->nullable();
			$table->foreign('mainimage_id')->references('id')->on('file_storage');
			$table->boolean('show_mainimage')->default(true);
			$table->integer('blog_post_status_id')->unsigned()->nullable();
			$table->foreign('blog_post_status_id')->references('id')->on('blog_post_status');
			$table->integer('blog_category_id')->unsigned()->nullable();
			$table->foreign('blog_category_id')->references('id')->on('blog_category');
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('user');
			$table->datetime('published_at');
			$table->integer('rating')->default(0);
			$table->timestamps();
		});
		
		Schema::create('blog_tag', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('code')->nullable();
			$table->timestamps();
		});
		
		Schema::create('blog_post_tag', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('blog_post_id')->unsigned()->nullable();
			$table->foreign('blog_post_id')->references('id')->on('blog_post');
			$table->integer('blog_tag_id')->unsigned()->nullable();
			$table->foreign('blog_tag_id')->references('id')->on('blog_tag');
			$table->timestamps();
		});
		
		Schema::create('blog_comment', function (Blueprint $table) {
			$table->increments('id');
			$table->text('content');
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('user');
			$table->integer('commentable_id');
			$table->string('commentable_type');
			$table->timestamps();
		});
		Schema::table('blog_comment', function (Blueprint $table) {
			$table->integer('parent_id')->unsigned()->nullable()->after('content');
			$table->foreign('parent_id')->references('id')->on('blog_comment');
		});
		
		Schema::create('blog_like', function (Blueprint $table) {
			$table->increments('id');
			$table->string('status');
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('user');
			$table->integer('likeable_id');
			$table->string('likeable_type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('blog_category', function (Blueprint $table) {
			$table->dropForeign(['parent_id']);
		});
		Schema::table('blog_post', function (Blueprint $table) {
			$table->dropForeign(['mainimage_id']);
			$table->dropForeign(['blog_category_id']);
			$table->dropForeign(['blog_post_status_id']);
			$table->dropForeign(['user_id']);
		});
		Schema::table('blog_post_tag', function (Blueprint $table) {
			$table->dropForeign(['blog_post_id']);
			$table->dropForeign(['blog_tag_id']);
		});
		Schema::table('blog_comment', function (Blueprint $table) {
			$table->dropForeign(['user_id']);
			$table->dropForeign(['parent_id']);
		});
		Schema::table('blog_like', function (Blueprint $table) {
			$table->dropForeign(['user_id']);
		});
		
		Schema::dropIfExists('blog_category');
		Schema::dropIfExists('blog_post_status');
		Schema::dropIfExists('blog_post');
		Schema::dropIfExists('blog_tag');
		Schema::dropIfExists('blog_post_tag');
		Schema::dropIfExists('blog_comment');
		Schema::dropIfExists('blog_like');
	}
}
