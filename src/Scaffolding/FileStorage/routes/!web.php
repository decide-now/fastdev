...
// Service
Route::get('/create-public-storage', function() {
	if (App::environment('local')) {
		Artisan::call('storage:link');
		echo 'The "storage" link was successfully created in "public" folder.';
	} else {
		echo 'Not allowed!';
	}
	
});
...
// File Storage

Route::get('/file-storage-group', 'FileStorageGroup\FileStorageGroupListController@get');
Route::post('/file-storage-group', 'FileStorageGroup\FileStorageGroupListController@post');
Route::get('/file-storage-group/item/{id?}', 'FileStorageGroup\FileStorageGroupItemController@get');
Route::post('/file-storage-group/item/{id?}', 'FileStorageGroup\FileStorageGroupItemController@post');
Route::get('/file-storage-group/select', 'FileStorageGroup\FileStorageGroupSelectController@get');
Route::post('/file-storage-group/select', 'FileStorageGroup\FileStorageGroupSelectController@post');
Route::get('/file-storage-group/filter', 'FileStorageGroup\FileStorageGroupFilterController@get');
Route::post('/file-storage-group/filter', 'FileStorageGroup\FileStorageGroupFilterController@post');

Route::get('/file-storage/list', 'FileStorage\FileStorageListController@get');
Route::post('/file-storage/list', 'FileStorage\FileStorageListController@post');
Route::get('/file-storage/select-list', 'FileStorage\FileStorageSelectController@get');
Route::post('/file-storage/select-list', 'FileStorage\FileStorageSelectController@post');
Route::get('/file-storage/item/{id?}', 'FileStorage\FileStorageItemController@get');
Route::post('/file-storage/item/{id?}', 'FileStorage\FileStorageItemController@post');
Route::get('/file-storage/item-ext/{id?}', 'FileStorage\FileStorageItemExtController@get');
Route::post('/file-storage/item-ext/{id?}', 'FileStorage\FileStorageItemExtController@post');
Route::get('/file-storage/filter', 'FileStorage\FileStorageFilterController@get');
Route::post('/file-storage/filter', 'FileStorage\FileStorageFilterController@post');

Route::get('/file-storage', 'FileStorage\FileStorageCombinedController@get');
Route::post('/file-storage', 'FileStorage\FileStorageCombinedController@post');
Route::get('/file-storage/select', 'FileStorage\FileStorageCombinedSelectController@get');
Route::post('/file-storage/select', 'FileStorage\FileStorageCombinedSelectController@post');

Route::get('/file-storage/download/{field}/{value}', 'FileStorage\FileStoragePublicController@download');
