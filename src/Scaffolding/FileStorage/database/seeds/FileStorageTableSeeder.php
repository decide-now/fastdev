<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FileStorageTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$file_storage_group = [
			['id' => '1', 'name' => 'Типы файлов', 'code' => 'file-types'], 
		];
		$file_storage = [
			[
				'name' => 'Все файлы', 
				'size' => '6503', 
				'real_name' => 'any-file.png', 
				'output_name' => 'Все файлы',
				'extension' => 'png',
				'mime' => 'image/png',
				'thumbnail' => NULL,
				'group_id' => '1',
				'user_id' => '1',
			],
			[
				'name' => 'Изображения PNG', 
				'size' => '10043', 
				'real_name' => 'png-file.png', 
				'output_name' => 'Изображения PNG',
				'extension' => 'png',
				'mime' => 'image/png',
				'thumbnail' => NULL,
				'group_id' => '1',
				'user_id' => '1',
			],
		];
		
		DB::table('file_storage_group')->insert($file_storage_group);
		DB::table('file_storage')->insert($file_storage);
	}
}
