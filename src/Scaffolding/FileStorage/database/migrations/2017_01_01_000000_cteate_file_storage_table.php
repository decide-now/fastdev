<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CteateFileStorageTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('file_storage_group', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('code');
			$table->boolean('is_public')->default(false);
			$table->timestamps();
		});
		Schema::table('file_storage_group', function (Blueprint $table) {
			$table->integer('parent_id')->unsigned()->nullable()->after('name');
			$table->foreign('parent_id')->references('id')->on('file_storage_group');
		});
		
		Schema::create('file_storage', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('size')->unsigned();
			$table->string('real_name');
			$table->text('output_name')->nullable();
			$table->string('extension');
			$table->string('mime');
			$table->string('thumbnail')->nullable();
			$table->integer('group_id')->unsigned()->nullable();
			$table->foreign('group_id')->references('id')->on('file_storage_group');
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('user');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('file_storage_group', function (Blueprint $table) {
			$table->dropForeign(['parent_id']);
		});
		Schema::table('file_storage', function (Blueprint $table) {
			$table->dropForeign(['group_id']);
			$table->dropForeign(['user_id']);
		});
		Schema::dropIfExists('file_storage_group');
    	Schema::dropIfExists('file_storage');
    }
}
