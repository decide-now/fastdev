<?php

namespace App\ORM;

use DecideNow\FastDev\ORM\ORMModel;
use DecideNow\FastDev\ORM\Treeable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;

class FileStorageGroup extends ORMModel
{
	use Treeable;
	
	protected $table = 'file_storage_group';
	protected $fillable = [
		'name', 'code', 'is_public', 'parent_id', 
	];
	
	public $allow_non_public = true;
	
	public static function getAliases()
	{
		return [
			'table_name' => 'Группы файлов', 
			'record_name' => 'Группа файлов', 
			'code' => 'Код', 
			'path' => 'Путь', 
			'is_public' => 'Общедоступная', 
			'parent_id' => 'Родитель',

		] + parent::getAliases();
	}
	
	public static function findByCode($code)
	{
		return static::where('code', '=', $code)->first();
	}
	
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ 'code' => '', 'is_public' => '', 'parent_id' => '', ] + parent::getFilterFields();
	}
	
	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'code', '');
		if ($tmp != '') {
			$query = $query->where('code', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'is_public', '');
		if ($tmp != '') {
			$query = $query->where('is_public', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'parent_id', '');
		if ($tmp != '') {
			$query = $query->where('parent_id', '=', ''.$tmp.'');
		}
		
		$query = parent::applyFilter($query, $filter_data);
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		$instance = new self; 
		$ret = ['name' => ''];
		
		if ($instance->getPermission('allow_non_public')) {
			$ret += ['is_public' => '', ]; 
		}
		return $ret;
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'code', '');
		if ($tmp != '') {
			$query = $query->orderBy('code', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'is_public', '');
		if ($tmp != '') {
			$query = $query->orderBy('is_public', $tmp);
		}

		
		$query = parent::applyOrdering($query, $ordering_data);
		return $query;
	}
	
	
	/* ordering select field */
	
	public static function getOrderingOptions()
	{
		$ret = ['none' => '<Без сортировки>'];
		foreach (array_keys(static::getOrderingFields()) as $key) {
			$ret[$key] = static::getAlias($key);
		}
		return $ret;
	}
	public static function getOrderingKey($ordering)
	{
		$ret = '';
		foreach ($ordering as $key => $value) {
			if ($value) {
				$ret = $key;
			}
		}
		return ($ret == '') ? 'none' : $ret;
	}
	public static function getOrderingValue($ordering)
	{
		$ret = '';
		foreach ($ordering as $value) {
			if ($value) {
				$ret = $value;
			}
		}
		return ($ret == '') ? 'asc' : $ret;
	}
	public static function getOrderingIcon($ordering)
	{
		$ret = '';
		$value = static::getOrderingValue($ordering);
		if ($value == 'asc') $ret = 'glyphicon glyphicon-sort-by-attributes';
		if ($value == 'desc') $ret =  'glyphicon glyphicon-sort-by-attributes-alt';
		return $ret;
	}
	public static function getOrderingTitle($ordering)
	{
		$ret = '';
		$value = static::getOrderingValue($ordering);
		if ($value == 'asc') $ret = __('util.asc');
		if ($value == 'desc') $ret = __('util.desc');
		$ret .= ' ('.__('util.change').')';
		return $ret;
	}
	
	
	/* relations */
	
	public function files()
	{
		return $this->hasMany(FileStorage::class, 'group_id');
	}
	
	/* list queries */
	
	public static function addQueryFilterAndOrdering($query = null)
	{
		$query = parent::addQueryFilterAndOrdering($query);
		if (!$query->getPermission('allow_non_public')) {
			$query = $query->where('is_public', '=', true);
		}
		return $query;
	}
	
	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		
		if ($permission == 'allow_non_public') {
			return $this->allow_non_public;
		}
		
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
	
	
	/* path & url */
	
	public function getFullCodeAttribute($item = null) {
		return $this->getFullNameAttribute($item, 'code', '/');
	}

	
	public function getIsPublicIconAttribute()
	{
		return ($this->is_public) ? '<i class="fa fa-eye"></i>' : '<i class="fa fa-eye-slash"></i>';
	}

	
	public function setIsPublicFlow($value)
	{
		if ($this->attributes['is_public'] != $value) {
			$this->attributes['is_public'] = $value;
		}
		$children = $this->children;
		foreach ($children as $child) {
			if ($child->is_public != $value) {
				$child->is_public = $value;
				$child->save();
			}
		}
	}
	
	
	public function getFullPath()
	{
		$path_chain = $this->getPathChain();
		$storage_disk = $this->getStorageDisk();
		$disk_full_path = Storage::disk($storage_disk)->getDriver()->getAdapter()->getPathPrefix();
		$full_path = $disk_full_path . $path_chain;
		return $full_path;
	}
	
	public function getShortPath()
	{
		$full_path = $this->getFullPath();
		$path = str_replace(storage_path('app'), '', $full_path);
		return $path;
	}
	
	
	public function getPathChain($code = null, $parent_id = '')
	{
		$code = ($code === null) ? $this->code : $code;
		$parent = ($parent_id === '') ? $this->parent : self::find($parent_id);
		$ret = (($parent) ? $parent->getPathChain() : '') . '/' . $code;
		return $ret;
	}
	
	public function getStorageDisk($is_public = null)
	{
		if ($is_public === null) {
			$is_public = $this->is_public;
		}
		return ($is_public) ? 'public' : 'local';
	}

	
	/* database */
	
	public function save(array $options = [])
	{	
		if ($this->parent) {
			if ($this->is_public != $this->parent->is_public) {
				Session::flash('flash_error_messages', ['Признак общедоступности не может отличаться от родительской группы!']);
				return false;
			}
		}
		
		$old_path = '';
		$original = $this->getOriginal();
		if (count($original)) {
			$old_chain = $this->getPathChain($original['code'], $original['parent_id']);
			$old_disk = $this->getStorageDisk($original['is_public']);
			$old_path = Storage::disk($old_disk)->getDriver()->getAdapter()->getPathPrefix() . $old_chain;
		}
		$new_chain = $this->getPathChain();
		$new_disk = $this->getStorageDisk();
		$new_path = Storage::disk($new_disk)->getDriver()->getAdapter()->getPathPrefix() . $new_chain;
		
		if ($old_path != $new_path) {
			if (File::exists($new_path)) {
				Session::flash('flash_error_messages', ['Папка с указанным кодом уже существует на диске!']);
				return false;
			} else {
				if (File::exists($old_path)) {
					File::move($old_path, $new_path);
				} else {
					File::makeDirectory($new_path);
				}
			}
		}
		
		if (count($original)) {
			if ($original['is_public'] != $this->is_public) {
				$this->setIsPublicFlow($this->is_public);
			}
		}
		parent::save($options = []);
	}
}
