<?php

namespace App\ORM;

use DecideNow\FastDev\ORM\ORMModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Arr;
use Intervention\Image\Facades\Image;

class FileStorage extends ORMModel
{
	protected $table = 'file_storage';
	protected $fillable = [
		'name', 'size', 'real_name', 'output_name', 'extension', 'mime', 'group_id', 'user_id', 
	];
	
	public static $allow_save_to_root_group = false;
	public static $thumbnail_dir = 'thumb'; // if empty then no thumbnails
	
	public static function getAliases()
	{
		return [
			'table_name' => 'Файлы',
			'record_name' => 'Файл',
			'name' => 'Имя',
			'size' => 'Размер',
			'real_name' => 'Реальное имя',
			'output_name' => 'Выводимое имя',
			'extension' => 'Расширение',
			'mime' => 'Тип',
			'user_id' => 'Пользователь',
			'created_at' => 'Загружен',
			'group_id' => 'Группа',

		] + parent::getAliases();
	}
	
	public static function findByRealName($real_name, $starts_with = false)
	{
		if ($starts_with) {
			$ret = self::where('real_name', 'LIKE', $real_name.'%')->first();
		} else {
			$ret = self::where('real_name', '=', $real_name)->first();
		}
		return $ret;
	}
	
	public static function findByOutputName($output_name, $starts_with = false)
	{
		if ($starts_with) {
			$ret = self::where('output_name', 'LIKE', $output_name.'%')->first();
		} else {
			$ret = self::where('output_name', '=', $output_name)->first();
		}
		return $ret;
	}
	
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ 'extension' => '', 'group_id' => '', 'user_id' => '', ] + parent::getFilterFields();
	}
	
	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'name', '');
		if ($tmp != '') {
			$query = $query->where('name', 'LIKE', '%'.$tmp.'%')->orWhere('real_name', 'LIKE', '%'.$tmp.'%')->orWhere('output_name', 'LIKE', '%'.$tmp.'%');
		}
		$tmp = Arr::get($filter_data, 'extension', '');
		if ($tmp != '') {
			$query = $query->where('extension', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'group_id', '');
		if ($tmp != '') {
			$query = $query->where('group_id', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'user_id', '');
		if ($tmp != '') {
			$query = $query->where('user_id', '=', ''.$tmp.'');
		}

		
		$query = parent::applyFilter($query, $filter_data);
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return [ 'id'=>'', 'created_at'=> 'asc', 'user_id'=>'', 'size'=>'', ] + parent::getOrderingFields();
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'created_at', '');
		if ($tmp != '') {
			$query = $query->orderBy('created_at', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'user_id', '');
		if ($tmp != '') {
			$query = $query->orderBy('user_id', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'size', '');
		if ($tmp != '') {
			$query = $query->orderBy('size', $tmp);
		}
		
		$query = parent::applyOrdering($query, $ordering_data);
		return $query;
	}
	
	
	/* relations */
	
	public function group()
	{
		return $this->belongsTo(FileStorageGroup::class);
	}
	
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	
	/* list queries */
	
	public static function addQueryByGroupId($query, $group_id = '')
	{
		$query = parent::addQueryFilterAndOrdering($query);
		if ($group_id) {
			$query = $query->where('group_id', '=', $group_id);
		}
		return $query;
	}
	
	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'download') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
	
	
	/* path & url */
	
	public function getDownloadUrl($by_name = false)
	{
		$ret = '/file-storage/download/'.( ($by_name) ? 'name/'.$this->real_name : 'id/'.$this->id );
		return url($ret);
	}
	
	public function getUrl($relative = true, $deeper_path = '')
	{
		$ret = 'storage'.$this->group->getPathChain().'/'.$deeper_path.$this->real_name;
		return ($relative) ? $ret : asset('/'.$ret);
	}

	public function getShortPath()
	{
		$full_path = $this->getFullPath();
		$path = str_replace(storage_path('app'), '', $full_path);
		return $path;
	}

	public function getFullPath($group_id = '') 
	{
		if ($group_id) {
			$group = FileStorageGroup::find($group_id);
		} else {
			$group = $this->group;
		}
		return (($group) ? $group->getFullPath() : storage_path('app')) . '/'.$this->real_name;
	}
	
	public function getThumbnailPath()
	{
		$ret = $this->getShortPath();
		$ret = File::dirname($ret);
		$ret .= '/' . $this->thumbnail;
		return $ret;
	}
	
	public function getThumbnailFullPath()
	{
		return storage_path('app/'.$this->getThumbnailPath());
	}
	
	public function getThumbnailUrl($relative = true)
	{
		if ($this->thumbnail) {
			$ret = 'storage'.$this->group->getPathChain().'/'.$this->thumbnail;
			return ($relative) ? $ret : asset('/'.$ret);
		} else {
			$file = FileStorage::findByRealName($this->extension.'-file.png');
			if ($file) {
				return $file->getUrl($relative);
			}
			$file = FileStorage::findByRealName('any-file.png');
			if ($file) {
				return $file->getUrl($relative);
			}
		}
		return '';
	}
	
	public function createThumbnail($update = false, $file_full_path = '', $update_db_record = true)
	{
		if (!$file_full_path) {
			$file_full_path = $this->getFullPath();
		}
		$extension = File::extension($file_full_path);
		if (!self::$thumbnail_dir || $extension !== 'jpg') {
			return;
		}
		
		$file_dir_path = File::dirname($file_full_path);
		$thumb_name = basename($file_full_path);
		$thumb_dir_path = $file_dir_path . '/' . self::$thumbnail_dir;
		$thumb_full_path = $thumb_dir_path. '/' . $thumb_name;
		
		$thumb_dir_path_exists = File::exists($thumb_dir_path);
		if (!$thumb_dir_path_exists) {
			File::makeDirectory($thumb_dir_path);
		}
		
		$thumb_itm_stor_path_exists = File::exists($thumb_full_path);
		if (!$thumb_itm_stor_path_exists || $update) {
			Image::make($file_full_path)->fit(400, 400, null, 'top-left')->save($thumb_full_path);
		}
		
		if ($update_db_record) {
			$this->thumbnail = self::$thumbnail_dir.'/'.$thumb_name;
			$this->save();
			return $this->thumbnail;
		}
		
		return self::$thumbnail_dir.'/'.$thumb_name;
	}
	
	public function deleteThumbnail($file_full_path = '', $thumbnail = '')
	{
		if (!$file_full_path || !$thumbnail) {
			$thumbnail_path = $this->getThumbnailFullPath();
		} else {
			$file_dir_path = File::dirname($file_full_path);
			$thumbnail_path = $file_dir_path . '/' . $thumbnail;
		}
		File::delete($thumbnail_path);
	}
	
	public function deleteFile()
	{
		File::delete($this->getFullPath());
		$this->deleteThumbnail();
	}
	
	
	/* database */
	
	public function save(array $options = [])
	{
		$old_path = '';
		$original = $this->getOriginal();
		if (count($original)) {
			$old_path = $this->getFullPath($original['group_id']);
		}
		$new_path = $this->getFullPath();
		
		if ($old_path != $new_path && $old_path != '') {
			if (File::exists($new_path)) {
				Session::flash('flash_error_messages', ['Файл с указанным именем уже существует в выбранной группе!']);
				return false;
			} else {
				if (File::exists($old_path)) {
					$this->deleteThumbnail($old_path, $original['thumbnail']);
					File::move($old_path, $new_path);
					$this->thumbnail = $this->createThumbnail(true, $new_path, false);
				} else {
					Session::flash('flash_error_messages', ['Файл с указанным именем не существует!']);
					return false;
				}
			}
		}
		
		parent::save($options = []);
	}

}
