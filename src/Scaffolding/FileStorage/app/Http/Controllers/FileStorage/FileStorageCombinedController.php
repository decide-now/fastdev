<?php
namespace App\Http\Controllers\FileStorage;

use DecideNow\FastDev\Scene\SceneBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\FileStorageGroup\FileStorageGroupListController;

class FileStorageCombinedController extends SceneBaseController
{
	static $scene_id = 'file_storage-combined';
	
	protected function defineChildren()
	{	
		$list_scene = new FileStorageListController($this);
		$this->scene_children->put('list_scene', $list_scene);
		
		$tree_scene = new FileStorageGroupListController($this);
		$this->scene_children->put('tree_scene', $tree_scene);
	}
	
	public function prepareContent(Request $request)
	{
		$tree_scene = $this->scene_children->get('tree_scene');
		$list_scene = $this->scene_children->get('list_scene');
		
		$tree_scene->selected_item_id = Session::get(get_class($list_scene).'.group_id', '');
		$tree_scene->prepareContent($request);
		
		$list_scene->group_id = $tree_scene->selected_item_id;
		$list_scene->page = Session::get(get_class($list_scene).'.page', '');
		$list_scene->prepareContent($request);
		
		
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			$request->flash();
			return redirect()->action($this->methodPath('get'))->with('ajax', $request->ajax());
		}
	}
}