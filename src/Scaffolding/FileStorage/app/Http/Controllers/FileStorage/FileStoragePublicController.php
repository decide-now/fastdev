<?php

namespace App\Http\Controllers\FileStorage;

use App\ORM\FileStorage;
use DecideNow\FastDev\Scene\SceneBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FileStoragePublicController extends SceneBaseController {

	public static function download(Request $request, $field, $value)
	{
		if ($field != 'name' &&  $field != 'id') {
			Session::flash('flash_error_messages', ['Файл не найден!']);
			return back();
		}
		
		$item = FileStorage::where($field, '=', $value)->first();
		if (!$item) {
			Session::flash('flash_error_messages', ['Файл не найден!']);
			return back();
		}
		
		$file_path = $item->getFullPath();
		return response()->download($file_path, $item->name, ['Content-Type: ' . $item->mime . '']);
	}
}