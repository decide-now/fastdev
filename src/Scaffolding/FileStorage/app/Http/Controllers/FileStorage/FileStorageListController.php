<?php
namespace App\Http\Controllers\FileStorage;

use App\Http\Controllers\FileStorageGroup\FileStorageGroupSelectController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ORM\FileStorageGroup;

class FileStorageListController extends FileStorageController
{
	static $scene_id = 'file_storage-list';
	
	public $list;
	public $filter;
	public $ordering;
	public $group_id;
	public $group;
	
	protected function defineChildren()
	{	
		$this->defineAlertChildScene();
		
		$filter_scene = new FileStorageFilterController($this);
		$this->scene_children->put('filter_scene', $filter_scene);
		
		$item_scene = new FileStorageItemController($this);
		$this->scene_children->put('item_scene', $item_scene);
		
		$file_storage_group_select_scene = new FileStorageGroupSelectController($this);
		$this->scene_children->put('file_storage_group_select_scene', $file_storage_group_select_scene);
	}
	
	public function prepareContent(Request $request)
	{
		$model = $this->model;
		$list_data = new $this->model;
		
		$list_data = $model::addQueryByGroupId($list_data, $this->group_id);
		
		$list_data = $model::applySessionFilter($list_data, $this->filter);
		$list_data = $model::applySessionOrdering($list_data, $this->ordering);
		
		$this->item_id = Session::get('item_id');
		$this->list = $this->paginateExt($list_data);
		
		$item_scene = $this->scene_children->get('item_scene');
		$item_scene->group_id = $this->group_id;
		$item_scene->prepareContent($request);
		
		if ($this->group_id) {
			$this->group = FileStorageGroup::find($this->group_id);
		}
		
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->page = $this->transferData('page', '1');
		$this->group_id = $this->transferData('group_id', '');
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferData('page', '1', $request);
		$group_id = $this->transferData('group_id', '', $request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($request->has('page')) {
			Session::put(get_called_class().'.page', $request->get('page'));
		}
		if ($request->has('group_id')) {
			Session::put(get_called_class().'.group_id', $request->get('group_id'));
		}
		
		if ($task == 'delete') {
			$this->itemDelete($request);
			return $this->redirectToList();
		} elseif ($task == 'move') {
			$group_id = $request->get('new_group_id', '');
			$item = $this->itemMove($request, '', $group_id);
			return $this->redirectToList($item);;
		} elseif ($task == 'set_output_name') {
			$item = $this->itemSetOutputName($request);
			return $this->redirectToList($item);;
		} elseif ($task == 'download') {
			$item_id = $request->get('item_id', '');
			return redirect()->action(FileStoragePublicController::methodPath('download'), ['field' => 'id', 'value' => $item_id]);
		} elseif ($task == 'refresh') {
			$request->flash();
			return redirect()->action($this->methodPath('get'))->with('ajax', $request->ajax());
		}
	}
}