<?php
namespace App\Http\Controllers\FileStorage;

use App\Http\Controllers\FileStorageGroup\FileStorageGroupListController;
use DecideNow\FastDev\Scene\SceneBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FileStorageCombinedSelectController extends SceneBaseController
{
	static $scene_id = 'file_storage-combined_select';
	
	public $absolute_links;
	public $call_button_id;
	
	protected function defineChildren()
	{	
		$list_scene = new FileStorageSelectController($this);
		$this->scene_children->put('list_scene', $list_scene);
		
		$tree_scene = new FileStorageGroupListController($this);
		$this->scene_children->put('tree_scene', $tree_scene);
	}
	
	public function prepareContent(Request $request)
	{	
		$tree_scene = $this->scene_children->get('tree_scene');
		$list_scene = $this->scene_children->get('list_scene');
		
		$tree_scene->selected_item_id = Session::get(get_class($list_scene).'.group_id', '');
		$tree_scene->prepareContent($request);
		
		$list_scene->call_button_id = $this->call_button_id;
		$list_scene->absolute_links = $this->absolute_links;
		$list_scene->group_id = $tree_scene->selected_item_id;
		$list_scene->page = Session::get(get_class($list_scene).'.page', '');
		$list_scene->prepareContent($request);
		
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->call_button_id = $this->transferData('call_button_id', '');
		$this->absolute_links = $this->transferData('absolute_links', '0');
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferData('call_button_id', '', $request);
		$this->transferData('absolute_links', '0', $request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			$request->flash();
			return redirect()->action($this->methodPath('get'))->with('ajax', $request->ajax());
		}
	}
}