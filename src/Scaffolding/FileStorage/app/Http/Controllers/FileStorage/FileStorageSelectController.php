<?php
namespace App\Http\Controllers\FileStorage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\ORM\FileStorageGroup;

class FileStorageSelectController extends FileStorageController
{	
	static $scene_id = 'file_storage-select';
	
	public $absolute_links;
	public $list;
	public $filter;
	public $ordering;
	public $page;
	public $group_id;
	public $group;
	public $call_button_id;
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
		
		$filter_scene = new FileStorageFilterController($this);
		$this->scene_children->put('filter_scene', $filter_scene);
		
		$item_scene = new FileStorageItemController($this);
		$this->scene_children->put('item_scene', $item_scene);
	}
	
	public function prepareContent(Request $request)
	{	
		$model = $this->model;
		$list_data = new $this->model;
		
		$list_data = $model::addQueryByGroupId($list_data, $this->group_id);
		
		$list_data = $model::applySessionFilter($list_data, $this->filter);
		$list_data = $model::applySessionOrdering($list_data, $this->ordering);
		
		$this->list = $list_data->paginate(null, ['*'], 'page', $this->page)->withPath(action($this->methodPath('get')), [], false);
		
		$item_scene = $this->scene_children->get('item_scene');
		$item_scene->group_id = $this->group_id;
		$item_scene->prepareContent($request);
		
		if ($this->group_id) {
			$this->group = FileStorageGroup::find($this->group_id);
		}
		
		$this->no_content = false;
	}

	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->page = $this->transferData('page', '1');
		$this->group_id = $this->transferData('group_id', '');
		$this->call_button_id = $this->transferData('call_button_id', '');
		$this->absolute_links = $this->transferData('absolute_links', '0');
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferData('page', '1', $request);
		$this->transferData('call_button_id', '', $request);
		$this->transferData('absolute_links', '0', $request);
		$this->transferData('group_id', '', $request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($request->has('page')) {
			Session::put(get_called_class().'.page', $request->get('page'));
		}
		if ($request->has('group_id')) {
			Session::put(get_called_class().'.group_id', $request->get('group_id'));
		}
		
		if ($task == 'refresh') {
			$request->flash();
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		}
	}
}