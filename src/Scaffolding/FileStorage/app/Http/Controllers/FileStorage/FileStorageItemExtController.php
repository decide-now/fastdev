<?php
namespace App\Http\Controllers\FileStorage;

use Illuminate\Http\Request;

class FileStorageItemExtController extends FileStorageController
{
	static $scene_id = 'file_storage-item_ext';
	
	public $task;
	public $refresh_parent_scene;
	
	public $item;
	public $group_id;
	public $title;
	
	public $allow_upload = 1;
	public $allow_delete = 1;
	public $allow_download = 1;
	
	public $after_upload;
	public $before_delete;
	
	
	private static function taskBeforeAfter($query, $item_id)
	{
		if (!$item_id) {
			return;
		}
		$callback_params = [];
		parse_str($query, $callback_params);
		$callback_name = array_shift($callback_params);
		if ($callback_name) {
			call_user_func_array([__CLASS__, $callback_name], array_prepend($callback_params, $item_id));
		}
	}
	
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
	}
	
	public function prepareContent(Request $request)
	{
		if (!$this->item) {
			$this->item = new $this->model;
			$this->item->group_id = $this->group_id;
		}
		if (!$this->title) {
			$this->title = 'Выберите файл или перетащите его в эту область...';
		}
		
		$this->no_content = false;
	}
	
	public function get(Request $request, $item_id = '')
	{
		$this->transferSceneId();
		$this->task = $this->transferData('task', '');
		$this->group_id = $this->transferData('group_id', '');
		$this->title = $this->transferData('title', '');
		$this->allow_upload = $this->transferData('allow_upload', '1');
		$this->allow_delete = $this->transferData('allow_delete', '1');
		$this->allow_download = $this->transferData('allow_download', '1');
		$this->after_upload = $this->transferData('after_upload', '');
		$this->before_delete = $this->transferData('before_delete', '');
		
		if ($item_id) {
			$this->item = $this->model->find($item_id);
		}
		
		$this->refresh_parent_scene = ($this->task == 'upload') ? '1' : $this->transferData('refresh_parent_scene', '0');
		$this->response_data = ['refresh_parent_scene' => $this->refresh_parent_scene];
		
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request, $item_id = '')
	{
		$this->transferSceneId($request);
		$this->transferData('refresh_parent_scene', '0', $request);
		
		$this->transferData('title', '', $request);
		$this->transferData('allow_upload', 1, $request);
		$this->transferData('allow_delete', 1, $request);
		$this->transferData('allow_download', 1, $request);
		
		$group_id = $this->transferData('group_id', '', $request);
		$task = $this->transferData('task', 'refresh', $request);
		
		$after_upload = $this->transferData('after_upload', '', $request);
		$before_delete = $this->transferData('before_delete', '', $request);
		
		if ($task == 'upload') {
			$items = $this->itemUpload($request, $group_id);
			$item = array_first($items, null, null);
			if ($after_upload) {
				self::taskBeforeAfter($after_upload, $item->id);
			}
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])->with('ajax', $request->ajax());
		} elseif ($task == 'delete') {
			$item_id = $request->get('item_id');
			if ($before_delete) {
				self::taskBeforeAfter($before_delete, $item_id);
			}
			$this->itemDelete($request);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'download') {
			$item_id = $request->get('item_id');
			return FileStoragePublicController::download($request, 'id', $item_id);
		} elseif ($task == 'refresh') {
			$item = $this->itemFind($request);
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])->with('ajax', $request->ajax());
		}
	}
	
	protected function contentErrorRedirect()
	{
		return redirect()->action(FileStorageListController::methodPath('get'));
	}
	
}