<?php
namespace App\Http\Controllers\FileStorage;

use Illuminate\Http\Request;

class FileStorageItemController extends FileStorageController
{
	static $scene_id = 'file_storage-item';
	
	public $item;
	public $group_id;
	
	public $task;
	public $refresh_parent_scene = '0';

	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
	}
	
	public function prepareContent(Request $request)
	{
		$this->item = new $this->model;
		$this->item->group_id = $this->group_id;
		
		$this->no_content = false;
	}
	
	public function get(Request $request, $item_id = '')
	{
		$this->transferSceneId();
		$this->task = $this->transferData('task', '');
		$this->group_id = $this->transferData('group_id', '');
		
		$this->refresh_parent_scene = ($this->task == 'upload') ? '1' : $this->transferData('refresh_parent_scene', '0');
		$this->response_data = ['refresh_parent_scene' => $this->refresh_parent_scene];
		
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request, $item_id = '')
	{
		$this->transferSceneId($request);
		$this->transferData('refresh_parent_scene', '0', $request);
		
		$group_id = $this->transferData('group_id', '', $request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'upload') {
			$this->itemUpload($request, $group_id);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'refresh') {
			$this->itemFind($request);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		}
	}
	
	protected function contentErrorRedirect()
	{
		return redirect()->action(FileStorageListController::methodPath('get'));
	}
	
}