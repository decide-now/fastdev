<?php
namespace App\Http\Controllers\FileStorage;

use App\ORM\FileStorage;
use App\ORM\FileStorageGroup;
use DecideNow\FastDev\Scene\SceneBaseController;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FileStorageController extends SceneBaseController
{	
	protected $model;
	protected $validator;
	public $scene_template_type;
	
	public $page;
	public $item_id;
	public function paginateExt($data, $perPage = null)
	{
		$perPage = $perPage ?: $data->getModel()->getPerPage();
		$list_page = $this->page;
		if ($this->item_id) {
			$dt = clone($data);
			$row_num = $dt->select($this->model->getTable().'.id')->pluck('id')->search($this->item_id) + 1;
			$list_page = ceil($row_num / $perPage);
		}
		return $data
		->paginate($perPage, ['*'], 'page', $list_page)
		->withPath(action($this->methodPath('get')), [], false);
	}
	
	public function __construct($scene_parent = null)
	{
		parent::__construct($scene_parent);
		$this->middleware('auth');
		$this->model = new FileStorage;
		$this->scene_template_type = 'singlepage';
	}
	
	protected function itemMove($request, $item_id = '', $group_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		
		if (!$group_id) {
			$group_id = $request->get('group_id');
		}
		$item->group_id = $group_id;
		$item->save();
		Session::flash('item', $item);
		return $item;
	}
	
	protected function itemSetOutputName($request, $item_id = '', $output_name = null)
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		
		if ($output_name === null) {
			$output_name = $request->get('output_name');
		}
		$item->output_name = $output_name;
		$item->save();
		Session::flash('item', $item);
		return $item;
	}
	
	protected function itemCreate($request, $group_id)
	{
		$item = new $this->model;
		if ($group_id) {
			$item->group_id = $group_id;
		}
		Session::flash('item', $item);
		return $item;
	}
	
	protected function itemFind($request, $item_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		Session::flash('item', $item);
		return $item;
	}
	
	protected function itemDelete($request, $item_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		
		DB::beginTransaction();
		try {
			$item->deleteFile();
			$item->delete();
			DB::commit();
			Session::flash('flash_success_messages', ['Запись удалена!']);
		} catch (QueryException $e) {
			DB::rollBack();
			$msg = $this->getDatabaseErrorMessage($e);
			Session::flash('flash_error_messages', [$msg]);
		}
	}
	
	protected function itemFromInput($request)
	{
		$itemClass = $this->model->getModelName();
		
		$item_id = $request->get('item_id');
		$item = ($item_id) ? $item = $this->model->find($item_id) : new $this->model;
		
		$item_input = $request->get($itemClass);
		
		$item->fill($item_input);
		
		return $item;
	}
	
	protected function itemValidate($request)
	{
		$item = $this->itemFromInput($request);
		Session::flash('item', $item);
		
		$this->validator = null;
		$itemClass = $this->model->getModelName();
		
		$rules = [
			$itemClass.'.name' => 'required|max:255',
			$itemClass.'.group_id' => 'required|max:255',
			$itemClass.'.user_id' => 'required|max:255',
		];
		
		$this->validator = Validator::make($request->all(), $rules, [], $this->model->getAliasesWithModelName());
		if ($this->validator->fails()) {
			Session::flash('flash_error_messages', $this->validator->errors()->messages());
			return false;
		} else {
			return true;
		}
	}
	
	protected function itemSave($request, $item = null)
	{
		if (!$item) {
			$item = $this->itemFromInput($request);
		}
		
		DB::beginTransaction();
		$item->save();
		DB::commit();
		
		Session::flash('item', $item);
		return $item;
	}
	
	protected function itemUpload($request, $group_id = '', $visible_name = '')
	{
		$ret = [];
		if (! $request->hasFile('files')) {
			return $ret;
		}
		
		if (!$group_id) {
			$group_id = $request->get('group_id', '');
		}
		$group = FileStorageGroup::find($group_id);
		
		if (!$this->model->allow_save_to_root_group && !$group) {
			Session::flash('flash_error_messages', ['Для загрузки файла необходимо выбрать группу!']);
			return $ret;
		}
		
		$files_array = $request->file('files');
		foreach ( $files_array as $file_item ) {
			
			$filename = '';
			if ($visible_name) {
				$filename = $visible_name;
				$file_visible_name = $visible_name;
			} else {
				$file_visible_name = $file_item->getClientOriginalName();
				if ($group) {
					if ($group->is_public) {
						$filename = $file_visible_name;
					}
				}
			}
			
			$group_path = ($group) ? $group->getShortPath() : '';
			if ($filename) {
				$path = Storage::putFileAs($group_path, $file_item, $filename);
			} else {
				$path = Storage::putFile($group_path, $file_item);
			}
			setlocale(LC_ALL,'ru_RU.UTF-8');
			$file_storage = FileStorage::create([
				'name' => $file_visible_name,
				'size' => $file_item->getClientSize(),
				'real_name' => basename($path),
				'output_name' => $file_visible_name,
				'extension' => $file_item->getClientOriginalExtension(),
				'mime' => $file_item->getClientMimeType(),
				'user_id' => Auth::user()->id,
				'group_id' => empty($group_id) ? null : $group_id,
			]);
			
			$file_storage->createThumbnail(true);
			
			$ret[] = $file_storage;
		}
		return $ret;
	}

	
	protected function redirectToItem($item)
	{
		return redirect()->action(FileStorageItemController::methodPath('get'), ['id' => ($item) ? $item->id : '']);
	}
	
	protected function redirectToList($item = null)
	{
		return redirect()
			->action(FileStorageListController::methodPath('get'))
			->with('item_id', ($item) ? $item->id : '');
	}
}