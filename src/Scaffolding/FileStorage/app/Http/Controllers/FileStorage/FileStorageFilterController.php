<?php
namespace App\Http\Controllers\FileStorage;

use Illuminate\Http\Request;

class FileStorageFilterController extends FileStorageController
{	
	static $scene_id = 'file_storage-filter';
	
	public $filter;
	
	public function prepareContent(Request $request)
	{	
		$model = $this->model;
		
		$this->filter = $model::filterGetFromSession();
		
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		}
	}	
}