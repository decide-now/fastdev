<?php
namespace App\Http\Controllers\FileStorageGroup;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class FileStorageGroupSelectController extends FileStorageGroupController
{	
	static $scene_id = 'file_storage_group-select';
	
	public $list;
	public $filter;
	public $ordering;
	public $page;
	public $call_button_id;
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
		
		$filter_scene = new FileStorageGroupFilterController($this);
		$this->scene_children->put('filter_scene', $filter_scene);
	}
	
	private function paginateTree(Collection $collection, $perPage = 10)
	{
		$currentPage = $this->page;
		$currentPath = LengthAwarePaginator::resolveCurrentPath();
		$currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
		$paginetedItems = new LengthAwarePaginator($currentPageItems, count($collection), $perPage, $currentPage);
		$paginetedItems->setPath($currentPath);
		return $paginetedItems;
	}
	
	public function prepareContent(Request $request)
	{	
		$model = $this->model;
		$list_data = new $this->model;
		
		$list_data = $model::addQueryFilterAndOrdering($list_data);
		
		$filtered_data = $model::applySessionFilter($list_data, $this->filter);
		$filtered_ids = $filtered_data->get()->pluck('id')->all();
		$list_data = $model::applySessionOrdering($list_data, $this->ordering);
		
		$list_data = $model::getTree($list_data);
		
		foreach ($list_data as $list_item) {
			if (!in_array($list_item->id, $filtered_ids)) {
				$list_data->forget($list_item->id);
			}
		}
		
		$this->list = $this->paginateTree($list_data);
		
		$this->no_content = false;
	}

	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->page = $this->transferData('page', '1');
		$this->call_button_id = $this->transferData('call_button_id', '');
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferData('page', '1', $request);
		$this->transferData('call_button_id', '', $request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			$request->flash();
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		}
	}
}