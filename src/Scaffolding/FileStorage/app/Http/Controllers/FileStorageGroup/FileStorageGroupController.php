<?php
namespace App\Http\Controllers\FileStorageGroup;

use App\ORM\FileStorage;
use App\ORM\FileStorageGroup;
use DecideNow\FastDev\Facades\FastDevHlpr;
use DecideNow\FastDev\Scene\SceneBaseController;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class FileStorageGroupController extends SceneBaseController
{	
	protected $model;
	protected $validator;
	public $scene_template_type;
	
	public function __construct($scene_parent = null)
	{
		parent::__construct($scene_parent);
		$this->middleware('auth');
		$this->model = new FileStorageGroup;
		$this->scene_template_type = 'singlepage';
	}
	
	protected function itemCreate($request, $parent_id = '')
	{
		if (!$parent_id) {
			$parent_id = $request->get('parent_id');
		}
		$item = new $this->model;
		if (!$item->getPermission('allow_non_public')) {
			$item->is_public = true;
		}
		if ($parent_id) {
			$item->parent_id = $parent_id;
		}
		Session::flash('item', $item);
	}
	
	protected function itemFind($request, $item_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		Session::flash('item', $item);
	}
	
	protected function itemDelete($request, $item_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		
		DB::beginTransaction();
		try {
			File::deleteDirectory($item->getFullPath());
			$item->delete();
			DB::commit();
			Session::flash('flash_success_messages', ['Запись удалена!']);
		} catch (QueryException $e) {
			DB::rollBack();
			$msg = $this->getDatabaseErrorMessage($e);
			Session::flash('flash_error_messages', [$msg]);
		}
	}
	
	protected function itemScan($request, $item_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		
		DB::beginTransaction();
		try {
			$files = File::files($item->getFullPath());
			foreach($files as $file) {
			
				$filename = basename($file);
				
				setlocale(LC_ALL,'ru_RU.UTF-8');
				$file_storage = FileStorage::take(1)
					->where('group_id', '=', $item->id)
					->where('real_name', '=', $filename)
					->get()
					->first();
				if (!$file_storage) {
					$file_storage = new FileStorage;
				}
				$file_storage->name = $filename;
				$file_storage->size = File::size($file);
				$file_storage->real_name = $filename;
				$file_storage->output_name = $filename;
				$file_storage->extension = File::extension($file);
				$file_storage->mime = File::mimeType($file);
				$file_storage->user_id = Auth::user()->id;
				$file_storage->group_id = $item->id;
				$file_storage->save();
			}
			
			DB::commit();
			Session::flash('flash_success_messages', ['Папка просканирована!']);
		} catch (QueryException $e) {
			DB::rollBack();
			$msg = $this->getDatabaseErrorMessage($e);
			Session::flash('flash_error_messages', [$msg]);
		}
	}
	
	protected function itemFromInput($request)
	{
		$itemClass = $this->model->getModelName();
		
		$item_id = $request->get('item_id');
		$item = ($item_id) ? $item = $this->model->find($item_id) : new $this->model;
		
		$item_input = $request->get($itemClass);
		$tmp_is_public = (array_key_exists('is_public', $item_input) && $item_input['is_public']);
		$item_input['is_public'] = $tmp_is_public;
		
		$item->fill($item_input);
		
		return $item;
	}
	
	protected function itemValidate($request)
	{
		$item = $this->itemFromInput($request);
		Session::flash('item', $item);
		
		$this->validator = null;
		$itemClass = $this->model->getModelName();
		
		$rules = [
			$itemClass.'.name' => 'required|max:255',
			//$itemClass.'.code' => 'required|max:255',
		];
		
		$this->validator = Validator::make($request->all(), $rules, [], $this->model->getAliasesWithModelName());
		if ($this->validator->fails()) {
			Session::flash('flash_error_messages', $this->validator->errors()->messages());
			return false;
		} else {
			return true;
		}
	}
	
	protected function itemSave($request, $item = null)
	{
		if (!$item) {
			$item = $this->itemFromInput($request);
		}
		
		// автоматическое формирование кода
		$wrong_code_message = 'Код не уникален! Внесите изменения.';
		if (!$item->code) {
			$item->code = FastDevHlpr::translitForURL($item->name);
			$wrong_code_message = 'Сгенерированный код не уникален! Укажите значение вручную.';
		}
		if ($this->model->where('id', '!=', $item->id)->where('code', '=', $item->code)->count()) {
			Session::flash('item', $item);
			Session::flash('flash_error_messages', [$wrong_code_message]);
			return;
		}
		
		DB::beginTransaction();
		$item->save();
		DB::commit();
		
		Session::flash('item', $item);
	}

}