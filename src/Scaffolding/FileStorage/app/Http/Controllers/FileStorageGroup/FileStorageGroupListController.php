<?php
namespace App\Http\Controllers\FileStorageGroup;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class FileStorageGroupListController extends FileStorageGroupController
{
	static $scene_id = 'file_storage_group-list';
	
	public $task;
	public $list;
	public $filter;
	public $ordering;
	public $page;
	public $selected_item_id;
	
	protected function defineChildren()
	{	
		$this->defineAlertChildScene();
		
		$filter_scene = new FileStorageGroupFilterController($this);
		$this->scene_children->put('filter_scene', $filter_scene);
		
		$item_scene = new FileStorageGroupItemController($this);
		$this->scene_children->put('item_scene', $item_scene);
	}
	
	private function paginateTree(Collection $collection, $perPage = 10)
	{
		$currentPage = $this->page;
		$currentPath = LengthAwarePaginator::resolveCurrentPath();
		$currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
		$paginetedItems = new LengthAwarePaginator($currentPageItems, count($collection), $perPage, $currentPage);
		$paginetedItems->setPath($currentPath);
		return $paginetedItems;
	}
	
	public function prepareContent(Request $request)
	{
		$model = $this->model;
		$list_data = new $this->model;
		
		$list_data = $model::addQueryFilterAndOrdering($list_data);
		
		$filtered_data = $model::applySessionFilter($list_data, $this->filter);
		$filtered_ids = $filtered_data->get()->pluck('id')->all();
		$list_data = $model::applySessionOrdering($list_data, $this->ordering);
		
		$list_data = $model::getTree($list_data);
		
		foreach ($list_data as $list_item) {
			if (!in_array($list_item->id, $filtered_ids)) {
				$list_data->forget($list_item->id);
			}
		}
		
		if (!$this->selected_item_id) {
			$selected_item = $this->model->first();
			if ($selected_item) {
				$this->selected_item_id = $selected_item->id;
			}
		}
		
		$this->list = $this->paginateTree($list_data);
		
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->page = $this->transferData('page', '1');
		$this->selected_item_id = $this->transferData('selected_item_id', '');
		$this->task = $this->transferData('task', 'refresh');
		
		$this->response_data = ['task' => $this->task, 'item_id' => $this->selected_item_id];
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferData('page', '1', $request);
		$this->transferData('selected_item_id', '', $request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'delete') {
			$this->itemDelete($request);
			return redirect()->action($this->methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'scan') {
			$this->itemScan($request);
			return redirect()->action($this->methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'refresh') {
			$request->flash();
			return redirect()->action($this->methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		} elseif ($task == 'ordering') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->orderingPutToSession($request, '', $force_clear);
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		}
	}
}