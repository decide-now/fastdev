
...

use Decidenow\FastDev\Helper\Helper;
use Illuminate\Http\Exceptions\PostTooLargeException;

...

class Handler extends ExceptionHandler
{

	...
	
	public function render($request, Exception $exception)
	{
		...
		if ($exception instanceof PostTooLargeException) {
			if ($request->ajax()) {
				$file_upload_max_size = round(Helper::file_upload_max_size() / 1024 / 1024);
				$reponseData = [
					'message' => [
						'Размер файла превышает допустимый (' . $file_upload_max_size . ' Мб)'
					]
				];
				return response()->json($reponseData, 422);
			}
		}
		
		...
		
	}
	
	...
	
}

