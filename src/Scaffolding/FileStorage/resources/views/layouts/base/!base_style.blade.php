...

/* File Storage */

[id^="inputfile"] {
	width: 0.1px;
	height: 0.1px;
	opacity: 0;
	overflow: hidden;
	position: absolute;
	z-index: -1;
}
#files_list {
	margin: 10px 0;
	text-align: left;
}
#files_list li {
	margin: 5px 0 0 0;
}
#upload_area {
	border: 1px solid #ccc;
	padding: 15px;
	position: relative;
	z-index: 0;
}
#upload_area .drag-here {
	position: absolute;
	top: 5px;
	bottom: 5px;
	left: 5px;
	right: 5px;
	background-color: #f9f9f9;
	border-radius: 5px;
	border: 1px dashed #ccc;
	display: block;
	pointer-events: none;
	z-index: -1;
}
#upload_area.dragover .drag-here {
	display: none;
}
#upload_area .hdr {
	font-size: 16px;
	line-height: 1.1em;
}

#upload_area.square {
	min-height: 150px;
}
#upload_area.square .hdr {
	min-height: 50px;
	font-size: 16px;
	line-height: 1.1em;
}
#upload_area.square .act {
	min-height: 50px;
}
#upload_area.square .info {
	min-height: 50px;
}


/* Collapsible  panel */
.panel-heading[data-toggle="collapse"] .panel-title {
	cursor: pointer;
}
.panel-heading[data-toggle="collapse"] .panel-title:after {
	font-family: 'Glyphicons Halflings';
	content: "\e079";
	float: right;
	padding-left: 1em;
}
.panel-heading[data-toggle="collapse"][aria-expanded="true"] .panel-title:after {
	content: "\e114";
}

...