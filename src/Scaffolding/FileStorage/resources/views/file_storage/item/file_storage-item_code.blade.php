<script>
new Scene('{{ $scene->sceneId() }}', {
	
	/* init */
	init : function() {
		
		var thisScene = this;
		
		thisScene.getRootElement().data('filesData', []);

		thisScene.getElement('[id="btn_ok_item"]').click( { thisScene: this }, thisScene.btnOkItem_Click );
		thisScene.getElement('[id^="inputfile"]').change( { thisScene: this }, thisScene.inputfile_Change );

		thisScene.getElement('[id="upload_area"]')
			.on('dragover', { thisScene: this }, thisScene.uploadDropdown_Dragover )
			.on('dragleave', { thisScene: this }, thisScene.uploadDropdown_Dragleave )
			.on('drop', { thisScene: this }, thisScene.uploadDropdown_Drop );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		var thisScene = this;

		var filesData = thisScene.getRootElement().data('filesData');
		data.alerts_scene_id = '{{ $scene_parent->sceneId() }}'+'-alert-message';
		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh', filesData);
	},

	afterSuccessAJAX : function(response) {
		thisScene = response.thisScene;

		var parentScene = stage.getScene('{{ ($scene_parent) ? $scene_parent->sceneId() : "" }}');
		parentScene.itemReturn({ refresh: response.refresh_parent_scene });
	},
	
	
	/**/
	
	btnOkItem_Click : function(e) {
		var thisScene = e.data.thisScene;

		var data = { task: 'upload', ajax: true, }
		thisScene.sceneRefresh(data);
		return false;
	},
	
	inputfileHas : function() {
		var thisScene = this;
		
		var lng = thisScene.getRootElement().data('filesData').length;
		return (lng = 0); 
	},
	
	inputfileUpdate : function() {
		var thisScene = this;

		var filesData = thisScene.getRootElement().data('filesData');
		
		var list = thisScene.getElement('[id="files_list"]');
		list.html('');
		
		var lng = filesData.length;
		for (i=0; i<lng; i++) {
			var item = $('<li>'+filesData[i].name+' <a href="" id="remove_file" data-idx="'+i+'"><i class="glyphicon glyphicon-remove"></i></a></li>');
			list.append(item);
		}

		var btn_ok_item = thisScene.getElement('[id="btn_ok_item"]');
		if (filesData.length > 0) {
			btn_ok_item.removeClass('hidden');
		} else {
			btn_ok_item.addClass('hidden');
		}
		thisScene.getElement('[id^="remove_file"]').click( { 'thisScene': thisScene },  thisScene.inputfileRemove );
	},

	inputfile_Change : function(e) {
		var thisScene = e.data.thisScene;
		
		var inputfile_el = $(this);
		var inputfile_el_js = inputfile_el.get(0);
		thisScene.inputfileAdd(inputfile_el_js.files);
		inputfile_el.val('');
	},
	
	inputfileAdd : function(filesData) {
		var thisScene = this;
		
		var multiple_inputfile = thisScene.getElement('[id^="inputfile"][multiple]');

		var tmpFilesData = [];
		if (multiple_inputfile.length > 0) {
			tmpFilesData = thisScene.getRootElement().data('filesData');
		}
		
		var lng = filesData.length;
		for (i=0; i<lng; i++) {
			tmpFilesData.push(filesData[i]);
			if (multiple_inputfile.length == 0) {
				break;
			}
		}
		thisScene.getRootElement().data('filesData', tmpFilesData);

		thisScene.inputfileUpdate();
	},

	inputfileRemove : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;

		var data_idx = parseInt($(this).attr('data-idx'));

		filesData = thisScene.getRootElement().data('filesData');
		filesData.splice(data_idx, 1);
		
		thisScene.inputfileUpdate();
		
		return false;
	},

	uploadDropdown_Dragover : function(e) {
		e.preventDefault();  
		e.stopPropagation();
		if ($(this).find('.drag-here').length) {
			$(this).addClass('dragover');
		}
	},
	
	uploadDropdown_Dragleave : function(e) {
		e.preventDefault();  
		e.stopPropagation();
		if ($(this).find('.drag-here').length) {
			$(this).removeClass('dragover');
		}
	},

	uploadDropdown_Drop : function(e) {
		var thisScene = e.data.thisScene;
		
		e.preventDefault();  
		e.stopPropagation();
		if ($(this).find('.drag-here').length) {
			$(this).removeClass('dragover');
			thisScene.inputfileAdd(e.originalEvent.dataTransfer.files);
		}
		
		return false;
	},
	
}).init();

</script>
