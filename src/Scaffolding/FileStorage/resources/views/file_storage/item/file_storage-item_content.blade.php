<div class="row space-bottom-1"><div class="col-sm-12">
	<div id="upload_area">
		@if (!$item->id)
			<div class="drag-here"></div>
		@endif
		<div class="hdr text-center">Выберите файлы или претащите их в эту облась...</div>
		<div class="act text-center">
			<label for="inputfile_{{ $scene->sceneID() }}" class="btn btn-link btn-lg" title="Выбрать"><i class="glyphicon glyphicon-folder-open"></i></label>
			<button class="btn btn-link btn-lg hidden" id="btn_ok_item" title="Загрузить"><i class="glyphicon glyphicon-cloud-upload"></i></button>
		</div>
		<div class="info text-center">
			<ul id="files_list"></ul>
		</div>
		
		<form method="post" action="" data-role="data_form">
			{{ csrf_field() }}
			{!! FastDevCtrl::textField('item_id')->type('hidden')->value($item->id)->out() !!}
			{!! FastDevCtrl::textField('group_id')->value($group_id)->type('hidden')->out() !!}
			<input type="file" name="file" id="inputfile_{{ $scene->sceneID() }}" multiple />
		</form>
	</div>
</div></div>