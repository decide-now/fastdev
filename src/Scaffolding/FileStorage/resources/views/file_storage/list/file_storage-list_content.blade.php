{!! FastDevCtrl::modal('filter-form')->title('Фильтр')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('filter_scene')->sceneArray())
{!! FastDevCtrl::modal('filter-form')->close() !!}

{!! FastDevCtrl::modal('select-form')->title('Выбор: '.\App\ORM\FileStorageGroup::getAlias('record_name'))->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('file_storage_group_select_scene')->sceneArray())
{!! FastDevCtrl::modal('select-form')->close() !!}

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-6">
			<h3>{{ $model::getAlias('table_name') }}</h3>
		</div>
		<div class="col-sm-6 text-right">
			<div class="btn-group btn-group-sm">
				{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->size('sm')->open() !!}
					{!! FastDevCtrl::button('btn-show-filter')->title('Фильтр')->iconBefore('glyphicon glyphicon-filter')->style( (FastDevHlpr::array_is_empty($filter)) ? '' : 'success' )->out() !!}
					{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->out() !!}
				{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
			</div>
		</div>
	</div>
	
	<div class="row"><div class="col-sm-12 text-muted">
		{{ ($group) ? $group->full_name : '' }}
	</div></div>
	
	<div class="row taskbar">
		<div class="col-sm-12">
			@include('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())
		</div>
	</div>

	<div class="row taskbar">
		<div class="col-sm-12">
			@include('fastdev::scene_content', $scene_children->get('item_scene')->sceneArray())
		</div>
	</div>
	@foreach($list as $item)
	<div class="row list-item bottom-border {{ ($item_id == $item->id) ? 'highlighted' : '' }}" data-role="list-item" data-item-id = "{{ $item->id }}" data-item-text="{{ $item->toString() }}"><div class="col-sm-12">
		<div class="row">
			<div class="col-sm-12">
				<a href="" rel="popover" data-popover-content="#file-info{{ $item->id }}" data-trigger="hover" data-role="custom-popover"><i class="glyphicon glyphicon-info-sign"></i></a>
				{{ $item->name }}
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8">
				{!! FastDevCtrl::textField('output_name')
					->value($item->output_name)
					->buttonBefore('glyphicon glyphicon-floppy-save', 'set')
					->attr('data-item-id', $item->id)
					->size('sm')
					->out() 
				!!}
			</div>
			<div class="col-sm-4 text-right">
				{!! FastDevCtrl::buttonGroup('row-actions')->size('sm')->open() !!}
					{!! FastDevCtrl::button('btn-download-item')->iconBefore('glyphicon glyphicon-download')->title('Скачать')->isDisabled(!$model->getPermission('download'))->out() !!}
					{!! FastDevCtrl::button('btn-delete-item')->iconBefore('glyphicon glyphicon-trash')->title('Удалить')->isDisabled(!$model->getPermission('delete'))->out() !!}
					{!! FastDevCtrl::button('btn-move-item'.$item->id)->iconBefore('glyphicon glyphicon-folder-open')->title('Группа')->isDisabled(!$model->getPermission('edit'))->out() !!}
				{!! FastDevCtrl::buttonGroup('row-actions')->close() !!}
			</div>
			<div class="hidden" id="file-info{{ $item->id }}">
				<div class="row">
					<div class="col-sm-6">{{ $item->getAlias('size') }}: </div>
					<div class="col-sm-6">{{ $item->size }}b</div>
				</div>
				<div class="row">
					<div class="col-sm-6">{{ $item->getAlias('extension') }}: </div>
					<div class="col-sm-6">{{ $item->extension }}</div>
				</div>
				<div class="row">
					<div class="col-sm-6">{{ $item->getAlias('group_id') }}: </div>
					<div class="col-sm-6">{{ ($item->group) ? $item->group->name : '' }}</div>
				</div>
				<div class="row">
					<div class="col-sm-6">{{ $item->getAlias('user_id') }}: </div>
					<div class="col-sm-6">{{ ($item->user) ? $item->user->name : '' }}</div>
				</div>
				<div class="row">
					<div class="col-sm-6">{{ $item->getAlias('created_at') }}: </div>
					<div class="col-sm-6">{{ FastDevHlpr::dateFormat($item->created_at, 'DMY') }}</div>
				</div>
			</div>
		</div>
	</div></div>
	@endforeach
	
	<div class="row">
		<div class="col-sm-12">
			{{ $list->links() }}
		</div>
	</div>
	
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		{!! FastDevCtrl::textField('group_id')->type('hidden')->value(!empty($group_id) ? $group_id : '')->out() !!}
	</form>
	
</div></div>
		