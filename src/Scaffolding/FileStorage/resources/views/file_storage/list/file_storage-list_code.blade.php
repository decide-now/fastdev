<script>

new Scene('{{ $scene->sceneId() }}', {
	
	/* init */
	
	init : function() {
		var thisScene = this;
		
		thisScene.getElement('[id="btn_show_filter"]').click( { thisScene: this }, thisScene.btnShowFilter_Click );
		thisScene.getElement('[id="btn_refresh"]').click( { thisScene: this }, thisScene.btnRefresh_Click );

		thisScene.getElement('[id^="btn_copy_link"]').click( { thisScene: this }, thisScene.btnCopyLink_Click );
		thisScene.getElement('[id^="btn_download_item"]').click( { thisScene: this }, thisScene.btnDownloadItem_Click );
		thisScene.getElement('[id^="btn_delete_item"]').click( { thisScene: this }, thisScene.btnDeleteItem_Click );
		thisScene.getElement('[id^="btn_move_item"]').click( { thisScene: this }, thisScene.btnMoveItem_Click );
		thisScene.getElement('[id^="btn_set_output_name"]').click( { thisScene: this }, thisScene.btnSetOutputName_Click );

		thisScene.getElement('.pagination a').click( { thisScene: this }, thisScene.pagination_Click );

		setTimeout(function (thisScene) { thisScene.getElement('.highlighted').removeClass('highlighted'); }, 1000, thisScene);
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},

	afterSuccessAJAX : function(response) {
		var thisScene = response.thisScene;
		stage.getScene('{{ $scene_children->get("item_scene")->sceneId() }}').init();
	},
	
	
	/**/
	
	getListItemData : function(el) {
		var list_item = $(el).closest('[data-role="list-item"]');
		var item_id_val = list_item.data('item-id');
		var item_text_val = list_item.data('item-text');
		return { id: item_id_val, text: item_text_val, }
	},

	btnSetOutputName_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var item_data = thisScene.getListItemData(this);
		var output_name_val = $(this).closest('.input-group').find('input').val();
			
		var data = { task: 'set_output_name', fields: { item_id: item_data.id, output_name: output_name_val, stop_refresh: 1, }, ajax: true, }
		thisScene.sceneRefresh(data);
		
		return false;
	},

	btnDownloadItem_Click : function(e) {
		var thisScene = thisScene = e.data.thisScene;
		var item_data = thisScene.getListItemData(this);
		
		var data = { task: 'download', fields: { item_id: item_data.id, stop_refresh: 1, }, ajax: false, }
		thisScene.sceneRefresh(data);
		
		return false;
	},

	btnCopyLink_Click : function(e) {
		var thisScene = thisScene = e.data.thisScene;
		var data_link = $(this).data('link');
		var el = $('<input type="text" value="'+data_link+'" style="position: fixed; top: -1000px; left: -1000px">');
		$(document.body).append(el);
		el.focus();
		el.select();
		document.execCommand('copy');
		el.remove();
		alert('Ссылка на файл скопирована.');
		return false;
	},

	btnDeleteItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		if (confirm("Удалить запись?")) {
			var item_data = thisScene.getListItemData(this);
			
			var data = { task: 'delete', fields: { item_id: item_data.id, stop_refresh: 1, }, ajax: true, }
			thisScene.sceneRefresh(data);
		}
		return false;
	},

	btnRefresh_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { ajax: true, };
		thisScene.sceneRefresh(data);
		return false;
	},

	itemReturn : function(data) {
		thisScene = this;

		/*
		var itemScene = stage.getScene('{{ $scene_children->get("item_scene")->sceneId() }}');
		itemScene.getRootElement().closest('.modal').modal('hide');
		*/
		
		if (data.refresh === '1') {
			thisScene.sceneRefresh({ ajax: true, });
		}
	},

	
	/* filter */
	
	btnShowFilter_Click : function(e) {
		var filterScene = stage.getScene('{{ $scene_children->get("filter_scene")->sceneId() }}');

		var data = { ajax: true, };
		
		filterScene.sceneRefresh(data);
		filterScene.getRootElement().closest('.modal').modal('show');

		return false;
	},

	filterReturn : function(data) {
		var thisScene = this;

		var filterScene = stage.getScene('{{ $scene_children->get("filter_scene")->sceneId() }}');
		filterScene.getRootElement().closest('.modal').modal('hide');
		filterScene.getRootElement().html('');

		if (!$.isEmptyObject(data)) {
			data.ajax = true;
			thisScene.sceneRefresh(data);
		}
	},


	/* select fields */

	btnMoveItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var selectScene = stage.getScene('{{ $scene_children->get("file_storage_group_select_scene")->sceneId() }}');
		var call_button_id_val =  $(this).attr('id');

		var data = { fields: { call_button_id: call_button_id_val, }, ajax: true, };
		if ($(this).attr('filter-changed') != null) {
			var field_id = $(this).attr('field-id')
			var field = thisScene.getElement('[id="'+field_id+'"]');
			data.fields['filter[name]'] = field.val();
		} else {
			data.fields['filter[name]'] = '';
		}

		selectScene.sceneRefresh(data);
		selectScene.getRootElement().closest('.modal').modal('show');
	},

	selectReturn : function(data) {
		var thisScene = this;

		var select_scene = data.scene;
		var select_form = select_scene.getRootElement();
		select_form.closest('.modal').modal('hide');
		select_form.html('');

		var call_button = thisScene.getElement('[id="'+data.call_button_id+'"]');

		// если выбрали группу
		if (data.scene.id == '{{ $scene_children->get("file_storage_group_select_scene")->sceneId() }}') {

			var group_id_val = data.item_id;
			
			var item_data = thisScene.getListItemData(call_button);
			var data = { task: 'move', fields: { item_id: item_data.id, new_group_id: group_id_val, stop_refresh: 1, }, ajax: true, }
			thisScene.sceneRefresh(data);
		} else {
			call_button.removeAttr('filter-changed');
			
			var field_id = call_button.attr('field-id')
			var field = thisScene.getElement('[id="'+field_id+'"]');
			field.val(data.item_text);
			
			var hidden_field_id = call_button.attr('hidden-field-id')
			var hidden_field = thisScene.getElement('[id="'+hidden_field_id+'"]');
			hidden_field.val(data.item_id);
		}
	},

	
	/* pagination */
	
	pagination_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		var data = {ajax: true, url: e.target.href, }
		thisScene.sceneRefresh(data)
		return false;
	},
	
}).init();

</script>
