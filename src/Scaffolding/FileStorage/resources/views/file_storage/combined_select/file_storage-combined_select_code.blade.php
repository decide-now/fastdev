<script>

new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},

	afterSuccessAJAX : function(response) {
		var tree_scene = stage.getScene('{{ $scene_children->get("tree_scene")->sceneId() }}');
		var list_scene = stage.getScene('{{ $scene_children->get("list_scene")->sceneId() }}');

		tree_scene.init();
		list_scene.init();
		list_scene.initItemScene();
	},
	
	
	/**/
	
	categorySelectReturn : function(return_data) {
		
		var listScene = stage.getScene('{{ $scene_children->get("list_scene")->sceneId() }}')
		listScene.getElement('[id="group_id"]').val(return_data.item_id);

		var data = { task: 'refresh', ajax: true };
		listScene.sceneRefresh(data);
		return false;
	},

	selectReturn : function(data) {
		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		if (parentScene.selectReturn) {
			parentScene.selectReturn(data);
		} else {
			parentScene.defaultSelectReturn(data);
		}
	},
	
}).init();

</script>
