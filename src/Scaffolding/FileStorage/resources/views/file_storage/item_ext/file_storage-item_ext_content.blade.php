
<div class="container-fluid"><div class="row"><div class="col-sm-12">
	<div id="upload_area">
		@if (!$item->id)
			<div class="drag-here"></div>
		@endif
		<div class="hdr">{{ $title }}</div>
		<div class="act text-center">
			@if ($item->id)
				@if ($allow_download)
					<button class="btn btn-link btn-lg" id="btn_download_item" title="Скачать"><i class="glyphicon glyphicon-download-alt"></i></button>
				@endif
				@if ($allow_delete)
					<button class="btn btn-link btn-lg" id="btn_remove_item" title="Удалить"><i class="glyphicon glyphicon-trash"></i></button>
				@endif
			@elseif ($allow_upload)
				<label for="inputfile_{{ $scene->sceneID() }}" class="btn btn-link btn-lg" title="Выбрать"><i class="glyphicon glyphicon-folder-open"></i></label>
				<button class="btn btn-link btn-lg hidden" id="btn_ok_item" title="Загрузить"><i class="glyphicon glyphicon-cloud-upload"></i></button>
			@endif
		</div>
		<div class="info text-center">
			<span id="file_name">{{ ($item) ? $item->toString() : '' }}</span>
		</div>
		
		<form method="post" action="" data-role="data_form">
			{{ csrf_field() }}
			{!! FastDevCtrl::textField('item_id')->type('hidden')->value($item->id)->out() !!}
			{!! FastDevCtrl::textField('group_id')->value($group_id)->type('hidden')->out() !!}
			{!! FastDevCtrl::textField('refresh_parent_scene')->type('hidden')->value($refresh_parent_scene)->out() !!}
			
			{!! FastDevCtrl::textField('title')->type('hidden')->value($title)->out() !!}
			{!! FastDevCtrl::textField('after_upload')->type('hidden')->value($after_upload)->out() !!}
			{!! FastDevCtrl::textField('before_delete')->type('hidden')->value($before_delete)->out() !!}
			
			{!! FastDevCtrl::textField('allow_upload')->type('hidden')->value($allow_upload)->out() !!}
			{!! FastDevCtrl::textField('allow_delete')->type('hidden')->value($allow_delete)->out() !!}
			{!! FastDevCtrl::textField('allow_download')->type('hidden')->value($allow_download)->out() !!}
			<input type="file" name="file" id="inputfile_{{ $scene->sceneID() }}" />
		</form>
	</div>
</div></div></div>