<script>
new Scene('{{ $scene->sceneId() }}', {
	
	/* init */
	init : function() {
		
		var thisScene = this;
		
		thisScene.getRootElement().data('filesData', []);

		thisScene.getElement('[id="btn_ok_item"]').click( { thisScene: this }, thisScene.btnOkItem_Click );
		thisScene.getElement('[id="btn_remove_item"]').click( { thisScene: this }, thisScene.btnRemoveItem_Click );
		thisScene.getElement('[id="btn_download_item"]').click( { thisScene: this }, thisScene.btnDownloadItem_Click );
		thisScene.getElement('[id^="inputfile"]').change( { thisScene: this }, thisScene.inputfile_Change );

		thisScene.getElement('[id="upload_area"]')
			.on('dragover', { thisScene: this }, thisScene.uploadArea_Dragover )
			.on('dragleave', { thisScene: this }, thisScene.uploadArea_Dragleave )
			.on('drop', { thisScene: this }, thisScene.uploadArea_Drop );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		var thisScene = this;

		var filesData = thisScene.getRootElement().data('filesData');
		data.alerts_scene_id = '{{ $scene_parent->sceneId() }}'+'-alert-message';
		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh', filesData);
	},

	afterSuccessAJAX : function(response) {
		thisScene = response.thisScene;

		var parentScene = stage.getScene('{{ ($scene_parent) ? $scene_parent->sceneId() : "" }}');
		if (parentScene.fileItemReturn) {
			parentScene.fileItemReturn({ refresh: response.refresh_parent_scene });
		}
	},
	
	
	/**/
	
	btnOkItem_Click : function(e) {
		var thisScene = e.data.thisScene;

		var data = { task: 'upload', ajax: true, }
		thisScene.sceneRefresh(data);
		return false;
	},

	btnRemoveItem_Click : function(e) {
		var thisScene = e.data.thisScene;

		var data = { task: 'delete', ajax: true, }
		thisScene.sceneRefresh(data);
		return false;
	},

	btnDownloadItem_Click : function(e) {
		var thisScene = e.data.thisScene;

		var data = { task: 'download', ajax: false, }
		thisScene.sceneRefresh(data);
		return false;
	},
	
	inputfileHas : function() {
		var thisScene = this;
		
		var lng = thisScene.getRootElement().data('filesData').length;
		return (lng = 0); 
	},
	
	inputfileUpdate : function() {
		var thisScene = this;

		var filesData = thisScene.getRootElement().data('filesData');
		
		var name_el = thisScene.getElement('[id="file_name"]');
		name_el.html('');
		
		var lng = filesData.length;
		for (i=0; i<lng; i++) {
			var item = filesData[i].name+' <a href="" id="remove_file" data-idx="'+i+'"><i class="glyphicon glyphicon-remove"></i></a>';
			name_el.html(item);
		}

		var btn_ok_item = thisScene.getElement('[id="btn_ok_item"]');
		if (filesData.length > 0) {
			btn_ok_item.removeClass('hidden');
		} else {
			btn_ok_item.addClass('hidden');
		}
		thisScene.getElement('[id^="remove_file"]').click( { 'thisScene': thisScene },  thisScene.inputfileRemove );
	},

	inputfile_Change : function(e) {
		var thisScene = e.data.thisScene;
		
		//var inputfile_el = thisScene.getElement('[id="inputfile"]');
		var inputfile_el = $(this);
		var inputfile_el_js = inputfile_el.get(0);
		thisScene.inputfileAdd(inputfile_el_js.files);
		inputfile_el.val('');
	},
	
	inputfileAdd : function(filesData) {
		var thisScene = this;
		
		var multiple_inputfile = thisScene.getElement('[id="inputfile"][multiple]');

		var tmpFilesData = [];
		if (multiple_inputfile.length > 0) {
			tmpFilesData = thisScene.getRootElement().data('filesData');
		}
		
		var lng = filesData.length;
		for (i=0; i<lng; i++) {
			tmpFilesData.push(filesData[i]);
			if (multiple_inputfile.length == 0) {
				break;
			}
		}
		thisScene.getRootElement().data('filesData', tmpFilesData);

		thisScene.inputfileUpdate();
	},

	inputfileRemove : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;

		var data_idx = parseInt($(this).attr('data-idx'));

		filesData = thisScene.getRootElement().data('filesData');
		filesData.splice(data_idx, 1);
		
		thisScene.inputfileUpdate();
		
		return false;
	},

	uploadArea_Dragover : function(e) {
		e.preventDefault();  
		e.stopPropagation();
		if ($(this).find('.drag-here').length) {
			$(this).addClass('dragover');
		}
	},
	
	uploadArea_Dragleave : function(e) {
		e.preventDefault();  
		e.stopPropagation();
		if ($(this).find('.drag-here').length) {
        	$(this).removeClass('dragover');
		}
	},

	uploadArea_Drop : function(e) {
		var thisScene = e.data.thisScene;
		
		e.preventDefault();  
		e.stopPropagation();

		if ($(this).find('.drag-here').length) {
			$(this).removeClass('dragover');
	
			thisScene.inputfileAdd(e.originalEvent.dataTransfer.files);
		}
		
		return false;
	},
	
}).init();

</script>
