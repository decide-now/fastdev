<script>

new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh', undefined);
	},

	afterSuccessAJAX : function(response) {
		stage.getScene('{{ $scene_children->get("tree_scene")->sceneId() }}').init();
		stage.getScene('{{ $scene_children->get("list_scene")->sceneId() }}').init();
	},
	
	
	/**/
	
	categorySelectReturn : function(return_data) {
		
		var listScene = stage.getScene('{{ $scene_children->get("list_scene")->sceneId() }}')
		listScene.getElement('[id="group_id"]').val(return_data.item_id);

		var data = { task: 'refresh', ajax: true };
		listScene.sceneRefresh(data);
		return false;
	},
	
}).init();

</script>
