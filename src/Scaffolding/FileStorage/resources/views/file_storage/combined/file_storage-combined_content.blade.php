<div class="row">
	<div class="col-sm-4">
		@include('fastdev::scene_content', $scene_children->get('tree_scene')->sceneArray())
	</div>
	<div class="col-sm-8">
		@include('fastdev::scene_content', $scene_children->get('list_scene')->sceneArray())
	</div>
</div>

<form method="post" action="" data-role="data_form">
	{{ csrf_field() }}
</form>