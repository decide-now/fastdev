@include('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())

{!! FastDevCtrl::modal('filter-form')->title('Фильтр')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('filter_scene')->sceneArray())
{!! FastDevCtrl::modal('filter-form')->close() !!}

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-6">
			<h3>{{ $model::getAlias('table_name') }}<small><br>{{ ($group) ? $group->full_name : '' }}</small></h3>
		</div>
		<div class="col-sm-6 text-right">
			<div class="btn-group btn-group-sm">
				{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
					{!! FastDevCtrl::button('btn-show-filter')->title('Фильтр')->iconBefore('glyphicon glyphicon-filter')->style( (FastDevHlpr::array_is_empty($filter)) ? '' : 'success' )->out() !!}
					{!! FastDevCtrl::button('btn-cancel-select')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->out() !!}
				{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
			</div>
		</div>
	</div>
	
	<div class="row taskbar">
		<div class="col-sm-12">
			@include('fastdev::scene_content', $scene_children->get('item_scene')->sceneArray())
		</div>
	</div>
	
	@foreach($list as $item)
	<div class="row list-item bottom-border" data-role="list-item" data-item-id = "{{ $item->id }}" data-item-text="{{ $item->toString() }}" data-item-url="{{ $item->getUrl(($absolute_links) ? false : true) }}">
		<div class="col-sm-8">
			{{ $item->output_name }}<br>
			<a href="" rel="popover" data-popover-content="#file-info{{ $item->id }}" data-trigger="hover"><i class="glyphicon glyphicon-info-sign"></i></a>
			{{ $item->name }}
		</div>
		<div class="hidden" id="file-info{{ $item->id }}">
			<div class="row">
				<div class="col-sm-6">{{ $item->getAlias('size') }}: </div>
				<div class="col-sm-6">{{ $item->size }}b</div>
			</div>
			<div class="row">
				<div class="col-sm-6">{{ $item->getAlias('extension') }}: </div>
				<div class="col-sm-6">{{ $item->extension }}</div>
			</div>
			<div class="row">
				<div class="col-sm-6">{{ $item->getAlias('group_id') }}: </div>
				<div class="col-sm-6">{{ ($item->group) ? $item->group->name : '' }}</div>
			</div>
			<div class="row">
				<div class="col-sm-6">{{ $item->getAlias('user_id') }}: </div>
				<div class="col-sm-6">{{ ($item->user) ? $item->user->name : '' }}</div>
			</div>
			<div class="row">
				<div class="col-sm-6">{{ $item->getAlias('created_at') }}: </div>
				<div class="col-sm-6">{{ FastDevHlpr::dateFormat($item->created_at, 'DMY') }}</div>
			</div>
		</div>
		
		<div class="col-sm-4 text-right">
			{!! FastDevCtrl::buttonGroup('row-actions')->size('sm')->open() !!}
				{!! FastDevCtrl::button('btn-copy-link')
					->iconBefore('glyphicon glyphicon-copy')
					->title('Скопировать ссылку')
					->data('link', url('/file-storage/download/name/'.$item->name))
					->out()
				!!}
				{!! FastDevCtrl::button('btn-select-item')->iconBefore('glyphicon glyphicon-ok')->title('Выбрать')->out() !!}
			{!! FastDevCtrl::buttonGroup('row-actions')->close() !!}
		</div>
	</div>
	@endforeach
	
	<div class="row">
		<div class="col-sm-12">
			{{ $list->links() }}
		</div>
	</div>
	
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		{!! FastDevCtrl::textField('group_id')->type('hidden')->value(!empty($group_id) ? $group_id : '')->out() !!}
		{!! FastDevCtrl::textField('call_button_id')->type('hidden')->value($call_button_id)->out() !!}
		{!! FastDevCtrl::textField('absolute_links')->type('hidden')->value($absolute_links)->out() !!}
	</form>
	
</div></div>
