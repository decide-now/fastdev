<script>

new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;

		thisScene.getElement('[id^="btn_cancel_select"]').click( { thisScene: this }, thisScene.btnCancelSelect_Click );
		thisScene.getElement('[id^="btn_show_filter"]').click( { thisScene: this }, thisScene.btnShowFilter_Click );

		thisScene.getElement('[id^="btn_copy_link"]').click( { thisScene: this }, thisScene.btnCopyLink_Click );
		thisScene.getElement('[id^="btn_select_item"]').click( { thisScene: this }, thisScene.btnSelectItem_Click );

		thisScene.getElement('.pagination a').click( { thisScene: this }, thisScene.pagination_Click );
	},

	initItemScene : function() {
		stage.getScene('{{ $scene_children->get("item_scene")->sceneId() }}').init();
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},

	afterSuccessAJAX : function(response) {
		var thisScene = response.thisScene;
		thisScene.initItemScene();
	},
	
	
	/**/
	
	getListItemData : function(el) {
		var list_item = $(el).closest('[data-role="list-item"]');
		var item_id_val = list_item.data('item-id');
		var item_text_val = list_item.data('item-text');
		var item_url_val = list_item.data('item-url');
		return { id: item_id_val, text: item_text_val, url: item_url_val, }
	},
	
	btnCancelSelect_Click : function(e) {
		var thisScene = e.data.thisScene;

		var data = { scene:thisScene, };

		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.selectReturn(data);
	},

	btnSelectItem_Click : function(e) {
		var thisScene = e.data.thisScene;

		var item_data = thisScene.getListItemData(this);
		var call_button_id_val = thisScene.getElement('[id^="call_button_id"]').val();
		
		var data = { 
			scene:thisScene, 
			call_button_id: call_button_id_val, 
			item_id: item_data.id, 
			item_text: item_data.text,
			item_url: item_data.url, 
		};
		
		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.selectReturn(data);
		
		return false;
	},

	btnCopyLink_Click : function(e) {
		var thisScene = thisScene = e.data.thisScene;
		var data_link = $(this).data('link');
		var el = $('<input type="text" value="'+data_link+'" style="position: fixed; top: -1000px; left: -1000px">');
		$(document.body).append(el);
		el.focus();
		el.select();
		document.execCommand('copy');
		el.remove();
		alert('Ссылка на файл скопирована.');
		return false;
	},

	itemReturn : function(data) {
		thisScene = this;

		/*
		var itemScene = stage.getScene('{{ $scene_children->get("item_scene")->sceneId() }}');
		itemScene.getRootElement().closest('.modal').modal('hide');
		*/
		
		if (data.refresh === '1') {
			thisScene.sceneRefresh({ ajax: true, });
		}
	},

	
	/* filter */
	
	btnShowFilter_Click : function(e) {
		var filterScene = stage.getScene('{{ $scene_children->get("filter_scene")->sceneId() }}');

		var data = { ajax: true, };
		
		filterScene.sceneRefresh(data);
		filterScene.getRootElement().closest('.modal').modal('show');

		return false;
	},

	filterReturn : function(data) {
		var thisScene = this;

		var filterScene = stage.getScene('{{ $scene_children->get("filter_scene")->sceneId() }}');
		filterScene.getRootElement().closest('.modal').modal('hide');
		filterScene.getRootElement().html('');

		if (!$.isEmptyObject(data)) {
			data.ajax = true;
			thisScene.sceneRefresh(data);
		}
	},
	

	/* pagination */
	
	pagination_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		var data = {ajax: true, url: e.target.href, }
		thisScene.sceneRefresh(data)
		return false;
	},
	
}).init();

</script>
