<script>

new Scene('{{ $scene->sceneId() }}', {
	
	/* init */
	
	init : function() {
		var thisScene = this;
		
		thisScene.getElement('[id="btn_cancel_filter"]').click( { thisScene: this }, thisScene.btnCancelFilter_Click );
		thisScene.getElement('[id="btn_ok_filter"]').click( { thisScene: this }, thisScene.btnOkFilter_Click );
		thisScene.getElement('[id="btn_clear_filter"]').click( { thisScene: this }, thisScene.btnClearFilter_Click );

		thisScene.getElement('[id^="btn_clear_"]').click( { thisScene: this }, thisScene.btnClearField_Click );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},
	
	
	/**/
	
	btnCancelFilter_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.filterReturn({});
	},

	btnOkFilter_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var fields_val = thisScene.serializeData({});
		
		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.filterReturn({ fields: fields_val });
	},

	btnClearFilter_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var fields_val = thisScene.serializeData({}, true);

		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.filterReturn({ fields: fields_val });
	},

	btnClearField_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var field_id = $(this).attr('field-id')
		var hidden_field_id = $(this).attr('hidden-field-id')
		var field = thisScene.getElement('[id="'+field_id+'"]');
		var hidden_field = thisScene.getElement('[id="'+hidden_field_id+'"]');
		
		field.val('');
		hidden_field.val('');
		return false;
	},

}).init();

</script>
