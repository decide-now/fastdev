@include('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())

{!! FastDevCtrl::modal('select-form')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('parent_select_scene')->sceneArray())
{!! FastDevCtrl::modal('select-form')->close() !!}

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-6">
			<h3>{{ $item::getAlias('record_name') }}</h3>
		</div>
		<div class="col-sm-6 text-right">
		{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
			{!! FastDevCtrl::button('btn-ok-item')->title('Сохранить и выйти')->iconBefore('glyphicon glyphicon-ok')->out() !!}
			{!! FastDevCtrl::button('btn-save-item')->title('Сохранить')->iconBefore('glyphicon glyphicon-floppy-save')->out() !!}
			{!! FastDevCtrl::button('btn-cancel-item')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->out() !!}
			{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->out() !!}
		{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
		</div>
	</div>
			
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		{!! FastDevCtrl::textField('item_id')->type('hidden')->value($item->id)->out() !!}
		{!! FastDevCtrl::textField('refresh_parent_scene')->type('hidden')->value($refresh_parent_scene)->out() !!}
		
		<div class="row">
			<div class="col-sm-3">
				{!! FastDevCtrl::textField('name', $item)->out() !!}
			</div>
			<div class="col-sm-3">
				{!! FastDevCtrl::textField('code', $item)->out() !!}
			</div>
			<div class="col-sm-3">
				{!! FastDevCtrl::textField('parent_id', $item)
					->visibleValue(\App\ORM\FileStorageGroup::idToString(($item->parent) ? $item->parent->id : null ))
					->hasClearButton()
					->hasSelectButton()
					->out()
				!!}
			</div>
			<div class="col-sm-3">
				{!! FastDevCtrl::textField('path')->label($item->getAlias('path'))->value($item->getPathChain())->isDisabled()->out() !!}
			</div>
			@if($item->getPermission('allow_non_public'))
				<div class="col-sm-6">
					{!! FastDevCtrl::checkbox('is_public', $item)->isChecked($item->is_public)->out() !!}
				</div>
			@else
				{!! FastDevCtrl::textField('is_public', $item)->type('hidden')->out() !!}
			@endif
		</div>
	</form>
		
</div></div>