<script>

new Scene('{{ $scene->sceneId() }}', {
	
	/* init */
	
	init : function() {
		var thisScene = this;
		
		thisScene.getElement('[id="btn_ok_item"]').click( { thisScene: this }, thisScene.btnOkItem_Click );
		thisScene.getElement('[id="btn_save_item"]').click( { thisScene: this }, thisScene.btnSaveItem_Click );
		thisScene.getElement('[id="btn_cancel_item"]').click( { thisScene: this }, thisScene.btnCancelItem_Click );
		thisScene.getElement('[id="btn_refresh"]').click( { thisScene: this }, thisScene.btnRefresh_Click );

		thisScene.getElement('[id^="btn_clear_"]').click( { thisScene: this }, thisScene.btnClearField_Click );
		thisScene.getElement('[field-has-select]').change( { thisScene: this }, thisScene.fieldHasSelect_Change);

		thisScene.getElement('[id="btn_select_file_storage_group_parent_id"]').click( { thisScene: this }, thisScene.btnSelectFileStorageGroupParentId_Click );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;
		
		var update_url_base = '{{ url($scene->updateURL()) }}';
		if ('fields' in data) {
			var item_id = ('item_id' in data.fields) ? data.fields.item_id : thisScene.getElement('[id="item_id"]').val();
		}
		item_id = (item_id) ? '/' + item_id : '';
		
		thisScene.defaultSceneRefresh(data, update_url_base + item_id, 'refresh');
	},

	afterSuccessAJAX : function(response) {
		thisScene = response.thisScene;
		if (response.page == '') {
			var parentScene = stage.getScene('{{ ($scene_parent) ? $scene_parent->sceneId() : "" }}');
			
			parentScene.itemReturn({ refresh: response.refresh_parent_scene });
		}
	},
	
	
	/**/
	
	btnOkItem_Click : function(e) {
		var thisScene = e.data.thisScene;

		var data = { task: 'ok', ajax: true, };
		thisScene.sceneRefresh(data);
		return false;
	},

	btnSaveItem_Click : function(e) {
		var thisScene = e.data.thisScene;

		var data = { task: 'save', ajax: true, };
		thisScene.sceneRefresh(data);
		return false;
	},

	btnCancelItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { task: 'cancel', ajax: true, };
		thisScene.sceneRefresh(data);
		return false;
	},

	btnRefresh_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { task: 'refresh', ajax: true, };
		thisScene.sceneRefresh(data);
		return false;
	},


	/* select fields */

	btnSelectFileStorageGroupParentId_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var selectScene = stage.getScene('{{ $scene_children->get("parent_select_scene")->sceneId() }}');
		var call_button_id_val =  $(this).attr('id');

		var data = { fields: { call_button_id: call_button_id_val, }, ajax: true, };
		if ($(this).attr('filter-changed') != null) {
			var field_id = $(this).attr('field-id')
			var field = thisScene.getElement('[id="'+field_id+'"]');
			data.fields['filter[name]'] = field.val();
		} else {
			data.fields['filter[name]'] = '';
		}

		selectScene.sceneRefresh(data);
		selectScene.getRootElement().closest('.modal').modal('show');
	},

	selectReturn : function(data) {
		var thisScene = this;

		var select_scene = data.scene;
		var select_form = select_scene.getRootElement();
		select_form.closest('.modal').modal('hide');
		select_form.html('');

		var call_button = thisScene.getElement('[id="'+data.call_button_id+'"]');

		call_button.removeAttr('filter-changed');
		
		var field_id = call_button.attr('field-id')
		var field = thisScene.getElement('[id="'+field_id+'"]');
		field.val(data.item_text);
		
		var hidden_field_id = call_button.attr('hidden-field-id')
		var hidden_field = thisScene.getElement('[id="'+hidden_field_id+'"]');
		hidden_field.val(data.item_id);
	},

	btnClearField_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var field_id = $(this).attr('field-id')
		var hidden_field_id = $(this).attr('hidden-field-id')
		var field = thisScene.getElement('[id="'+field_id+'"]');
		var hidden_field = thisScene.getElement('[id="'+hidden_field_id+'"]');
		
		field.val('');
		hidden_field.val('');
		return false;
	},

	fieldHasSelect_Change : function(e) {
		var thisScene = e.data.thisScene;
		
		var select_button = thisScene.getElement('[id^="btn_select_"][field-id="'+$(this).attr('id')+'"]');
		select_button.attr('filter-changed', 'true');
		
		var hidden_field_id = select_button.attr('hidden-field-id');
		var hidden_field = thisScene.getElement('[id="'+hidden_field_id+'"]');
		hidden_field.val('');
	},
	
}).init();

</script>
