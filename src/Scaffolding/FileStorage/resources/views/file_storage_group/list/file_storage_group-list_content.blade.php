@include('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())

{!! FastDevCtrl::modal('item-form')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('item_scene')->sceneArray())
{!! FastDevCtrl::modal('item-form')->close() !!}

{!! FastDevCtrl::modal('filter-form')->title('Фильтр')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('filter_scene')->sceneArray())
{!! FastDevCtrl::modal('filter-form')->close() !!}

<div class="row"><div class="col-sm-12">

<div class="panel panel-default">
	<div class="panel-heading" data-toggle="collapse" data-target="#more" aria-expanded="true">
		<h3 class="panel-title">{{ $model::getAlias('table_name') }}</h3>
	</div>
	<div class="panel-body collapse in" id="more">
	
		<div class="row taskbar bottom-border">
			<div class="col-sm-12">
				<div class="btn-group btn-group-sm">
					{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
						{!! FastDevCtrl::button('btn-create-item')->title('Создать')->iconBefore('glyphicon glyphicon-plus')->isDisabled(!$model->getPermission('create'))->out() !!}
						{!! FastDevCtrl::button('btn-edit-item')->title('Редактировать')->iconBefore('glyphicon glyphicon-pencil')->isDisabled(!$model->getPermission('edit'))->out() !!}
						{!! FastDevCtrl::button('btn-delete-item')->title('Удалить')->iconBefore('glyphicon glyphicon-trash')->isDisabled(!$model->getPermission('delete'))->out() !!}
						{!! FastDevCtrl::button('btn-show-filter')->title('Фильтр')->iconBefore('glyphicon glyphicon-filter')->style( (FastDevHlpr::array_is_empty($filter)) ? '' : 'success' )->out() !!}
						{!! FastDevCtrl::button('btn-scan')->title('Сканировать папку')->iconBefore('glyphicon glyphicon-sunglasses')->out() !!}
						{!! FastDevCtrl::button('btn-refresh')->title('Обновить')->iconBefore('glyphicon glyphicon-refresh')->out() !!}
					{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
				</div>
			</div>
			<div class="col-sm-12">
				{!! FastDevCtrl::selectField('select-ordering')
					->value($model::getOrderingKey($ordering))
					->options($model::getOrderingOptions())
					->buttonBefore(
						$model::getOrderingIcon($ordering), 
						'toggle', 
						$model::getOrderingTitle($ordering),
						['direction' => $model::getOrderingValue($ordering), 'role' => 'sortable-toggle']
					)
					->attr('data-direction', $model::getOrderingValue($ordering))
					->attr('data-role', 'sortable-select')
					->size('sm')
					->out()
				!!}
			</div>
		</div>
		
		<div class="row list-item {{ (!$selected_item_id) ? 'selected' : '' }}" data-role="list-item" data-item-id="" data-item-text="">
			<div class="col-sm-12">
				<a id="link_select_item" href="">&lt;Все&gt;</a>
			</div>
		</div>
		@foreach($list as $item)
		<div class="row list-item {{ ($item->id == $selected_item_id) ? 'selected' : '' }}" data-role="list-item" data-item-id="{{ $item->id }}" data-item-text="{{ $item->toString() }}">
			<div class="col-sm-12">
				{!! $item->tree_level_string !!}
				@if($model->getPermission('edit'))
					<a id="link_select_item" href="">
				@endif
					{{ $item->toString() }}
				@if($model->getPermission('edit'))
					</a>
				@endif
				@if($item->getPermission('allow_non_public'))
					{!! $item->is_public_icon !!}
				@endif
			</div>
		</div>
		@endforeach
		
		<div class="row">
			<div class="col-sm-12">
				{{ $list->links() }}
			</div>
		</div>
		
		<form method="post" action="" data-role="data_form">
			{{ csrf_field() }}
			{!! FastDevCtrl::textField('selected_item_id')->type('hidden')->value($selected_item_id)->out() !!}
		</form>
	
	</div>
</div>

</div></div>