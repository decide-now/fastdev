<script>

new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;
		
		thisScene.getElement('[data-role="sortable-select"]').change( { thisScene: this }, thisScene.selectSortable_Change );
		thisScene.getElement('[data-role="sortable-toggle"]').click( { thisScene: this }, thisScene.btnSortableToggle_Click );

		thisScene.getElement('[id^="link_select_item"]').click( { thisScene: this }, thisScene.linkSelectItem_Click );
		
		thisScene.getElement('[id^="btn_create_item"]').click( { thisScene: this }, thisScene.btnCreateItem_Click );
		thisScene.getElement('[id^="btn_edit_item"]').click( { thisScene: this }, thisScene.btnEditItem_Click );
		thisScene.getElement('[id^="btn_delete_item"]').click( { thisScene: this }, thisScene.btnDeleteItem_Click );
		thisScene.getElement('[id^="btn_show_filter"]').click( { thisScene: this }, thisScene.btnShowFilter_Click );
		thisScene.getElement('[id^="btn_scan"]').click( { thisScene: this }, thisScene.btnScan_Click );
		thisScene.getElement('[id^="btn_refresh"]').click( { thisScene: this }, thisScene.btnRefresh_Click );

		thisScene.getElement('.pagination a').click( { thisScene: this }, thisScene.pagination_Click );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		var thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},

	afterSuccessAJAX : function(response) {
		var thisScene = response.thisScene;
		if (response.task == 'scan') {
			var data = { scene:thisScene, item_id: response.item_id, item_text: '', }; 
			var parentScene = stage.getScene('{{ empty($scene_parent) ? '' : $scene_parent->sceneId() }}');
			if (parentScene) {
				parentScene.categorySelectReturn(data);
			}
		}
	},
	
	
	/**/
	
	getListItemData : function(el) {
		if (typeof el == 'undefined') {
			thisScene = this;
			var selected_item_id = thisScene.getElement('[id="selected_item_id"]').val();
			if (!selected_item_id) {
				return { id: '', text: '', };
			}
			var list_item = $('[data-role="list-item"][data-item-id="'+selected_item_id+'"]');
		} else {
			var list_item = $(el).closest('[data-role="list-item"]');
		}
		var item_id_val = list_item.data('item-id');
		var item_text_val = list_item.data('item-text');
		return { id: item_id_val, text: item_text_val, }
	},

	linkSelectItem_Click : function(e) {
		var thisScene = thisScene = e.data.thisScene;
		
		var item_data = thisScene.getListItemData(this);
		$('[data-role="list-item"].list-item.selected').removeClass('selected');
		var item_row = $(this).closest('[data-role="list-item"]').addClass('selected');

		thisScene.getElement('[id="selected_item_id"]').val(item_data.id);

		var data = { scene:thisScene, item_id: item_data.id, item_text: item_data.text, }; 
		var parentScene = stage.getScene('{{ empty($scene_parent) ? '' : $scene_parent->sceneId() }}');
		if (parentScene) {
			parentScene.categorySelectReturn(data);
		}
		
		return false;
	},
	
	btnCreateItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		var itemScene = stage.getScene('{{ $scene_children->get("item_scene")->sceneId() }}');

		var item_data = thisScene.getListItemData();
		
		var data = { task: 'create', fields: { parent_id: item_data.id, stop_refresh: 1, }, ajax: true, }
		itemScene.sceneRefresh(data);
		itemScene.getRootElement().closest('.modal').modal('show');
		
		return false;
	},

	btnEditItem_Click : function(e) {
		var thisScene = thisScene = e.data.thisScene;
		var itemScene = stage.getScene('{{ $scene_children->get("item_scene")->sceneId() }}');
		
		var item_data = thisScene.getListItemData();

		if (item_data.id) {
			var data = { task: 'edit', fields: { item_id: item_data.id, stop_refresh: 1, }, ajax: true, }
			itemScene.sceneRefresh(data);
			itemScene.getRootElement().closest('.modal').modal('show');
		}
		return false;
	},

	btnScan_Click : function(e) {
		var thisScene = e.data.thisScene;
		var item_data = thisScene.getListItemData();
		
		if (item_data.id) {
			var data = { task: 'scan', fields: { item_id: item_data.id, stop_refresh: 1, }, ajax: true, }
			thisScene.sceneRefresh(data);
		} else {
			alert('Не выбрана группа файлов!');
		}
		
		return false;
	},
	
	btnDeleteItem_Click : function(e) {
		var thisScene = e.data.thisScene;

		var item_data = thisScene.getListItemData();
		if (item_data.id) {
			if (confirm("Удалить запись?")) {
				var data = { task: 'delete', fields: { item_id: item_data.id, stop_refresh: 1, }, ajax: true, }
				thisScene.sceneRefresh(data);
			}
		}
		return false;
	},

	btnRefresh_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { ajax: true, };
		thisScene.sceneRefresh(data);
		return false;
	},

	itemReturn : function(data) {
		thisScene = this;
		
		var itemScene = stage.getScene('{{ $scene_children->get("item_scene")->sceneId() }}');
		itemScene.getRootElement().closest('.modal').modal('hide');
		
		if (data.refresh === '1') {
			thisScene.sceneRefresh({ ajax: true, });
		}
	},

	
	/* filter */
	
	btnShowFilter_Click : function(e) {
		var filterScene = stage.getScene('{{ $scene_children->get("filter_scene")->sceneId() }}');

		var data = { ajax: true, };
		
		filterScene.sceneRefresh(data);
		filterScene.getRootElement().closest('.modal').modal('show');

		return false;
	},

	filterReturn : function(data) {
		var thisScene = this;

		var filterScene = stage.getScene('{{ $scene_children->get("filter_scene")->sceneId() }}');
		filterScene.getRootElement().closest('.modal').modal('hide');
		filterScene.getRootElement().html('');

		if (!$.isEmptyObject(data)) {
			data.ajax = true;
			data.task = 'filter';
			thisScene.sceneRefresh(data);
		}
	},

	
	/* ordering */
	
	hdrSortable_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data_field = $(this).attr('data-field');
		var data_direction = $(this).attr('data-direction');
		data_direction = ( (data_direction == null || data_direction == 'desc') ? 'asc' : 'desc' );
		
		var data = { ajax: true, fields: {}, };
		data.fields['ordering[' + data_field + ']'] = data_direction;
		data.task = 'ordering';
		
		thisScene.sceneRefresh(data);
		return false;
	},

	selectSortable_Change : function(e) {
		var thisScene = e.data.thisScene;
		
		var data_field = $(this).find('option:selected').val();
		var data_direction = $(this).attr('data-direction');
		if (data_direction == null) {
			data_direction = 'asc';
		}
		
		var data = { ajax: true, fields: {}, };
		data.fields['ordering[' + data_field + ']'] = data_direction;
		data.task = 'ordering';
		
		thisScene.sceneRefresh(data);
		return false;
	},

	btnSortableToggle_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data_field = $(this).closest('.input-group').find('[data-role="sortable-select"] option:selected').val();
		var data_direction = $(this).attr('data-direction');
		data_direction = ( (data_direction == null || data_direction == 'desc') ? 'asc' : 'desc' );
		
		var data = { ajax: true, fields: {}, };
		data.fields['ordering[' + data_field + ']'] = data_direction;
		data.task = 'ordering';
		
		thisScene.sceneRefresh(data);
		return false;
	},

	
	/* pagination */
	
	pagination_Click : function(e) {
		e.preventDefault();
		var thisScene = e.data.thisScene;
		var data = {ajax: true, url: e.target.href, }
		thisScene.sceneRefresh(data)
		return false;
	},
	
}).init();

</script>
