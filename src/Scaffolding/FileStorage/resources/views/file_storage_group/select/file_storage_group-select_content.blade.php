@include('fastdev::scene_content', $scene_children->get('alert_message_scene')->sceneArray())

{!! FastDevCtrl::modal('filter-form')->title('Фильтр')->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('filter_scene')->sceneArray())
{!! FastDevCtrl::modal('filter-form')->close() !!}

<div class="row"><div class="col-sm-12">
	
	<div class="row taskbar">
		<div class="col-sm-12">
			<div class="btn-group btn-group-sm">
				{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
					{!! FastDevCtrl::button('btn-show-filter')->title('Фильтр')->iconBefore('glyphicon glyphicon-filter')->style( (FastDevHlpr::array_is_empty($filter)) ? '' : 'success' )->out() !!}
					{!! FastDevCtrl::button('btn-cancel-select')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->out() !!}
				{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
			</div>
		</div>
		<div class="col-sm-3">
			{!! FastDevCtrl::selectField('select-ordering')
				->value($model::getOrderingKey($ordering))
				->options($model::getOrderingOptions())
				->buttonBefore(
					$model::getOrderingIcon($ordering), 
					'toggle', 
					$model::getOrderingTitle($ordering),
					['direction' => $model::getOrderingValue($ordering), 'role' => 'sortable-toggle']
				)
				->attr('data-direction', $model::getOrderingValue($ordering))
				->attr('data-role', 'sortable-select')
				->size('sm')
				->out()
			!!}
		</div>
	</div>
	
	<div class="row list-item" data-role="list-item" data-item-id="" data-item-text="">
		<div class="col-sm-12">
			<a id="link_select_item" href="">&lt;Пусто&gt;</a>
		</div>
	</div>
	
	@foreach($list as $item)
	<div class="row list-item" data-role="list-item" data-item-id = "{{ $item->id }}" data-item-text="{{ $item->toString() }}">
		<div class="col-md-12">
			{!! $item->tree_level_string !!}
			<a id="link_select_item" href="">{{ $item->toString() }}</a>
			@if($item->getPermission('allow_non_public'))
				{!! $item->is_public_icon !!}
			@endif
		</div>
	</div>
	@endforeach
	
	<div class="row">
		<div class="col-md-12">
			{{ $list->links() }}
		</div>
	</div>
	
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
		{!! FastDevCtrl::textField('call_button_id')->type('hidden')->value($call_button_id)->out() !!}
	</form>

</div></div>
