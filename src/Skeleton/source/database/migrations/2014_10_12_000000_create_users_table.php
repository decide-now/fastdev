<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('password_resets', function (Blueprint $table) {
			$table->string('email')->index();
			$table->string('token');
			$table->timestamp('created_at')->nullable();
		});
		
		Schema::create('user_role', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('code');
			$table->timestamps();
		});
		
		Schema::create('user_status', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->timestamps();
		});
		
		Schema::create('user', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('username', 100)->unique();
			$table->string('password');
			$table->integer('user_status_id')->unsigned()->default(1);
			$table->foreign('user_status_id')->references('id')->on('user_status');
			$table->integer('user_role_id')->unsigned()->nullable();
			$table->foreign('user_role_id')->references('id')->on('user_role');
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user', function(Blueprint $table) {
			$table->dropForeign(['user_role_id']);
			$table->dropForeign(['user_status_id']);
		});
		Schema::dropIfExists('password_resets');
		Schema::dropIfExists('user_role');
		Schema::dropIfExists('user_status');
		Schema::dropIfExists('user');
	}
}
