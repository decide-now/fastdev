SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

truncate table `user`;
truncate table `user_role`;
truncate table `user_status`;

INSERT INTO `user_status` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Активен', NULL, NULL),
(2, 'Заблокирован', NULL, NULL);

INSERT INTO `user_role` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Администратор', 'admin', NULL, NULL),
(2, 'Посетитель', 'visitor', NULL, NULL);

INSERT INTO `user` (`id`, `name`, `username`, `password`, `user_status_id`, `user_role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Администратор', 'admin', '$2y$10$C/k8kNYDu/GmI1he6Op.VOVIu5KBXv9YbaLNYK97VzYkm02NxARpC', 1, 1, '', NULL, NULL),
(2, 'Посетитель', 'visitor@email.ru', '$2y$10$HzgFUJSi3JWpc3VkWsjUJ.m4qVaszUoR1rBi.Ekb3ojO23ZYIOH56', 1, 2, '', NULL, NULL);