<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$user_role = array(
			array('id' => '1', 'code' => 'admin', 'name' => 'Администратор'), 
			array('id' => '2', 'code' => 'visitor', 'name' => 'Посетитель')
		);
		
		$user_status = array(
			array('id' => '1', 'name' => 'Активен'), 
			array('id' => '2', 'name' => 'Заблокирован')
		);
		
		$user = array(
			array('username' => 'admin', 'name' => 'Администратор', 'password' => bcrypt('adminadmin'), 'user_status_id' => '1', 'user_role_id' => '1'),
			array('username' => 'visitor@email.ru', 'name' => 'Посетитель', 'password' => bcrypt('123456'), 'user_status_id' => '1', 'user_role_id' => '2')
		);
		
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		DB::table('user_role')->truncate();
		DB::table('user_status')->truncate();
		DB::table('user')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
		
		DB::table('user_role')->insert($user_role);
		DB::table('user_status')->insert($user_status);
		DB::table('user')->insert($user);
	}
}
