<?php

return [
	
	'locale' => 'ru',
	
	/* ... */
		
	'aliases' => [
	
		'FastDevCtrl' => DecideNow\FastDev\Facades\FastDevCtrl::class,
		'FastDevHlpr' => DecideNow\FastDev\Facades\FastDevHlpr::class,

	],

];
