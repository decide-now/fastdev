<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

	'failed' => 'Неверная комбинация логин/пароль или вход для пользователя запрещен.',
	'throttle' => 'Слишком много попыток авторизации. Пожалуйста, попробуйте через :seconds секунд.',
		
	'login_link' => 'Вход',
	'logout_link' => 'Выйти',
	'register_link' => 'Регистрация',
	'forgotpass_link' => 'Забыли пароль?',
	'profile_link' => 'Профиль',
		
	'login_header' => 'Авторизация',
	'register_header' => 'Регистрация',
	'resetpass_header' => 'Смена пароля',
		
	'login_button' => 'Войти',
	'register_button' => 'Регистрация',
	'sendlink_button' => 'Отправить ссылку',
	'resetpass_button' => 'Сменить',
		
	'username' => 'Логин/E-mail',
	'name' => 'Имя',
	'email' => 'E-mail',
	'password' => 'Пароль',
	'password_confirmation' => 'Подтверждение пароля',
	'remember' => 'Запомнить меня',
	
	'to_site' => 'На сайт',

];
