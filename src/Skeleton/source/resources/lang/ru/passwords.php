<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен содержать минимум 6 символов и совпадать с подтверждением пароля.',
    'reset' => 'Ваш пароль изменен!',
    'sent' => 'На указанный адрес отправлена ссылка для смены пароля!',
    'token' => 'Неверный ключ восстановления пароля.',
    'user' => "Пользователь с указанным адресом не найден.",

];
