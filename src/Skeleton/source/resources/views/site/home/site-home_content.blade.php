<div class="container full-width"><div class="row"><div class="col-sm-12 text-center">
	<h2>Для обновления страницы нажмите кнопку...</h2>
	<div class="space-top-2">
		{!! FastDevCtrl::button('btn-refresh')->label('Обновить')->style('success')->size('lg')->out() !!}
		<a class="btn btn-default btn-lg" href="{{ route('base') }}">В приложение</a>
	</div>
	<form method="post" action="" data-role="data_form">
		{{ csrf_field() }}
	</form>
</div></div></div>