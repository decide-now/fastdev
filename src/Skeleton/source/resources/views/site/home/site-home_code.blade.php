<script>
new Scene('{{ $scene->sceneId() }}', {
	
	/* init */
	
	init : function() {
		var thisScene = this;
		thisScene.getElement('[id="btn_refresh"]').click({ thisScene: this }, thisScene.btnRefreshClick);
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;
		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},
	
	afterSuccessAJAX : function(response) {
		//
	},
	
	
	/**/
	
	btnRefreshClick : function(e) {
		var thisScene = e.data.thisScene;
		var data = { ajax: true };
		thisScene.sceneRefresh(data);
	},

}).init();
	
</script>