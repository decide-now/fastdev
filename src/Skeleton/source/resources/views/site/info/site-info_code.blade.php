<script>
new Scene('{{ $scene->sceneId() }}', {
	
	/* init */
	
	init : function() {
		var thisScene = this;
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;
		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},
	
	afterSuccessAJAX : function(response) {
		//
	},
	
	
	/**/

}).init();
	
</script>