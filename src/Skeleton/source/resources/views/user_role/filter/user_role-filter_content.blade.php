<div class="row taskbar">
	<div class="col-sm-12 text-right">
		{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
			{!! FastDevCtrl::button('btn-ok-filter')->title('Применить')->iconBefore('glyphicon glyphicon-ok')->out() !!}
			{!! FastDevCtrl::button('btn-cancel-filter')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->out() !!}
			{!! FastDevCtrl::button('btn-clear-filter')->title('Очистить')->iconBefore('glyphicon glyphicon-erase')->out() !!}
		{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
	</div>
</div>
<div class="row"><div class="col-sm-12">
	<form data-role="data_form">
		{{ csrf_field() }}
		{!! FastDevCtrl::textField('filter[name]')->label($model::getAlias('name'))->value($filter['name'])->hasClearButton()->out() !!}
				{!! FastDevCtrl::textField('filter[code]')->label($model::getAlias('code'))->value($filter['code'])->hasClearButton()->out() !!}
	</form>
</div></div>
