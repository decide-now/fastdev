@extends('layouts.base.base_container')
@section('content')

	@include('fastdev::scene_content', [ 
		'scene' => (object)['scene_id' => 'app-alerts', 'no_content' => false, 'template_content' => 'alert.message.alert-message_content'], 
		'success_messages' => ($success_messages) ?: [],
		'error_messages' => ($error_messages) ?: [], 
	])
	
	<div class="applogo">
		<h1>{{ config('app.name', 'FastDev') }}</h1>
	</div>
	
@endsection
