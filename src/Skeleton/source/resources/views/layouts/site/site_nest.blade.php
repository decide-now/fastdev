@extends('layouts.site.site_container')
@section('content')
	
	<div class="container">
    	@include('fastdev::scene_content', $scene->sceneArray())
    </div>
	
	@push('stage_code')
		@include('fastdev::stage')
	@endpush
	
	@include('fastdev::scene_code_cascade', ['scene' => $scene])
	
@endsection