@extends('layouts.site.site_container')
@section('content')
	
	@include('fastdev::scene_content', $scene->sceneArray())
	
	@push('stage_code')
		@include('fastdev::stage')
	@endpush
	
	@include('fastdev::scene_code_cascade', ['scene' => $scene])
	
@endsection