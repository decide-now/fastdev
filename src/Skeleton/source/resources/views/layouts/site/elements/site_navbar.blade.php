<div class="container">
	<div class="row space-top-1">
		<div class="col-sm-12">
			<ul class="nav nav-pills">
				<li class="{{ (isset($active_page) && $active_page == 'home') ? 'active' : '' }}"><a href="{{ url('/') }}">Главная</a></li>
				<li class="{{ (isset($active_page) && $active_page == 'info') ? 'active' : '' }}"><a href="{{ url('/info') }}">Информация</a></li>
			</ul>
			<hr>
		</div>
	</div>
</div>