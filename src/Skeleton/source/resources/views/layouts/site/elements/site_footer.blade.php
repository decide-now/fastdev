<div class="container"><div class="row">
	<div class="pull-left"><i class="glyphicon glyphicon-envelope"></i> {{ config('mail.from.address', 'info@email.com') }}</div>
	<div class="pull-right">&copy; {{ config('app.name', 'FastDev') }}</div>
</div></div>