@extends('layouts.common.frame')

@section('head')
	@include('layouts.site.site_style')
	@stack('stage_style')
@endsection

@section('menu')
	@include('layouts.site.elements.site_navbar')	
@endsection

@section('page-content')
	<div class="app content">
		@yield('content')
	</div>
@endsection

@section('footer')
	@include('layouts.site.elements.site_footer')	
@endsection

@section('script')
	@stack('stage_code')
@endsection