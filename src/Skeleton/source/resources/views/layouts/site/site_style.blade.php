<style>

	:root {
		--link-color: #337ab7;
		--link-hover-color: #23527c;
		
		--footer-text-color: #fff;
		--footer-link-color: #ddd;
		--footer-link-hover-color: #fff;
	}
	
	/* Можно изменить цвет ссылок */
	/*
	a, .btn-link {
		color: var(--link-color);
	}
	a:focus, a:hover, .btn-link:focus, .btn-link:hover {
		color: var(--link-hover-color);
	}
	*/
	

	/* Можно скрыть Footer, изменить шрифт и т.п. */
	/*
	body {
		font-family: "Century Gothic";
	}
	label {
		font-weight: normal;
	}
	footer {
		color: var(--footer-text-color);
		*display: none;
	}
	.content-wrap {
		*padding-bottom: 0;
	}
	footer a {
		color: var(--footer-link-color);
	}
	footer a:focus, footer a:hover {
		color: var(--link-color);
		color: var(--footer-link-hover-color);
	}
	*/
	
	/* Высота подвала, зависит от содержимого и размера экрана */
	/*
	.content-wrap {
		padding-bottom: 100px; // выше подвала - отступ
	}
	footer {
		height: 60px;
		margin-top: -60px;
	}
	@media (min-width: 768px) {
		.content-wrap {
			padding-bottom: 140px; // выше подвала - отступ
		}
		footer {
			height: 100px;
			margin-top: -140px;
		}
	}
	*/
		
</style>