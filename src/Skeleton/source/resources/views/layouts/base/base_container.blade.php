@extends('layouts.common.frame')

@section('head')
	<link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet" />
	<link href="{{ URL::asset('/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
	@include('layouts.base.base_style')
@endsection

@section('menu')
	<div class="app mainmenu"><nav class="navbar navbar-default navbar-static-top"><div class="container">
	
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
				<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{ route('base') }}">{{ config('app.name', 'FastDev') }}</a>
		</div>
	
		<div class="collapse navbar-collapse" id="app-navbar-collapse">
			
			<!-- Left Side Of Navbar -->
			<ul class="nav navbar-nav">
				@include('layouts.base.elements.base_navbar_left')
			</ul>
	
			<!-- Right Side Of Navbar -->
			<ul class="nav navbar-nav navbar-right">
				@include('layouts.base.elements.base_navbar_right')
			</ul>
		</div>
			
	</div></nav></div>
@endsection

@section('page-content')
	<div class="app content">
		<div class="container">
			@yield('content')
		</div>
	</div>
@endsection

@section('footer')
	<div class="container"><div class="row">
		<div class="col-sm-6"><i class="glyphicon glyphicon-envelope"></i> {{ config('mail.from.address', 'info@email.com') }}</div>
		<div class="col-sm-6 text-right">&copy; {{ config('app.name', 'FastDev') }}</div>
	</div></div>
@endsection

@section('script')
	<script src="{{ URL::asset('/js/moment-with-locales.js') }}"></script>
	<script src="{{ URL::asset('/js/bootstrap-datetimepicker.min.js') }}"></script>
	@include('layouts.base.base_code')
	@stack('stage_code')
@endsection


