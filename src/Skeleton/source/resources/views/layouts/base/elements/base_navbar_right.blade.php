
<!-- Authentication Links -->
@if (Auth::guest())
	<li><a href="{{ route('login') }}">@lang('auth.login_link')</a></li>
	<li><a href="{{ route('register') }}">@lang('auth.register_link')</a></li>
@else
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
			{{ Auth::user()->name }} <span class="caret"></span>
		</a>

		<ul class="dropdown-menu" role="menu">
			<li>
				<a href="{{ url('/') }}">@lang('auth.to_site')</a>
			</li>
			<li>
				<a href="{{ url('/user/item/'.Auth::user()->id) }}">@lang('auth.profile_link')</a>
			</li>
			<li>
				<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
					@lang('auth.logout_link')
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
				</form>
			</li>
		</ul>
	</li>
@endif