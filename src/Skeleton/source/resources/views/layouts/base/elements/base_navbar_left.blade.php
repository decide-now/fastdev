
@if (!Auth::guest())

	@if (Auth::user()->userRole->code == 'admin')
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
				Доступ <span class="caret"></span>
			</a>
			<ul class="dropdown-menu" role="menu">
				<li><a href="{{ url('/user') }}">{{ App\ORM\User::getAlias('table_name') }}</a></li>
				<li><a href="{{ url('/user-role') }}">{{ App\ORM\UserRole::getAlias('table_name') }}</a></li>
				<li><a href="{{ url('/user-status') }}">{{ App\ORM\UserStatus::getAlias('table_name') }}</a></li>
			</ul>
		</li>
	@elseif (Auth::user()->userRole->code == 'visitor')
		<!--  -->
	@endif

@endif