<style>

body {
	font-family: 'Exo 2', sans-serif;
}

.applogo {
	margin-top: 30vh;
	margin-left: auto;
	margin-right: auto;
	text-align: center;
	color: #cfcfcf;
	*white-space: nowrap;			
}
	
/* Sidebar menu */
:root {
  --sidebar-active-bg: rgba(255, 255, 255, .5);
  --sidebar-devider-color: 1px solid rgba(200, 200, 200, .6);
}
@media (min-width: 768px) {
	
	.fixed-left > nav.navbar {
		position: fixed;
		left: 0;
		top: 0;
		bottom: 0;
		width: 250px;
		margin-bottom: 0;
		z-index: 5;
		overflow: auto;
		-webkit-border-radius: 0;
		-moz-border-radius: 0;
		border-radius: 0;
		padding-bottom: 30px;
		border: none;
	}
	
	.fixed-left > nav.navbar .container {
		width: auto;
		padding: 0;
	}
	
	.fixed-left > nav.navbar .navbar-collapse,
	.fixed-left > nav.navbar .navbar-header,
	.fixed-left > nav.navbar .navbar-nav,
	.fixed-left > nav.navbar .navbar-nav>li 
	{
		float: none;
		padding: 0;
		margin: 0;
	}
	
	.fixed-left > nav.navbar .navbar-nav li a {
		padding-top: 10px;
		padding-bottom: 10px;
	}
	
	.fixed-left > nav.navbar .navbar-brand,
	.fixed-left > nav.navbar .navbar-brand
	{
		margin: 0!important;
		font-size: 1.7em;
	}
	
	
	.fixed-left > nav.navbar .navbar-nav.navbar-right {
		float: none!important;		
		padding: 0;
		margin: 0;
		border-top: var(--sidebar-devider-color);
	}
	
	.fixed-left > nav.navbar .navbar-nav .open .dropdown-menu {
		position: static;
		float: none;
		width: auto;
		margin: 0;
		padding: 0;
		background-color: transparent;
		border: 0;
		-webkit-box-shadow: none;
		box-shadow: none;
	}
	
	.fixed-left > nav.navbar .dropdown-menu>li>a {
		padding: 10px 30px;
		color: #777;
	}
	
	
	/**/
	.fixed-left > nav.navbar .navbar-nav>.open>a {
    	color: inherit;
    	background-color: inherit;
	}
	.fixed-left > nav.navbar .navbar-nav a.active,
	.fixed-left > nav.navbar .navbar-nav > .open a.active
	{
		background-color: var(--sidebar-active-bg);
	}
	/**/
	
	.fixed-left > nav.navbar .caret {
		border: none;
		float: right;
		position: absolute;
		right: 15px;
		top: 10px;
		display: inline-block;
		font-family: 'Glyphicons Halflings';
		font-style: normal;
		font-weight: 400;
		line-height: 20px;
		-webkit-font-smoothing: antialiased;
		width: auto;
		height: auto;
		
	}
	.fixed-left > nav.navbar a > .caret:before {
		content: '\e257';
	}
	.fixed-left > nav.navbar .open > a > .caret:before {
		content: '\e259';
	}
	
	
	.fixed-left ~ .app.content {
		margin-left: 270px;
		padding: 15px;
	}
	.fixed-left ~ .app.content .container {
		padding-right: 15px;
		padding-left: 15px;
		margin-right: auto;
		margin-left: auto;
		width: auto;
	}
	
	.fixed-left-footer {
		margin-left: 250px;
	}
	.fixed-left-footer .container {
		padding-right: 15px;
		padding-left: 15px;
		margin-right: auto;
		margin-left: auto;
		width: auto;
	}
}


/* Modal Left and Right */
.modal.dock-lft .modal-dialog,
.modal.dock-rgt .modal-dialog {
	position: fixed;
	margin: auto;
	width: 320px;
	height: 100%;
	-webkit-transform: translate3d(0%, 0, 0);
	-ms-transform: translate3d(0%, 0, 0);
	-o-transform: translate3d(0%, 0, 0);
	transform: translate3d(0%, 0, 0);
}

.modal.dock-lft .modal-content,
.modal.dock-rgt .modal-content {
	height: 100%;
	overflow-y: auto;
}

.modal.dock-lft .modal-body,
.modal.dock-rgt .modal-body {
	padding: 15px 15px 80px;
	}

.modal.dock-lft.fade .modal-dialog{
	left: -320px;
	-webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
	-moz-transition: opacity 0.3s linear, left 0.3s ease-out;
	-o-transition: opacity 0.3s linear, left 0.3s ease-out;
	transition: opacity 0.3s linear, left 0.3s ease-out;
}

.modal.dock-lft.fade.in .modal-dialog{
	left: 0;
}

.modal.dock-rgt.fade .modal-dialog {
	right: -320px;
	-webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
	-moz-transition: opacity 0.3s linear, right 0.3s ease-out;
	-o-transition: opacity 0.3s linear, right 0.3s ease-out;
	transition: opacity 0.3s linear, right 0.3s ease-out;
}

.modal.dock-rgt.fade.in .modal-dialog {
	right: 0;
}

.modal.dock-lft .modal-content, 
.modal.dock-rgt .modal-content {
	border-radius: 0;
	border: none;
}

.modal.dock-lft .modal-header,
.modal.dock-rgt .modal-header {
	border-bottom-color: #EEEEEE;
	background-color: #FAFAFA;
}

/* Modal FS (full-screen) */
.modal-fs {
	position: fixed;
	left: 1%;
	margin: auto;
	width: 98%;
	height: 100%;
	-webkit-transform: translate3d(0%, 0, 0);
	-ms-transform: translate3d(0%, 0, 0);
	-o-transform: translate3d(0%, 0, 0);
	transform: translate3d(0%, 0, 0);
	
	-webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
	-moz-transition: opacity 0.3s linear, left 0.3s ease-out;
	-o-transition: opacity 0.3s linear, left 0.3s ease-out;
	transition: opacity 0.3s linear, left 0.3s ease-out;
}

.modal-fs .modal-content {
	height: 100%;
	overflow-y: auto;
	border-radius: 0;
	border: none;
}

.modal-fs .modal-body {
	padding: 15px 15px 80px;
}


/*
	Стили для CRUD
*/

.taskbar h3 {
	margin: 0 0 15px 0;
	line-height: 30px;
}
.taskbar.bottom-border {
	border-bottom: 1px solid #e7e7e7;
	margin-bottom: 15px;
}

.list-hdr {
	border-top: 1px solid #e7e7e7;
	border-bottom: 1px solid #e7e7e7;
	font-weight: normal;
	padding-top: 3px;
	padding-bottom: 3px;
}
.list-hdr [class^="col-sm-"] {
	*border-right: 1px solid #e7e7e7;
	float: left;
}
.list-hdr [class^="col-sm-"]:last-child {
	border-right: none;
}
@media (min-width: 768px) {
	.list-hdr {
		font-weight: bold;
	}
	.list-hdr [class^="col-sm-"] {
		border-right: none;		
	}
}

.list-hdr.no-top-border {
	border-top: none;
}
.list-hdr.no-bottom-border {
	border-bottom: none;
}

.list-item {
	border: none;
	padding-top: 3px;
	padding-bottom: 3px;
}
.list-item.bottom-border {
	border-bottom: 1px solid #e7e7e7;
}
.list-item.top-border {
	border-top: 1px solid #e7e7e7;
}
.list-item.left-border {
	border-left: 1px solid #e7e7e7;
}
.list-item.right-border {
	border-right: 1px solid #e7e7e7;
}
.list-item.selected {
	background-color: #ededed;
}

.label-top-space {
	margin-top: 30px;
}

.form-group.no-margin {
	margin-bottom: 0;
}


/* Multilevel Checkbox Group */

[data-tree-level="1"] {
	padding-left: 20px;
}
[data-tree-level="2"] {
	padding-left: 40px;
}
[data-tree-level="3"] {
	padding-left: 60px;
}
[data-tree-level="4"] {
	padding-left: 80px;
}

.checkbox-group-toggle[data-toggle="collapse"] {
	cursor: pointer;
	text-decoration: none;
	font-size: 12px;
}
.checkbox-group-toggle[data-toggle="collapse"]:after {
	font-family: 'Glyphicons Halflings';
	content: "\e079";
	padding-left: 1em;
}
.checkbox-group-toggle[data-toggle="collapse"][aria-expanded="true"]:after {
	content: "\e114";
}
		
</style>