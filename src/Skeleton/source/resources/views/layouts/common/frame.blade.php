<!DOCTYPE html>
<html lang="ru">
	<head>
		<base href="{{ env('APP_URL') }}" />
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>{{ config('app.name', 'FastDev') }}</title>
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script>window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token(), ]) !!};</script>
		
		<link href="{{ URL::asset('/favicon.ico') }}" rel="icon" type="image/x-icon"  />
		
		<link href="{{ URL::asset('/css/bootstrap.min.css') }}" rel="stylesheet" />
		<link href="{{ URL::asset('/css/font-awesome.min.css') }}" rel="stylesheet" />
		
		@include('layouts.common.frame_style')
		
		<!-- Head section -->
		@yield('head')
		<!-- /Head section -->
		
	</head>
	
	<body>
	
		<a href="#" id="back-to-top" title="Наверх">&uarr;</a>
		
		<div class="page-wrap">
			<div class="content-wrap">
			
				<!-- Menu section -->
				@yield('menu')
				<!-- /Menu section -->
				
				<!-- Page-content section -->
				@yield('page-content')
				<!-- /Page-content section -->
				
			</div>
			
			<footer>
				
				<!-- Footer section -->
				@yield('footer')
				<!-- /Footer section -->
				
			</footer>
		</div>
		
		<script src="{{ URL::asset('/js/jquery.min.js') }}"></script>
		<script src="{{ URL::asset('/js/bootstrap.min.js') }}"></script>
		
		@include('layouts.common.frame_code')
		
		<!-- Script section -->
		@yield('script')
		<!-- /Script section -->
		
	</body>
</html>