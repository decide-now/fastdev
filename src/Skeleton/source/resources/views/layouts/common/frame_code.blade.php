<script>

	$(document).ready(function() {
		if (!$('#back-to-top').length) {
			return;
		}
		
		var scrollTrigger = 100;
		var backToTopVisibility = function () {
			var scrollTop = $(window).scrollTop();
			$('#back-to-top').toggleClass('show', (scrollTop > scrollTrigger));
		};

		backToTopVisibility();
		$(window).on('scroll', function () {
			backToTopVisibility();
		});
		
		$('#back-to-top').on('click', function (e) {
			e.preventDefault();
			$('html,body').animate({ scrollTop: 0 }, 'slow');
		});
	});

</script>
