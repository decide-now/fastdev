<style>
	html, body, .page-wrap {
		height: 100%;
		margin: 0;
		padding: 0;
	}
	.content-wrap {
		box-sizing: border-box;
		min-height: 100%;
	}
	
	#alerts_success, #alerts_danger {
		min-height: 0;
	}
	
	[data-scene-id] {
		position: relative;
	}
	
	.scene-loading {
		background-color: rgba(255, 255, 255, .8);
		position: absolute;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		z-index: 5;
		text-align: center;
	}
	
	
	/* Back-to-top button */
	#back-to-top {
		position: fixed;
		top: 20px;
		left: 20px;
		z-index: 9999;
		width: 32px;
		height: 32px;
		text-align: center;
		line-height: 30px;
		background: rgba(245, 245, 245, 0.7);
		color: #444;
		cursor: pointer;
		border: 1px solid #d5d5d5;;
		border-radius: 2px;
		text-decoration: none;
		transition: opacity 0.2s ease-out;
		opacity: 0;
	}
	#back-to-top:hover {
		background: #e9ebec;
	}
	#back-to-top.show {
		opacity: 1;
	}
	/* /Back-to-top button */
	
	
	/* Footer style */
	.content-wrap {
		padding-bottom: 100px;
	}
	footer {
		padding: 20px 0;
		background-color: #666666;
		color: white;
		height: 70px;
		margin-top: -70px;
	}
	/* /Footer style */
	
	
	/* Markup */
	.container.full-width {
		width: 100%;
		padding-right: 0;
		padding-left: 0;
	}
	
	.container.full-width > .row {
		margin-right: 0;
		margin-left: 0;
	}
	.container.full-width > .row > .col-sm-12 {
		padding-right: 0;
		padding-left: 0;
	}
	
	.space-top-1 { padding-top: 20px; }
	.space-top-2 { padding-top: 40px; }
	.space-bottom-1 { padding-bottom: 20px; }
	.space-bottom-2 { padding-bottom: 40px; }
	.push-top-1 { margin-top: 20px; }
	.push-top-2 { margin-top: 40px; }
	.push-bottom-1 { margin-bottom: 20px; }
	.push-bottom-2 { margin-bottom: 40px; }
	
	.img-half-width {
		max-width: 100%;
	}
	@media (min-width: 768px) {
		.img-half-width {
			max-width: 50%;
		}
	}
	
	.img-center {
		margin-left: auto;
		margin-right: auto;
	}
	
</style>