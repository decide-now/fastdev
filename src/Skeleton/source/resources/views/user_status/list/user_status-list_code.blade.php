<script>

new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;
		
		thisScene.getElement('[data-role="sortable-header"]').click( { thisScene: this }, thisScene.hdrSortable_Click );
		thisScene.getElement('[id="btn_create_item"]').click( { thisScene: this }, thisScene.btnCreateItem_Click );
		thisScene.getElement('[id="btn_show_filter"]').click( { thisScene: this }, thisScene.btnShowFilter_Click );
		thisScene.getElement('[id="btn_refresh"]').click( { thisScene: this }, thisScene.btnRefresh_Click );

		thisScene.getElement('[id^="btn_edit_item"]').click( { thisScene: this }, thisScene.btnEditItem_Click );
		thisScene.getElement('[id^="btn_delete_item"]').click( { thisScene: this }, thisScene.btnDeleteItem_Click );
		thisScene.getElement('[id^="link_edit_item"]').click( { thisScene: this }, thisScene.linkEditItem_Click );

		thisScene.getElement('.pagination a').click( { thisScene: this }, thisScene.pagination_Click );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},

	itemRefresh : function(data) {
		thisScene = this;
		if ('ajax' in data && data.ajax == true) {
			var itemScene = stage.getScene('{{ $scene_children->get("item_scene")->sceneId() }}');
			itemScene.sceneRefresh(data);
			itemScene.getRootElement().closest('.modal').modal('show');
		} else {
			thisScene.sceneRefresh(data);
			return false;
		}
	},

	afterSuccessAJAX : function(response) {
		//
	},

	itemReturn : function(data) {
		thisScene = this;
		
		var itemScene = stage.getScene('{{ $scene_children->get("item_scene")->sceneId() }}');
		itemScene.getRootElement().closest('.modal').modal('hide');
		
		if (data.refresh === '1') {
			thisScene.sceneRefresh({ fields : { item_id: data.item_id, }, ajax: true, });
		}
	},

	
	/**/
	
	getListItemData : function(el) {
		var list_item = $(el.closest('[data-role="list-item"]'));
		var item_id_val = list_item.data('item-id');
		var item_text_val = list_item.data('item-text');
		return { id: item_id_val, text: item_text_val, }
	},
	
	btnCreateItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { fields: {task: 'create', }, };
		data = thisScene.addElementActionTechnology($(this), data);

		thisScene.itemRefresh(data);
		return false;
	},

	btnEditItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var item_data = thisScene.getListItemData(this);

		var data = { fields: { task: 'edit', item_id: item_data.id, }, };
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.itemRefresh(data);
		return false;
	},

	linkEditItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var item_data = thisScene.getListItemData(this);

		var data = { fields: { task: 'edit', item_id: item_data.id, }, };
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.itemRefresh(data);
		return false;
	},

	btnDeleteItem_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		if (confirm("Удалить запись?")) {
			var item_data = thisScene.getListItemData(this);
			
			var data = { fields: { task: 'delete', item_id: item_data.id, }, ajax: false, };
			data = thisScene.addElementActionTechnology($(this), data);
			
			thisScene.sceneRefresh(data);
		}
		return false;
	},

	btnRefresh_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data = { ajax: true, };
		data = thisScene.addElementActionTechnology($(this), data);
		
		thisScene.sceneRefresh(data);
		return false;
	},

	
	/* filter */
	
	btnShowFilter_Click : function(e) {
		var filterScene = stage.getScene('{{ $scene_children->get("filter_scene")->sceneId() }}');

		var data = { ajax: true, };
		
		filterScene.sceneRefresh(data);
		filterScene.getRootElement().closest('.modal').modal('show');

		return false;
	},

	filterReturn : function(data) {
		var thisScene = this;

		var filterScene = stage.getScene('{{ $scene_children->get("filter_scene")->sceneId() }}');
		filterScene.getRootElement().closest('.modal').modal('hide');
		filterScene.getRootElement().html('');

		if (!$.isEmptyObject(data)) {
			data.fields = ('fields' in data) ? data.fields : {};
			data.fields.task = 'filter';
			data.ajax = true;
			thisScene.sceneRefresh(data);
		}
	},

	
	/* ordering */
	
	hdrSortable_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var data_field = $(this).attr('data-field');
		var data_direction = $(this).attr('data-direction');
		data_direction = ( (data_direction == null || data_direction == 'desc') ? 'asc' : 'desc' );
		
		var data = { fields: { task: 'ordering', } };
		data = thisScene.addElementActionTechnology($(this), data);
		data.fields['ordering[' + data_field + ']'] = data_direction;
		
		thisScene.sceneRefresh(data);
		return false;
	},

	
	/* pagination */
	
	pagination_Click : function(e) {
		var thisScene = e.data.thisScene;
		var data = { url: e.target.href, }
		data = thisScene.addElementActionTechnology($(this), data);
		if (data.ajax) {
			e.preventDefault();
			thisScene.sceneRefresh(data)
			return false;
		}
	},
	
}).init();

</script>