@extends('layouts.site.site_container')
@section('content')
<div class="container push-top-1">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<h1>Ошибка 404.</h1>
			<h3>Запрашиваемая страница не найдена!</h3>
			<p class="space-top-2">Вы можете перейти <a href="{{ url('/') }}">на главную</a> страницу или вернуться <a href="{{ URL::previous() }}">назад</a>.</p>
		</div>
	</div>
</div>
@endsection

