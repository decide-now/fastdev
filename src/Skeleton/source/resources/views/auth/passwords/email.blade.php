@extends('layouts.base.base_container')
@section('content')

@include('fastdev::scene_content', [ 'scene' => (object)['scene_id' => 'app-alerts', 'no_content' => false, 'template_content' => 'alert.message.alert-message_content'], ])
	
<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title">@lang('auth.resetpass_header')</h3></div>
			<div class="panel-body">
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif
				
				<form role="form" method="POST" action="{{ route('password.email') }}">
					{{ csrf_field() }}
					
					{!! FastDevCtrl::textField('username')
						->value(old('username'))
						->state(($errors->has('username')) ? 'error' : '')
						->helper(($errors->has('username')) ? $errors->first('username') : '')
						->placeholder(__('auth.username'))
						->iconBefore('glyphicon glyphicon-envelope')
						->attr('required', 'true')
						->out()
					!!}
					
					{!! FastDevCtrl::button('sendlink_button')
						->type('submit')
						->label(__('auth.sendlink_button'))
						->iconBefore('glyphicon glyphicon-ok')
						->out()
					!!}
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
