@extends('layouts.base.base_container')
@section('content')

@include('fastdev::scene_content', [ 'scene' => (object)['scene_id' => 'app-alerts', 'no_content' => false, 'template_content' => 'alert.message.alert-message_content'], ])
	
<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title">@lang('auth.resetpass_header')</h3></div>
			<div class="panel-body">
				@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
				@endif

				<form method="POST" action="{{ route('password.request') }}">
					{{ csrf_field() }}

					<input type="hidden" name="token" value="{{ $token }}">

					{!! FastDevCtrl::textField('username')
						->value(old('username'))
						->state(($errors->has('username')) ? 'error' : '')
						->helper(($errors->has('username')) ? $errors->first('username') : '')
						->placeholder(__('auth.username'))
						->iconBefore('glyphicon glyphicon-user')
						->attr('required', 'true')
						->attr('autofocus', 'true')
						->out()
					!!}

					{!! FastDevCtrl::textField('password')
						->type('password')
						->state(($errors->has('password')) ? 'error' : '')
						->helper(($errors->has('password')) ? $errors->first('password') : '')
						->placeholder(__('auth.password'))
						->iconBefore('glyphicon glyphicon-lock')
						->attr('required', 'true')
						->out()
					!!}
					
					{!! FastDevCtrl::textField('password_confirmation')
						->type('password')
						->placeholder(__('auth.password_confirmation'))
						->iconBefore('glyphicon glyphicon-lock')
						->attr('required', 'true')
						->out()
					!!}

				  	{!! FastDevCtrl::button('resetpass_button')
						->type('submit')
						->label(__('auth.resetpass_button'))
						->iconBefore('glyphicon glyphicon-ok')
						->out()
					!!}
					
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
