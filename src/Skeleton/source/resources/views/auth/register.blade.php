@extends('layouts.base.base_container')
@section('content')

@include('fastdev::scene_content', [ 'scene' => (object)['scene_id' => 'app-alerts', 'no_content' => false, 'template_content' => 'alert.message.alert-message_content'], ])

<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title">@lang('auth.register_header')</h3></div>
			<div class="panel-body">
				<form role="form" method="POST" action="{{ url('/register') }}">
					{{ csrf_field() }}
					
					{!! FastDevCtrl::textField('username')
						->value(old('username'))
						->state(($errors->has('username')) ? 'error' : '')
						->helper(($errors->has('username')) ? $errors->first('username') : '')
						->placeholder(__('auth.username'))
						->iconBefore('glyphicon glyphicon-user')
						->attr('required', 'true')
						->attr('autofocus', 'true')
						->out()
					!!}
					
					{!! FastDevCtrl::textField('name')
						->value(old('name'))
						->state(($errors->has('name')) ? 'error' : '')
						->helper(($errors->has('name')) ? $errors->first('name') : '')
						->placeholder(__('auth.name'))
						->iconBefore('glyphicon glyphicon-edit')
						->attr('required', 'true')
						->out()
					!!}
					
					{!! FastDevCtrl::textField('password')
						->type('password')
						->state(($errors->has('password')) ? 'error' : '')
						->helper(($errors->has('password')) ? $errors->first('password') : '')
						->placeholder(__('auth.password'))
						->iconBefore('glyphicon glyphicon-lock')
						->attr('required', 'true')
						->out()
					!!}
					
					{!! FastDevCtrl::textField('password_confirmation')
						->type('password')
						->placeholder(__('auth.password_confirmation'))
						->iconBefore('glyphicon glyphicon-lock')
						->attr('required', 'true')
						->out()
					!!}
					
					{!! FastDevCtrl::button('register_button')
						->type('submit')
						->label(__('auth.register_button'))
						->iconBefore('glyphicon glyphicon-ok')
						->out()
					!!}
					
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
