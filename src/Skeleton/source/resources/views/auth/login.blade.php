@extends('layouts.base.base_container')
@section('content')

@include('fastdev::scene_content', [ 'scene' => (object)['scene_id' => 'app-alerts', 'no_content' => false, 'template_content' => 'alert.message.alert-message_content'], ])

<div class="container">

<div class="row">
	<div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title">{{ __('auth.login_header') }}</h3></div>
			<div class="panel-body">
				<form role="form" method="POST" action="{{ url('/login') }}">
					{{ csrf_field() }}
					
					{!! FastDevCtrl::textField('username')
						->value(old('username'))
						->state(($errors->has('username')) ? 'error' : '')
						->helper(($errors->has('username')) ? $errors->first('username') : '')
						->placeholder(__('auth.username'))
						->iconBefore('glyphicon glyphicon-user')
						->attr('required', 'true')
						->attr('autofocus', 'true')
						->out()
					!!}
					
					{!! FastDevCtrl::textField('password')
						->type('password')
						->state(($errors->has('password')) ? 'error' : '')
						->helper(($errors->has('password')) ? $errors->first('password') : '')
						->placeholder(__('auth.password'))
						->iconBefore('glyphicon glyphicon-lock')
						->attr('required', 'true')
						->out()
					!!}
					
					<div class="row">
						<div class="col-xs-6">
							{!! FastDevCtrl::button('login_button')
								->type('submit')
								->label(__('auth.login_button'))
								->extClass('btn-block')
								->out()
							!!}
						</div>
						<div class="col-xs-6 text-right">
							{!! FastDevCtrl::checkbox('remember')
								->value(old('remember'))
								->label(__('auth.remember'))
								->isNotGroup()
								->out()
							!!}
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6 text-right text-center-xs">
		<a href="{{ route('register') }}">{{ __('auth.register_link') }}</a>
	</div>
	<div class="col-sm-6 text-center-xs">
		<a href="{{ route('password.request') }}">{{ __('auth.forgotpass_link') }}</a>
	</div>
</div>

</div>
@endsection
