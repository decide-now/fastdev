<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12" id="alerts_success">
			@if (!empty($success_messages))
				@foreach ($success_messages as $message)
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{ $message }}
					</div>
				@endforeach
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12" id="alerts_danger">
			@if (!empty($errors))
				@foreach ($errors->all() as $message)
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{ $message }}
					</div>
				@endforeach
			@endif
			@if (!empty($error_messages))
				@foreach ($error_messages as $message)
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						{{ $message }}
					</div>
				@endforeach
			@endif
		</div>
	</div>
</div>