<script>

new Scene('{{ $scene->sceneId() }}', {
	
	clearSuccess : function() {
		var thisScene = this;
		
		var lst = thisScene.getElement('[id="alerts_success"] .alert');
		lst.each(function() {
			$(this).alert('close');
		});
	},

	clearError : function() {
		var thisScene = this;
		
		var lst = thisScene.getElement('[id="alerts_danger"] .alert');
		lst.each(function() {
			$(this).alert('close');
		});
	},

	clearAlerts : function() {
		var thisScene = this;
		
		thisScene.clearSuccess();
		thisScene.clearError();
	},
	
	showSuccess : function(msg, disable_close) {
		var thisScene = this;

		var el = '<div class="alert alert-success">';
		el += (typeof disable_close === 'undefined' || disable_close === false) ? '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' : '';
		el += msg;
		el += '</div>';

		thisScene.getElement('[id="alerts_success"]').append(el);
	},

	showError : function(msg, disable_close) {
		var thisScene = this;
		
		var el = '<div class="alert alert-danger">';
		el += (typeof disable_close === 'undefined' || disable_close === false) ? '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' : '';
		el += msg;
		el += '</div>';

		thisScene.getElement('[id="alerts_danger"]').append(el);
	},
	
}).init();
	
</script>