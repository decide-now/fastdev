<script>

new Scene('{{ $scene->sceneId() }}', {

	/* init */
	
	init : function() {
		var thisScene = this;
		
		thisScene.getElement('[id="btn_cancel_filter"]').click( { thisScene: this }, thisScene.btnCancelFilter_Click );
		thisScene.getElement('[id="btn_ok_filter"]').click( { thisScene: this }, thisScene.btnOkFilter_Click );
		thisScene.getElement('[id="btn_clear_filter"]').click( { thisScene: this }, thisScene.btnClearFilter_Click );

		thisScene.getElement('[id="btn_select_filter_user_role_id"]').click( { thisScene: this }, thisScene.btnSelectFilterUserRoleId_Click );
	},
	
	
	/* refresh */
	
	sceneRefresh : function(data) {
		thisScene = this;

		thisScene.defaultSceneRefresh(data, '{{ url($scene->updateURL()) }}', 'refresh');
	},
	
	
	/**/
	
	btnCancelFilter_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.filterReturn({});
	},

	btnOkFilter_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var fields_val = thisScene.serializeData({});
		
		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.filterReturn({ fields: fields_val });
	},

	btnClearFilter_Click : function(e) {
		var thisScene = e.data.thisScene;
		
		var fields_val = thisScene.serializeData({}, true);

		var parentScene = stage.getScene('{{ $scene_parent->sceneId() }}');
		parentScene.filterReturn({ fields: fields_val });
	},

	
	/* select fields */

	btnSelectFilterUserRoleId_Click : function(e) {
		var thisScene = e.data.thisScene;
		thisScene.defaultSelect_Click(e, '{{ $scene_children->get("user_role_select_scene")->sceneId() }}');
	},
	
}).init();

</script>
