{!! FastDevCtrl::modal('select-form')->title('Выбор: '.\App\ORM\UserRole::getAlias('record_name'))->isStatic()->size('fs')->open() !!}
	@include('fastdev::scene_content', $scene_children->get('user_role_select_scene')->sceneArray())
{!! FastDevCtrl::modal('select-form')->close() !!}

<div class="row taskbar">
	<div class="col-sm-12 text-right">
		{!! FastDevCtrl::buttonGroup('taskbar')->size('sm')->isFormGroup()->open() !!}
			{!! FastDevCtrl::button('btn-ok-filter')->title('Применить')->iconBefore('glyphicon glyphicon-ok')->out() !!}
			{!! FastDevCtrl::button('btn-cancel-filter')->title('Отмена')->iconBefore('glyphicon glyphicon-ban-circle')->out() !!}
			{!! FastDevCtrl::button('btn-clear-filter')->title('Очистить')->iconBefore('glyphicon glyphicon-erase')->out() !!}
		{!! FastDevCtrl::buttonGroup('taskbar')->isFormGroup()->close() !!}
	</div>
</div>
<div class="row"><div class="col-sm-12">
	<form data-role="data_form">
		{{ csrf_field() }}
		{!! FastDevCtrl::textField('filter[name]')->label($model::getAlias('name'))->value($filter['name'])->hasClearButton()->out() !!}
		{!! FastDevCtrl::textField('filter[username]')->label($model::getAlias('username'))->value($filter['username'])->hasClearButton()->out() !!}
		{!! FastDevCtrl::textField('filter[user_role_id]')
			->label($model::getAlias('user_role_id'))
			->value($filter['user_role_id'])
			->visibleValue(\App\ORM\UserRole::idToString($filter['user_role_id']))->placeholder('- Не выбрана -')
			->hasClearButton()
			->hasSelectButton()
			->out()
		!!}
	</form>
</div></div>
