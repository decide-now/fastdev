<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

// Site base

Route::get('/', 'Site\SiteHomeController@get');
Route::post('/', 'Site\SiteHomeController@post');
Route::get('/info', 'Site\SiteInfoController@get');
Route::post('/info', 'Site\SiteInfoController@post');


// Application base

Route::get('/base', ['as' => 'base', 'uses' => 'AppHomeController@index']);


// Authentication

Auth::routes();

Route::get('/user', 'User\UserListController@get');
Route::post('/user', 'User\UserListController@post');
Route::get('/user/item/{id?}', 'User\UserItemController@get');
Route::post('/user/item/{id?}', 'User\UserItemController@post');
Route::get('/user/select', 'User\UserSelectController@get');
Route::post('/user/select', 'User\UserSelectController@post');
Route::get('/user/filter', 'User\UserFilterController@get');
Route::post('/user/filter', 'User\UserFilterController@post');

Route::get('/user-role', 'UserRole\UserRoleListController@get');
Route::post('/user-role', 'UserRole\UserRoleListController@post');
Route::get('/user-role/item/{id?}', 'UserRole\UserRoleItemController@get');
Route::post('/user-role/item/{id?}', 'UserRole\UserRoleItemController@post');
Route::get('/user-role/select', 'UserRole\UserRoleSelectController@get');
Route::post('/user-role/select', 'UserRole\UserRoleSelectController@post');
Route::get('/user-role/filter', 'UserRole\UserRoleFilterController@get');
Route::post('/user-role/filter', 'UserRole\UserRoleFilterController@post');

Route::get('/user-status', 'UserStatus\UserStatusListController@get');
Route::post('/user-status', 'UserStatus\UserStatusListController@post');
Route::get('/user-status/item/{id?}', 'UserStatus\UserStatusItemController@get');
Route::post('/user-status/item/{id?}', 'UserStatus\UserStatusItemController@post');
Route::get('/user-status/select', 'UserStatus\UserStatusSelectController@get');
Route::post('/user-status/select', 'UserStatus\UserStatusSelectController@post');
Route::get('/user-status/filter', 'UserStatus\UserStatusFilterController@get');
Route::post('/user-status/filter', 'UserStatus\UserStatusFilterController@post');
