<?php

namespace App\ORM;

use DecideNow\FastDev\ORM\ORMModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class User extends ORMModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
	// < Авторизация и восстановление пароля >
	use Authenticatable, Authorizable, CanResetPassword, Notifiable;
	
	public function getEmailForPasswordReset()
	{
		if (filter_var($this->username, FILTER_VALIDATE_EMAIL)) {
			return $this->username;
		} else {
			return config('mail.from.address');
		}
	}
	public function routeNotificationForMail()
	{
		if (filter_var($this->username, FILTER_VALIDATE_EMAIL)) {
			return $this->username;
		} else {
			return config('mail.from.address');
		}
	}
	// </ Авторизация и восстановление пароля >
	
	
	protected $table = 'user';
	protected $fillable = [
		'name', 'username', 'password', 'user_status_id', 'user_role_id', 
	];
	protected $hidden = [
		'password', 'remember_token',
	];
	
	public static function getAliases()
	{
		return [
			'table_name' => 'Пользователи',
			'record_name' => 'Пользователь',
			'name' => 'Имя',
			'username' => 'Логин/E-mail',
			'password' => 'Пароль',
			'user_status_id' =>'Статус',
			'user_role_id' => 'Роль',
		] + parent::getAliases();
	}
	
	
	public static function superUser()
	{
		return self::find(1);
	}
	
	public function getUserStatusIconAttribute()
	{
		return $this->userStatusIconForValue($this->userStatus);
	}
	public static function userStatusIconForValue($val)
	{
		if ($val) {
			if ($val->id == '1') return '<i class="fa fa-sun-o"></i>';
			if ($val->id == '2') return '<i class="fa fa-moon-o"></i>';
			return $val->toString();
		}
		return '';
	}
	
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ 'username' => '', 'user_status_id' => '', 'user_role_id' => '', ] + parent::getFilterFields();
	}
	
	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'username', '');
		if ($tmp != '') {
			$query = $query->where('username', 'LIKE', '%'.$tmp.'%');
		}
		$tmp = Arr::get($filter_data, 'user_status_id', '');
		if ($tmp != '') {
			$query = $query->where('user_status_id', '=', ''.$tmp.'');
		}
		$tmp = Arr::get($filter_data, 'user_role_id', '');
		if ($tmp != '') {
			$query = $query->where('user_role_id', '=', ''.$tmp.'');
		}

		$query = parent::applyFilter($query, $filter_data);
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return [ 'username' => '', 'user_status_id' => '', 'user_role_id' => '', ] + parent::getOrderingFields();
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'username', '');
		if ($tmp != '') {
			$query = $query->orderBy('username', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'user_status_id', '');
		if ($tmp != '') {
			$query = $query->orderBy('user_status_name', $tmp);
		}
		$tmp = Arr::get($ordering_data, 'user_role_id', '');
		if ($tmp != '') {
			$query = $query->orderBy('user_role_name', $tmp);
		}

		$query = parent::applyOrdering($query, $ordering_data);
		
		return $query;
	}
	
	
	/* relations */
	
	public function userRole()
	{
		return $this->belongsTo('App\ORM\UserRole');
	}
	
	public function userStatus()
	{
		return $this->belongsTo('App\ORM\UserStatus');
	}
	
	
	/* list queries */
	
	public static function addQueryFilterAndOrdering($query = null)
	{
		$query = parent::addQueryFilterAndOrdering($query);
		$query = $query->select('user.*', 'user_role.name as user_role_name', 'user_status.name as user_status_name')
			->leftJoin('user_role', 'user.user_role_id', '=', 'user_role.id')
			->leftJoin('user_status', 'user.user_status_id', '=', 'user_status.id');
		return $query;
	}

	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if ($param == 'id') {
				if (Auth::user()->userRole->code == 'admin') {
					return true;
				}
				return false;
			}
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'allow_custom_login') {
			if (Auth::user()->userRole->code == 'admin') {
				return (Auth::user()->id == $param);
			}
			return false;
		}
		if ($permission == 'allow_other_profile') {
			if (Auth::user()->userRole->code != 'admin') {
				return (Auth::user()->id == $param);
			}
			return true;
		}
	}
}
