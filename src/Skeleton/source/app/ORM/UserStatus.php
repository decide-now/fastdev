<?php

namespace App\ORM;

use DecideNow\FastDev\ORM\ORMModel;
use Illuminate\Support\Facades\Auth;

class UserStatus extends ORMModel
{
	protected $table = 'user_status';
	protected $fillable = [
		'name', 
	];
	
	public static function getAliases()
	{
		return [
			'table_name' => 'Статусы пользователей',
			'record_name' => 'Статус пользователей',
		] + parent::getAliases();
	}
	
	
	/* relations */
	
	public function users()
	{
		return $this->hasMany('App\ORM\User');
	}
	
	
	/* list queries */
	
	public static function addQueryFilterAndOrdering($query = null)
	{
		$query = self::defaultListQuery($query);
		if (Auth::user()->userRole->code !== 'admin') {
			return $query->whereRaw('false');
		}
		return $query;
	}
	
	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if ($param == 'id') {
				if (Auth::user()->userRole->code == 'admin') {
					return true;
				}
				return false;
			}
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
}
