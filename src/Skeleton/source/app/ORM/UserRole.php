<?php

namespace App\ORM;

use DecideNow\FastDev\ORM\ORMModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class UserRole extends ORMModel
{
	protected $table = 'user_role';
	protected $fillable = [
		'name', 'code', 
	];
	
	public static function getAliases()
	{
		return [
			'table_name' => 'Роли пользователей',
			'record_name' => 'Роль пользователей',
			'code' => 'Код',
		] + parent::getAliases();
	}
	
	
	/* filter & ordering */
	
	public static function getFilterFields($filter_key = '')
	{
		return [ 'code' => '', ] + parent::getFilterFields();
	}
	
	public static function applyFilter($query, $filter_data)
	{
		$tmp = Arr::get($filter_data, 'code', '');
		if ($tmp != '') {
			$query = $query->where('code', 'LIKE', '%'.$tmp.'%');
		}

		$query = parent::applyFilter($query, $filter_data);
		return $query;
	}
	
	public static function getOrderingFields($ordering_key = '')
	{
		return [ 'code' => '', ] + parent::getOrderingFields();
	}
	
	public static function applyOrdering($query, $ordering_data)
	{
		$tmp = Arr::get($ordering_data, 'code', '');
		if ($tmp != '') {
			$query = $query->orderBy('code', $tmp);
		}

		$query = parent::applyOrdering($query, $ordering_data);
		
		return $query;
	}
	
	
	/* relations */
	
	public function users()
	{
		return $this->hasMany('App\ORM\User');
	}
	
	
	/* list queries */
	
	public static function addQueryFilterAndOrdering($query = null)
	{
		$query = self::defaultListQuery($query);
		if (Auth::user()->userRole->code !== 'admin') {
			return $query->where('code', '<>', 'admin');
		}
		return $query;
		
	}
	
	
	/* permissions */
	
	public function getPermission($permission, $param = '')
	{
		if ($permission == 'create') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		
		if ($permission == 'edit') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'delete') {
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
		if ($permission == 'list') {
			if ($param == 'id') {
				if (Auth::user()->userRole->code == 'admin') {
					return true;
				}
				return false;
			}
			if (Auth::user()->userRole->code == 'admin') {
				return true;
			}
			return false;
		}
	}
}
