
...

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

...

class Handler extends ExceptionHandler
{

	...
	
	public function render($request, Exception $exception)
	{
		...
		if ($exception instanceof NotFoundHttpException) {
			if (!$request->ajax() && !config('app.debug')) {
				return response()->view('errors_custom.404', [], 404);
			}
		} 
		if ($exception instanceof \ErrorException) {
			if (!$request->ajax() && !config('app.debug')) {
				return response()->view('errors_custom.500', [], 500);
			}
		}
		
		...
		
	}
	
	...
	
}

