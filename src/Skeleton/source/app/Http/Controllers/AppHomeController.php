<?php

namespace App\Http\Controllers;

use DecideNow\FastDev\Scene\SceneBaseController;

class AppHomeController extends SceneBaseController
{	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$success_messages = $this->getSceneMessages('success');
		$error_messages = $this->getSceneMessages('error');
		return view('app_home')->with('success_messages', $success_messages)->with('error_messages', $error_messages);
	}
}
