<?php

namespace App\Http\Controllers\Auth;

use DecideNow\FastDev\Scene\SceneBaseController;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends SceneBaseController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function sendResetLinkEmail(Request  $request)
    {
    	$this->validate($request, ['username' => 'required|email']);
    	
    	// We will send the password reset link to this user. Once we have attempted
    	// to send the link, we will examine the response then see the message we
    	// need to show to the user. Finally, we'll send out a proper response.
    	$response = $this->broker()->sendResetLink(
    		$request->only('username')
    	);
    	
    	return $response == Password::RESET_LINK_SENT
    		? $this->sendResetLinkResponse($response)
    		: $this->sendResetLinkFailedResponse($request, $response);
    }
    
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
    	return back()->withErrors(
    		['username' => trans($response)]
    	);
    }
}
