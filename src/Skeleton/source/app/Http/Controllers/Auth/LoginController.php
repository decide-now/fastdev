<?php

namespace App\Http\Controllers\Auth;

use DecideNow\FastDev\Scene\SceneBaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class LoginController extends SceneBaseController
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected function redirectTo() {
		return route('base');
	}
	
	protected function authenticated(Request $request, $user)
	{
		/*
		if ($user->userRole->code == 'admin') {
			//Session::put('some_data', 'some_data_value');
			//return redirect()->route('some_route');
		} else {
			//
		}
		return redirect()->route('base');
		*/
	}

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest', ['except' => 'logout']);
	}
	
	
	public function username() { return 'username'; }
	
	protected $active_user_status_id = 1;
	protected function credentials(Request $request)
	{
		$credentials = $request->only($this->username(), 'password');
		$credentials = Arr::add($credentials, 'user_status_id', $this->active_user_status_id);
		return $credentials;
	}
}
