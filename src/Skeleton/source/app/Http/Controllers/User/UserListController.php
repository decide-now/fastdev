<?php
namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

class UserListController extends UserController
{	
	static $scene_id = 'user-list';
	
	public $list;
	public $filter;
	public $ordering;
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
		
		$filter_scene = new UserFilterController($this);
		$this->scene_children->put('filter_scene', $filter_scene);
		
		$item_scene = new UserItemController($this);
		$this->scene_children->put('item_scene', $item_scene);
	}
	
	public function prepareContent(Request $request)
	{	
		$model = $this->model;
		
		$list_data = new $this->model;
		$list_data = $model::addQueryFilterAndOrdering($list_data);

		$list_data = $model::applySessionFilter($list_data, $this->filter);
		$list_data = $model::applySessionOrdering($list_data, $this->ordering);
		
		$this->list = $this->paginateExt($list_data);
		
		$this->no_content = false;
	}
	
	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->page = $this->transferData('page', $request->get('page', '1'));
		$this->item_id = $this->transferData('item_id', '');

		$this->checkPermissionsList();
		
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{	
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferData('page', '1', $request);
		$this->transferData('item_id', '', $request);

		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			return redirect()->action(self::methodPath('get'));
		} elseif ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
			return redirect()->action(self::methodPath('get'));
		} elseif ($task == 'ordering') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->orderingPutToSession($request, '', $force_clear);
			return redirect()->action(self::methodPath('get'));
		} elseif ($task == 'create') {
			$this->itemCreate($request);
			return redirect()->action(UserItemController::methodPath('get'));
		} elseif ($task == 'edit') {
			$item = $this->itemFind($request);
			return redirect()->action(UserItemController::methodPath('get'), ['id' => ($item) ? $item->id : '']);
		} elseif ($task == 'delete') {
			$this->itemDelete($request);
			return redirect()->action(self::methodPath('get'));
		}
	}
}