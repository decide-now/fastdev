<?php
namespace App\Http\Controllers\User;

use App\Http\Controllers\UserRole\UserRoleSelectController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserItemController extends UserController
{	
	static $scene_id = 'user-item';

	public $task;
	public $refresh_parent_scene;
	
	public $item;
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
		
		$user_role_select_scene = new UserRoleSelectController($this);
		$this->scene_children->put('user_role_select_scene', $user_role_select_scene);
	}
	
	public function prepareContent(Request $request)
	{
		$this->item = Session::get('item');
		if (!$this->item) {
			$this->item = $this->itemCreate($request);
		}
		if (!$this->item->getPermission('allow_other_profile', $this->item->id)) {
			Session::flash('flash_error_messages', ['Доступ к чужому профилю запрещен!']);
			$this->content_error = true;
			return;
		}
		
		$this->password_value = ($this->item->id == '') ? '' : '******';
		
		$this->no_content = false;
	}
	
	public function get(Request $request, $item_id = '')
	{
		$this->transferSceneId();
		$this->task = $this->transferData('task', '');

		$this->transferSceneId();
		$this->transferPrimitives();
		
		$this->item = $this->checkPermissionsItem($request, $item_id);
		$this->response_data['item_id'] = ($this->item) ? $this->item->id : '';
		
		if ($this->task == 'ok' || $this->task == 'save') {
			$this->setPrimitives('refresh_parent_scene', '1');
		}
		
		$this->content_error = Session::has('flash_error_messages');
		
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request, $item_id = '')
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferData('refresh_parent_scene', '0', $request);

		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			$item = $this->itemFind($request);
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
		} elseif ($task == 'create') {
			$item = $this->itemCreate($request);
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
		} elseif ($task == 'edit') {
			$item = $this->itemFind($request);
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
		} elseif ($task == 'cancel') {
			$item = $this->itemFind($request);
			if (Session::get('ajax', false) == true) {
				return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
			} else {
				Session::flash('item_id', $item->id);
				return redirect()->action(UserListController::methodPath('get'));
			}
		} elseif ($task == 'ok') {
			$validated = $this->itemValidate($request);
			$item = Session::get('item');
			if (!$validated) {
				return redirect()
				->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])
				->with('ajax', $request->ajax())
				->withErrors($this->validator);
			}
			$this->itemSave($request, $item);
			if (Session::get('ajax', false) == true) {
				return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
			} else {
				Session::flash('item_id', $item->id);
				return redirect()->action(UserListController::methodPath('get'));
			}
		} elseif ($task == 'save') {
			$validated = $this->itemValidate($request);
			$item = Session::get('item');
			if (!$validated) {
				return redirect()
				->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])
				->with('ajax', $request->ajax())
				->withErrors($this->validator);
			}
			$this->itemSave($request, $item);
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
		}
	}
}