<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

class SiteHomeController extends SiteBaseController {

	static $scene_id = 'site-home';
	public $nest_layout = 'site.site_nest_blank';
	
	public function __construct($scene_parent = null)
	{
		parent::__construct($scene_parent);
		$this->active_page = 'home';
	}
	
	public function prepareContent(Request $request)
	{
		$this->no_content = false;
	}
	
	public function get(Request $request) {
		$this->transferSceneId();
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			$request->flash();
			return redirect()->action(self::methodPath('get'))->with('ajax', $request->ajax());
		}
	}
}
