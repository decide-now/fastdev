<?php

namespace App\Http\Controllers\Site;

use DecideNow\FastDev\Scene\SceneBaseController;

class SiteBaseController extends SceneBaseController {
	
	public $nest_layout = 'site.site_nest';
	
	public $active_page;
	
	public function __construct($scene_parent = null)
	{
		parent::__construct($scene_parent);
		$this->active_page = 'home';
	}
}
