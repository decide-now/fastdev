<?php
namespace App\Http\Controllers\UserStatus;

use App\ORM\UserStatus;
use DecideNow\FastDev\Scene\SceneBaseController;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserStatusController extends SceneBaseController
{	
	protected function itemDefaultFields($request)
	{
		$ret = [
			// 'is_published' => true,
		];

		return $ret;
	}
	
	protected function validationRules($itemClass = '', $data = [])
	{
		return [
			$itemClass.'.name' => 'required|max:255',

		];
	}
	
	protected $default_actions_tech = '';
	protected $actions_tech = [
		/*
		'list_btn_create_item' => 'ajax',
		'list_btn_refresh' => 'ajax',
		'list_link_edit_item' => 'ajax',
		'list_edit_create_item' => 'ajax',
		'list_btn_delete_item' => 'ajax',
		'list_pagination' => 'ajax',
		'list_sortable_header' => 'ajax',
		
		'item_btn_ok_item' => 'ajax',
		'item_btn_save_item' => 'ajax',
		'item_btn_cancel_item' => 'ajax',
		'item_btn_refresh' => 'ajax',
		*/
	];
	
	public function getActionTech($action)
	{
		if (array_key_exists($action, $this->actions_tech)) {
			return $this->actions_tech[$action];
		}
		return $this->default_actions_tech;
	}
	
	protected $model;
	protected $validator;
	

	public $page;
	public $item_id;
	public function paginateExt($data, $perPage = null)
	{
		$perPage = $perPage ?: $data->getModel()->getPerPage();
		$list_page = $this->page;
		if ($this->item_id) {
			$dt = clone($data);
			$row_num = $dt->select($this->model->getTable().'.id')->pluck('id')->search($this->item_id) + 1;
			$list_page = ceil($row_num / $perPage);
		}
		return $data
		->paginate($perPage, ['*'], 'page', $list_page)
		->withPath(action($this->methodPath('get')), [], false);
	}
	
	public function __construct($scene_parent = null)
	{
		parent::__construct($scene_parent);
		$this->middleware('auth');
		$this->model = new UserStatus;
	}
	
	protected function itemCreate($request)
	{
		$item = new $this->model;
		
		$default_fields = $this->itemDefaultFields($request);
		foreach($default_fields as $key => $value) {
			$item->$key = $value;
		}
		
		Session::flash('item', $item);
		return $item;
	}
	
	protected function itemFind($request, $item_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		
		Session::flash('item', $item);
		return $item;
	}
	
	protected function itemDelete($request, $item_id = '')
	{
		if (!$item_id) {
			$item_id = $request->get('item_id');
		}
		$item = $this->model->find($item_id);
		
		DB::beginTransaction();
		try {
			$item->delete();
			DB::commit();
			Session::flash('flash_success_messages', ['Запись удалена!']);
		} catch (QueryException $e) {
			DB::rollBack();
			$msg = $this->getDatabaseErrorMessage($e);
			Session::flash('flash_error_messages', [$msg]);
		}
	}
	
	protected function itemFromInput($request)
	{
		$itemClass = $this->model->getModelName();
		
		$item_id = $request->get('item_id');
		$item = ($item_id) ? $item = $this->model->find($item_id) : new $this->model;
		
		$item_input = $request->get($itemClass);
		
		$item->fill($item_input);
		
		return $item;
	}
	
	protected function itemValidate($request)
	{
		$item = $this->itemFromInput($request);
		Session::flash('item', $item);
		
		$this->validator = null;
		$itemClass = $this->model->getModelName();
		//$item_id = $request->get('item_id');
		
		$this->validator = Validator::make($request->all(), $this->validationRules($itemClass), [], $this->model->getAliasesWithModelName());
		if ($this->validator->fails()) {
			if ($request->ajax()) {
				Session::flash('flash_error_messages', $this->validator->errors()->messages());
			}
			return false;
		} else {
			return true;
		}
	}
	
	protected function itemSave($request, $item = null)
	{
		if (!$item) {
			$item = $this->itemFromInput($request);
		}
		
		DB::beginTransaction();
		$item->save();
		DB::commit();
		
		Session::flash('item', $item);
		return $item;
	}

	
	protected function redirectToItem($item)
	{
		return redirect()->action(UserStatusItemController::methodPath('get'), ['id' => ($item) ? $item->id : '']);
	}
	
	protected function redirectToList($item)
	{
		return redirect()
			->action(UserStatusListController::methodPath('get'))
			->with('item_id', ($item) ? $item->id : '');
	}
	
	
	protected function checkPermissionsList() {
		if (!$this->model->getPermission('list')) {
			$this->setContentError('Доступ запрещен!');
		}
	}
	
	protected function checkPermissionsItem($request, $item_id) {
		$item = null;
		if (Session::has('item')) {
			$item = Session::get('item');
		} else {
			
			if ($item_id) {
				$item = $this->itemFind($request, $item_id);
				if (!$item) {
					$this->setContentError('Не найдена запись с кодом '.$item_id.'!');
				} else {
					if (!$item->getPermission('edit')) {
						$this->setContentError('Доступ запрещен!');
					}
				}
			} else {
				if (!$this->model->getPermission('create')) {
					$this->setContentError('Доступ запрещен!');
				}
			}
		}
		return $item;
	}
}