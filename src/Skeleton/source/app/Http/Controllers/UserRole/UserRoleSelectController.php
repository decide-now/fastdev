<?php
namespace App\Http\Controllers\UserRole;

use Illuminate\Http\Request;

class UserRoleSelectController extends UserRoleController
{	
	static $scene_id = 'user_role-select';
	
	public $list;
	public $filter;
	public $ordering;

	public $call_button_id;
	
	protected function defineChildren()
	{	
		$this->defineAlertChildScene();
	
		$filter_scene = new UserRoleFilterController($this);
		$this->scene_children->put('filter_scene', $filter_scene);
	}
	
	public function prepareContent(Request $request)
	{	
		$model = $this->model;
		
		$this->filter = $model::filterGetFromSession();
		$this->ordering = $model::orderingGetFromSession();
		
		$list_data = new $this->model;
		$list_data = $model::addQueryFilterAndOrdering($list_data);

		$list_data = $model::applySessionFilter($list_data, $this->filter);
		$list_data = $model::applySessionOrdering($list_data, $this->ordering);
		
		$this->list = $this->paginateExt($list_data);
		
		$this->no_content = false;
	}

	public function get(Request $request)
	{
		$this->transferSceneId();
		$this->transferAJAXFlag($request);
		$this->page = $this->transferData('page', '1');
		$this->call_button_id = $this->transferData('call_button_id', '');

		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request)
	{
		$this->transferSceneId($request);
		$this->transferData('page', '1', $request);
		$this->transferData('call_button_id', '', $request);

		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			return redirect()->action(self::methodPath('get'));
		} elseif ($task == 'filter') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->filterPutToSession($request, '', $force_clear);
			return redirect()->action(self::methodPath('get'));
		} elseif ($task == 'ordering') {
			$force_clear = $this->handleRequestBoolean('force_clear', false, $request);
			$this->model->orderingPutToSession($request, '', $force_clear);
			return redirect()->action(self::methodPath('get'));
		}
	}
}