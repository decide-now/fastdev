<?php
namespace App\Http\Controllers\UserRole;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserRoleItemController extends UserRoleController
{	
	static $scene_id = 'user_role-item';

	public $task;
	public $refresh_parent_scene;
	
	public $item;
	
	protected function defineChildren()
	{
		$this->defineAlertChildScene();
	}
	
	public function prepareContent(Request $request)
	{
		$this->item = Session::get('item');
		if (!$this->item) {
			$this->item = $this->itemCreate($request);
		}
		
		$this->no_content = false;
	}
	
	public function get(Request $request, $item_id = '')
	{
		$this->transferSceneId();
		$this->task = $this->transferData('task', '');

		$this->transferSceneId();
		$this->transferPrimitives();
		
		$this->item = $this->checkPermissionsItem($request, $item_id);
		$this->response_data['item_id'] = ($this->item) ? $this->item->id : '';
		
		if ($this->task == 'ok' || $this->task == 'save') {
			$this->setPrimitives('refresh_parent_scene', '1');
		}
		
		$this->content_error = Session::has('flash_error_messages');
		
		$this->prepareContent($request);
		return $this->sceneOutput();
	}
	
	public function post(Request $request, $item_id = '')
	{
		$this->transferSceneId($request);
		$this->transferAJAXFlag($request);
		$this->transferData('refresh_parent_scene', '0', $request);

		$task = $this->transferData('task', 'refresh', $request);
		
		if ($task == 'refresh') {
			$item = $this->itemFind($request);
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
		} elseif ($task == 'create') {
			$item = $this->itemCreate($request);
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
		} elseif ($task == 'edit') {
			$item = $this->itemFind($request);
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
		} elseif ($task == 'cancel') {
			$item = $this->itemFind($request);
			if (Session::get('ajax', false) == true) {
				return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
			} else {
				Session::flash('item_id', $item->id);
				return redirect()->action(UserRoleListController::methodPath('get'));
			}
		} elseif ($task == 'ok') {
			$validated = $this->itemValidate($request);
			$item = Session::get('item');
			if (!$validated) {
				return redirect()
				->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])
				->with('ajax', $request->ajax())
				->withErrors($this->validator);
			}
			$this->itemSave($request, $item);
			if (Session::get('ajax', false) == true) {
				return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
			} else {
				Session::flash('item_id', $item->id);
				return redirect()->action(UserRoleListController::methodPath('get'));
			}
		} elseif ($task == 'save') {
			$validated = $this->itemValidate($request);
			$item = Session::get('item');
			if (!$validated) {
				return redirect()
				->action(self::methodPath('get'), ['id' => ($item) ? $item->id : ''])
				->with('ajax', $request->ajax())
				->withErrors($this->validator);
			}
			$this->itemSave($request, $item);
			return redirect()->action(self::methodPath('get'), ['id' => ($item) ? $item->id : '']);
		}
	}
}