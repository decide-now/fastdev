@if (method_exists($scene, 'sceneId'))
	<div data-scene-id="{{ $scene->sceneId() }}">
		@includeWhen( !($scene->no_content), $scene->template_content() )
	</div>
@elseif (property_exists($scene, 'scene_id'))
	<div data-scene-id="{{ $scene->scene_id }}">
		@includeWhen( !($scene->no_content), $scene->template_content )
	</div>
@elseif (is_array($scene))
	@if (array_key_exists('scene_id', $scene))
		<div data-scene-id="{{ $scene['scene_id'] }}">
			@includeWhen( !($scene['no_content']), $scene['template_content'] )
		</div>
	@endif
@endif