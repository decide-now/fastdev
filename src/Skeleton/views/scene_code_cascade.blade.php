
@foreach($scene->scene_children as $subscene)
	@include('fastdev::scene_code_cascade', ['scene' => $subscene])
@endforeach

@push('stage_code')
	@include($scene->getTemplateCode(), $scene->sceneVariables())
@endpush
