<script>

	// Stage object

	function Stage() {

		this.scenes = {};
		
		this.addScene = function (scene) {
			this.scenes[scene.id] = scene;
		};
		
		this.getScene = function (scene_id) {
			return this.scenes[scene_id];
		};

		this.isBusy = function () {
			for (key in this.scenes) {
				if (this.scenes[key].is_busy) {
					return true;
				}
			}
			return false;
		};

		this.extend = function (options) {
			for (key in options) {
				this[key] = options[key];
			}
		};
	}

	
	// Scene object

	function Scene(scene_id, extension) {
		
		this.escapeHtml = function escapeHtml(text) {
			if (typeof(text) !== 'string') {
				return text;
			}
			var map = { '&': '&amp;', '<': '&lt;', '>': '&gt;', '"': '&quot;', "'": '&#039;' };
			return text.replace(/[&<>"']/g, function(m) { return map[m]; });
		}
		
		this.getRootElement = function() {
			return $('[data-scene-id="'+this.id+'"]');
		}
		
		this.getElement = function(selector) {
			var scene_el = this.getRootElement();
			var children = scene_el.find('[data-scene-id]');
			var in_children = children.find(selector, children);
			var ret = scene_el.find(selector).not(in_children);
			return ret;
		}

		this.serializeData = function(post_data, empty) {
			var form_selector = ('form_selector' in post_data) ? post_data.form_selector : '[data-role="data_form"]';
			var data_form_el = this.getElement(form_selector);

			var append_data = data_form_el.serializeArray();
			append_data = append_data.concat(
				data_form_el.find('input[type=checkbox]:not(:checked)').map(function() { 
					return { name: this.name, value: null };
				}).get()
		    );
			$.each(append_data, function() {
				var field = this;
				if (!(field.name in post_data)) {
					post_data[field.name] = field.value;
				}
			}, post_data);

			empty = (typeof empty === 'undefined') ? false : empty;
			if (empty) {
				for (key in post_data) {
					post_data[key] = '';
				}
			}
			return post_data;
		}
		
		this.postRequest = function(post_data) {
			post_data.method = ('method' in post_data) ? post_data.method : 'POST';
			
			post_data = this.serializeData(post_data);

			var form_selector = ('form_selector' in post_data) ? post_data.form_selector : '[data-role="data_form"]';
			var data_form_el = this.getElement(form_selector);
			
			var addedInputs = [];
			for (var key in post_data) {
				if (key == 'url') {
					data_form_el.attr('action', post_data.url)
				}
				if (key == 'target') {
					data_form_el.attr('target', post_data.target)
				}
				if (key == 'method') {
					data_form_el.attr('method', post_data.method)
				}
				data_form_el.append( $('<input type="hidden" name="' + key + '" value="' + this.escapeHtml(post_data[key]) + '" />') );
			}
			
			this.getRootElement().append('<div class="scene-loading"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
			
			data_form_el.submit();
			for (var idx in addedInputs) {
				addedInputs[idx].remove();
			}
		}

		this.processAJAXResponse = function(context, response, further) {
			
			if (!response) {
				console.warn('Scene response is empty!');
				response = {};
			}
			if (response !== Object(response)) {
				console.warn('Scene response is not an Object!');
				response = {};
			}
			
			response.thisScene = context.thisScene;
			response.secretly = context.secretly;
			response.alerts_scene_id = context.alerts_scene_id;

			context.thisScene.is_busy = false;
			if (context.secretly === false) {
				context.thisScene.getElement('.scene-loading').remove();
			}
			
			further(response);
		}
		
		this.postRequestAJAX = function() {

			var post_data = null;
			var onSuccess = null;
			var onError = null;
			var filesData = null;
			var has_files = false;

			post_data = arguments[0];
			post_data._token = $('meta[name=csrf-token]').attr('content');
			post_data.method = ('method' in post_data) ? post_data.method : 'POST';
			post_data.secretly = ('secretly' in post_data) ? post_data.secretly : false;
			post_data = this.serializeData(post_data);
			
			if (arguments.length === 3) {
				onSuccess = arguments[1];
				onError = arguments[2];
			} else if (arguments.length === 4) {
				has_files = true;
				filesData = arguments[1];
				onSuccess = arguments[2];
				onError = arguments[3];

				// add files data to request
				var form_selector = ('form_selector' in post_data) ? post_data.form_selector : '[data-role="data_form"]';
				var data_form_el = this.getElement(form_selector);
			
				formData = new FormData(data_form_el.get(0));
				
				var files = [];
				var lng = filesData.length;
				for (i=0; i<lng; i++) {
					formData.append('files['+i+']', filesData[i]);
				}
				
				for (key in post_data) {
					formData.append(key, post_data[key]); 
				}
			}

			this.is_busy = true;
			if (post_data.secretly === false) {
				this.getRootElement().append('<div class="scene-loading"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
			}
			
			var ajax_data = {
				type: post_data.method,
				url: post_data.url,
				context: {
					thisScene: this,
					secretly: post_data.secretly,
					alerts_scene_id: post_data.alerts_scene_id,
				},
				success: function(response) {
					this.thisScene.processAJAXResponse(this, response, onSuccess);
				},
				error: function(response) {
					this.thisScene.processAJAXResponse(this, response, onError);
				}
			};

			if (has_files) {
				ajax_data.data = formData;
				ajax_data.cache = false;
				ajax_data.contentType = false;
				ajax_data.processData = false;
			} else {
				ajax_data.data = post_data;
			}
			
			$.ajax(ajax_data);
		}
		
		this.defaultSceneRefresh = function() {

			var data = null;
			var default_url = null;
			var default_task = null;
			var filesData = null;
			var has_files_data = false;
			
			if (arguments.length === 3) {
				data = arguments[0];
				default_url = arguments[1];
				default_task = arguments[2];
			} else if (arguments.length === 4) {
				has_files_data = true;
				data = arguments[0];
				default_url = arguments[1];
				default_task = arguments[2];
				filesData = arguments[3];
			}
			
			post_data = {};
			post_data.scene_id = ('scene_id' in data) ? data.scene_id : this.id;
			post_data.url = ('url' in data) ? data.url : default_url;
			post_data.alerts_scene_id = ('alerts_scene_id' in data) ? data.alerts_scene_id : this.id + '-alert-message';
			post_data.task = ('task' in data) ? data.task : default_task;
			post_data.secretly = ('secretly' in data) ? data.secretly : false;
			post_data.target = ('target' in data) ? data.target : '';
			post_data.fields = ('fields' in data) ? data.fields : {};
			post_data.form = ('form' in data) ? data.form : {};
			for (key in data.fields) {
				post_data[key] = data.fields[key];
			}
			for (key in data.form) {
				post_data[key] = data.form[key];
			}
			post_data.task = ('task' in post_data) ? post_data.task : default_task;

			var alertsScene = stage.getScene(post_data.alerts_scene_id);
			if (typeof alertsScene !== 'undefined') {
				alertsScene.clearAlerts();
			}

			var onSuccessAJAX = (this.hasOwnProperty('onSuccessAJAX') ? this.onSuccessAJAX : this.defaultOnSuccessAJAX);
			var onErrorAJAX = (this.hasOwnProperty('onErrorAJAX') ? this.onErrorAJAX : this.defaultOnErrorAJAX);
			
			if ('ajax' in data && data.ajax === true) {
				if (has_files_data) {
					this.postRequestAJAX(post_data, filesData, onSuccessAJAX, onErrorAJAX);		
				} else {
					this.postRequestAJAX(post_data, onSuccessAJAX, onErrorAJAX);
				}
			} else {
				this.postRequest(post_data);			
			}
			
			return false;
		}
		
		this.defaultOnSuccessAJAX = function(response, alerts_scene_id) {

			var thisScene = response.thisScene;
			var scene_el = thisScene.getRootElement();
			
			var alerts_scene_id = response.alerts_scene_id;
			var alertsScene = stage.getScene(alerts_scene_id);
			
			var afterSuccessAJAX = (thisScene.hasOwnProperty('afterSuccessAJAX') ? thisScene.afterSuccessAJAX : thisScene.defaultAfterSuccessAJAX);
			
			var resp = response;
			for (var field in resp) {
				if (field == 'message') {
					if (typeof alertsScene !== 'undefined') {
						for (var msg in resp[field]) {
							alertsScene.showSuccess(resp[field][msg]);
						}
					}
				}
				if (field == 'page') {
					scene_el.html(response.page);
					thisScene.htmlInit();
					thisScene.init();
				}
			}
			afterSuccessAJAX(response);
		}

		this.defaultOnErrorAJAX = function(response, alerts_scene_id) {

			var thisScene = response.thisScene;
			var scene_el = thisScene.getRootElement();
			
			var alerts_scene_id = response.alerts_scene_id;
			var alertsScene = stage.getScene(alerts_scene_id);

			var afterErrorAJAX = (thisScene.hasOwnProperty('afterErrorAJAX') ? thisScene.afterErrorAJAX : thisScene.defaultAfterErrorAJAX);

			if (typeof alertsScene !== 'undefined') {
				if (response.status == 422 || response.status == 423) {
					var resp = response.responseJSON;
					if ('message' in resp) {
						resp = resp.message;
					}
					for (var field in resp) {
						if ((typeof resp[field]) === 'string') {
							alertsScene.showError(resp[field]);
						} else {
							for (var msg in resp[field]) {						
								alertsScene.showError(resp[field][msg]);
							}
						}
					}
				}
			}
			afterErrorAJAX();
		}

		this.defaultAfterSuccessAJAX = function() {
			// add "afterSuccessAJAX" method to scene to define actions after successfull AJAX response
		}
		this.defaultAfterErrorAJAX = function() {
			// add "afterErrorAJAX" method to scene to define actions after wrong AJAX response
		}

		this.initHtmlButtonGroups = function() {
			this.getElement('.btn-group label[disabled]').click(function(event) {
				event.preventDefault();
				return false;
			});
		}

		this.initHtmlPopovers = function() {
			this.getElement('[data-role="custom-popover"]:not([data-popover-no-content])')
			.popover({
				container: 'body',
				html: true,
				content: function () {
					var clone = $($(this).data('popover-content')).clone(true).removeClass('hidden');
					return clone;
				}
			})
			.click(function(e) {
				e.preventDefault();
			});
			
			this.getElement('[data-role="custom-popover"][data-trigger="hover-stay"]')
			.popover({
				trigger: 'manual',
			})
			.on('mouseenter', function () {
			    var _this = this;
			    $(this).popover('show');
			    $('.popover').on('mouseleave', function () {
			        $(_this).popover('hide');
			    });
			})
			.on('mouseleave', function () {
			    var _this = this;
			    setTimeout(function () {
			        if (!$('.popover:hover').length) {
			            $(_this).popover('hide');
			        }
			    }, 300);
			});
		},
		
		this.defaultBtnClearField_Click = function(e) {
			var thisScene = e.data.thisScene;
			var call_button = $(e.currentTarget);

			call_button.trigger('scene:clearing');
			
			var field_id = $(e.target).attr('field-id')
			var hidden_field_id = $(e.target).attr('hidden-field-id')
			var field = thisScene.getElement('[id="'+field_id+'"]');
			var hidden_field = thisScene.getElement('[id="'+hidden_field_id+'"]');
			
			field.val('');
			hidden_field.val('');

			call_button.trigger('scene:cleared');

			return false;
		},

		this.btnClearField_Click = function(e) {
			var thisScene = e.data.thisScene;
			thisScene.defaultBtnClearField_Click(e);
		},

		this.defaultSelect_Click = function(e, select_scene_id, ext_data) {
			var thisScene = e.data.thisScene;
			var extFields = e.data.fields;

			if (typeof ext_data == 'undefined') {
				ext_data = {};
			}
			
			var selectScene = stage.getScene(select_scene_id);
			var call_button_id_val = $(e.target).closest('button').attr('id');
			var data = { fields: { call_button_id: call_button_id_val, }, ajax: true, };

			for (key in ext_data) {
				if (key == 'fields') {
					for (key_f in ext_data.fields) {
						data.fields[key_f] = ext_data.fields[key_f];
					}
				} else {
					data[key] = ext_data[key];
				}
			}
			
			if ($(e.target).attr('filter-changed') != null) {
				var field_id = $(e.target).attr('field-id')
				var field = thisScene.getElement('[id="'+field_id+'"]');
				data.fields['filter[name]'] = field.val();
				data.fields.task = 'filter';
			} else {
				data.fields['filter[name]'] = '';
			}

			for (var attrname in extFields) {
				data.fields[attrname] = extFields[attrname];
			}

			selectScene.sceneRefresh(data);
			selectScene.getRootElement().closest('.modal').modal('show');
		},

		this.modalShow = function (data, options) {
			data = data || {};
			options = options || {};
			

			var p1_data = {
				'class': 'modal fade in',
				'tabindex': '-1',
				'role': 'dialog',
				'data-backdrop': 'static',
			};
			if (options.hasOwnProperty('non_staic') && options.non_staic == true) {
				delete p1_data['data-backdrop'];
			}
			if (!options.hasOwnProperty('size') || !options.size) {
				options['size'] = 'fw';
			}
			if (!options.hasOwnProperty('height') || !options.height) {
				options['height'] = 'fh';
			}
			if (!options.hasOwnProperty('position') || !options.position) {
				options['position'] = '';
			}
			modalScene = this;
			
			modalScene.getRootElement().wrap($('<div>', p1_data
			)).wrap($('<div>', {
				'class': 'modal-dialog'
					+ ((options.size) ? ' modal-' + options.size : '')
					+ ((options.height) ? ' modal-' + options.height : '')
					+ ((options.position) ? ' modal-' + options.position : '')
			})).wrap($('<div>', {
				'class': 'modal-content',
			})).wrap($('<div>', {
				'class': 'modal-body',
			})).wrap($('<div>', {
				'class': 'container-fluid',
			}));

			var scene_modal = modalScene.getRootElement().closest('.modal');
			scene_modal.on('shown.bs.modal', function (event) {
				data.ajax = true;
				modalScene.sceneRefresh(data);
			});
			scene_modal.modal('show');
		},

		this.modalHide = function (data) {
			modalScene = this;
			modalScene.getRootElement().html('');
			var scene_modal = modalScene.getRootElement().closest('.modal');
			scene_modal.on('hidden.bs.modal', function (event) {
				modalScene.getRootElement()
					.unwrap('.container-fluid')
					.unwrap('.modal-body')
					.unwrap('.modal-content')
					.unwrap('.modal-dialog')
					.unwrap('.modal');
			});
			scene_modal.modal('hide')
		},

		this.defaultSelectReturn = function(data) {
			var thisScene = this;
			var call_button = thisScene.getElement('[id="'+data.call_button_id+'"]');

			call_button.trigger('scene:selecting');

			var select_scene = data.scene;
			var select_form = select_scene.getRootElement();
			select_form.closest('.modal').modal('hide');
			select_form.html('');

			call_button.removeAttr('filter-changed');

			/* TinyMCE image select */
			if (call_button.length == 0) {
				call_button =  $('input:text#'+data.call_button_id);
				if (call_button.length > 0) {
					call_button.val(data.item_url);
					call_button.closest('[role="dialog"]').css('z-index', 65536);
					$('#mce-modal-block').css('z-index', 65535);
					return;
				}
			}
			
			var field_id = call_button.attr('field-id')
			var field = thisScene.getElement('[id="'+field_id+'"]');
			field.val(data.item_text);
			
			var hidden_field_id = call_button.attr('hidden-field-id')
			var hidden_field = thisScene.getElement('[id="'+hidden_field_id+'"]');
			hidden_field.val(data.item_id);

			call_button.trigger('scene:selected');
		},

		this.selectReturn = function(data) {
			this.defaultSelectReturn(data);
		},

		this.defaultFieldHasSelect_Change = function(e) {
			var thisScene = e.data.thisScene;
			
			var select_button = thisScene.getElement('[id^="btn_select_"][field-id="'+$(e.target).attr('id')+'"]');
			select_button.attr('filter-changed', 'true');
			
			var hidden_field_id = select_button.attr('hidden-field-id');
			var hidden_field = thisScene.getElement('[id="'+hidden_field_id+'"]');
			hidden_field.val('');
		},

		this.addElementActionTechnology = function(el, data) {
			var tech = el.data('action-tech');
			data.ajax = (tech ===  'ajax');
			return data;
		},

		this.defaultScrollTo_Click = function(e) {
			var thisScene = e.data.thisScene;
			e.preventDefault();
			var tar = thisScene.getElement($(e.target).closest('[data-role="scroll-to"]').data('target'));
			$('html,body').animate({ scrollTop: tar.offset().top }, 'slow');
			return false;
		},
		
		this.htmlInit = function() {
			this.initHtmlButtonGroups();
			this.initHtmlPopovers();

			this.getElement('[id^="btn_clear_"]').click( { thisScene: this }, this.btnClearField_Click );
			this.getElement('[field-has-select]').change( { thisScene: this }, this.defaultFieldHasSelect_Change );
			this.getElement('[data-role="scroll-to"]').click( { thisScene: this }, this.defaultScrollTo_Click );
		}

		this.init = function() {
		}

		/* Scene "constructor" */
		
		this.id = scene_id;
		this.is_busy = false;
		if (stage != null) {
			stage.addScene(this);
		}
		if (typeof DecideNowObjects !== 'undefined') {
			DecideNowObjects.stage.addScene(this);
		}
		this.htmlInit();
		
		for (key in extension) {
			this[key] = extension[key];
		}

	}
	
	var stage = new Stage();
	stage.extend({
		calendar_t_format : {
			locale: 'ru', 
			format: 'DD.MM.YYYY HH:mm:ss', 
			showTodayButton: true,
			tooltips: {
				today: 'Сегодня',
			}
		},
		calendar_d_format : {
			locale: 'ru', 
			format: 'DD.MM.YYYY', 
			showTodayButton: true,
			tooltips: {
				today: 'Сегодня',
			}
		},
		calendar_s_format : {
			locale: 'ru', 
			format: 'DD MMMM YYYY', 
			showTodayButton: true,
			tooltips: {
				today: 'Сегодня',
			}
		},
	});

	if (typeof DecideNowObjects !== 'undefined') {
		for (scene_id in DecideNowObjects.stage.scenes) {
			stage.addScene(DecideNowObjects.stage.scenes[scene_id]);
		}
	}
	
	$(document).on('click', function (e) {
		var target_el = $(e.target);
		$('[data-role="custom-popover"]').each(function () {
			var popover_el = $(this);
			var popover_content = $('.popover-content ' + $(this).data('popover-content'));
			var popover_content_wrap = popover_content.parent();
			if ( !target_el.is(popover_el) && !target_el.is(popover_content_wrap) && !popover_content_wrap.children().has(target_el).length ) {
				popover_el.popover('hide');
			}
		});
	});
	
</script>
